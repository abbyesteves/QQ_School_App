//
//  ApiServices.swift
//  QQ_School_App
//
//  Created by Abby Esteves on 02/03/2018.
//  Copyright © 2018 Abby Esteves. All rights reserved.
//

import UIKit
import Foundation

class ApiService: UIViewController { //URLSessionDataDelegate, URLSessionDelegate, URLSessionTaskDelegate
    
    func get_image(url_str:String, thumbnailView : UIImageView)
    {
        let url = URL(string: "\(url_str)")
        let task = URLSession.shared.dataTask(with: url!) { data, response, error in
            guard let data = data, error == nil else { return }
            
            DispatchQueue.main.async() {    // execute on main thread
                thumbnailView.image = UIImage(data: data)
            }
            
            if error != nil {
                print("ERROR \(error ?? "" as! Error)")
            }
        }
        task.resume()
    }
    
    func generateQRCode(from string: String) -> UIImage? {
        let data = string.data(using: String.Encoding.ascii)
        
        if let filter = CIFilter(name: "CIQRCodeGenerator") {
            filter.setValue(data, forKey: "inputMessage")
            let transform = CGAffineTransform(scaleX: 3, y: 3)
            
            if let output = filter.outputImage?.transformed(by: transform) {
                return UIImage(ciImage: output)
            }
        }
        
        return nil
    }
    
//    let myID = [
//        "qq_id" : "QQuser001",
//        "school_id" : "schooluser001",
//        "name": "Jen",
//        "address": "San Ildefonso",
//        "contact": "0917 222 2222",
//        "grade": "12",
//        "section": "1st",
//        "img": "base64"
//    ]
    
//    let myID = [
//        "qq_id" : "QQuser004",
//        "school_id" : "schooluser004",
//        "name": "Mark Barrientos",
//        "address": "San Ildefonso",
//        "contact": "0917 222 2222",
//        "grade": "12",
//        "section": "1st",
//        "img": "attractions_head"
//    ]

    //my QR info
//    let myID = [
//        "qq_id" : "QQuser002",
//        "school_id" : "schooluser002",
//        "name": "Abby",
//        "address": "Las Piñas City, Metro Manila",
//        "contact": "0917 306 9221",
//        "grade": "12",
//        "section": "1st",
//        "img": "base64"
//    ]
    
//    let myID = [
//        "qq_id" : "QQuser003",
//        "school_id" : "schooluser003",
//        "name": "Vincent",
//        "address": "Bocaue",
//        "contact": "0917 333 3333",
//        "grade": "12",
//        "section": "1st",
//        "img": "base64"
//    ]
    
    var student = [
        "qq_id" : "",
        "address" : "",
        "birthday" : "",
        "contact" : "",
        "email" : "",
        "firstname" : "",
        "gender" : "",
        "student_id" : "",
        "lastname" : "",
        "middlename" : "",
        "emg_person" : "",
        "emg_contact" : "",
        "user_type" : "student",
        "guardian" : [],
        "subjects" : [],
        "channels" : [],
        "grade" : "",
        "section" : "",
        "img" : "",
        "b_64" : ""
    ] as [String: Any]
    
    var teacher = [
        "qq_id" : "",
        "address" : "",
        "birthday" : "",
        "contact" : "",
        "email" : "",
        "firstname" : "",
        "gender" : "",
        "school_id" : "",
        "lastname" : "",
        "middlename" : "",
        "user_type" : "",
        "teacher_type" : "",
        "teacher_coordinator" : "",
        "grade" : "",
        "section" : "",
        "subjects" : [],
        "channels" : [] ,
        "emg_person" : "",
        "emg_contact" : "",
        "img" : "",
        "b_64" : ""
    ] as [String: Any]

    
    //URLS
//    let url = URL(string: "http://192.168.1.3:4984/qqschool/")! // pldthomedsl
//    let modulesURL =  URL(string: "http://192.168.1.114/android_api/file_upload.php")
//    let updateStatus =  URL(string: "http://192.168.1.114/local-school-backend/set_is_logged_in.php")
//    let urlConfig = URL(string: "http://192.168.1.114/android_api/get_school_config.php")!
    
    //LOCAL
//    let urlConfig = URL(string: "http://192.168.1.109/local-school-backend/get_school_config.php")!
//    let updateStatus =  URL(string: "http://192.168.1.109/local-school-backend/set_is_logged_in.php")
//    let modulesURL =  URL(string: "http://192.168.1.109/local-school-backend/file_upload.php")
//    let friendsTeacherURL = URL(string: "http://192.168.1.109/local-school-backend/backend/get_list_teachers")
//    let friendsStudentURL = URL(string: "http://192.168.1.109/local-school-backend/backend/get_list_students")
    
    //LIVE
    let urlConfig = URL(string: "http://128.199.228.235/local-school-backend/get_school_config.php")!
    let updateStatus =  URL(string: "http://128.199.228.235/local-school-backend/set_is_logged_in.php")
    let modulesURL =  URL(string: "http://128.199.228.235/local-school-backend/file_upload.php")
    let friendsTeacherURL = URL(string: "http://128.199.228.235/local-school-backend/backend/get_list_teachers")
    let friendsStudentURL = URL(string: "http://128.199.228.235/local-school-backend/backend/get_list_students")
    
    
    func myStringURL() -> String {
//        return "http://192.168.1.114:4984/"
        return "http://192.168.1.96:4984/"
//        return "http://47.74.226.132/qqschool/" // live
//        return "http://47.74.226.132"
    }
    
    func myURL() -> URL {
//        return URL(string: "\(myStringURL())\((configuration()["bucket_name"])!)")!
//        return URL(string: "http://192.168.1.96:4984/qqschool/")! // my server
        return URL(string: "\((configuration()["bucket_name"])!)")! // Local
    }
    
    //creating mock data
    func createMockNews(){
        let manager = CBLManager.sharedInstance()
        let database = try! manager.databaseNamed("news")
        let channels = ["QQSCHOOL_NEWS"]
        let document: CBLDocument = database.document(withID: "NEWS_\(Int(Date().timeIntervalSince1970))")!
        
        let announce = [
            "channels": channels,
            "title" : "news title test 3",
            "desc" : "Wala tayong pasok. Time to spend the day with your family.",
            "author": "Student Teacher Faculty",
            "b64": "",
            "date" : "\(Int(Date().timeIntervalSince1970))"
        ] as [String : Any]
        
        do {
//            try document.putProperties(announce)
            try database.delete()
        } catch {
            print("Can't save document in database")
            return
        }
        
        let push = database.createPushReplication(myURL())
        let pull = database.createPullReplication(myURL())
        pull.channels = channels
        push.continuous = true
        pull.continuous = true
        push.start()
        pull.start()
        
    }
    
    func createMockAnnouncements(){
        let manager = CBLManager.sharedInstance()
        let database = try! manager.databaseNamed("announcements")
        let channels = ["QQSCHOOL_ANNOUNCEMENTS"]
        let document: CBLDocument = database.document(withID: "ANNOUNCE_\(Int(Date().timeIntervalSince1970))")!
        
        let announce = [
            "channels": channels,
            "author" : "qq school",
            "b64" : "",
            "date": "\(Int(Date().timeIntervalSince1970))",
            "date_from" : "March 26,2018",
            "date_to": "March 28,2018",
            "desc" : "dress to impress",
            "title": "junior / senior promenade",
            "img": "",
            "location": "gymnasium",
            "type": "event"
        ] as [String : Any]
        
        do {
            try document.putProperties(announce)
//            try database.delete()
        } catch {
            print("Can't save document in database")
            return
        }
        
    }
    
    func createMockAttendance(){
        let manager = CBLManager.sharedInstance()
        let database = try! manager.databaseNamed("attendance")
        let channels = ["ATTENDANCE_\((myID()["qq_id"])!)"]
        let document: CBLDocument = database.document(withID: "ATT_\((myID()["qq_id"])!)_02/08/2018")!
//        let properties = document.properties
        
//        if properties != nil {
            let attendance = [
                "channels": channels,
                "scan_id": "1518072134",
                "scan_date": "February 08,2018",
                "time_in": "09:51 AM"
            ] as [String: Any]
//        } else {
//            let attendance = [
//                "date": "1518072134",
//                "in": "",
//                "scan_subj": "",
//                "state": ""
//            ] as [String: Any]
//        }
        do {
//            try document.putProperties(attendance)
                        try database.delete()
        } catch {
            print("Can't save document in database")
            return
        }
        
        let push = database.createPushReplication(myURL())
        let pull = database.createPullReplication(myURL())
        pull.channels = channels
        push.continuous = true
        pull.continuous = true
        push.start()
        pull.start()
    }
    
    func createUserSimulator(){
        let manager = CBLManager.sharedInstance()
        let name = "mydata"
        let channels = ["QQSCHOOL_USER", "USER_TC000006"]
        let database = try! manager.databaseNamed("\(name)")
        let document = database.document(withID: "\(name)")!
        
        var teacher = [
            "qq_id" : "TC000006",
            "address" : "address sample #11 barangay magsaysay st.",
            "birthday" : "03/11/1979",
            "contact" : "0923 000 4444",
            "email" : "glenda@gmail.com",
            "firstname" : "Glenda",
            "gender" : "female",
            "school_id" : "SIDTC000006",
            "lastname" : "Yason",
            "middlename" : "Perez",
            "user_type" : "teacher",
            "teacher_type" : "floating",
            "teacher_coordinator" : "",
            "grade" : "12",
            "section" : "1st",
            "schedule" : [
                "monday": [
                    [
                        "time_start": "9:00 AM",
                        "time_end": "10:00 AM",
                        "subject_code": "MATH101",
                        "subject": "Math",
                        "duration": "60 Minutes"
                    ],
                    [
                        "time_start": "10:00 AM",
                        "time_end": "12:00 PM",
                        "subject_code": "SCI101",
                        "subject": "Science",
                        "duration": "120 Minutes"
                    ],
                    [
                        "time_start": "12:00 AM",
                        "time_end": "1:00 PM",
                        "subject_code": "",
                        "subject": "Lunch",
                        "duration": "60 Minutes"
                    ],
                    [
                        "time_start": "1:00 AM",
                        "time_end": "3:00 AM",
                        "subject_code" : "FIL101",
                        "subject": "Filipino",
                        "duration": "120 Minutes"
                    ],
                    [
                        "time_start": "3:00 AM",
                        "time_end": "5:00 AM",
                        "subject_code": "ENG101",
                        "subject": "English",
                        "duration": "120 Minutes"
                    ]
                ],
                "tuesday" : [
                    [
                        "time_start": "10:00 AM",
                        "time_end": "11:00 AM",
                        "subject_code": "ENG101",
                        "subject": "English",
                        "duration": "60 Minutes"
                    ],
                    [
                        "time_start": "11:00 AM",
                        "time_end": "12:00 PM",
                        "subject_code": "TLE101",
                        "subject": "TLE",
                        "duration": "60 Minutes"
                    ],
                    [
                        "time_start": "12:00 PM",
                        "time_end": "1:00 PM",
                        "subject_code": "",
                        "subject": "Lunch",
                        "duration": "60 Minutes"
                    ],
                    [
                        "time_start": "1:00 PM",
                        "time_end": "3:00 PM",
                        "subject_code": "MATH01",
                        "subject": "Math",
                        "duration": "120 Minutes"
                    ]
                ],
                "wednesday" :[
                    [
                        "time_start": "10:00 AM",
                        "time_end": "11:00 AM",
                        "subject_code": "ENG101",
                        "subject": "English",
                        "duration": "60 Minutes"
                    ],
                    [
                        "time_start": "11:00 AM",
                        "time_end": "12:00 PM",
                        "subject_code": "SCI01",
                        "subject": "Science",
                        "duration": "60 Minutes"
                    ],
                    [
                        "time_start": "1:00 PM",
                        "time_end": "2:00 PM",
                        "subject_code": "ALG101",
                        "subject": "Algebra 1",
                        "duration": "60 Minutes"
                    ],
                    [
                        "time_start": "2:00 PM",
                        "time_end": "3:00 PM",
                        "subject_code": "",
                        "subject": "Lunch",
                        "duration": "60 Minutes"
                    ],
                    [
                        "time_start": "3:00 PM",
                        "time_end": "4:00 PM",
                        "subject_code": "BIO101",
                        "subject": "Biology",
                        "duration": "60 Minutes"
                    ],
                    [
                        "time_start": "4:00 PM",
                        "time_end": "6:00 PM",
                        "subject_code": "ALG102",
                        "subject": "Algebra 2",
                        "duration": "120 Minutes"
                    ]
                ],
                "thursday" : [
                    [
                        "time_start": "9:00 AM",
                        "time_end": "10:00 AM",
                        "subject_code": "A&D101",
                        "subject": "Analysis & Design",
                        "duration": "60 Minutes"
                    ],
                    [
                        "time_start": "10:00 AM",
                        "time_end": "11:00 AM",
                        "subject_code": "CAL101",
                        "subject": "Basic Calculus",
                        "duration": "60 Minutes"
                    ],
                    [
                        "time_start": "11:00 AM",
                        "time_end": "12:00 PM",
                        "subject_code": "",
                        "subject": "Lunch",
                        "duration": "60 Minutes"
                    ],
                    [
                        "time_start": "1:00 PM",
                        "time_end": "3:00 PM",
                        "subject_code": "PE101",
                        "subject": "PE / Badminton",
                        "duration": "120 Minutes"
                    ]
                ],
                "friday" : [
                    [
                        "time_start": "8:00 AM",
                        "time_end": "9:30 AM",
                        "subject_code": "LIT101",
                        "subject": "Literature",
                        "duration": "90 Minutes"
                    ],
                    [
                        "time_start": "9:30 AM",
                        "time_end": "10:30 AM",
                        "subject_code": "TLE101",
                        "subject": "TLE",
                        "duration": "60 Minutes"
                    ],
                    [
                        "time_start": "10:30 AM",
                        "time_end": "3:00 PM",
                        "subject_code": "SHS101",
                        "subject": "Study Hall Session",
                        "duration": "270 Minutes"
                    ],
                    [
                        "time_start": "4:00 PM",
                        "time_end": "6:00 PM",
                        "subject_code": "3DD101",
                        "subject": "3d Design",
                        "duration": "120 Minutes"
                    ]
                ],
                "saturday" : [
                    [
                        "time_start": "10:00 AM",
                        "time_end": "12:00 PM",
                        "subject_code": "STAT101",
                        "subject": "Satistics",
                        "duration": "120 Minutes"
                    ],
                    [
                        "time_start": "12:00 PM",
                        "time_end": "2:00 PM",
                        "subject_code": "EL101",
                        "subject": "E-Learning",
                        "duration": "120 Minutes"
                    ],
                    [
                        "time_start": "2:00 PM",
                        "time_end": "4:00 PM",
                        "subject_code": "ALGO102",
                        "subject": "Algorithim",
                        "duration": "60 Minutes"
                    ]
                ]
            ],
            "channels" : channels ,
            "emg_person" : "David Yason",
            "emg_contact" : "0917 555 7777",
            "img" : "",
            "b_64" : ""
            ] as [String: Any]
        
        do {
            try document.putProperties(teacher)
            ApiService().createDocChat()
            LoginController().saveQR()
//            try database.delete()
        } catch {
            print("Can't save document in database")
        }
        
        
    }
    
    func createMockUserDoc(){
        let manager = CBLManager.sharedInstance()
        let database = try! manager.databaseNamed("users")
        print(" user document count : ", database.documentCount)
        let channels = ["QQSCHOOL_USER", "USER_TC000001"]
        let document: CBLDocument = database.document(withID: "TC000001_1522120998")!
//        let document: CBLDocument = database.document(withID: "TC000002_1522059554")!
//        let document: CBLDocument = database.document(withID: "TC000006_1522060454")!
//        let document: CBLDocument = database.document(withID: "TC000019_1521435900")!
        
        var teacher = [
            "qq_id" : "TC000001",
            "address" : "address sample #11 barangay magsaysay st.",
            "birthday" : "03/11/1979",
            "contact" : "0923 000 4444",
            "email" : "bryan@gmail.com",
            "firstname" : "Bryan",
            "gender" : "male",
            "school_id" : "SIDTC000001",
            "lastname" : "Geronimo",
            "middlename" : "Marquez",
            "user_type" : "teacher",
            "teacher_type" : "advisory",
            "teacher_coordinator" : "",
            "grade" : "12",
            "section" : "1st",
            "schedule" : [
                "monday": [
                    [
                        "time_start": "9:00 AM",
                        "time_end": "10:00 AM",
                        "subject_code": "MATH101",
                        "subject": "Math",
                        "duration": "60 Minutes"
                    ],
                    [
                        "time_start": "10:00 AM",
                        "time_end": "12:00 PM",
                        "subject_code": "SCI101",
                        "subject": "Science",
                        "duration": "120 Minutes"
                    ],
                    [
                        "time_start": "12:00 AM",
                        "time_end": "1:00 PM",
                        "subject_code": "",
                        "subject": "Lunch",
                        "duration": "60 Minutes"
                    ],
                    [
                        "time_start": "1:00 AM",
                        "time_end": "3:00 AM",
                        "subject_code" : "FIL101",
                        "subject": "Filipino",
                        "duration": "120 Minutes"
                    ],
                    [
                        "time_start": "3:00 AM",
                        "time_end": "5:00 AM",
                        "subject_code": "ENG101",
                        "subject": "English",
                        "duration": "120 Minutes"
                    ]
                ],
                "tuesday" : [
                    [
                        "time_start": "10:00 AM",
                        "time_end": "11:00 AM",
                        "subject_code": "ENG101",
                        "subject": "English",
                        "duration": "60 Minutes"
                    ],
                    [
                        "time_start": "11:00 AM",
                        "time_end": "12:00 PM",
                        "subject_code": "TLE101",
                        "subject": "TLE",
                        "duration": "60 Minutes"
                    ],
                    [
                        "time_start": "12:00 PM",
                        "time_end": "1:00 PM",
                        "subject_code": "",
                        "subject": "Lunch",
                        "duration": "60 Minutes"
                    ],
                    [
                        "time_start": "1:00 PM",
                        "time_end": "3:00 PM",
                        "subject_code": "MATH01",
                        "subject": "Math",
                        "duration": "120 Minutes"
                    ]
                ],
                "wednesday" :[
                    [
                        "time_start": "10:00 AM",
                        "time_end": "11:00 AM",
                        "subject_code": "ENG101",
                        "subject": "English",
                        "duration": "60 Minutes"
                    ],
                    [
                        "time_start": "11:00 AM",
                        "time_end": "12:00 PM",
                        "subject_code": "SCI01",
                        "subject": "Science",
                        "duration": "60 Minutes"
                    ],
                    [
                        "time_start": "1:00 PM",
                        "time_end": "2:00 PM",
                        "subject_code": "ALG101",
                        "subject": "Algebra 1",
                        "duration": "60 Minutes"
                    ],
                    [
                        "time_start": "2:00 PM",
                        "time_end": "3:00 PM",
                        "subject_code": "",
                        "subject": "Lunch",
                        "duration": "60 Minutes"
                    ],
                    [
                        "time_start": "3:00 PM",
                        "time_end": "4:00 PM",
                        "subject_code": "BIO101",
                        "subject": "Biology",
                        "duration": "60 Minutes"
                    ],
                    [
                        "time_start": "4:00 PM",
                        "time_end": "6:00 PM",
                        "subject_code": "ALG102",
                        "subject": "Algebra 2",
                        "duration": "120 Minutes"
                    ]
                ],
                "thursday" : [
                    [
                        "time_start": "9:00 AM",
                        "time_end": "10:00 AM",
                        "subject_code": "A&D101",
                        "subject": "Analysis & Design",
                        "duration": "60 Minutes"
                    ],
                    [
                        "time_start": "10:00 AM",
                        "time_end": "11:00 AM",
                        "subject_code": "CAL101",
                        "subject": "Basic Calculus",
                        "duration": "60 Minutes"
                    ],
                    [
                        "time_start": "11:00 AM",
                        "time_end": "12:00 PM",
                        "subject_code": "",
                        "subject": "Lunch",
                        "duration": "60 Minutes"
                    ],
                    [
                        "time_start": "1:00 PM",
                        "time_end": "3:00 PM",
                        "subject_code": "PE101",
                        "subject": "PE / Badminton",
                        "duration": "120 Minutes"
                    ]
                ],
                "friday" : [
                    [
                        "time_start": "8:00 AM",
                        "time_end": "9:30 AM",
                        "subject_code": "LIT101",
                        "subject": "Literature",
                        "duration": "90 Minutes"
                    ],
                    [
                        "time_start": "9:30 AM",
                        "time_end": "10:30 AM",
                        "subject_code": "TLE101",
                        "subject": "TLE",
                        "duration": "60 Minutes"
                    ],
                    [
                        "time_start": "10:30 AM",
                        "time_end": "3:00 PM",
                        "subject_code": "SHS101",
                        "subject": "Study Hall Session",
                        "duration": "270 Minutes"
                    ],
                    [
                        "time_start": "4:00 PM",
                        "time_end": "6:00 PM",
                        "subject_code": "3DD101",
                        "subject": "3d Design",
                        "duration": "120 Minutes"
                    ]
                ],
                "saturday" : [
                    [
                        "time_start": "10:00 AM",
                        "time_end": "12:00 PM",
                        "subject_code": "STAT101",
                        "subject": "Satistics",
                        "duration": "120 Minutes"
                    ],
                    [
                        "time_start": "12:00 PM",
                        "time_end": "2:00 PM",
                        "subject_code": "EL101",
                        "subject": "E-Learning",
                        "duration": "120 Minutes"
                    ],
                    [
                        "time_start": "2:00 PM",
                        "time_end": "4:00 PM",
                        "subject_code": "ALGO102",
                        "subject": "Algorithim",
                        "duration": "60 Minutes"
                    ]
                ]
            ],
            "channels" : channels ,
            "emg_person" : "Sheila Geronimo",
            "emg_contact" : "0917 555 7777",
            "img" : "",
            "b_64" : ""
            ] as [String: Any]

        let student = [
            "channels" : channels,
            "qq_id" : "ST000002",
            "address" : "san fernando, pampanga",
            "birthday" : "11/11/1995",
            "contact" : "0917 222 2222",
            "email" : "markbarrientos@gmail.com",
            "firstname" : "Mark Zaime",
            "gender" : "male",
            "student_id" : "SN000002",
            "lastname" : "Barrientos",
            "middlename" : "",
            "emg_person" : "Nanay niya",
            "emg_contact" : "0917 333 3333",
            "user_type" : "student",
            "guardian" : [],
            "subjects" : [],
            "grade" : "12",
            "section" : "1st",
            "img" : "",
            "b_64" : ""
        ] as [String: Any]
        do {
            try document.putProperties(teacher)
//            try database.delete()
        } catch {
            print("Can't save document in database")
        }
        
        let push = database.createPushReplication(myURL())
        let pull = database.createPullReplication(myURL())
        pull.channels = channels
        push.continuous = true
        pull.continuous = true
        push.start()
        pull.start()
    }
    
    func createEventsMock(title : String, desc: String, type :String){
        let manager = CBLManager.sharedInstance()
        let database = try! manager.databaseNamed("events")
        let channels = ["QQSCHOOL_EVENT"]
        let document: CBLDocument = database.document(withID: "EVENT_\((myID()["qq_id"])!)_\(Int(Date().timeIntervalSince1970))")!
        
        let announce = [
            "channels": channels,
            "author" : "\((myID()["firstname"])!) \((myID()["lastname"])!)",
            "b64" : "",
            "date": "\(Int(Date().timeIntervalSince1970))",
            "date_from" : "04/04/2018",
            "date_to": "04/06/2018",
            "desc" : "\(desc)",
            "title": "\(title)",
            "img": "",
            "location": "badminton court #4",
            "type": "\(type)"
            ] as [String : Any]
        
        do {
            try document.putProperties(announce)
        } catch {
            print("Can't save document in database")
            return
        }
    }
    
    //end mock data
    
    func resetStatus() {
        let manager = CBLManager.sharedInstance()
        let database = try! manager.databaseNamed("mydata")
        let document = database.document(withID: "\((self.myQR()["i"])!)")!
        var property = document.properties
        property!["status"] = "0"
        //        print(" check ", document)
        do {
            try document.putProperties(property!)
//            self.deleteDb()
        } catch {
            print(" change status failed! ", error)
        }
    }
    
    func saveProfilePicture(image: UIImage) {
        let manager = CBLManager.sharedInstance()
        let database = try! manager.databaseNamed("mydata")
        let document = database.document(withID: "\((self.myQR()["i"])!)")!
        let imageData:Data = UIImagePNGRepresentation(image)!//UIImageJPEGRepresentation(image, 0.4)!
        let b64 = imageData.base64EncodedString()
        
        var property = document.properties
        property!["img"] = "data:image/png;base64,\(b64)"
        property!["b64"] = b64
        
//        print(" didFinishPickingMediaWithInfo ", property!)
        do {
            try document.putProperties(property!)
        } catch {
            print(" change status failed! ", error)
        }
    }
    
    func createDocChat(){
        let manager = CBLManager.sharedInstance()
        let database = try! manager.databaseNamed("chatlist")
        let document: CBLDocument = database.document(withID: "USER_\((myID()["qq_id"])!)")!
        let channel = ["USER_\((myID()["qq_id"])!)", "CHAT_\((myID()["grade"])!)_\((myID()["section"])!)"]
        let chatroom = ["\((myID()["grade"])!)-\((myID()["section"])!)"]
        let chatlist = [
            "channels": channel,
            "chatrooms": chatroom,
            "grade": "\((myID()["grade"])!)",
            "firstname": "\((myID()["firstname"])!)",
            "lastname": "\((myID()["lastname"])!)",
            "middlename": "\((myID()["middlename"])!)",
            "qq_id": "\((myID()["qq_id"])!)",
            "section": "\((myID()["section"])!)"
        ] as [String : Any]
        
        do {
            try document.putProperties(chatlist)
        } catch {
            print("Can't save document in database")
            return
        }
    }
    
    func myQR() -> [String : Any] {
        let name = "mydata"
        let manager = CBLManager.sharedInstance()
        let database = try! manager.databaseNamed("\(name)")
        let document = database.document(withID: "myqr")!
        let properties = document.properties
        let data = properties as! [String:Any]
        
        let myID = [
            "d": "\((data["d"])!)",
            "i": "\((data["i"])!)",
            "p": "\((data["p"])!)",
            "t": "\((data["t"])!)",
            "s": "\((data["s"])!)",
            "n": "\((data["n"])!)",
            "g": "\((data["g"])!)",
            "gen": "\((data["gen"])!)",
            "sct": "\((data["sct"])!)"
        ] as [String : Any]
        
        return myID
    }
    
    func myQRString(type: String) -> String {
        return "{\"d\":\"\((myQR()["d"])!)\",\"i\":\"\((myQR()["i"])!)\",\"p\":\"\((myQR()["p"])!)\",\"t\":\"\(type)\",\"s\":\"\((myQR()["s"])!)\",\"n\":\"\((myQR()["n"])!)\",\"g\":\"\((myQR()["g"])!)\",\"gen\":\"\((myQR()["gen"])!)\",\"sct\":\"\((myQR()["sct"])!)\"}"
    }
    
    func myID() -> [String : Any] {
        let manager = CBLManager.sharedInstance()
        let database = try! manager.databaseNamed("mydata")
        let document = database.document(withID: "\((myQR()["i"])!)")!
        let properties = document.properties
//        print(" myID() ", properties)
        return properties!
    }
    
    func configuration() -> [String:Any] {
        let manager = CBLManager.sharedInstance()
        let database = try! manager.databaseNamed("mydata")
        let document = database.document(withID: "configuration")!
        let properties = document.properties
        return properties!
    }
    //start replication
    func classChat() {
        let manager = CBLManager.sharedInstance()
        let chatList = try! manager.databaseNamed("chatlist")
        let document = chatList.document(withID: "USER_\((myID()["qq_id"])!)")!
        let channels = document["channels"] as! [String]
        //removeing channel "USER_QQID"
        let filterChannels = channels.filter { $0 != "USER_\((myID()["qq_id"])!)" }

        let database = try! manager.databaseNamed("chat")
        
        let push = database.createPushReplication(myURL())
        let pull = database.createPullReplication(myURL())
        pull.channels = filterChannels
        push.continuous = true
        pull.continuous = true
        push.start()
        pull.start()
    }
    
    func exams() {
        if "\((myID()["user_type"])!)" == "teacher" {
            replicate(channel: ["EXAM_\((myID()["qq_id"])!)"], database: "exams")
        } else if "\((myID()["user_type"])!)" == "student" {
            var subject_codes = [String]()
            for subjects in classes(day: FileService().getDateTime(format: "EEEE")) {
                subject_codes.append("EXAM_\((subjects.subject_code)!)")
            }
            replicate(channel: subject_codes, database: "exams")
        }
    }
    
    func merits() {
        replicate(channel: ["MERITS_\((myID()["qq_id"])!)"], database: "merits")
    }
    
    func seatplan() {
//        replicate(channel: ["CLASS_SEAT_\((myID()["qq_id"])!)"], database: "seatplan")
        replicate(channel: ["CLASS_SEAT_GENERAL"], database: "seatplan")
    }
    
    func chatlist(){
        replicate(channel: ["USER_\((myID()["qq_id"])!)"], database: "chatlist")
    }
    
    func chatrequest() {
        replicate(channel: ["GROUP_REQ_\((myID()["qq_id"])!)"], database: "chatrequest")
    }
    
    func attendance() {
        if "\((myID()["user_type"])!)" == "student" {
            replicate(channel: ["ATTENDANCE_\((myID()["qq_id"])!)"], database: "attendance")
            
        } else if "\((myID()["user_type"])!)" == "teacher" {
            var channels = [String]()
            for subjects in classes(day: FileService().getDateTime(format: "EEEE")) {
                if !(channels.contains("ATTENDANCE_GR:\((subjects.grade_lvl)!)_SEC:\((subjects.section)!)")) {
                    channels.append("ATTENDANCE_GR:\((subjects.grade_lvl)!)_SEC:\((subjects.section)!)")
                }
            }
//            print(" attendance channels ", channels)
            replicate(channel: channels, database: "attendance")
        }
    }
    
    func document() {
        replicate(channel: ["USER_\((myID()["qq_id"])!)"], database: "mydata")
    }
    
    func schedule() {
        if "\((myID()["user_type"])!)" == "student" {
//            print(" schedule ", "SCHEDULE_GR:\((myID()["grade"])!)_SEC:\((myID()["section"])!)")
            replicate(channel: ["SCHEDULE_GR:\((myID()["grade"])!)_SEC:\((myID()["section"])!)"], database: "schedule")
        }
    }
    
    func request() {
        replicate(channel: ["FRIEND_REQ_\((myID()["qq_id"])!)"], database: "request")
    }
    
    func friends() {
        if "\((myID()["user_type"])!)" == "student" {
            replicate(channel: ["FRI_\((myID()["qq_id"])!)"], database: "friends")
        } else if "\((myID()["user_type"])!)" == "teacher" {
            replicate(channel: ["COL_\((myID()["qq_id"])!)"], database: "friends")
        }
    }
    
    func tasks() {
        if "\((myID()["user_type"])!)" == "teacher" {
            replicate(channel: ["TASK_\((myID()["qq_id"])!)"], database: "tasks")
        }
    }
    
    func notes() {
        replicate(channel: ["NOTES_\((myID()["qq_id"])!)"], database: "notes")
    }
    
    func announcements() {
        replicate(channel: ["QQSCHOOL_ANNOUNCEMENTS"], database: "announcements")
    }
    
    func events() {
        replicate(channel: ["QQSCHOOL_EVENTS"], database: "events")
    }
    
    func news() {
        replicate(channel: ["QQSCHOOL_NEWS"], database: "news")
    }
    
    func modules() {
        if "\((myID()["user_type"])!)" == "teacher" {
            replicate(channel: ["MODULES_GR:\((myID()["grade"])!)_SEC:\((myID()["section"])!)", "MODULES_\((myID()["qq_id"])!)"], database: "modules")
            
        } else if "\((myID()["user_type"])!)" == "student" {
            replicate(channel: ["MODULES_GR:\((myID()["grade"])!)_SEC:\((myID()["section"])!)"], database: "modules")
        }
    }
    
    func replicate(channel: [String], database: String){
        let manager = CBLManager.sharedInstance()
        let database = try! manager.databaseNamed("\(database)")
        
//        do {
//            try database.delete()
//        } catch {
//            print("Can't save document in database")
//        }

        let push = database.createPushReplication(myURL())
        let pull = database.createPullReplication(myURL())
        pull.channels = channel
        push.continuous = true
        pull.continuous = true
        push.start()
        pull.start()
    }
    
    func replicateExam(channel: [String], database: String) {
        // EXAM_\(subject_code)
        let manager = CBLManager.sharedInstance()
        let database = try! manager.databaseNamed("\(database.lowercased())")
        
        let push = database.createPushReplication(myURL())
        let pull = database.createPullReplication(myURL())
        pull.channels = channel
        push.continuous = true
        pull.continuous = true
        push.start()
        pull.start()
    }
    
    func setupReplications(){
//        print(" my object ", myURL())
        // change url to bucket name
        
//        print(" my configuration", (myID()["schedule"])!)
        
        document()
        modules()
        
        schedule()
        merits()
        exams()
//        news()
        announcements()
        events()
        tasks()
        attendance()
        seatplan()
        
//        notes()
//        request()
//        friends()
//        chatlist()
//        classChat()
//        chatrequest()
        
    }
    
    func deleteDb(){
        let manager = CBLManager.sharedInstance()
        let databases = ["chatlist", "chatrequest", "attendance", "request", "friends", "notes", "mydata", "news", "announcements","events", "modules", "merits", "seatplan", "tasks", "schedule"]
        
        for database in databases {
            let database = try! manager.databaseNamed("\(database)")
            do {
                try database.delete()
            } catch {
                print("Can't save document in database")
                return
            }
        }
    }
    
    func updateNotes(documentID : String, title: String, note: String){
        let channel = "NOTES_\((myID()["qq_id"])!)"
        let manager = CBLManager.sharedInstance()
        let database = try! manager.databaseNamed("notes")
        let document = database.document(withID: documentID)!
        print(" updateNotes ", documentID, title)
        //update
        var properties = document.properties
        properties!["title"] = title
        properties!["note"] =  note
        properties!["date"] = "\(Int(Date().timeIntervalSince1970))"
        //
        
        do {
            try document.putProperties(properties!)
        } catch {
            print("Can't save update document in database")
            return
        }
    }
    
    func createNotes(title: String, note: String){
        let channel = "NOTES_\((myID()["qq_id"])!)"
        let manager = CBLManager.sharedInstance()
        let database = try! manager.databaseNamed("notes")
        let document = database.document(withID: "NOTES_\((myID()["qq_id"])!)_\(Int(Date().timeIntervalSince1970))")!
        
        let note = [
            "channels": channel,
            "date": "\(Int(Date().timeIntervalSince1970))",
            "title": title,
            "note": note
        ] as [String : Any]
        
        do {
            try document.putProperties(note)
        } catch {
            print("Can't save document in database")
            return
        }
    }
    
    func createTasks(title: String, details: String, subject_code: String, due_date: String){
        let channel = ["TASK_\((myID()["qq_id"])!)"]
        let manager = CBLManager.sharedInstance()
        let database = try! manager.databaseNamed("tasks")
        let document = database.document(withID: "TASK_\((myID()["qq_id"])!)_\(Int(Date().timeIntervalSince1970))")!
        
        let task = [
            "channels": channel,
            "task": title,
            "details": details,
            "subj_code": subject_code,
            "due_date": due_date
        ] as [String : Any]
        
        print(" createTask ", task)
        
        do {
            try document.putProperties(task)
        } catch {
            print("Can't save document in database")
            return
        }
    }
    
    func getAttendance(studentId : String, subject_code: String, is_on_time: String, note: String, grade: String, section: String) {
        let manager = CBLManager.sharedInstance()
        let database = try! manager.databaseNamed("attendance")
        do {
            //purge last db
            try database.delete()
            // new db for student
            self.studentReplicate(studentId : studentId, subject_code: subject_code, is_on_time: is_on_time, note: note, grade: grade, section: section)
        } catch {
            print("Can't save update document in database")
            return
        }
    }
    
    func studentReplicate(studentId : String, subject_code: String, is_on_time: String, note : String, grade: String, section: String){
        let manager = CBLManager.sharedInstance()
        let database = try! manager.databaseNamed("attendance")
        let pull = database.createPullReplication(myURL())
        pull.channels = ["ATTENDANCE_\(studentId)"]
        pull.start()
        
        NotificationCenter.default.addObserver(forName: NSNotification.Name.cblReplicationChange, object: pull, queue: nil) {
            (notification) -> Void in
            //determins if replication is over
            let active = pull.status != CBLReplicationStatus.active
            if active {
                self.createAttendance(studentId : studentId, subject_code: subject_code, is_on_time: is_on_time, note: note, grade: grade, section: section)
            }
        }
    }
    
    func createAttendance(studentId : String, subject_code: String, is_on_time: String, note: String, grade: String, section: String) {
        let manager = CBLManager.sharedInstance()
        let subject = currentClass()
        let database = try! manager.databaseNamed("attendance")
        let channels = ["ATTENDANCE_\(studentId)", "ATTENDANCE_GR:\(grade)_SEC:\(section)", "ATTENDANCE_GENERAL"]
        let document = database.document(withID: "ATT_\(studentId)_\(FileService().getDateTime(format: "MM/dd/yyyy"))_\(subject_code)")!
        var properties = document.properties
        
        if properties != nil {
            properties!["scan_datetime"] = "\(Int(Date().timeIntervalSince1970))"
            properties!["is_on_time"] = "\(is_on_time)"
            properties!["note"] = "\(note)"
            properties!["time_start"] = subject.time_start
            properties!["time_end"] = subject.time_end
            
            do {
                try document.putProperties(properties!)
            } catch {
                print("Can't save update document in database")
                return
            }
            
        } else {
            var attendance = [
                "channels": channels,
                "scan_id": "\(studentId)",
                "scan_datetime": "\(Int(Date().timeIntervalSince1970))",
                "scan_date": "\(FileService().getDateTime(format: "MM/dd/yyyy"))",
                "scan_subj": "\(subject_code)",
                "is_on_time": "\(is_on_time)",
                
            ] as [String: Any]
            
            if is_on_time == "4" {
                attendance["note"] = "\(note)"
            }
            
            if subject_code != "parent" {
                attendance["time_start"] = subject.time_start
                attendance["time_end"] = subject.time_end
            }
            
            do {
                try document.putProperties(attendance)
            } catch {
                print("Can't save document in database")
                return
            }
            
        }
        
        let push = database.createPushReplication(myURL())
        push.start()
    }
    
    func sendChat(message : String, channel : String){
        let manager = CBLManager.sharedInstance()
        let database = try! manager.databaseNamed("chat")
        let document: CBLDocument = database.document(withID: "CHAT_\((myID()["qq_id"])!)_\(channel)_\(Int(Date().timeIntervalSince1970))")!
        let chat = [
            "channels" : [channel],
            "sender_qq_id" : "\((myID()["qq_id"])!)",
            "sender_name": "\((myID()["firstname"])!) \((myID()["lastname"])!)",
            "message": "\(message)",
            "datetime": "\(Int(Date().timeIntervalSince1970))"
        ] as [String : Any]
        
        do {
            if "\(message)" != "" {
                try document.putProperties(chat)
            }
        } catch {
            print("Can't save document in database")
            return
        }
    }
    
    func createAnnouncement(title : String, desc: String, type :String){
        let manager = CBLManager.sharedInstance()
        let database = try! manager.databaseNamed("announcements")
        let channels = ["QQSCHOOL_ANNOUNCEMENTS"]
        let document: CBLDocument = database.document(withID: "AN\(Int(Date().timeIntervalSince1970))")!
        
        let announce = [
            "channels": channels,
            "author" : "\((myID()["firstname"])!) \((myID()["lastname"])!)",
            "b64" : "",
            "date": "\(Int(Date().timeIntervalSince1970))",
            "date_from" : "",
            "date_to": "",
            "desc" : "\(desc)",
            "title": "\(title)",
            "img": "",
            "location": "",
            "type": "\(type)"
            ] as [String : Any]
        
        do {
            try document.putProperties(announce)
        } catch {
            print("Can't save document in database")
            return
        }
    }
    
    func createChat(name : String, sendTo : [String]){
        var sendTo = sendTo
        let channels = sendTo.append("GROUP_REQ_\((myID()["qq_id"])!)")
        let manager = CBLManager.sharedInstance()
        let database = try! manager.databaseNamed("chatrequest")
        let document: CBLDocument = database.document(withID: "GROUP_REQ_\((myID()["qq_id"])!)_\(Int(Date().timeIntervalSince1970))")!
        
        let request = [
            "channels" : channels,
            "group_id" : "CHAT_\(name)_\(Int(Date().timeIntervalSince1970))",
            "group_name" : "\(name)"
            ] as [String : Any]
        
        do {
            try document.putProperties(request)
        } catch {
            print("Can't save document in database")
            return
        }
    }
    
    func classes(day: String) -> [Subjects]{
        let schedule = scheduleList()
        var classes = [Subjects]()
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-dd-MM h:mm a"
        formatter.locale = Locale.current
        
        if schedule.count != 0 {
            for subject in schedule {
                if day == subject.key.capitalized {
                    let subjects = (subject.value) as! [[String : Any]]
                    for subject in subjects {
                        // get sched per day
                        if "\((subject["subject_code"])!)" != "LUNCH" {
                            let classStartLong = formatter.date(from: "\(FileService().getDateTime(format: "yyyy-dd-MM")) \((subject["time_start"])!)")
                            let classEndLong = formatter.date(from: "\(FileService().getDateTime(format: "yyyy-dd-MM")) \((subject["time_end"])!)")
                            
                            var classToday = Subjects()
                            classToday.subject = "\((subject["subject"])!)"
                            classToday.time_start = "\((subject["time_start"])!)"
                            classToday.time_end = "\((subject["time_end"])!)"
                            classToday.subject_code = "\((subject["subject_code"])!)"
                            classToday.grade_lvl = "\((subject["grade_lvl"])!)"
                            classToday.section = "\((subject["section"])!)"
                            classToday.teacher = "\((subject["teacher"])!)"
                            classToday.teacher_id = "\((subject["teacher_id"])!)"
                            classToday.time_start_long = "\(Int(classStartLong!.timeIntervalSince1970))"
                            classToday.time_end_long = "\(Int(classEndLong!.timeIntervalSince1970))"
                            classes.append(classToday)

                        }
                    }
                    classes = classes.sorted(by: { $0.time_start_long.compare($1.time_start_long) == .orderedAscending })
                }
            }
        }
        return classes
    }
    
    func currentClass() -> Subjects {
        var current = Subjects()
        let classes = ApiService().classes(day: FileService().getDateTime(format: "EEEE"))
        
        for (index, subject) in classes.enumerated() {
            let condition = Int(subject.time_start_long)! <= Int(Date().timeIntervalSince1970) && Int(Date().timeIntervalSince1970) <=  Int(subject.time_end_long)!
            
            var allotted = Int(subject.time_start_long)!-1800 <= Int(Date().timeIntervalSince1970) && Int(Date().timeIntervalSince1970) <  Int(subject.time_start_long)!
            
            if index > 0 {
                let next =  classes[Int(index-1)]
                
                allotted = Int(subject.time_start_long)!-1800 <= Int(Date().timeIntervalSince1970) && Int(Date().timeIntervalSince1970) <  Int(subject.time_start_long)! && next.time_end != subject.time_start
            }
            
            let classNext = Int(subject.time_end_long)! <= Int(Date().timeIntervalSince1970) && index < classes.count-1
            
            if classNext {
                current = subject
            }
            
            if allotted {
                current = subject
            }
            
            if condition {
                current = subject
            }
        }
        
        return current
    }
    
    func noteList() -> [Notes] {
        let manager = CBLManager.sharedInstance()
        let database = try! manager.databaseNamed("notes")
        let query = database.createAllDocumentsQuery()
        var notes = [Notes]()
        do {
            let result = try query.run()
            if result != nil {
                while let row = result.nextRow(){
                    let document = row.document?.properties
                    var note = Notes()
                    note.title = "\((document!["title"])!)"
                    note.date = "\((document!["date"])!)"
                    note.content = "\((document!["note"])!)"
                    note.documentID = "\((row.documentID)!)"
                    notes.append(note)
                }
            }
            notes = notes.sorted(by: { $0.date.compare($1.date) == .orderedDescending })
        } catch {
            print(" error getting announcements :", error)
        }
        return notes
    }
    
    func taskList() -> [Tasks] {
        let manager = CBLManager.sharedInstance()
        let database = try! manager.databaseNamed("tasks")
        let query = database.createAllDocumentsQuery()
        var tasks = [Tasks]()
        
        do {
            let result = try query.run()
            if result != nil {
                while let row = result.nextRow(){
                    let document = row.document?.properties
                    var task = Tasks()
                    task.subject_code = "\((document!["subj_code"])!)"
                    task.title = "\((document!["task"])!)"
                    task.description = "\((document!["details"])!)"
                    task.due = "\((document!["due_date"])!)"
                    tasks.append(task)
                }
            }
        } catch {
            print(" error getting announcements :", error)
        }
        return tasks
    }
    
    func examParentList() -> [Exams] {
        let manager = CBLManager.sharedInstance()
        let database = try! manager.databaseNamed("exams")
        let query = database.createAllDocumentsQuery()
        var exams = [Exams]()
        
        do {
            let result = try query.run()
            if result != nil {
                while let row = result.nextRow(){
                    let document = row.document?.properties
//                    print(" examParentList ", document!["exam_name"])
                    if "\((document!["type"])!)" == "exam" {
                        var exam = Exams()
                        exam.allotted_time = "\((document!["allotted_time"])!)"
                        exam.exam_name = "\((document!["exam_name"])!)"
                        exam.exam_desc = "\((document!["exam_desc"])!)"
                        exam.exam_due = "\((document!["exam_due"])!)"
                        exam.upload_date = "\((document!["upload_date"])!)"
                        exam.documentID = "\((row.documentID)!)"
                        exam.subj_code = "\((document!["subj_code"])!)"
                        exams.append(exam)
                    }
                }
            }
            exams = exams.sorted(by: { $0.upload_date.compare($1.upload_date) == .orderedDescending })
        } catch {
            print(" error getting announcements :", error)
        }
        return exams
    }
    
    func studentList() -> [Students] {
        let manager = CBLManager.sharedInstance()
        let database = try! manager.databaseNamed("merits")
        let query = database.createAllDocumentsQuery()
        var studentList = [Students]()
        
        do {
            let result = try query.run()
            if result != nil {
                while let row = result.nextRow(){
                    let document = row.document?.properties
                    var student = Students()
                    student.qq_id = "\((document!["qq_id"])!)"
                    student.name = "\((document!["name"])!)"
                    student.gender = "\((document!["gender"])!)"
                    student.merits = "\((document!["merits"])!)"
                    student.demerits = "\((document!["demerits"])!)"
                    student.subj_code = "\((document!["subj_code"])!)"
                    student.grade = "\((document!["grade"])!)"
                    student.section = "\((document!["section"])!)"
                    student.seat_pos = "\((document!["seat_pos"])!)"
                    student.note = "\((document!["note"])!)"
                    student.documentID = "\((row.documentID)!)"
                    student.isExpanded = false
                    studentList.append(student)
                }
            }
        } catch {
            print(" error getting announcements :", error)
        }
        return studentList
    }
    
    
//    func examChildList(documentID : String) -> [] {
//        let manager = CBLManager.sharedInstance()
//        let database = try! manager.databaseNamed("exams")
//        let query = database.createAllDocumentsQuery()
//        let documentParent = database.document(withID: documentID)!
//        let parent = documentParent.properties
//        // exam information
//        if parent != nil {
//            examNameLabel.text = "\((parent!["exam_name"])!)"
//            examInstructionsLabel.text = "\((parent!["exam_desc"])!)"
//            do {
//                let result = try query.run()
//                if result != nil {
//                    while let row = result.nextRow(){
//                        let document = row.document?.properties
//                        // exam questions
//                        if "\((document!["type"])!)" == "question" && documentID == "\((document!["parent"])!)" {
//                            let choices = ApiService().stringToJsonObject(string : "\((document!["choices"])!)")
//                            var question = Questions()
//                            question.answer = "\((document!["answer"])!)"
//                            question.question = "\((document!["question"])!)"
//                            question.choice1 = "\((choices["choice 1"]!))"
//                            question.choice2 = "\((choices["choice 2"]!))"
//                            question.choice3 = "\((choices["choice 3"]!))"
//                            question.choice4 = "\((choices["choice 4"]!))"
//                            question.number = "\(questions.count+1)"
//                            questions.append(question)
//                        }
//                    }
//                    
//                }
//                //            print(questions)
//                if questions.count > 1 {
//                    nextButton.alpha = 1
//                }
//                DispatchQueue.main.async(execute: {
//                    self.ExamView.reloadData()
//                })
//            } catch {
//                print(" error getting announcements :", error)
//            }
//        }
//    }
    
    func announcementList() -> [Announcements]{
        let manager = CBLManager.sharedInstance()
        let database = try! manager.databaseNamed("announcements")
        let query = database.createAllDocumentsQuery()
        var announcements = [Announcements]()
        
        do {
            let result = try query.run()
            if result != nil {
                while let row = result.nextRow(){
                    let document = row.document?.properties
//                    if "\((document!["type"])!)" == "announcement" {
                        var announcement = Announcements()
                        announcement.title = "\((document!["title"])!)"
                        announcement.description = "\((document!["desc"])!)"
                        announcement.thumbnail = document!["b64"] == nil ? "news1" : "\((document!["b64"])!)"
                        announcement.author = document!["author"] == nil ? "Admin" : "\((document!["author"])!)"
                        announcement.date = "\((document!["date"])!)"
                        announcements.append(announcement)
//                    }
                }
            }
            announcements = announcements.sorted(by: { $0.date.compare($1.date) == .orderedDescending })
        } catch {
            print(" error getting announcements :", error)
        }
        return announcements
    }
    
    func getGateAttendance(studentID: String) -> Bool {
        let manager = CBLManager.sharedInstance()
        let database = try! manager.databaseNamed("attendance")
        let document = database.document(withID: "ATT_\(studentID)_\(FileService().getDateTime(format: "MM/dd/yyyy"))_parent")!
        let properties = document.properties
        var isPresent = false
        
        if properties != nil {
            isPresent = true
        }
        
        return isPresent
    }
    
    func attendanceList() -> [Attendances] {
        let manager = CBLManager.sharedInstance()
        let database = try! manager.databaseNamed("attendance")
        let query = database.createAllDocumentsQuery()
        var attendances = [Attendances]()
        
        do {
            let result = try query.run()
            if result != nil {
                while let row = result.nextRow(){
                    let document = row.document?.properties
                    var attendance = Attendances()
                    attendance.is_on_time = "\((document!["is_on_time"])!)"
                    attendance.scan_date = "\((document!["scan_date"])!)"
                    attendance.scan_datetime = "\((document!["scan_datetime"])!)"
                    attendance.scan_id = "\((document!["scan_id"])!)"
                    attendance.scan_subj = "\((document!["scan_subj"])!)"
                    attendance.note = (document!["note"]) == nil ? "" : "\((document!["note"])!)"
                    attendances.append(attendance)
                }
            }
            attendances = attendances.sorted(by: { $0.scan_datetime.compare($1.scan_datetime) == .orderedDescending })
        } catch {
            print(" error getting attendances :", error)
        }
        return attendances
    }
    
    func moduleList() -> [Modules] {
        let manager = CBLManager.sharedInstance()
        let database = try! manager.databaseNamed("modules")
        let query = database.createAllDocumentsQuery()
        var modules = [Modules]()
        
        do {
            let result = try query.run()
            if result != nil {
                while let row = result.nextRow(){
                    let document = row.document?.properties
                    var module = Modules()
                    module.name = "\((document!["file_name"])!)"
                    module.date = "\((document!["upload_date"])!)"
                    module.type = "file"
                    module.subj_code = "\((document!["subj_code"])!)"
                    let types = ["jpeg", "jpg", "png", "gif", "avi", "doc", "docx", "mp3", "mp4", "pdf", "ppt", "txt", "xls", "zip"]
                    
                    for type in types {
                        let url = "\((document!["file_name"])!)"
                        if url.contains(".\(type)") {
                            module.type = "\(type)"
                        }
                    }
                    modules.append(module)
                }
            }
            modules = modules.sorted(by: { $0.date.compare($1.date) == .orderedDescending })
        } catch {
            print(" error getting announcements :", error)
        }
        return modules
    }
    
    func scheduleList() -> [String: Any]{
        if "\((myID()["user_type"])!)" == "student" {
            let manager = CBLManager.sharedInstance()
            let database = try! manager.databaseNamed("schedule")
            let document = database.document(withID: "SCHEDULE_GR:\((myID()["grade"])!)_SEC:\((myID()["section"])!)")!
            let properties = document.properties
            if properties != nil {
                return stringToJsonObject(string: "\((properties!["schedule"])!)")
            }
            return [String: Any]()
        }
//        else if "\((myID()["user_type"])!)" == "teacher" {
//            let manager = CBLManager.sharedInstance()
//            let database = try! manager.databaseNamed("mydata")
//            let document = database.document(withID: "mydata")!
//            let properties = document.properties
//
//            print(" scheduleList teacher ", "\((properties!["schedule"])!)")
//            if properties != nil {
//                return stringToJsonObject(string: "\((properties!["schedule"])!)")
//            }
//            return [String: Any]()
//
//        }
        return stringToJsonObject(string: "\((myID()["schedule"])!)")
    }
    
    func stringToJsonObject(string : String) -> [String: Any] {
        let data = string.data(using: .utf8)
        var jsonObject = [String : Any]()
        do {
            if let json = try JSONSerialization.jsonObject(with: data!, options : .allowFragments) as? [String : Any]
            {
                jsonObject = json
            } else {
                print("bad json")
            }
        } catch let error as NSError {
            print(" string NSError : ",error)
        }
        return jsonObject
    }
    
    func schduleToday()->[[String : Any]]{
        let scheduleString =  "\((myID()["schedule"])!)"
        let data = scheduleString.data(using: .utf8)
        var schedule = [[String : Any]]()
        do {
            if let json = try JSONSerialization.jsonObject(with: data!, options : .allowFragments) as? [String : Any]
            {
                for days in json {
                    if FileService().getDateTime(format: "EEEE") == days.key.capitalized {
                        let subjects = (days.value) as! [[String : Any]]
                        schedule = subjects
                    }
                }
            } else {
                print("bad json")
            }
        } catch let error as NSError {
            print(" schduleToday ",error)
        }
        return schedule
    }
    
    func friendsList(database : String) -> [Friends] {
        let databaseName = database
        let manager = CBLManager.sharedInstance()
        let database = try! manager.databaseNamed("\(databaseName)")
        let query = database.createAllDocumentsQuery()
        var requests = [Friends]()
        
        do {
            let result = try query.run()
            if result != nil {
                if "\(databaseName)" == "friends"{
                    while let row = result.nextRow(){
                        let document = row.document?.properties
                        var request = Friends()
                        request.name = "\((document!["name"])!)"
                        request.grade = "\((document!["grade"])!)"
                        request.qq_id = "\((document!["qq_id"])!)"
                        request.section = "\((document!["section"])!)"
                        request.b_64 = "\((document!["img"])!)"
                        requests.append(request)
                    }
                } else {
                    while let row = result.nextRow(){
                        let document = row.document?.properties
                        var request = Friends()
                        request.name = "\((document!["name"])!)"
                        request.img = "\((document!["img"])!)"
                        request.grade = "\((document!["grade"])!)"
                        request.qq_id = "\((document!["qq_id"])!)"
                        request.section = "\((document!["section"])!)"
                        request.status = "\((document!["status"])!)"
                        request.req_name = "\((document!["req_name"])!)"
                        request.req_id = "\((document!["req_id"])!)"
                        request.req_grade = "\((document!["req_grade"])!)"
                        request.req_section = "\((document!["req_section"])!)"
                        request.req_img = "\((document!["req_img"])!)"
                        requests.append(request)
                    }
                }
            }
        } catch let error as NSError {
            print("ERROR ", error)
        }
        return requests
    }
    
    func addChannelToChatList(channel : String, chatroom : String){
        let manager = CBLManager.sharedInstance()
        let database = try! manager.databaseNamed("chatlist")
        let docu = database.document(withID: "USER_\((ApiService().myID()["qq_id"])!)")
        var document = docu?.properties
        var chatRooms = document!["chatrooms"] as? [String]
        var chatChannel = document!["channels"] as? [String]
        
        //check if channel already exist
        let ifChannelExist = chatChannel!.contains("\(channel)")
        
//        print("ifChannelExist : ", channel, chatroom)
//        print("ifChannelExist : ", ifChannelExist, chatRooms!, chatChannel!)
        
        do {
            if ifChannelExist == false {
                chatRooms!.append("\(chatroom)")
                chatChannel!.append("\(channel)")
                document!["chatrooms"]  = chatRooms!
                document!["channels"] = chatChannel!
                
                try docu?.putProperties(document!)
            }
        } catch {
            print("Can't save document in database")
            return
        }
    }
    
    func sendFriendRequest(friend : Users, status : String) {
        let manager = CBLManager.sharedInstance()
        let channel = ["FRIEND_REQ_\((friend.qq_id)!)", "FRIEND_REQ_\((myID()["qq_id"])!)"]
        let database = try! manager.databaseNamed("request") //db to watch friends request
        let document: CBLDocument = database.document(withID: "FRIEND_REQ_\((myID()["qq_id"])!)_\((friend.qq_id)!)")!
        
        let request = [
            "channels" : channel,
            "qq_id" : "\((friend.qq_id)!)",
            "name": "\((friend.firstname)!) \((friend.middlename)!) \((friend.lastname)!)",
            "grade": "\((friend.grade)!)",
            "section": "\((friend.section)!)",
            "img": "\((friend.b_64)!)",
            "req_id" : "\((myID()["qq_id"])!)",
            "req_name" : "\((myID()["firstname"])!) \((myID()["middlename"])!) \((myID()["lastname"])!)",
            "req_grade" : "\((myID()["grade"])!)",
            "req_section" : "\((myID()["section"])!)",
            "req_img" : "",
            "status": "\(status)"
        ] as [String : Any]
        
//        print(" your request ",request)
        do {
            try document.putProperties(request)
        } catch {
            print("Can't save document in database")
            return
        }
    }
    
    func addFriend(friend : Friends){
        let manager = CBLManager.sharedInstance()
        
        var channel = [String]()
        if "\((myID()["user_type"])!)" == "student" {
            channel = ["FRI_\((myID()["qq_id"])!)"]
        } else {
            channel = ["COL_\((myID()["qq_id"])!)"]
        }
        
        let database = try! manager.databaseNamed("friends") //db to watch friends request
        let document: CBLDocument = database.document(withID: "FRI_ADD_\((myID()["qq_id"])!)_\((friend.qq_id)!))_\(Int(Date().timeIntervalSince1970))")!
        let friends = friendsList(database: "friends") as? [Friends]
        
        //check if friend already exist
        let ifFriendExist = friends?.contains(where:  { $0.qq_id == friend.qq_id })
        
        let request = [
            "channels" : channel,
            "qq_id" : "\((friend.qq_id)!)",
            "name": "\((friend.name)!)",
            "grade": "\((friend.grade)!)",
            "section": "\((friend.section)!)",
            "img": "\((friend.img)!)"
            ] as [String : Any]
        
//        print(" your request ",request)
        do {
            if (ifFriendExist)! == false {
                try document.putProperties(request)
            }
        } catch {
            print("Can't save document in database")
            return
        }
    }
    
    func updateFriendRequest(friend : Friends, status : String) {
        let manager = CBLManager.sharedInstance()
        let database = try! manager.databaseNamed("request")
        
        let query = database.createAllDocumentsQuery()
        do {
            let result = try query.run()
            if result != nil {
                while let row = result.nextRow(){
                    var document = row.document?.properties
                    if "\((friend.qq_id)!)" == "\((document!["req_id"])!)" {
                        let getdocument = database.document(withID: "\((document!["_id"])!)")!
                        var properties = getdocument.properties
                        (properties!["status"])! = "\(status)"
                        
                        do {
                            try getdocument.putProperties(properties!)
                        } catch {
                            print("Can't save document in database")
                            return
                        }
                        
                    }
                }
            }
        } catch {
            print("error...")
            return
        }
    }
    
    func deleteFriendRequest(friend : Friends) {
        let manager = CBLManager.sharedInstance()
        let database = try! manager.databaseNamed("request")
        
        let query = database.createAllDocumentsQuery()
        do {
            let result = try query.run()
            if result != nil {
                while let row = result.nextRow(){
                    var document = row.document?.properties
                    if "\((friend.qq_id)!)" == "\((document!["req_id"])!)" {
                        let getdocument = database.document(withID: "\((document!["_id"])!)")!
                        
                        do {
                            try getdocument.delete()
                        } catch {
                            print("Can't save document in database")
                            return
                        }
                        
                    }
                }
            }
        } catch {
            print("error...")
            return
        }
    }
    
    func createModule(file_name : String, subj_code: String, fileExtension: String){
        let manager = CBLManager.sharedInstance()
        let channel = ["MODULES_\((myID()["qq_id"])!)", "MODULES_GR:\((myID()["grade"])!)_SEC:\((myID()["section"])!)"]
        let database = try! manager.databaseNamed("modules")
        let document: CBLDocument = database.document(withID: "MODULES_\((myID()["qq_id"])!)_\(Int(Date().timeIntervalSince1970))")!
        
        let module = [
            "channels": channel,
            "file_location": "http://192.168.1.114/android_api/uploads/\(file_name).\(fileExtension)",
            "file_name" : "\(file_name).\(fileExtension)",
            "subj_code" : "\(subj_code)",
            "teacher_id": "\((myID()["qq_id"])!)",
            "upload_date": "\(Int(Date().timeIntervalSince1970))"
        ] as [String : Any]
        print(" module to post :", module)
        do {
            try document.putProperties(module)
        } catch {
            print("Can't save document in database")
            return
        }
    }
    
    func createSeating(seatplan : [String:Any], subject_code: String) {
        let manager = CBLManager.sharedInstance()
        let database = try! manager.databaseNamed("seatplan")
        let document: CBLDocument = database.document(withID: "CLASS_SEAT_\(subject_code)_GR:\((myID()["grade"])!)_SEC:\((myID()["section"])!)")!
        
        do {
            try document.putProperties(seatplan)
        } catch {
            print("Can't save document in database")
            return
        }
    }
    
    func createMerit(subject_code: String, studentID : [String:Any], seatnumber : String, alias : String) {
        let manager = CBLManager.sharedInstance()
        let database = try! manager.databaseNamed("merits")
        let document = database.document(withID: "MERITS_\((studentID["i"])!)_\(subject_code)_GR:\((myID()["grade"])!)_SEC:\((myID()["section"])!)")!
        let channel = ["MERITS_\((myID()["qq_id"])!)", "MERITS_\((studentID["i"])!)"]
        let studentsMerit = [
            "qq_id": "\((studentID["i"])!)",
            "name": "\((studentID["n"])!)",
            "gender": "\((studentID["gen"])!)",
            "grade": "\((studentID["g"])!)",
            "section": "\((studentID["sct"])!)",
            "seat_pos": "\(Int(seatnumber)!-1)",
            "note": "",
            "subj_code": subject_code,
            "merits": "0",
            "demerits": "0",
            "alias": alias,
        ] as [String:Any]
        
//        print(" createMerit ", studentsMerit)
        
        do {
            try document.putProperties(studentsMerit)
        } catch {
            print("Can't save update document in database")
            return
        }
    }
    
    func updateSeatPlan(studentID : [String:Any], current_seat: String, change_seat: String, documentID : String, alias : String) {
        let manager = CBLManager.sharedInstance()
        let database = try! manager.databaseNamed("seatplan")
        let document = database.document(withID: documentID)!
        var properties = document.properties
        // update to null
        properties!["\(Int(current_seat)!)"] = "null"
        properties!["\(Int(change_seat)!)"] =  "{\"qq_id\":\"\((studentID["i"])!)\",\"name\":\"\((studentID["n"])!)\"}"
        
//        print(" updateSeatPlan ", properties)
        
        do {
            try document.putProperties(properties!)
        } catch {
            print("Can't save update document in database")
            return
        }
    }
    
    func addSeatPlan(subject_code: String, seatnumber: String, studentID : [String:Any], alias : String, documentID :String){
        let manager = CBLManager.sharedInstance()
        let database = try! manager.databaseNamed("seatplan")
        
        let document = database.document(withID: documentID)!
        var properties = document.properties
        //update
        properties!["\(Int(seatnumber)!)"] =  "{\"qq_id\":\"\((studentID["i"])!)\",\"name\":\"\((studentID["n"])!)\"}"
        
//        print(" addSeatPlan ", properties)
        do {
            try document.putProperties(properties!)
            //create merit document
            createMerit(subject_code: subject_code, studentID: studentID, seatnumber: seatnumber, alias: alias)
        } catch {
            print("Can't save update document in database")
            return
        }
    }
    
    func updateExamParent(exam : [String:Any], questions: [Questions], documentID: String) {
        let manager = CBLManager.sharedInstance()
        let database = try! manager.databaseNamed("exams")
        let document = database.document(withID: documentID)!
        var property = document.properties
        
        property!["allotted_time"] = "\((exam["allotted_time"])!)"
        property!["exam_desc"] = "\((exam["exam_desc"])!)"
        property!["exam_due"] = "\((exam["exam_due"])!)"
        property!["exam_name"] = "\((exam["exam_name"])!)"
        property!["subj_code"] = "\((exam["subj_code"])!)"
        property!["teacher_id"] = "\((exam["teacher_id"])!)"
        property!["type"] = "exam"
        property!["upload_date"] = "\(Int(Date().timeIntervalSince1970))"
        
//        print(" updateExamParent ", property)
        
        do {
            try document.putProperties(property!)
            self.updateExamChild(subj_code: "\((exam["subj_code"])!)", documentID: documentID, questions: questions)
        } catch {
            print("Can't save update document in database")
            view.showToast(message: "Can't be saved. Try Again.")
            return
        }
    }
    
    func updateExamChild(subj_code: String, documentID: String, questions: [Questions]) {
        let manager = CBLManager.sharedInstance()
        let database = try! manager.databaseNamed("exams")
        let query = database.createAllDocumentsQuery()
        let documentParent = database.document(withID: documentID)!
        let parent = documentParent.properties
        
        print(" updateExamChild ", questions)
        
        if parent != nil {
            do {
                let result = try query.run()
                if result != nil {
                    while let row = result.nextRow(){
                        let document = row.document?.properties
                        if "\((document!["type"])!)" == "question" && documentID == "\((document!["parent"])!)" {
                        }
                    }
                }
            } catch {
                 print(" error getting questions for exam :", error)
            }
        }
//
//        for (index, question) in questions.enumerated() {
//            let document = database.document(withID: "EXAM_QUESTION_\(index+1)_\(subj_code)_\((myID()["qq_id"])!)_\(Int(Date().timeIntervalSince1970))")!
//            let child = [
//                "subj_code":"\(subj_code)",
//                "question":"\((question.question)!)",
//                "choices": "{\"choices\":{\"choice 1\":\"\((question.choice1)!)\",\"choice 2\":\"\((question.choice2)!)\",\"choice 3\":\"\((question.choice3)!)\",\"choice 4\":\"\((question.choice4)!)\"}}",
//                "answer":"\((question.answer)!)",
//                "parent": documentID,
//                "type": "question",
//                "channels" : channels
//                ] as [String: Any]
//
//            //            print(" createChild ", document.documentID, child)
//
//            do {
////                try document.putProperties(child)
//            } catch {
//                print("Can't save update document in database")
//                return
//            }
//        }
        view.showToast(message: "Exam updated successfuly!")
        
    }
    
    func createExamParent(exam : [String:Any], questions: [Questions]) {
        let manager = CBLManager.sharedInstance()
        let database = try! manager.databaseNamed("exams")
        let document = database.document(withID: "EXAM_\((exam["subj_code"])!)_\((myID()["qq_id"])!)_\(Int(Date().timeIntervalSince1970))")!
        let channels = ["EXAM_\((exam["subj_code"])!)", "EXAM_\((myID()["qq_id"])!)"]
        
        let parent = [
            "allotted_time": "\((exam["allotted_time"])!)",
            "exam_desc": "\((exam["exam_desc"])!)",
            "exam_due": "\((exam["exam_due"])!)",
            "exam_name": "\((exam["exam_name"])!)",
            "subj_code": "\((exam["subj_code"])!)",
            "teacher_id": "\((exam["teacher_id"])!)",
            "type": "exam",
            "upload_date": "\(Int(Date().timeIntervalSince1970))",
            "channels" : channels
        ] as [String:Any]
       
//        print(" createParent ", parent)
        
        do {
            try document.putProperties(parent)
            self.createExamChild(subj_code: "\((exam["subj_code"])!)", documentID: document.documentID, questions: questions)
        } catch {
            print("Can't save update document in database")
            view.showToast(message: "Can't be saved. Try Again.")
            return
        }
    }
    
    func createExamChild(subj_code: String, documentID: String, questions: [Questions]) {
        let manager = CBLManager.sharedInstance()
        let database = try! manager.databaseNamed("exams")
        let channels = ["EXAM_\(subj_code)", "EXAM_\((myID()["qq_id"])!)"]
        
        for (index, question) in questions.enumerated() {
            let document = database.document(withID: "EXAM_QUESTION_\(index+1)_\(subj_code)_\((myID()["qq_id"])!)_\(Int(Date().timeIntervalSince1970))")!
            let child = [
                "subj_code":"\(subj_code)",
                "question":"\((question.question)!)",
                "choices": "{\"choices\":{\"choice 1\":\"\((question.choice1)!)\",\"choice 2\":\"\((question.choice2)!)\",\"choice 3\":\"\((question.choice3)!)\",\"choice 4\":\"\((question.choice4)!)\"}}",
                "answer":"\((question.answer)!)",
                "parent": documentID,
                "type": "question",
                "channels" : channels
            ] as [String: Any]
            
//            print(" createChild ", document.documentID, child)
            
            do {
                try document.putProperties(child)
            } catch {
                print("Can't save update document in database")
                return
            }
        }
        view.showToast(message: "Exam successfuly saved!")
        
    }
    
}

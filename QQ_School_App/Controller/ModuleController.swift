//
//  ModuleController.swift
//  QQ_School_App
//
//  Created by Abby Esteves on 23/03/2018.
//  Copyright © 2018 Abby Esteves. All rights reserved.
//

import UIKit
class ModulesController: BaseCell {
    
    private func setupView() {
        
    }
    
    private func setupConstraint() {
        
    }
    
    override func setupViews() {
        super.setupViews()
        setupView()
        setupConstraint()
    }
    
}

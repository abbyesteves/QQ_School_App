//
//  SchoolCalendarController.swift
//  QQ_School_App
//
//  Created by Abby Esteves on 02/03/2018.
//  Copyright © 2018 Abby Esteves. All rights reserved.
//

import UIKit
import FSCalendar

class CalendarController: UIViewController, FSCalendarDelegate, FSCalendarDataSource, FSCalendarDelegateAppearance, UIScrollViewDelegate {
    
    fileprivate weak var calendar: FSCalendar!
    let manager = CBLManager.sharedInstance()
    var events = [Events]()
    var dateSelected = String()
    var classDays = [String]()
    var eventDays = [String]()
    var eventDaysFrom = [String]()
    var eventDaysTo = [String]()
    var MainScrollView = UIScrollView()
    var calendarFrame = CGRect(x: 0, y: 0, width: 0, height: 0)
    var schedule = ApiService().scheduleList()//(ApiService().myID()["schedule"])! as! [String : Any]
    let navbarStatusHeight = UIApplication.shared.statusBarFrame.height
    let bottomSpacing = UIApplication.shared.keyWindow?.safeAreaInsets.bottom
    let tabBarHeight = UITabBarController.init().tabBar.frame.height
    
    let weekView : UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.Background(alpha: 1.0)
        view.layer.shadowColor = UIColor.gray.cgColor
        view.layer.shadowOpacity = 0.5
        view.layer.shadowOffset = CGSize(width: 0, height: 2)
        view.layer.shadowRadius = 3
        return view
    }()
    
    let eventLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.rgba(red: 53, green: 4, blue: 88, alpha: 1.0)
        label.text = "EVENT"
        label.backgroundColor = UIColor.rgba(red: 112, green: 50, blue: 157, alpha: 0.3)
        label.textAlignment = .right
        label.font = label.font.withSize(8)
        label.font = UIFont.boldSystemFont(ofSize: label.font.pointSize)
        return label
    }()
    
    let classLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.rgba(red: 131, green: 128, blue: 0, alpha: 1.0)
        label.backgroundColor = UIColor.rgba(red: 212, green: 207, blue: 22, alpha: 0.3)
        label.text = "CLASS"
        label.textAlignment = .right
        label.font = label.font.withSize(8)
        label.font = UIFont.boldSystemFont(ofSize: label.font.pointSize)
        return label
    }()
    
    let eventIndicator : UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.rgba(red: 53, green: 4, blue: 88, alpha: 1.0) 
        return view
    }()
    
    let classIndicator : UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.rgba(red: 171, green: 167, blue: 7, alpha: 1.0)
        return view
    }()
    
    @objc func handlePan(_ gestureRecognizer: UIPanGestureRecognizer) {
        if let swipeGesture = gestureRecognizer as? UISwipeGestureRecognizer {
            switch swipeGesture.direction {
            case UISwipeGestureRecognizerDirection.right:
                print("Swiped right")
            case UISwipeGestureRecognizerDirection.down:
                print("Swiped down")
            case UISwipeGestureRecognizerDirection.left:
                print("Swiped left")
            case UISwipeGestureRecognizerDirection.up:
                print("Swiped up")
                
            default:
                break
            }
        }
    }
    
    private func setupView() {
        MainScrollView = UIScrollView(frame: view.bounds)
        MainScrollView.delegate = self
        MainScrollView.showsVerticalScrollIndicator = true
        MainScrollView.backgroundColor = .clear
        
        calendarFrame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height-110)
        let calendar = FSCalendar(frame: calendarFrame)
        calendar.dataSource = self
        calendar.delegate = self
        calendar.handleScopeGesture(UIPanGestureRecognizer(target: self, action: #selector(handlePan)))
        
//        calendar.backgroundColor = UIColor.Background(alpha: 1.0)
//        calendar.calendarHeaderView.backgroundColor = UIColor.Theme(alpha: 1.0)
        calendar.calendarWeekdayView.backgroundColor = UIColor.ThemeDark(alpha: 1.0)
        calendar.appearance.selectionColor = UIColor.ThemeDarkGreen(alpha: 1.0)
        calendar.appearance.weekdayTextColor = UIColor.white
        calendar.appearance.headerTitleColor = UIColor.ThemeDark(alpha: 1.0)//UIColor.white
        calendar.appearance.eventDefaultColor = UIColor.lightGray
        calendar.setScope(.week, animated: true)
        self.calendar = calendar
        
        view.addSubview(MainScrollView)
        view.addSubview(weekView)
        weekView.addSubview(calendar)
        view.addSubview(eventLabel)
        view.addSubview(classLabel)
        eventLabel.addSubview(eventIndicator)
        classLabel.addSubview(classIndicator)
        
        eventLabel.alpha = 1
        classLabel.alpha = 1
        
        let y = view.frame.height/3-40
        let height = view.frame.height-y
        
        eventLabel.frame = CGRect(x: view.frame.width-50-55, y: 5, width: 45, height: 13)
        classLabel.frame = CGRect(x: view.frame.width-55, y: 5, width: 45, height: 13)
        
        eventIndicator.frame = CGRect(x: 0, y: 0, width: 4, height: eventLabel.frame.height)
        classIndicator.frame = CGRect(x: 0, y: 0, width: 4, height: classLabel.frame.height)
        
        weekView.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height/3-40)
       
        MainScrollView.frame = CGRect(x: 0, y: y, width: view.frame.width, height: height-(navbarStatusHeight+tabBarHeight+bottomSpacing!+20))
        MainScrollView.contentSize = CGSize(width: view.frame.width, height: MainScrollView.frame.height)
    }
    
    private func setupHours(){
        var y = 10
        var rep = 1
        var i = 1
        let vertical = view.frame.height/3-40
        let height = view.frame.height-vertical
        
        repeat {
            repeat {
                
                let timeLabel: UILabel = {
                    let label = UILabel()
                    label.textColor = UIColor.lightGray
                    label.numberOfLines = 2
                    label.textAlignment = .right
                    label.font = label.font.withSize(10)
                    label.font = UIFont.boldSystemFont(ofSize: label.font.pointSize)
                    return label
                }()
                
                let boarder : UIView = {
                    let view = UIView()
                    view.backgroundColor = UIColor.Background(alpha: 1.0)
                    return view
                }()
                
                if rep == 1 {
                    if i == 12 {
                        timeLabel.text = "Noon"
                    } else {
                        timeLabel.text = "\(i) AM"
                    }
                } else {
                    
                    if i == 12 {
                        timeLabel.text = "\(i) AM"
                    } else {
                        timeLabel.text = "\(i) PM"
                    }
                }
                MainScrollView.addSubview(timeLabel)
                MainScrollView.addSubview(boarder)
                timeLabel.frame = CGRect(x: 5, y: y, width: 40, height: 20)
                boarder.frame = CGRect(x: Int(timeLabel.frame.maxX)+5, y: y+10, width: Int(view.frame.width)-60, height: 1)
                y = y + 40
                i = i + 1
            } while i <= 12
            rep = rep + 1
            i = 1
        } while rep <= 2
        
        if CGFloat(y) > height {
            MainScrollView.contentSize = CGSize(width: view.frame.width, height: CGFloat(y)+20)
        }
    }
    
    private func resetScroll() {
        for view in MainScrollView.subviews {
            view.removeFromSuperview()
        }
        self.setupHours()
    }

    func calendar(_ calendar: FSCalendar, didSelect date: Date, at monthPosition: FSCalendarMonthPosition) {
        let daySelected = self.dateFormatter2.string(from: date)
//        print(" setupSchedule ", self.dateEventFormat.string(from: date), ApiService().getDateTime(format: "MM/dd/yyyy"))
//        var scope: FSCalendarScope = .week
//        var frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
        
        setupEvent(date: self.dateEventFormat.string(from: date))
        setupSchedule(day : daySelected)
        
//        if (FileService().getDateTime(format: "MM/dd/yyyy") == self.dateEventFormat.string(from: date)) {
//            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
//                self.eventLabel.alpha = 1
//                self.classLabel.alpha = 1
//            }, completion: { (Bool) in })
//            setupEvent(date: self.dateEventFormat.string(from: date))
//            setupSchedule(day : daySelected)
//            dateSelected = self.dateEventFormat.string(from: date)
//            scope = .week
//            frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: view.frame.height/3-20)
//
//        } else {
//            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
//                self.eventLabel.alpha = 0
//                self.classLabel.alpha = 0
//            }, completion: { (Bool) in })
//            dateSelected = String()
//            scope = .month
//            frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
//        }
        
//        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
//            self.calendar.setScope(scope, animated: true)
//        }, completion: { (Bool) in
//            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
//                self.weekView.frame = frame
//            }, completion: { (Bool) in })
//        })
    }
    
    private func setupEvent(date: String){
        self.resetScroll()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "dd"
        
        for event in events {
            let eventTo = self.dateEventFormat.date(from: event.date_to)
            let eventFrom = self.dateEventFormat.date(from: event.date_from)
            let dateSelect = self.dateEventFormat.date(from: date)
            let to = Int(dateFormatter.string(from: eventTo!))
            let from = Int(dateFormatter.string(from: eventFrom!))
            let select = Int(dateFormatter.string(from: dateSelect!))
            //filter event depends on date
            if ((select! >= from!) && (select! <= to!)) {
                let titleLabel: UILabel = {
                    let label = UILabel()
                    label.textColor = UIColor.rgba(red: 53, green: 4, blue: 88, alpha: 1.0)
                    label.numberOfLines = 2
                    label.text = "\((event.title)!)"
                    label.font = label.font.withSize(10)
                    label.font = UIFont.boldSystemFont(ofSize: label.font.pointSize)
                    return label
                }()
                
                let descLabel: UITextView = {
                    let textView = UITextView()
                    textView.text = "\((event.desc)!) \n\n📍 \((event.location)!)"
                    textView.textColor = UIColor.rgba(red: 53, green: 4, blue: 88, alpha: 1.0)
                    textView.backgroundColor = UIColor.clear
                    textView.isScrollEnabled = false
                    textView.isEditable = false
                    textView.font = textView.font?.withSize(10)
                    return textView
                }()
                
                let scope : UIView = {
                    let view = UIView()
                    view.backgroundColor = UIColor.rgba(red: 112, green: 50, blue: 157, alpha: 0.3)
                    return view
                }()
                
                let scopeBoarder : UIView = {
                    let view = UIView()
                    view.backgroundColor = UIColor.rgba(red: 72, green: 12, blue: 115, alpha: 1.0)
                    return view
                }()
                
                MainScrollView.addSubview(scope)
                scope.addSubview(titleLabel)
                scope.addSubview(descLabel)
                scope.addSubview(scopeBoarder)
                
                scope.frame = CGRect(x: 48, y: 0, width: Int(view.frame.width)-58, height: Int(MainScrollView.contentSize.height))
                scope.addConstraintsFormat(format: "H:|-15-[v0]-15-|", views: titleLabel)
                scope.addConstraintsFormat(format: "H:|-15-[v0]-15-|", views: descLabel)
                scope.addConstraintsFormat(format: "V:|-5-[v0(15)][v1]-5-|", views: titleLabel, descLabel)
                scopeBoarder.frame = CGRect(x: 0, y: 0, width: 2, height: scope.frame.height)
                
            }
            
        }
    }
    
    private func setupSchedule(day : String) {
//        self.resetScroll()
//        if "\((ApiService().myID()["user_type"])!)" == "teacher" {
//            let schedule = (ApiService().myID()["schedule"])! as! [String : Any]
            
            if schedule.count != 0 {
                for dayOfWeek in schedule {
                    if dayOfWeek.key.capitalized == day {
                        let week = (dayOfWeek.value) as! [[String : Any]]
                        for weekday in week {
                            let start = "\((weekday["time_start"])!)"
                            let end = "\((weekday["time_end"])!)"
                            let index = start.index(of: ":")!
                            let indexEnd = end.index(of: ":")!
                            let hourStart = start.prefix(index.encodedOffset)
                            let hourEnd = end.prefix(indexEnd.encodedOffset)
                            let ampm = start.suffix(2)
                            var y = 10
                            var rep = 1
                            var i = 1
                            
        //                    print(" day : ", hourStart, hourEnd)
                            
                            repeat {
                                repeat {
                                    let titleLabel: UILabel = {
                                        let label = UILabel()
                                        label.textColor = UIColor.rgba(red: 131, green: 128, blue: 0, alpha: 1.0)
                                        label.numberOfLines = 2
                                        label.font = label.font.withSize(10)
                                        label.font = UIFont.boldSystemFont(ofSize: label.font.pointSize)
                                        return label
                                    }()
                                    
                                    let descLabel: UILabel = {
                                        let label = UILabel()
                                        label.textColor = UIColor.rgba(red: 131, green: 128, blue: 0, alpha: 1.0)
                                        label.numberOfLines = 2
                                        label.text = "\((weekday["subject_code"])!)"
                                        label.font = label.font.withSize(10)
                                        return label
                                    }()
                                    
                                    let scope : UIView = {
                                        let view = UIView()
                                        view.backgroundColor = UIColor.rgba(red: 212, green: 207, blue: 22, alpha: 0.3)
                                        return view
                                    }()
                                    
                                    let scopeBoarder : UIView = {
                                        let view = UIView()
                                        view.backgroundColor = UIColor.rgba(red: 171, green: 167, blue: 7, alpha: 1.0)
                                        return view
                                    }()
                                    
                                    if "\((weekday["subject_code"])!)" == "" {
                                        titleLabel.text = "Break / Vacant".uppercased()
                                    } else {
                                        titleLabel.text = "\((weekday["subject"])!)"
                                    }
                                    
                                    if rep == 1 {
                                        if i == 12 {
                                            if "\(i) PM" == "\(hourStart) \(ampm)"{
                                                MainScrollView.addSubview(scope)
                                                
                                                let rep =  2-(Int(hourEnd)!)
                                                scope.frame = CGRect(x: 52, y: y+12, width: Int(view.frame.width)-62, height: 37*rep)
                                                
                                            }
                                        } else {
                                            if "\(i) AM" == "\(hourStart) \(ampm)"{
                                                MainScrollView.addSubview(scope)
                                                
                                                let rep =  (Int(hourEnd)!)-(Int(hourStart)!)
                                                scope.frame = CGRect(x: 52, y: y+12, width: Int(view.frame.width)-62, height: 37*rep)
                                            }
                                        }
                                    } else {
                                        
                                        if i == 12 {
                                            if "\(i) AM" == "\(hourStart) \(ampm)"{
                                                MainScrollView.addSubview(scope)
                                                
                                                let rep =  2-(Int(hourEnd)!)
                                                scope.frame = CGRect(x: 52, y: y+12, width: Int(view.frame.width)-62, height: 37*rep)
                                            }
                                        } else {
                                            if "\(i) PM" == "\(hourStart) \(ampm)"{
                                                MainScrollView.addSubview(scope)
                                                
                                                
                                                let rep =  (Int(hourEnd)!)-(Int(hourStart)!)
                                                scope.frame = CGRect(x: 52, y: y+12, width: Int(view.frame.width)-62, height: 37*rep)
                                                
                                            }
                                        }
                                    }
                                    scope.addSubview(scopeBoarder)
                                    scope.addSubview(titleLabel)
                                    scope.addSubview(descLabel)
                                    
                                    scope.addConstraintsFormat(format: "H:|-15-[v0]-15-|", views: titleLabel)
                                    scope.addConstraintsFormat(format: "H:|-15-[v0]-15-|", views: descLabel)
                                    scope.addConstraintsFormat(format: "V:|-5-[v0(15)][v1(15)]", views: titleLabel, descLabel)
                                    scopeBoarder.frame = CGRect(x: 0, y: 0, width: 2, height: scope.frame.height)
                                    y = y + 40
                                    i = i + 1
                                } while i <= 12
                                rep = rep + 1
                                i = 1
                            } while rep <= 2
                        }
                    }
                }
            }
//        }
        
    }
  
    fileprivate lazy var dateFormatter2: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "EEEE"
        return formatter
    }()
    
    fileprivate lazy var dateEventFormat: DateFormatter = {
        let formatter = DateFormatter()
        formatter.dateFormat = "MM/dd/yyyy"
        return formatter
    }()
    
    func calendar(_ calendar: FSCalendar, numberOfEventsFor date: Date) -> Int {
//        let day = self.dateFormatter2.string(from: date)
        let eventday = self.dateEventFormat.string(from: date)
        //set classes days
//        if self.classDays.contains(day) {
//            return 1
//        }

        //set event days
        if self.eventDays.contains(eventday) {
            return 1
        }

        return 0
    }
    
    func showRange(between startDate: Date, and endDate: Date) {
        guard startDate < endDate else { return }
        
        let calendar = Calendar.current
        let calendarEndDate = calendar.startOfDay(for: endDate)
        
        var currentDate = calendar.startOfDay(for: startDate)
        
        while(currentDate <= calendarEndDate) {
            self.eventDays.append(self.dateEventFormat.string(from: currentDate))
            currentDate = Calendar.current.date(byAdding: .day, value: 1, to: currentDate)!
        }
    }
    
    private func watchEvents() {
        let events = try! manager.databaseNamed("events")
        self.getEvents()
        NotificationCenter.default.addObserver(forName: NSNotification.Name.cblDatabaseChange, object: events, queue: nil) {
            (notification) -> Void in
            if let changes = notification.userInfo!["changes"] as? [CBLDatabaseChange] {
                for change in changes {
                    print("announcement, revision ID '%@'", change.description)
                    self.getEvents()
                }
            }
        }
    }
    
    private func getEvents(){
        let database = try! manager.databaseNamed("events")
        let query = database.createAllDocumentsQuery()
        
        do {
            let result = try query.run()
            if result != nil {
                while let row = result.nextRow(){
                    let document = row.document?.properties
                    var event = Events()
                    event.author = "\((document!["author"])!)"
                    event.date = "\((document!["date"])!)"
                    event.date_from = "\((document!["date_from"])!)"
                    event.date_to = "\((document!["date_to"])!)"
                    event.title = "\((document!["title"])!)".capitalized
                    event.desc = "\((document!["desc"])!)"
                    event.location = "\((document!["location"])!)".capitalized
                    event.img = "\((document!["img"])!)"
                    event.b64 = "\((document!["b64"])!)"
                    event.type = "\((document!["type"])!)"
                    
                    self.events.append(event)
                    self.eventDaysFrom.append("\((document!["date_from"])!)")
                    self.eventDaysTo.append("\((document!["date_to"])!)")
                }
            }
            calendar.reloadData()
        } catch {
            print(" error getting announcements :", error)
        }
        
        //get days range of event date
        for (index, days) in eventDaysFrom.enumerated() {
            let dateTo = self.dateEventFormat.date(from: eventDaysTo[index])
            let dateFrom = self.dateEventFormat.date(from: days)
            let dateFormatter = DateFormatter()
            dateFormatter.dateFormat = "dd"
            let from = Int(dateFormatter.string(from: dateFrom!))
            let to = Int(dateFormatter.string(from: dateTo!))
            let range = Calendar.current.date(byAdding: .day, value: Int(to!-from!), to: dateFrom!)!
            showRange(between: dateFrom!, and: range)
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.white
        
        setupView()
        setupHours()
        setupEvent(date: FileService().getDateTime(format: "MM/dd/yyyy"))
        setupSchedule(day : FileService().getDateTime(format: "EEEE"))
        watchEvents()
    }
}

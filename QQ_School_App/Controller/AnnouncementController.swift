//
//  AnnouncementController.swift
//  QQ_School_App
//
//  Created by Abby Esteves on 02/03/2018.
//  Copyright © 2018 Abby Esteves. All rights reserved.
//

import UIKit
class AnnouncementController: UICollectionViewController, UICollectionViewDelegateFlowLayout {
    
    var cellId = "cellId"
    let manager = CBLManager.sharedInstance()
    var announcements = [Announcements]()
    var myAnnouncements = [Announcements]()
    let navbarStatusHeight = UIApplication.shared.statusBarFrame.height
    let bottomSpacing = UIApplication.shared.keyWindow?.safeAreaInsets.bottom
    let tabBarHeight = UITabBarController.init().tabBar.frame.height
    
    lazy var newsDetail: NewsDetail = {
        let launcher = NewsDetail()
        return launcher
    }()
    
    let cancelButton: UIButton = {
        let button = UIButton()
        button.backgroundColor = UIColor.rgba(red: 155, green: 229, blue: 233, alpha: 1.0)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 13)
        button.setTitleColor(UIColor.rgba(red: 94, green: 138, blue: 141, alpha: 1.0), for: .normal)
        button.setTitle("cancel".uppercased(), for: .normal)
        button.layer.cornerRadius = 5
        return button
    }()
    
    let addButton: UIButton = {
        let button = UIButton()
        button.backgroundColor = UIColor.ThemeDarkGreen(alpha: 1.0)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 13)
        button.setTitleColor(UIColor.white, for: .normal)
        button.setTitle("add".uppercased(), for: .normal)
        button.layer.cornerRadius = 5
        return button
    }()
    
    let blackView : UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(white: 0, alpha: 0.6)
        return view
    }()
    
    let addAnnouncementView : UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.layer.cornerRadius = 10
        return view
    }()
    
    let addImage: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "ic_add")?.withRenderingMode(.alwaysTemplate)
        imageView.tintColor = UIColor.white
        return imageView
    }()
    
    let addView : UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.ThemeDarkGreen(alpha: 1.0)
        view.layer.shadowColor = UIColor.gray.cgColor
        view.layer.shadowOpacity = 0.5
        view.layer.shadowOffset = CGSize(width: 0, height: 2)
        view.layer.shadowRadius = 3
        view.layer.cornerRadius = 25
        return view
    }()
    
    let titleText: UITextField = {
        let text = UITextField()
        text.layer.borderWidth = 1.0
        text.layer.borderColor = UIColor.lightGray.cgColor
        text.backgroundColor = .clear//UIColor.rgba(red: 255, green: 255, blue: 255, alpha: 0.5)
        text.textColor = UIColor.darkGray
        var placeholder = NSMutableAttributedString()
        placeholder = NSMutableAttributedString(attributedString: NSAttributedString(string: "Title", attributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 11.0), .foregroundColor: UIColor.lightGray]))
        text.setLeftPadding(space: 8)
        text.font = text.font?.withSize(11)
        text.attributedPlaceholder = placeholder
        text.layer.cornerRadius = 5
        return text
    }()
    
    let contentText: UITextView = {
        let textView = UITextView()
        textView.backgroundColor = UIColor.clear
        textView.textColor = UIColor.darkGray
        textView.layer.borderWidth = 1.0
        textView.layer.borderColor = UIColor.lightGray.cgColor
        textView.isEditable = true
        textView.isSelectable = true
        textView.isScrollEnabled = true
        textView.layer.cornerRadius = 5
        return textView
    }()
    
    let emptyLabel: UILabel = {
        let label = UILabel()
        label.text = "No Announcement"
        label.textColor = UIColor.lightGray
        label.numberOfLines = 2
        label.textAlignment = .center
        label.font = UIFont.boldSystemFont(ofSize: label.font.pointSize)
        label.font = label.font.withSize(12)
        label.alpha = 0.5
        return label
    }()
    
    let emptyImage: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.clipsToBounds = true
        imageView.backgroundColor = .clear
        imageView.layer.masksToBounds = true
        imageView.image = UIImage(named: "ic_empty")?.withRenderingMode(.alwaysTemplate)
        imageView.tintColor = UIColor.lightGray
        imageView.alpha = 0.2
        return imageView
    }()
    
    @objc func addAnnouncement() {
        if let window = UIApplication.shared.keyWindow {
            blackView.alpha = 0
            window.addSubview(blackView)
            window.addSubview(addAnnouncementView)
            addAnnouncementView.addSubview(cancelButton)
            addAnnouncementView.addSubview(addButton)
            addAnnouncementView.addSubview(titleText)
            addAnnouncementView.addSubview(contentText)
            
            let height = window.frame.height/3
            blackView.frame = CGRect(x: 0, y: 0, width: window.frame.width, height: window.frame.height)
            addAnnouncementView.frame = CGRect(x: window.frame.width/2, y: window.frame.height/2, width: 0, height: 0)
            addButton.frame = CGRect(x: 0, y: 0, width: 0, height: 0)
            cancelButton.frame = CGRect(x: 0, y: 0, width: 0, height: 0)
            titleText.frame = CGRect(x: 0, y: 0, width: 0, height: 0)
            contentText.frame = CGRect(x: 0, y: 0, width: 0, height: 0)
            
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                self.blackView.alpha = 1
                self.addAnnouncementView.frame = CGRect(x: 10, y: (self.view.frame.height/2)-(height/2), width: self.view.frame.width-20, height: height)
                self.addButton.frame = CGRect(x: self.addAnnouncementView.frame.maxX-70, y: height-40, width: 50, height: 30)
                self.cancelButton.frame = CGRect(x: self.addAnnouncementView.frame.maxX-90-60, y: height-40, width: 70, height: 30)
                self.titleText.frame = CGRect(x: 10, y: 10, width: self.addAnnouncementView.frame.width-20, height: 30)
                self.contentText.frame = CGRect(x: 10, y: self.titleText.frame.maxY+10, width: self.addAnnouncementView.frame.width-20, height: self.addAnnouncementView.frame.height-50-30-20)
            }, completion: { (Bool) in })
        }
    }
    
    @objc func add(){
        self.close()
        ApiService().createAnnouncement(title: "\((titleText.text)!)", desc: "\((contentText.text)!)", type: "announcement")
    }
    
    @objc func cancel(){
        self.close()
    }
    
    func close(){
        self.contentText.resignFirstResponder()
        self.titleText.resignFirstResponder()
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.blackView.alpha = 0
            self.addAnnouncementView.frame = CGRect(x: self.view.frame.width/2, y: self.view.frame.height/2, width: 0, height: 0)
            self.addButton.frame = CGRect(x: 0, y: 0, width: 0, height: 0)
            self.cancelButton.frame = CGRect(x:0, y: 0, width: 0, height: 0)
            self.titleText.frame = CGRect(x: 0, y: 0, width: 0, height: 0)
            self.contentText.frame = CGRect(x: 0, y: 0, width: 0, height: 0)
        }, completion: { (Bool) in })
    }
    
    private func watchAnnouncements() {
        let announcements = try! manager.databaseNamed("announcements")
        self.getAnnouncements()
        NotificationCenter.default.addObserver(forName: NSNotification.Name.cblDatabaseChange, object: announcements, queue: nil) {
            (notification) -> Void in
            if let changes = notification.userInfo!["changes"] as? [CBLDatabaseChange] {
                for change in changes {
                    print("announcement, revision ID '%@'", change.description)
                    self.getAnnouncements()
                }
            }
        }
    }
    
    private func getAnnouncements(){
        announcements = ApiService().announcementList().filter { $0.author != "\((ApiService().myID()["firstname"])!) \((ApiService().myID()["lastname"])!)" }
        myAnnouncements = ApiService().announcementList().filter { $0.author == "\((ApiService().myID()["firstname"])!) \((ApiService().myID()["lastname"])!)" }
        for mine in myAnnouncements {
            announcements.insert(mine, at: 0)
        }
        
        if announcements.count > 0 {
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                self.emptyLabel.alpha = 0
                self.emptyImage.alpha = 0
            }, completion:  { (Bool) in })
        }
        collectionView?.reloadData()
    }
    
    private func setupView(){
        collectionView?.scrollsToTop = true
        if "\((ApiService().myID()["user_type"])!)" == "teacher" {
            collectionView?.contentInset = UIEdgeInsetsMake(20, 0, 110, 0)
        } else if "\((ApiService().myID()["user_type"])!)" == "student" {
            collectionView?.contentInset = UIEdgeInsetsMake(20, 0, 20, 0)
        }
        collectionView?.backgroundColor = UIColor.Background(alpha: 1.0)
        collectionView?.register(AnnouncementsCell.self, forCellWithReuseIdentifier: cellId)
        collectionView?.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height)
        
        view.addSubview(emptyImage)
        view.addSubview(emptyLabel)
        
        emptyLabel.frame = CGRect(x: 10, y: view.frame.height/3, width: view.frame.width-20, height: 20)
        emptyImage.frame = CGRect(x: (view.frame.width/2)-((view.frame.width/3)/2), y: emptyLabel.frame.maxY, width: view.frame.width/3, height: view.frame.width/3)
        
        
        if "\((ApiService().myID()["user_type"])!)" == "teacher" {
            view.addSubview(addView)
            addView.addSubview(addImage)
            addView.frame = CGRect(x: view.frame.width-70, y: CGFloat((collectionView?.frame.height)!)-(navbarStatusHeight+bottomSpacing!+tabBarHeight+110), width: 50, height: 50)
            addImage.frame = CGRect(x: 15, y: 15, width: 20, height: 20)
        }
        
    }
    
    private func setupGestures(){
        addView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(addAnnouncement)))
        addButton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(add)))
        cancelButton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(cancel)))
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return announcements.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! AnnouncementsCell
        cell.backgroundColor = UIColor.white
        cell.layer.shadowColor = UIColor.gray.cgColor
        cell.layer.shadowOpacity = 0.5
        cell.layer.shadowOffset = CGSize(width: 0, height: 1)
        cell.layer.shadowRadius = 1
        cell.layer.cornerRadius = 3
        cell.announcement = announcements[indexPath.item]
        
        if cell.announcement?.author == "\((ApiService().myID()["firstname"])!) \((ApiService().myID()["lastname"])!)" {
            cell.backgroundColor = UIColor.Highlight(alpha: 0.3)
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width-20, height: 110)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let announcement = announcements[indexPath.item] as Announcements
//        print(" announcements ", announcement)
        newsDetail.showAnnouncement(news : announcement)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.Background(alpha: 1.0)
        
        setupView()
        setupGestures()
        watchAnnouncements()
        
    }
    
}

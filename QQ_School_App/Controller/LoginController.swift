//
//  LoginController.swift
//  QQ_School_App
//
//  Created by Abby Esteves on 01/03/2018.
//  Copyright © 2018 Abby Esteves. All rights reserved.
//

import UIKit
import AVFoundation
import SVGKit

class LoginController: UIViewController, UITextFieldDelegate, AVCaptureMetadataOutputObjectsDelegate, UIScrollViewDelegate {
    
    var captureSession: AVCaptureSession!
    var previewLayer: AVCaptureVideoPreviewLayer!
    var bucket = String()
    static var keyboardHeight = CGFloat()
    var activityIndicator : UIActivityIndicatorView = UIActivityIndicatorView()
    var MainScrollView = UIScrollView()
    
    let loginView: UIImageView = {
        let image = UIImageView()
        image.image = UIImage(named: "login_qq_bg")
        image.contentMode = .scaleAspectFill
        return image
    }()
    
    let gridView: UIView = {
        let view = UIView()
        view.backgroundColor = .clear
        return view
    }()
    
    let gridImageView: UIImageView = {
        let image = UIImageView()
        image.image = UIImage(named: "grid")?.withRenderingMode(.alwaysTemplate)
        image.tintColor = UIColor.white
        image.contentMode = .scaleAspectFill
        return image
    }()
    
    let loginButton: UIButton = {
        let button = UIButton()
        button.backgroundColor = UIColor.Theme(alpha: 1.0)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 13)
        button.setTitleColor(UIColor.white, for: .normal)
        button.setTitle("continue".uppercased(), for: .normal)
        return button
    }()
    
    let loginLabel : UILabel = {
        let label = UILabel()
        label.text = "Scan your QR code to log in your account"
        label.textColor = UIColor.white
        label.textAlignment = .center
        label.numberOfLines = 5
        label.font = label.font.withSize(12)
        return label
    }()
    
    
    let passwordText: UITextField = {
        let text = UITextField()
        text.borderStyle = .none
        text.backgroundColor = .white
        text.textColor = UIColor.darkGray
        text.font = .systemFont(ofSize: 13)
        var placeholder = NSMutableAttributedString()
        placeholder = NSMutableAttributedString(attributedString: NSAttributedString(string: "Enter Password...", attributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 13.0), .foregroundColor: UIColor.gray]))
        text.setLeftPadding(space: 8)
        text.attributedPlaceholder = placeholder
        text.isSecureTextEntry = true
        return text
    }()
    
    //mock configuration
    let mockConfig = [
        "mission": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
        "address_province": "Bulacan",
        "email": "qqschoo/@yahoo.com",
        "alias": "QIS",
        "school_code": "QIS",
        "operation_hours": "6:00 AM-6:00 PM",
        "school_name": "QQ International School",
        "class_hrs_start": "7:00",
        "class_hrs_end": "17:00",
        "vision": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.",
        "bucket_name": "http://192.168.1.96:4984/qqschool/",
        "contact": "09123232121",
        "about": "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.",
        "address_street": "0158 BS Aquino Ave, Tangos",
        "address_zipcode": "3006",
        "login_status": "1",
        "address_municipality": "Baliuag"
    ]
    
    // for simulator login

//    var myID = [
//        "d": "ST1530165078",
//        "i": "ST1530165078",
//        "p": "$2a$10$s.0XMr6CBHHqHTPqbwmMCeacdrJ7bU0DDZfgF0INQHZQw2iOjmhV2",//"$2a$10$WZYOmsCwjoRIv5oAMcjAne.YFJ1sdpxHfZz9IweGQ5ZAYXB.tqJrO",//"$2a$10$GfUCgOkPwNxRGqyCwo7t7uayTICIIpmQWcMVm0.sa3zvS5NgxSp0q"
//        "t": "2",
//        "s": "QIS",
//        "n": "T25lLFN0dWRlbnQgTnVtYmVy",
//        "g": "MQ==",
//        "sct": "MQ==",
//        "gen":"bWFsZQ=="
//    ] as [String : Any]
    
//    var myID = [
//        "d": "TC1530165019",
//        "i": "TC1530165019",
//        "p": "$2a$10$GfUCgOkPwNxRGqyCwo7t7uayTICIIpmQWcMVm0.sa3zvS5NgxSp0q",
//        "t": "1",
//        "s": "QIS",
//        "n": "T25lLFN0dWRlbnQgTnVtYmVy",
//        "g": "MQ==",
//        "sct": "MQ==",
//        "gen":"bWFsZQ=="
//        ] as [String : Any]
    
    var myID = [
        "d": "",
        "i": "",
        "p": "",
        "t": "",
        "s": "",
        "n": "",
        "g": "",
        "gen": "",
        "sct": ""
    ] as [String : Any]
    
    //@objc
    
    @objc private func exitKeyboard() {
        self.validate()
    }
    
    @objc func restart(){
//        tries = 0
        myID = [
            "d": "",
            "i": "",
            "p": "",
            "t": "",
            "s": "",
            "n": "",
            "g": "",
            "gen": "",
            "sct": ""
        ] as [String : Any]
        loginLabel.removeFromSuperview()
        MainScrollView.addSubview(loginLabel)
        loginLabel.frame = CGRect(x: 20, y: previewLayer.frame.maxY+10, width: view.frame.width-40, height: 20)
        passwordText.resignFirstResponder()
        gridView.gestureRecognizers?.removeAll()
        passwordText.text = ""
        self.activityIndicator.stopAnimating()
        gridImageView.image = UIImage(named: "grid")?.withRenderingMode(.alwaysTemplate)
        gridImageView.tintColor = UIColor.white
        
        self.captureSession.startRunning()
        
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.loginLabel.text = "Scan your QR code to log in your account"
            self.loginLabel.textColor = .white
            self.loginLabel.frame = CGRect(x: 20, y: self.previewLayer.frame.maxY+10, width: self.view.frame.width-40, height: 20)
            self.passwordText.alpha = 0
            self.loginButton.alpha = 0
        }, completion: { (Bool) in
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                self.loginLabel.alpha = 1
            }, completion: { (Bool) in })
        })
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if keyboardSize.height != 0.0 {
                LoginController.keyboardHeight = keyboardSize.height
            }
            self.MainScrollView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.MainScrollView.frame.height-LoginController.keyboardHeight)
            //scroll to bottom
            let bottomOffset = CGPoint(x : 0, y : MainScrollView.contentSize.height-MainScrollView.bounds.size.height+MainScrollView.contentInset.bottom)
            MainScrollView.setContentOffset(bottomOffset, animated: false)
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        self.MainScrollView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
    }
    
    
    //func
    func validate() {
        if passwordText.text == "" {
            passwordText.becomeFirstResponder()
        } else {
            gridView.gestureRecognizers?.removeAll()
            passwordText.resignFirstResponder()
            activityIndicator.startAnimating()
            //        self.goToHomeController()
            
            
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                self.loginLabel.text = "Verifying QR code and Password..."
                self.loginLabel.textColor = UIColor.white
                self.passwordText.alpha = 0
                self.loginButton.alpha = 0
            }, completion: { (Bool) in
//                self.verifyPassword(hash: "\((self.myID["p"])!)", passwordInput: "\((self.passwordText.text)!)")
                // ONE device login
                self.oneDeviceLogin(parameters: "hash=\((self.myID["p"])!)&password=\((self.passwordText.text)!)&user_type=\((self.myID["t"])!)&qq_id=\((self.myID["i"])!)&school_code=\((self.myID["s"])!)")
            })
        }
    }
    
    private func goToHomeController(){
        ApiService().setupReplications()
        let homeTabBarController = HomeTabBarController()
        self.navigationController?.pushViewController(homeTabBarController, animated: true)
    }
    
//    private func goToHomeController(){
//        ApiService().setupReplications()
//        let homeController = HomeController()
//        self.navigationController?.pushViewController(homeController, animated: true)
//    }
    
    private func goToTeacherHomeController(){
        ApiService().setupReplications()
        let teacherHomeController = TeacherHomeController()
        self.navigationController?.pushViewController(teacherHomeController, animated: true)
    }
    
    private func attribute(text : UITextField, label: UILabel, boarder: UIView){
        text.resignFirstResponder()
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            text.textColor = UIColor.white
            label.textColor = UIColor.white
            boarder.backgroundColor = UIColor.white
            if text.text!.count == 0 {
                label.font = label.font.withSize(12)
                label.frame = CGRect(x: 10, y: 5, width: self.view.frame.width-20, height: 20)
            }
        }, completion:  nil)
    }
    
    private func selected(text : UITextField, label: UILabel, boarder: UIView) {
        text.becomeFirstResponder()
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            label.font = label.font.withSize(10)
            label.textColor = UIColor.rgba(red: 174, green: 226, blue: 241, alpha: 1.0)
            label.frame = CGRect(x: 0, y: -15, width: self.view.frame.width-20, height: 20)
            text.textColor = UIColor.rgba(red: 174, green: 226, blue: 241, alpha: 1.0)
            boarder.backgroundColor = UIColor.rgba(red: 174, green: 226, blue: 241, alpha: 1.0)
        }, completion:  { (Bool) in })
    }
    
    private func setupView() {
        
        captureSession = AVCaptureSession()
        
        guard let videoCaptureDevice = AVCaptureDevice.default(for: .video) else { return }
        let videoInput: AVCaptureDeviceInput
        
        do {
            videoInput = try AVCaptureDeviceInput(device: videoCaptureDevice)
        } catch {
            return
        }
        
        if (captureSession.canAddInput(videoInput)) {
            captureSession.addInput(videoInput)
        } else {
            failed()
            return
        }
        
        let metadataOutput = AVCaptureMetadataOutput()
        
        if (captureSession.canAddOutput(metadataOutput)) {
            captureSession.addOutput(metadataOutput)
            
            metadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
            metadataOutput.metadataObjectTypes = [.qr]
        } else {
            failed()
            return
        }
        
        activityIndicator.center = self.view.center
        activityIndicator.hidesWhenStopped = true
        activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.white
        
        previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        previewLayer.videoGravity = .resizeAspectFill
        
        MainScrollView = UIScrollView(frame: view.bounds)
        MainScrollView.delegate = self
        MainScrollView.showsVerticalScrollIndicator = true
        MainScrollView.backgroundColor = UIColor.Background(alpha: 1.0)
        MainScrollView.isScrollEnabled = false
        
        view.addSubview(MainScrollView)
        MainScrollView.addSubview(loginView)
        MainScrollView.layer.addSublayer(previewLayer)
        MainScrollView.addSubview(loginLabel)
        MainScrollView.addSubview(gridView)
        gridView.addSubview(gridImageView)
        MainScrollView.addSubview(activityIndicator)
        
        MainScrollView.addSubview(passwordText)
        MainScrollView.addSubview(loginButton)
        
        passwordText.alpha = 0
        loginButton.alpha = 0
        
    }
    
    private func setupConstraints() {
        let size = view.frame.width-60
        let sizeGrid = view.frame.width-120
        
        MainScrollView.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height)
        MainScrollView.contentSize = CGSize(width: view.frame.width, height: view.frame.height)
        
        loginView.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height)
        
        previewLayer.frame = CGRect(x: 30, y: (view.frame.height/2)-(size/2), width: size, height: size)
        gridView.frame = CGRect(x: 60, y: (view.frame.height/2)-(sizeGrid/2), width: sizeGrid, height: sizeGrid)
        gridImageView.frame = CGRect(x: 0, y: 0, width: gridView.frame.width, height: gridView.frame.height)
        loginLabel.frame = CGRect(x: 20, y: previewLayer.frame.maxY+10, width: view.frame.width-40, height: 20)
        activityIndicator.frame = CGRect(x: 20, y: previewLayer.frame.maxY+40, width: view.frame.width-40, height: 20)
        
        passwordText.frame = CGRect(x: 30, y: loginLabel.frame.maxY+20, width: previewLayer.frame.width, height: 50)
        loginButton.frame = CGRect(x: view.frame.width/2-(((view.frame.width/2)-60)/2), y: passwordText.frame.maxY+10, width: (view.frame.width/2)-60, height: 50)
        
        captureSession.startRunning()
    }
    
    private func setupGestures() {
        loginButton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(exitKeyboard)))
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.validate()
        return false
    }
    
    func failed() {
        let ac = UIAlertController(title: "Scanning not supported", message: "Your device does not support scanning a code from an item. Please use a device with a camera.", preferredStyle: .alert)
        ac.addAction(UIAlertAction(title: "OK", style: .default))
        present(ac, animated: true)
        captureSession = nil
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        ApiService().deleteDb()
        if (captureSession?.isRunning == false) {
            captureSession.startRunning()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        
        if (captureSession?.isRunning == true) {
            captureSession.stopRunning()
        }
    }
    
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        captureSession.stopRunning()
        
        if let metadataObject = metadataObjects.first {
            guard let readableObject = metadataObject as? AVMetadataMachineReadableCodeObject else { return }
            guard let stringValue = readableObject.stringValue else { return }
            AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
            found(code: stringValue)
        }
        
        dismiss(animated: true)
    }
    
    func found(code: String) {
        print("Information generated : \n \(code)")
        
        //generate
        let string = "\(code)"
        let data = string.data(using: .utf8)!
        
        do {
            let jsonArray = try JSONSerialization.jsonObject(with: data, options : .allowFragments) as? [String:AnyObject]
            if jsonArray?["i"] != nil && jsonArray?["p"] != nil && jsonArray?["d"] != nil && jsonArray?["t"] != nil && jsonArray?["s"] != nil && jsonArray?["g"] != nil && jsonArray?["gen"] != nil && jsonArray?["sct"] != nil {
                myID["p"] = (jsonArray!["p"])!
                myID["d"] = (jsonArray!["d"])!
                myID["i"] = (jsonArray!["i"])!
                myID["t"] = (jsonArray!["t"])!
                myID["s"] = (jsonArray!["s"])!
                myID["g"] = (jsonArray!["g"])!
                myID["gen"] = (jsonArray!["gen"])!
                myID["sct"] = (jsonArray!["sct"])!
                
            } else {
//                self.restart()
                DispatchQueue.main.async(execute: {
                    self.failedLoggin(message: "Opps.. Invalid QR scanned. \n Tap QR to re-scan code")
                })
            }
            
//            print("Information generated : \n \(code)", jsonArray , myID)
        } catch {
            print(error)
        }
        
        self.captureSession.stopRunning()
        self.gridImageView.image = ApiService().generateQRCode(from: "\(code)")
        
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.loginLabel.text = "Tap QR to re-scan code"
            self.passwordText.alpha = 1
            self.loginButton.alpha = 1
        }, completion: { (Bool) in
            self.gridView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.restart)))
        })
    }
    
//    private func verifyPassword(hash : String, passwordInput : String){
////        print(" test vrify : \(passwordInput) \n \(hash)")
//        if let compare = BCryptSwift.verifyPassword("\(passwordInput)", matchesHash: hash) {
//            if compare {
//                print("The phrase was a SUCCESS! ")
//                UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
//                    self.loginLabel.text = "Fetching data..."
//                }, completion: { (Bool) in
//
//                    // ONE device login
//                    self.oneDeviceLogin(parameters: "hash=\((self.myID["p"])!)&password=\((self.passwordText.text)!)&user_type=\((self.myID["t"])!)&qq_id=\((self.myID["i"])!)&school_code=\((self.myID["s"])!)")
//
//                    // MULTIPLE device log in
////                    self.startReplicationLogin(docuId : "\((self.myID["d"])!)")
//                })
//
//            } else {
//                print("Compare phrase does NOT match hashed value")
//                self.failedPass(message: "Opps.. Password does not match. \n Re-enter password or Tap QR to re-scan code")
//            }
//        } else {
//            print("Compare hash generation failed")
//            self.loginLabel.text = "Opps.. QR seems to be broken. Contact your dministrator \n Tap QR to re-scan code"
//            self.loginLabel.textColor = UIColor.Alert(alpha: 1.0)
//            self.loginLabel.frame = CGRect(x: 20, y: previewLayer.frame.maxY+10, width: view.frame.width-40, height: 50)
//            self.activityIndicator.stopAnimating()
//            self.gridView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.restart)))
//        }
//
//    }
    
    private func failedPass(message : String){
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.loginLabel.text = message
            self.passwordText.text = ""
            self.passwordText.alpha = 1
            self.loginButton.alpha = 1
            self.loginLabel.textColor = UIColor.Alert(alpha: 1.0)
            self.loginLabel.frame = CGRect(x: 20, y: self.previewLayer.frame.maxY, width: self.view.frame.width-40, height: 50)
            self.activityIndicator.stopAnimating()
        }, completion: { (Bool) in
            self.gridView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.restart)))
        })
    }
    
    private func failedLoggin(message : String){
        print(" failedLoggin ", message)
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.passwordText.alpha = 0
            self.loginButton.alpha = 0
            self.loginLabel.text = message
            self.loginLabel.textColor = UIColor.Alert(alpha: 1.0)
            self.loginLabel.frame = CGRect(x: 20, y: self.previewLayer.frame.maxY+10, width: self.view.frame.width-40, height: 50)
            self.activityIndicator.stopAnimating()
        }, completion: { (Bool) in
            self.gridView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.restart)))
        })
    }
    
    func oneDeviceLogin(parameters : String) {
//        print(" Information parameters : ", parameters)
//        guard let addurl = URL(string: "http://192.168.1.114/android_api/ninang-test.php") else { return }
        var request = URLRequest(url: ApiService().urlConfig)//ApiService().urlConfig
        request.httpMethod = "POST"
        request.timeoutInterval = 5
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.httpBody = parameters.data(using: String.Encoding.utf8)

        let session = URLSession.shared
        session.dataTask(with: request) { (data, response, error) in
            if let response = response {
                print(" oneDeviceLogin repsonse",response)
            }

            if let data = data {
                let responseString = String(data: data, encoding: .utf8)!
                print(" data reponse : oneDeviceLogin", responseString)
//                if responseString != "" {
                if responseString == "Data does not exist!" {
                    DispatchQueue.main.async(execute: {
                        self.failedLoggin(message: "Data does not exist. Please contact your administrator. \n or Tap QR to re-scan code")
                    })
                } else if responseString == "You can only log in your account once. Please contact your administrator." {
                    DispatchQueue.main.async(execute: {
                        self.failedLoggin(message: "\(responseString) \n or Tap QR to re-scan code")
                    })
                } else if responseString == "Incorrect password!" {
                    DispatchQueue.main.async(execute: {
                        self.failedPass(message: "Opps.. Password does not match. \n Re-enter password or Tap QR to re-scan code")
                    })
                } else if responseString == "Maximum number of guardians is reached!" {
                    DispatchQueue.main.async(execute: {
                        self.failedPass(message: "Opps.. Maximum number of guardians is reached. Please contact your administrator. \n or Tap QR to re-scan code")
                    })
                } else {
                    do {
                        let json = try JSONSerialization.jsonObject(with: data, options: []) as! [String: Any]
//                        print(" data reponse : oneDeviceLogin", json)
                        if "\((json["login_status"])!)" == "success" {
                            DispatchQueue.main.async(execute: {
                                self.startReplicationLogin(docuId : "\((self.myID["d"])!)", configuration: json)
                            })
                        } else {
                            DispatchQueue.main.async(execute: {
                                self.failedLoggin(message: "Cannot connect at this moment. Check your internet connectivity \n or Tap QR to re-scan code")
                            })
                        }
                        
                    } catch {
                        print(" error do oneDeviceLogin", error)
                        DispatchQueue.main.async(execute: {
                            self.failedLoggin(message: "Cannot connect at this moment. Check your internet connectivity \n or Tap QR to re-scan code")
                        })
                    }
                    
                }
                
            }
            
            if let error = error {
                print(" error catch responseString ", error)
                DispatchQueue.main.async(execute: {
                    self.failedLoggin(message: "\(error.localizedDescription.capitalized) \n Tap QR to re-scan code")
                    self.loginButton.alpha = 1
                })
            }
        }.resume()
    }
    
    private func set_is_logged_in(parameters : String){
//        print(" data reponse : ", parameters)
        var request = URLRequest(url: ApiService().updateStatus! )
        request.httpMethod = "POST"
        request.timeoutInterval = 5
        request.setValue("application/x-www-form-urlencoded", forHTTPHeaderField: "Content-Type")
        request.httpBody = parameters.data(using: String.Encoding.utf8)
        
        let session = URLSession.shared
        session.dataTask(with: request) { (data, response, error) in
            if let response = response {
                print(response)
            }
            
            if let data = data {
                let responseString = String(data: data, encoding: .utf8)!
//                print(" data reponse set_is_logged_in : ", responseString)
                if responseString == "success" {
                    DispatchQueue.main.async(execute: {
//                        self.failedLoggin(message: "Data does not exist. Please contact your administrator. \n or Tap QR to re-scan code")
                    })
                } else {
                    do {
                        let json = try JSONSerialization.jsonObject(with: data, options: []) as! [String: Any]
//                        print(" data reponse : ", json)
                        if "\((json["error_msg"])!)" == "QQ ID or user type is missing!" {
                            DispatchQueue.main.async(execute: {
                                self.failedLoggin(message: "QQ ID or user type is missing. Contact your administrator. \n or Tap QR to re-scan code")
                            })
                        } else {
                            DispatchQueue.main.async(execute: {
                                self.failedLoggin(message: "Cannot connect at this moment. Check your internet connectivity \n or Tap QR to re-scan code")
                            })
                        }
                        
                    } catch {
                        print(" error do ", error)
                        DispatchQueue.main.async(execute: {
                            self.failedLoggin(message: "Cannot connect at this moment. Check your internet connectivity \n or Tap QR to re-scan code")
                        })
                    }
                    
                }
                
            }
            
            if let error = error {
                print(" error catch responseString ", error)
                DispatchQueue.main.async(execute: {
                    self.failedLoggin(message: "\(error.localizedDescription.capitalized) \n Tap QR to re-scan code")
                    self.loginButton.alpha = 1
                })
            }
        }.resume()
    }

    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    func startReplicationLogin(docuId : String, configuration : [String:Any]){
        let channel = ["USER_\((myID["i"])!)"]
        let url = URL(string: "\((configuration["bucket_name"])!)") //URL(string: "http://212.237.51.226:4984/qqschool/")
        let manager = CBLManager.sharedInstance()
        let database = try! manager.databaseNamed("mydata")
        
        self.checkURL(urlString : "\((configuration["bucket_name"])!)")
        
        let push = database.createPushReplication(url!)
        let pull = database.createPullReplication(url!)
        pull.channels = channel
        push.start()
        pull.start()
        
        NotificationCenter.default.addObserver(forName: NSNotification.Name.cblReplicationChange, object: pull, queue: nil) {
            (notification) -> Void in
            //determins if replication is over
            let active = pull.status != CBLReplicationStatus.active
            print(" cblReplicationChange ", active)
            if active {
                self.getDocumentUser(docuId : "\((self.myID["d"])!)", configuration: configuration)
            }
        }
    }
    
    func checkURL(urlString : String) {
        let url = URL(string: urlString)
        var request = URLRequest(url: url!)
        request.timeoutInterval = 5
        let session = URLSession.shared
        session.dataTask(with: request) { (data, response, error) in
            if let response = response {
                print(" startReplicationLogin repsone ",response)
            }
            if let error = error {
                print(" error catch startReplicationLogin ", error)
                DispatchQueue.main.async(execute: {
                    self.failedLoggin(message: "\(error.localizedDescription.capitalized) \n Tap QR to re-scan code")
                    self.loginButton.alpha = 1
                })
            }
        }.resume()
    }
    
    func getDocumentUser(docuId : String, configuration: [String:Any]){
        let manager = CBLManager.sharedInstance()
        let database = try! manager.databaseNamed("mydata")
        let doc = database.document(withID: docuId)!
        var properties = doc.properties
        
//        print(" getDocumentUser ", database.documentCount, docuId, doc, properties)
        
        if properties != nil {
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                // set status to logged in
                self.loginLabel.text = "Saving data..."
                self.loginLabel.textColor = .white
                let user_type = "\((self.myID["t"])!)" == "1" ? "teachers" : "students"
                
                self.set_is_logged_in(parameters: "qq_id=\((self.myID["i"])!)&user_type=\(user_type)")
            }, completion: { (Bool) in
                self.updateDocumentStatus(object: properties!, configuration: configuration)
            })
            
        } else {
            do {
                try database.delete()
            } catch {
                print("Can't save document in database")
            }
            self.failedLoggin(message: "Data does not exist. Please contact your administrator. \n or Tap QR to re-scan code")
        }
    }
    
    func updateDocumentStatus(object : [String : Any], configuration: [String:Any]) {
        let manager = CBLManager.sharedInstance()
        let database = try! manager.databaseNamed("mydata")
        let document = database.document(withID: "\((self.myID["d"])!)")!
        let configurationDocu = database.document(withID: "configuration")!
        //school configuration
        let config = [
            "mission": "\((configuration["mission"])!)",
            "address_province": "\((configuration["address_province"])!)",
            "email": "\((configuration["email"])!)",
            "alias": "\((configuration["alias"])!)",
            "school_code": "\((configuration["school_code"])!)",
            "operation_hours": "\((configuration["operation_hours"])!)",
            "school_name": "\((configuration["school_name"])!)",
            "class_hrs_start": "\((configuration["class_hrs_start"])!)",
            "class_hrs_end": "\((configuration["class_hrs_end"])!)",
            "vision": "\((configuration["vision"])!)",
            "bucket_name": "\((configuration["bucket_name"])!)",
            "contact": "\((configuration["contact"])!)",
            "about": "\((configuration["about"])!)",
            "address_street": "\((configuration["address_street"])!)",
            "address_zipcode": "\((configuration["address_zipcode"])!)",
            "login_status": "\((configuration["login_status"])!)",
            "address_municipality": "\((configuration["address_municipality"])!)"
        ]
        // update status
        var property = document.properties
        property!["status"] = "1"
        
        do {
            try document.putProperties(property!)
            try configurationDocu.putProperties(config)
//            self.saveMyData(object: property!, configuration: configuration)
            
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                self.loginLabel.text = "Almost done..."
            }, completion: { (Bool) in
                self.saveQR()
            })
        } catch {
            print(" change status failed! ", error)
        }
    }
    
    func saveQR(){
        let manager = CBLManager.sharedInstance()
        let database = try! manager.databaseNamed("mydata")
        let document = database.document(withID: "myqr")!
        
        do {
            try document.putProperties(myID)
            
            ApiService().createDocChat()
            activityIndicator.stopAnimating()
            self.loginLabel.removeFromSuperview()
            if "\((self.myID["t"])!)" == "3"{
                let profileController = ProfileController()
                navigationController?.pushViewController(profileController, animated: true)
                profileController.navigationItem.title = ""

                activityIndicator.stopAnimating()
            }
//            else if "\((self.myID["t"])!)" == "2"{
//                self.goToHomeController()
//            }
            else {
                self.goToHomeController()
//                self.goToTeacherHomeController()
            }
        } catch {
            print("Can't save document in database")
            return
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        UIApplication.shared.isStatusBarHidden = true
        self.navigationController?.isNavigationBarHidden = true
        view.backgroundColor = UIColor.Background(alpha: 1.0)
        
        passwordText.delegate = self
        
        setupView()
        setupConstraints()
        setupGestures()
        
        // for simulator login
//        startReplicationLogin(docuId : "\((self.myID["d"])!)", configuration: mockConfig)
        
//        oneDeviceLogin(parameters: "hash=$2a$10$s.0XMr6CBHHqHTPqbwmMCeacdrJ7bU0DDZfgF0INQHZQw2iOjmhV2&password=student&user_type=2&qq_id=ST1530165078&school_code=QIS")
        
//        oneDeviceLogin(parameters: "password=teacher&qq_id=TC1530165019")

        
        NotificationCenter.default.addObserver(self, selector: #selector(LoginController.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(LoginController.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
}

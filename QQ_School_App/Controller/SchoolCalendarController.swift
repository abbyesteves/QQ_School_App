//
//  SchoolCalendarController.swift
//  QQ_School_App
//
//  Created by Abby Esteves on 02/03/2018.
//  Copyright © 2018 Abby Esteves. All rights reserved.
//

import UIKit
import FSCalendar

class SchoolCalendarController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.Background(alpha: 1.0)
        
        let calendar = CalendarView(frame: CGRectMake(0, 0, CGRectGetWidth(view.frame), 320))
        view.addSubview(calendar)
    }
}

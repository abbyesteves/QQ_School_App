//
//  NewsDetail.swift
//  QQ_School_App
//
//  Created by Abby Esteves on 22/03/2018.
//  Copyright © 2018 Abby Esteves. All rights reserved.
//

import UIKit
class NewsDetail: UIViewController, UIScrollViewDelegate {
    
    let mainView : UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.rgba(red: 245, green: 245, blue: 255, alpha: 1.0)
        return view
    }()
    
    var MainScrollView = UIScrollView()
    
    let thumbnailView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.backgroundColor = UIColor.lightGray
        imageView.layer.masksToBounds = true
        imageView.image = UIImage(named: "news")
        return imageView
    }()
    
    let QRView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.clipsToBounds = true
        imageView.backgroundColor = .white
        imageView.layer.masksToBounds = true
        return imageView
    }()
    
    let backView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.backgroundColor = .clear
        imageView.layer.masksToBounds = true
        imageView.image = UIImage(named: "ic_back")?.withRenderingMode(.alwaysTemplate)as UIImage?
        imageView.tintColor = UIColor.white
        return imageView
    }()
    
  
    let titleLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.darkGray
        label.numberOfLines = 2
        label.font = UIFont.boldSystemFont(ofSize: label.font.pointSize)
        label.font = label.font.withSize(15)
        return label
    }()
    
    let dateAuthorLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.lightGray
        label.numberOfLines = 2
        label.font = label.font.withSize(11)
        return label
    }()
    
    let contextLabel: UITextView = {
        let textView = UITextView()
        textView.textColor = UIColor.gray
        textView.backgroundColor = UIColor.clear
        textView.isScrollEnabled = false
        textView.isEditable = false
        textView.font = textView.font?.withSize(11)
        return textView
    }()
    
    @objc func hideProfile(){
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.backView.alpha = 0
        }, completion: { (Bool) in
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                self.mainView.frame = CGRect(x: 0, y: self.view.frame.height, width: self.view.frame.width, height: self.view.frame.height)
            }, completion: { (Bool) in })
        })
        
    }
    
    @objc func scan(){
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.backView.alpha = 0
        }, completion: { (Bool) in
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                self.mainView.frame = CGRect(x: 0, y: self.view.frame.height, width: self.view.frame.width, height: self.view.frame.height)
            }, completion: { (Bool) in
                ScanQRLauncher().setup()
            })
        })
    }
    
    func showProfile(news : News){

        MainScrollView = UIScrollView(frame: view.bounds)
        MainScrollView.delegate = self
        MainScrollView.showsVerticalScrollIndicator = true
        MainScrollView.backgroundColor = UIColor.Background(alpha: 1.0)
        
        let size = CGSize(width: self.view.frame.width-20, height: self.view.frame.height)
        let options = NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin)
        let estimatedFrameTitle = NSString(string: "\((news.title)!)").boundingRect(with: size, options: options, attributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 15)], context: nil)
        
        let estimatedFrameContext = NSString(string: "\((news.desc)!)").boundingRect(with: size, options: options, attributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 12)], context: nil)
        
        titleLabel.text = (news.title)!
        dateAuthorLabel.text = "\((news.author)!) • \(FileService().epochConvertString(date: (news.date)!, format: "MMMM dd,yyyy"))"
        contextLabel.text = news.desc
        let image = (news.b64)!
        if image != "" {
            thumbnailView.backgroundColor = UIColor.ThemeDark(alpha: 1.0)
            thumbnailView.image = UIView.convertBase64ToImage(imageString: "\(image)")
        } else {
            thumbnailView.backgroundColor = .clear
            thumbnailView.image = UIImage(named: "news1")
        }
        
        if let window = UIApplication.shared.keyWindow {
            backView.alpha = 0
            
            window.addSubview(mainView)
            window.addSubview(MainScrollView)
            window.addSubview(backView)
            
            QRView.image = ApiService().generateQRCode(from: "\(ApiService().myQR())")
            
            mainView.addSubview(thumbnailView)
            mainView.addSubview(MainScrollView)
            MainScrollView.addSubview(titleLabel)
            MainScrollView.addSubview(dateAuthorLabel)
            MainScrollView.addSubview(contextLabel)
            
            mainView.frame = CGRect(x: 0, y: window.frame.height, width: window.frame.width, height: window.frame.height)
            thumbnailView.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height/3)
            backView.frame = CGRect(x: 10, y: 25, width: 25, height: 25)
            titleLabel.frame = CGRect(x: 10, y: 10, width: view.frame.width-20, height: estimatedFrameTitle.height)
            dateAuthorLabel.frame = CGRect(x: 15, y: titleLabel.frame.maxY+10, width: view.frame.width-20, height: 20)
//            let height = thumbnailView.frame.height + titleLabel.frame.height + dateAuthorLabel.frame.height + 60.0
            contextLabel.frame = CGRect(x: 10, y: dateAuthorLabel.frame.maxY+10, width: view.frame.width-20, height: estimatedFrameContext.height+10)
            
            MainScrollView.frame = CGRect(x: 0, y: thumbnailView.frame.maxY, width: view.frame.width, height: view.frame.height-(view.frame.height/3))
            MainScrollView.contentSize = CGSize(width: view.frame.width, height: estimatedFrameTitle.height + 20 + estimatedFrameContext.height + 40)
            
            mainView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(hideProfile)))
            
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                self.mainView.frame = CGRect(x: 0, y: 0, width: window.frame.width, height: window.frame.height)
            }, completion: { (Bool) in
                UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                    self.backView.alpha = 1
                }, completion: { (Bool) in })
            })
        }
    }
    
    func showAnnouncement(news : Announcements){
        MainScrollView = UIScrollView(frame: view.bounds)
        MainScrollView.delegate = self
        MainScrollView.showsVerticalScrollIndicator = true
        MainScrollView.backgroundColor = UIColor.Background(alpha: 1.0)
        
        let size = CGSize(width: self.view.frame.width-20, height: self.view.frame.height)
        let options = NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin)
        let estimatedFrameTitle = NSString(string: "\((news.title)!)").boundingRect(with: size, options: options, attributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 15)], context: nil)
        
        let estimatedFrameContext = NSString(string: "\((news.description)!)").boundingRect(with: size, options: options, attributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 12)], context: nil)
        
        titleLabel.text = (news.title)!
        dateAuthorLabel.text = "\((news.author)!) • \(FileService().epochConvertString(date: (news.date)!, format: "MMMM dd,yyyy"))"
        contextLabel.text = news.description
        let image = (news.thumbnail)!
        if image != "" {
            thumbnailView.backgroundColor = UIColor.ThemeDark(alpha: 1.0)
            thumbnailView.image = image == "news1" ? UIImage(named: "news1") : UIView.convertBase64ToImage(imageString: "\(image)")
        } else {
            thumbnailView.backgroundColor = .clear
            thumbnailView.image = UIImage(named: "news1")
        }
        
        if let window = UIApplication.shared.keyWindow {
            backView.alpha = 0
            
            window.addSubview(mainView)
            window.addSubview(MainScrollView)
            window.addSubview(backView)
            
            QRView.image = ApiService().generateQRCode(from: "\(ApiService().myQR())")
            
            mainView.addSubview(thumbnailView)
            mainView.addSubview(MainScrollView)
            MainScrollView.addSubview(titleLabel)
            MainScrollView.addSubview(dateAuthorLabel)
            MainScrollView.addSubview(contextLabel)
            
            mainView.frame = CGRect(x: 0, y: window.frame.height, width: window.frame.width, height: window.frame.height)
            thumbnailView.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height/3)
            backView.frame = CGRect(x: 10, y: 25, width: 25, height: 25)
            titleLabel.frame = CGRect(x: 10, y: 10, width: view.frame.width-20, height: estimatedFrameTitle.height)
            dateAuthorLabel.frame = CGRect(x: 15, y: titleLabel.frame.maxY+10, width: view.frame.width-20, height: 20)
            //            let height = thumbnailView.frame.height + titleLabel.frame.height + dateAuthorLabel.frame.height + 60.0
            contextLabel.frame = CGRect(x: 10, y: dateAuthorLabel.frame.maxY+10, width: view.frame.width-20, height: estimatedFrameContext.height+10)
            
            MainScrollView.frame = CGRect(x: 0, y: thumbnailView.frame.maxY, width: view.frame.width, height: view.frame.height-(view.frame.height/3))
            MainScrollView.contentSize = CGSize(width: view.frame.width, height: estimatedFrameTitle.height + 20 + estimatedFrameContext.height + 40)
            
            mainView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(hideProfile)))
            
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                self.mainView.frame = CGRect(x: 0, y: 0, width: window.frame.width, height: window.frame.height)
            }, completion: { (Bool) in
                UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                    self.backView.alpha = 1
                }, completion: { (Bool) in })
            })
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
}

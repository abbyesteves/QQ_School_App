//
//  ClassesController.swift
//  QQ_School_App
//
//  Created by Abby Esteves on 02/03/2018.
//  Copyright © 2018 Abby Esteves. All rights reserved.
//

import UIKit

class SubjectsController: UICollectionViewController, UICollectionViewDelegateFlowLayout, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIDocumentPickerDelegate, UITextFieldDelegate {
    
    let libraryController = UIImagePickerController()
    let cameraPickerController = UIImagePickerController()
    let documentController = UIDocumentPickerViewController(documentTypes: [], in: .import)
    let manager = CBLManager.sharedInstance()
    var subjectSelected = Subjects()
    var schedule = ApiService().scheduleList()
    var cellId = "cellId"
    var classes = ApiService().classes(day: FileService().getDateTime(format: "EEEE"))
    var timer : Timer?
    var currentLabelClosed = false
    
    let dayOption: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.white
        view.layer.shadowColor = UIColor.gray.cgColor
        view.layer.shadowOpacity = 0.5
        view.layer.shadowOffset = CGSize(width: 0, height: 1)
        view.layer.shadowRadius = 1
        return view
    }()
    
    let blackView : UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(white: 0, alpha: 0)
        return view
    }()
    
    let emptyLabel: UILabel = {
        let label = UILabel()
        label.text = "No Schedule Available"
        label.textColor = UIColor.lightGray
        label.numberOfLines = 2
        label.textAlignment = .center
        label.font = UIFont.boldSystemFont(ofSize: label.font.pointSize)
        label.font = label.font.withSize(12)
        label.alpha = 0.5
        return label
    }()
    
    let emptyImage: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.clipsToBounds = true
        imageView.backgroundColor = .clear
        imageView.layer.masksToBounds = true
        imageView.image = UIImage(named: "ic_empty")?.withRenderingMode(.alwaysTemplate)
        imageView.tintColor = UIColor.lightGray
        imageView.alpha = 0.2
        return imageView
    }()
    
    let attendanceButton: UIButton = {
        let button = UIButton()
        button.backgroundColor = UIColor.ThemeDarkGreen(alpha: 1.0)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 13)
        button.setTitleColor(UIColor.white, for: .normal)
        button.setTitle("scan attendance".uppercased(), for: .normal)
        return button
    }()
    
    let floatImage1: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        return view
    }()
    
    let floatImage2: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        return view
    }()
    
    let floatMenu: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.ThemeDarkGreen(alpha: 1.0)
        view.layer.shadowColor = UIColor.black.cgColor
        view.layer.shadowOpacity = 0.5
        view.layer.shadowOffset = CGSize(width: 3, height: 3)
        view.layer.shadowRadius = 5
        view.layer.cornerRadius = 30
        return view
    }()
    
    let seatingLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.white
        label.text = "Classroom View"
        label.backgroundColor = UIColor.black
        label.font = label.font.withSize(12)
        label.textAlignment = .center
        label.layer.cornerRadius = 3
        label.layer.shadowColor = UIColor.black.cgColor
        label.layer.shadowOpacity = 0.3
        label.layer.shadowOffset = CGSize(width: 3, height: 3)
        label.layer.shadowRadius = 2
        return label
    }()
    
    let seatingImage: UIImageView = {
        let image = UIImageView()
        image.safeAreaInsetsDidChange()
        image.image = UIImage(named: "ic_seatplan")?.withRenderingMode(.alwaysTemplate)as UIImage?
        image.tintColor = UIColor.white
        image.layer.cornerRadius = 20
        return image
    }()
    
    let floatSeating: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.ThemeDarkGreen(alpha: 1.0)
        view.layer.shadowColor = UIColor.black.cgColor
        view.layer.shadowOpacity = 0.5
        view.layer.shadowOffset = CGSize(width: 3, height: 3)
        view.layer.shadowRadius = 5
        view.layer.cornerRadius = 25
        return view
    }()
    
    let scanLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.white
        label.text = "Scan Attendance"
        label.backgroundColor = UIColor.black
        label.font = label.font.withSize(12)
        label.textAlignment = .center
        label.layer.cornerRadius = 3
        label.layer.shadowColor = UIColor.black.cgColor
        label.layer.shadowOpacity = 0.3
        label.layer.shadowOffset = CGSize(width: 3, height: 3)
        label.layer.shadowRadius = 2
        return label
    }()
    
    let scanImage: UIImageView = {
        let image = UIImageView()
        image.safeAreaInsetsDidChange()
        image.image = UIImage(named: "ic_scan")?.withRenderingMode(.alwaysTemplate)as UIImage?
        image.tintColor = UIColor.white
        image.layer.cornerRadius = 20
        return image
    }()
    
    let floatScan: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.ThemeDarkGreen(alpha: 1.0)
        view.layer.shadowColor = UIColor.black.cgColor
        view.layer.shadowOpacity = 0.5
        view.layer.shadowOffset = CGSize(width: 3, height: 3)
        view.layer.shadowRadius = 5
        view.layer.cornerRadius = 25
        return view
    }()
    
    let examLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.white
        label.text = "Create Exam"
        label.backgroundColor = UIColor.black
        label.font = label.font.withSize(12)
        label.textAlignment = .center
        label.layer.cornerRadius = 3
        label.layer.shadowColor = UIColor.black.cgColor
        label.layer.shadowOpacity = 0.3
        label.layer.shadowOffset = CGSize(width: 3, height: 3)
        label.layer.shadowRadius = 2
        return label
    }()
    
    let examImage: UIImageView = {
        let image = UIImageView()
        image.safeAreaInsetsDidChange()
        image.image = UIImage(named: "ic_exam")?.withRenderingMode(.alwaysTemplate)as UIImage?
        image.tintColor = UIColor.white
        image.layer.cornerRadius = 20
        return image
    }()
    
    let floatExam: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.ThemeDarkGreen(alpha: 1.0)
        view.layer.shadowColor = UIColor.black.cgColor
        view.layer.shadowOpacity = 0.5
        view.layer.shadowOffset = CGSize(width: 3, height: 3)
        view.layer.shadowRadius = 5
        view.layer.cornerRadius = 25
        return view
    }()
    
    let uploadLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.white
        label.text = "Upload Module"
        label.backgroundColor = UIColor.black
        label.font = label.font.withSize(12)
        label.textAlignment = .center
        label.layer.cornerRadius = 3
        label.layer.shadowColor = UIColor.black.cgColor
        label.layer.shadowOpacity = 0.3
        label.layer.shadowOffset = CGSize(width: 3, height: 3)
        label.layer.shadowRadius = 2
        return label
    }()
    
    let uploadImage: UIImageView = {
        let image = UIImageView()
        image.safeAreaInsetsDidChange()
        image.image = UIImage(named: "ic_upload")?.withRenderingMode(.alwaysTemplate)as UIImage?
        image.tintColor = UIColor.white
        image.layer.cornerRadius = 20
        return image
    }()
    
    let floatUpload: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.ThemeDarkGreen(alpha: 1.0)
        view.layer.shadowColor = UIColor.black.cgColor
        view.layer.shadowOpacity = 0.5
        view.layer.shadowOffset = CGSize(width: 3, height: 3)
        view.layer.shadowRadius = 5
        view.layer.cornerRadius = 25
        return view
    }()
    
    let titleText: UITextField = {
        let text = UITextField()
        text.layer.borderWidth = 1.0
        text.layer.borderColor = UIColor.lightGray.cgColor
        text.backgroundColor = .clear//UIColor.rgba(red: 255, green: 255, blue: 255, alpha: 0.5)
        text.textColor = UIColor.darkGray
        var placeholder = NSMutableAttributedString()
        placeholder = NSMutableAttributedString(attributedString: NSAttributedString(string: "Title", attributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 11.0), .foregroundColor: UIColor.lightGray]))
        text.setLeftPadding(space: 8)
        text.font = text.font?.withSize(11)
        text.attributedPlaceholder = placeholder
        text.layer.cornerRadius = 5
        return text
    }()
    
    let thumbnailView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.clipsToBounds = true
        imageView.backgroundColor = .clear
        imageView.layer.masksToBounds = true
        imageView.image = UIImage(named: "base64")
        return imageView
    }()
    
    let currentView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.ThemeOrange(alpha: 1.0)
        return view
    }()
    
    let currentLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.white
        label.text = ""
        label.font = label.font.withSize(12)
        label.textAlignment = .center
        return label
    }()
    
    let closeView: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "ic_close")?.withRenderingMode(.alwaysTemplate)
        imageView.tintColor = UIColor.white
        return imageView;
    }()
    
    @objc func goToSeatingPlan() {
        let manager = CBLManager.sharedInstance()
        let database = try! manager.databaseNamed("seatplan")
        let document = database.document(withID: "CLASS_SEAT_\((self.subjectSelected.subject_code)!)_GR:\((self.subjectSelected.grade_lvl)!)_SEC:\((self.subjectSelected.section)!)")!
        let properties = document.properties
//        print(" goToSeatingPlan ", "\(document.documentID)")
        if properties != nil {
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                self.closeFloat()
            }, completion: { (Bool) in
                 print(" goToSeatingPlan ", properties!)
                DispatchQueue.main.async(execute: {
                    let titleLabel : UILabel = {
                        let label = UILabel()
                        label.textColor = UIColor.white
                        label.font = UIFont(name: "GillSans-Bold", size: UIFont.systemFontSize)!
                        label.font = label.font.withSize(15)
                        label.text = "\((self.subjectSelected.subject_code)!) classroom view".uppercased()
                        return label
                    }()
                    
                    let seatPlanDetail = SeatPlanDetail()
                    self.navigationController?.pushViewController(seatPlanDetail, animated: true)
                    seatPlanDetail.navigationItem.title = "\((self.subjectSelected.subject_code)!) \(document.documentID)"
                    seatPlanDetail.navigationItem.titleView = titleLabel
                })
            })
        } else {
            view.showToast(message: "Go to your Configuration and add students for class \(self.subjectSelected.subject_code.uppercased()).")
//            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
//                self.closeFloat()
//            }, completion: { (Bool) in
//                DispatchQueue.main.async(execute: {
//                    let titleLabel : UILabel = {
//                        let label = UILabel()
//                        label.textColor = UIColor.white
//                        label.font = UIFont(name: "GillSans-Bold", size: UIFont.systemFontSize)!
//                        label.font = label.font.withSize(15)
//                        label.text = "\((self.subjectSelected.subject_code)!) class seating".uppercased()
//                        return label
//                    }()
//
//                    let createSeatPlanDetail = CreateSeatPlanDetail()
//                    self.navigationController?.pushViewController(createSeatPlanDetail, animated: true)
//                    createSeatPlanDetail.navigationItem.title = "\((self.subjectSelected.subject_code)!)".uppercased()
//                    createSeatPlanDetail.navigationItem.titleView = titleLabel
//                })
//            })
        }
    }
    
    @objc func openUpload() {
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.closeFloat()
        }, completion: { (Bool) in
            DispatchQueue.main.async(execute: {
                let alertController = UIAlertController(title: "Add a File", message: "Choose from where to add...", preferredStyle: UIAlertControllerStyle.actionSheet)
                alertController.view.frame = CGRect(x: (self.view.frame.width-(self.view.frame.width-20))/2, y: (self.view.frame.height-270)/2, width: self.view.frame.width-20, height: 270)
                
                alertController.addAction(UIAlertAction(title: NSLocalizedString("Take a Photo", comment: ""), style: UIAlertActionStyle.default, handler: { _ in
                    
                    self.cameraPickerController.delegate = self
                    self.cameraPickerController.sourceType = .camera
                    
                    let alertWindow = UIWindow(frame: UIScreen.main.bounds)
                    alertWindow.rootViewController = UIViewController()
                    alertWindow.windowLevel = UIWindowLevelAlert + 1;
                    alertWindow.makeKeyAndVisible()
                    alertWindow.rootViewController?.present(self.cameraPickerController, animated: true, completion: nil)
                }))
                
                alertController.addAction(UIAlertAction(title: NSLocalizedString("Insert from library", comment: ""), style: UIAlertActionStyle.default, handler: { _ in
                    self.libraryController.delegate = self
                    self.libraryController.sourceType = .photoLibrary
                    self.libraryController.mediaTypes = UIImagePickerController.availableMediaTypes(for: .photoLibrary)!
                    self.present(self.libraryController, animated: true, completion: nil)
                }))
                
                alertController.addAction(UIAlertAction(title: NSLocalizedString("Add Attachment", comment: ""), style: UIAlertActionStyle.default, handler: { _ in
                    self.documentController.delegate = self
                    self.present(self.documentController, animated: true, completion: nil)
                }))
                
                alertController.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: UIAlertActionStyle.cancel, handler: { _ in
                    
                }))
                
                self.present(alertController, animated: true, completion: nil)
            })
        })
    }
    
    @objc func openExam() {
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.closeFloat()
        }, completion: { (Bool) in
            let titleLabel : UILabel = {
                let label = UILabel()
                label.textColor = UIColor.white
                label.font = UIFont(name: "GillSans-Bold", size: UIFont.systemFontSize)!
                label.font = label.font.withSize(15)
                label.text = "create exam for \((self.subjectSelected.subject_code)!)".uppercased()
                return label
            }()
            
            let createExamDetail = CreateExamDetail()
            self.navigationController?.pushViewController(createExamDetail, animated: true)
            
            createExamDetail.navigationItem.title = "\((self.subjectSelected.subject_code)!) documentID".uppercased()
            createExamDetail.navigationItem.titleView = titleLabel
        })
    }
    
    @objc func openFloat() {
        floatMenu.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(closeFloat)))
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.floatImage1.frame = CGRect(x: 18, y: 28, width: self.floatImage1.frame.width, height: self.floatImage1.frame.height)
            self.floatImage2.frame = CGRect(x: 18, y: 28, width:  self.floatImage2.frame.width, height:  self.floatImage2.frame.height)
            
            self.floatImage1.transform = CGAffineTransform(rotationAngle: 0.9)
            self.floatImage2.transform = CGAffineTransform(rotationAngle: -0.9)
            
            self.blackView.alpha = 1
            self.floatScan.alpha = 1
            self.floatUpload.alpha = 1
            self.floatExam.alpha = 1
            self.floatSeating.alpha = 1
            
            self.floatScan.frame = CGRect(x: self.view.frame.width-75, y: self.floatMenu.frame.maxY-130, width: 50, height: 50)
            self.scanLabel.frame = CGRect(x: -120, y: 12.5, width: 110, height: 25)
            
            self.floatSeating.frame = CGRect(x: self.view.frame.width-75, y: self.floatScan.frame.maxY-115, width: 50, height: 50)
            self.seatingLabel.frame = CGRect(x: -110, y: 12.5, width: 100, height: 25)
            
            self.floatExam.frame = CGRect(x: self.view.frame.width-75, y: self.floatSeating.frame.maxY-115, width: 50, height: 50)
            self.examLabel.frame = CGRect(x: -100, y: 12.5, width: 90, height: 25)
            
            self.floatUpload.frame = CGRect(x: self.view.frame.width-75, y: self.floatExam.frame.maxY-115, width: 50, height: 50)
            self.uploadLabel.frame = CGRect(x: -110, y: 12.5, width: 100, height: 25)
            
        }, completion: { (Bool) in
        })
    }
    
    @objc func closeFloat() {
        floatMenu.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(openFloat)))
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.floatImage1.transform = CGAffineTransform(rotationAngle: 0)
            self.floatImage2.transform = CGAffineTransform(rotationAngle: 0)
            
            self.blackView.alpha = 0
            self.floatScan.alpha = 0
            self.floatUpload.alpha = 0
            self.floatExam.alpha = 0
            self.floatSeating.alpha = 0
            
            self.floatScan.frame = CGRect(x: self.view.frame.width-75, y: self.view.frame.height-100, width: 50, height: 50)
            self.floatSeating.frame = CGRect(x: self.view.frame.width-75, y: self.view.frame.height-150, width: 50, height: 50)
            self.floatUpload.frame = CGRect(x: self.view.frame.width-75, y: self.view.frame.height-100, width: 50, height: 50)
            self.floatExam.frame = CGRect(x: self.view.frame.width-75, y: self.view.frame.height-100, width: 50, height: 50)
            self.floatImage1.frame = CGRect(x: 18, y: 25, width: 25, height: 1.5)
            self.floatImage2.frame = CGRect(x: 18, y: self.floatImage1.frame.maxY+8, width: 25, height: 1.5)
        }, completion: { (Bool) in})
    }
    
    @objc func goToScanner(){
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.closeFloat()
        }, completion: { (Bool) in
            DispatchQueue.main.async(execute: {
                let titleLabel : UILabel = {
                    let label = UILabel()
                    label.textColor = UIColor.white
                    label.font = UIFont(name: "GillSans-Bold", size: UIFont.systemFontSize)!
                    label.font = label.font.withSize(15)
                    label.text = "\((self.subjectSelected.subject)!) attendance".uppercased()
                    return label
                }()
                
                let scanQRLauncher = ScanQRLauncher()
                self.navigationController?.pushViewController(scanQRLauncher, animated: true)
                scanQRLauncher.navigationItem.title = "\((self.subjectSelected.subject_code)!)".uppercased()
                scanQRLauncher.navigationItem.titleView = titleLabel
            })
        })
    }
    
    @objc func selectorTapped(sender: UITapGestureRecognizer) {
        self.resetColor()
        let label = sender.view!.subviews[0] as? UILabel
        label?.alpha = 1
        
        if getDayfromArr(day: (label?.text)!) != FileService().getDateTime(format: "EEEE") {
            label?.layer.backgroundColor = UIColor.ThemeDark(alpha: 1.0).cgColor
            self.timer?.invalidate()
        } else {
            self.timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.getSchedule), userInfo: nil, repeats: true)
        }
        
        classes = ApiService().classes(day: getDayfromArr(day: (label?.text)!))
        self.isEmpty(count: classes.count, day : self.getDayfromArr(day: (label?.text)!))
        collectionView?.reloadData()
    }
    
    private func isEmpty(count: Int, day : String) {
        if count == 0 {
            //no schedule
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                self.emptyLabel.text = "No Schedule for \(day)"
                self.emptyLabel.alpha = 0.5
                self.emptyImage.alpha = 0.2
            }, completion: { (Bool) in})
        } else {
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                self.emptyImage.alpha = 0
                self.emptyLabel.alpha = 0
            }, completion: { (Bool) in})
        }
    }
    
    private func resetColor(){
        for view in dayOption.subviews {
            let label = view.subviews[0] as? UILabel
            label?.layer.backgroundColor = UIColor.lightGray.cgColor
            label?.alpha = 0.5
            
            if getDayfromArr(day: (label?.text)!) == FileService().getDateTime(format: "EEEE") {
                label?.layer.backgroundColor = UIColor.ThemeDarkGreen(alpha: 1.0).cgColor
            }
        }
    }
    
    private func showFloatCurrentLabel() {
        if subjectSelected.subject_code != nil {
            self.currentView.gestureRecognizers?.removeAll()
            self.currentView.backgroundColor = UIColor.ThemeOrange(alpha: 1.0)
            if "\((ApiService().myID()["user_type"])!)" == "teacher" {
                self.currentLabel.text = "Your current class is \((subjectSelected.subject_code)!), grade: \((subjectSelected.grade_lvl)!) - section: \((subjectSelected.section)!)"
            } else {
                self.currentLabel.text = "Your current class is \((subjectSelected.subject_code)!)"
            }
            
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                self.floatMenu.alpha = 1
            }, completion: { (Bool) in
                UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                    self.closeView.alpha = 0
                    self.currentView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: 30)
                    self.currentLabel.frame = CGRect(x: 0, y: 0, width: self.currentView.frame.width, height: self.currentView.frame.height)
                    self.dayOption.frame = CGRect(x: 0, y: self.currentLabel.frame.maxY, width: self.view.frame.width, height: 50)
                    self.collectionView?.frame = CGRect(x: 0, y: self.dayOption.frame.maxY, width: self.view.frame.width, height: self.view.frame.height-(self.currentLabel.frame.height+self.dayOption.frame.height))
                }, completion: { (Bool) in})
            })
        } else {
            self.currentView.backgroundColor = UIColor.Alert(alpha: 1.0)
            self.currentLabel.text = "No current class"
            currentView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(closeCurrentLabel)))
            
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                self.floatMenu.alpha = 0
            }, completion: { (Bool) in
                UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                    if !self.currentLabelClosed {
                        self.closeView.alpha = 1
                        self.currentView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: 30)
                        self.currentLabel.frame = CGRect(x: 0, y: 0, width: self.currentView.frame.width, height: self.currentView.frame.height)
                        self.closeView.frame = CGRect(x: self.currentLabel.frame.width-(self.currentLabel.frame.height+15), y: 2.5, width: self.currentLabel.frame.height-5, height: self.currentLabel.frame.height-5)
                        self.dayOption.frame = CGRect(x: 0, y: self.currentLabel.frame.maxY, width: self.view.frame.width, height: 50)
                        self.collectionView?.frame = CGRect(x: 0, y: self.dayOption.frame.maxY, width: self.view.frame.width, height: self.view.frame.height-(self.currentLabel.frame.height+self.dayOption.frame.height))
                    }
                }, completion: { (Bool) in})
            })
        }
    }
    
    @objc func closeCurrentLabel() {
        currentLabelClosed = true
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.currentView.frame = CGRect(x: 0, y: self.currentLabel.frame.minY, width: self.view.frame.width, height: 0)
            self.currentLabel.frame = CGRect(x: 0, y: 0, width: self.currentView.frame.width, height: self.currentView.frame.height)
            self.closeView.frame = CGRect(x: self.currentLabel.frame.width-(self.currentLabel.frame.height+15), y: 2.5, width: self.currentLabel.frame.height-5, height: self.currentLabel.frame.height-5)
            self.dayOption.frame = CGRect(x: 0, y: self.currentLabel.frame.maxY, width: self.view.frame.width, height: 50)
            self.collectionView?.frame = CGRect(x: 0, y: self.dayOption.frame.maxY, width: self.view.frame.width, height: self.view.frame.height-(self.dayOption.frame.height))
        }, completion: { (Bool) in})
    }
    
    @objc func getSchedule() {
        classes = ApiService().classes(day: FileService().getDateTime(format: "EEEE"))
        self.isEmpty(count: classes.count, day : FileService().getDateTime(format: "EEEE"))
        self.subjectSelected = Subjects()
        
        // get current
        if classes.count != 0 {
            for (index, subject) in classes.enumerated() {
                let condition = Int(subject.time_start_long)! <= Int(Date().timeIntervalSince1970) && Int(Date().timeIntervalSince1970) <=  Int(subject.time_end_long)!
                
                if condition {
                    if subject.subject_code != "LUNCH" {
                        self.subjectSelected.subject_code = subject.subject_code.uppercased()
                        self.subjectSelected.subject = subject.subject
                        self.subjectSelected.time_start = subject.time_start
                        self.subjectSelected.grade_lvl = subject.grade_lvl
                        self.subjectSelected.section = subject.section
                    }
                }
            }
        }
        self.showFloatCurrentLabel()
        self.collectionView?.reloadData()
    }
    
    func getSubject() -> Subjects {
        return subjectSelected
    }
    
    private func setupDayOption() {
        let days = ["M", "T", "W", "Th", "F", "S"]
        let width = (self.dayOption.frame.width-20)/CGFloat(days.count)
        let height = width-30
        var x = CGFloat(10)
        
        for day in days {
            let dayView : UIView = {
                let view = UIView()
                return view
            }()
            
            let dayLabel : UILabel = {
                let label = UILabel()
                label.text = day
                label.font = label.font.withSize(12)
                label.textColor = UIColor.white
                label.textAlignment = .center
                label.layer.cornerRadius = (height)/2
                label.layer.backgroundColor = UIColor.lightGray.cgColor
                label.alpha = 0.5
                return label
            }()
            
            if getDayfromArr(day: day) == FileService().getDateTime(format: "EEEE") {
                dayLabel.layer.backgroundColor = UIColor.ThemeDarkGreen(alpha: 1.0).cgColor
                dayLabel.alpha = 1
            }
            
            self.dayOption.addSubview(dayView)
            dayView.addSubview(dayLabel)
            
            dayView.frame = CGRect(x: CGFloat(x), y: CGFloat(0), width: width, height: self.dayOption.frame.height)
            dayLabel.frame = CGRect(x: 15, y: (dayView.frame.height-height)/2, width: height, height: height)
            
            dayView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.selectorTapped(sender:))))
            x = x + dayView.frame.width
        }
    }
    
    private func getDayfromArr(day: String) -> String {
        var getDay = "Sunday"
        
        if day == "M" {
            getDay = "Monday"
        } else if day == "T" {
            getDay = "Tuesday"
        } else if day == "W" {
            getDay = "Wednesday"
        } else if day == "Th" {
            getDay = "Thursday"
        } else if day == "F" {
            getDay = "Friday"
        } else if day == "S" {
            getDay = "Saturday"
        }
        return getDay
    }
    
    private func watchSchedule() {
        
        if "\((ApiService().myID()["user_type"])!)" == "student" {
            let schedule = try! manager.databaseNamed("schedule")
            NotificationCenter.default.addObserver(forName: NSNotification.Name.cblDatabaseChange, object: schedule, queue: nil) {
                (notification) -> Void in
                if let changes = notification.userInfo!["changes"] as? [CBLDatabaseChange] {
                    self.schedule = ApiService().scheduleList()
                    self.classes = ApiService().classes(day: FileService().getDateTime(format: "EEEE"))
                    self.collectionView?.reloadData()
                    
                    for change in changes {
                        print("schedule , revision ID '%@'", change.description)
                    }
                }
            }
        } else if "\((ApiService().myID()["user_type"])!)" == "teacher" {
            let mydata = try! manager.databaseNamed("mydata")
            NotificationCenter.default.addObserver(forName: NSNotification.Name.cblDatabaseChange, object: mydata, queue: nil) {
                (notification) -> Void in
//                if let changes = notification.userInfo!["changes"] as? [CBLDatabaseChange] {
                    self.schedule = ApiService().scheduleList()
                    self.classes = ApiService().classes(day: FileService().getDateTime(format: "EEEE"))
                    self.collectionView?.reloadData()
//                }
            }
        }
        
    }
//
//    private func getSubjects() {
//
//    }
    
    private func setupScanView() {
        collectionView?.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height-40)
        view.addSubview(attendanceButton)
        
        attendanceButton.frame = CGRect(x: 0, y: view.frame.height-40, width: view.frame.width, height: 40)
    }
    
    private func setupView(){
        collectionView?.scrollsToTop = true
        collectionView?.contentInset = UIEdgeInsetsMake(20, 0, 70, 0)
        collectionView?.backgroundColor = UIColor.Background(alpha: 1.0)
        collectionView?.register(SubjectCell.self, forCellWithReuseIdentifier: cellId)
        
        view.addSubview(dayOption)
        view.addSubview(currentView)
        currentView.addSubview(closeView)
        currentView.addSubview(currentLabel)
        view.addSubview(currentLabel)
        view.addSubview(emptyImage)
        view.addSubview(emptyLabel)
        
        self.emptyImage.alpha = 0
        self.emptyLabel.alpha = 0
    }
    
    private func setupConstraints() {
        currentView.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: 0)
        currentLabel.frame = CGRect(x: 0, y: 0, width: currentView.frame.width, height: currentView.frame.height)
        closeView.frame = CGRect(x: currentLabel.frame.width-(currentLabel.frame.height+10), y: 2.5, width: currentLabel.frame.height-5, height: currentLabel.frame.height-5)
        dayOption.frame = CGRect(x: 0, y: currentLabel.frame.maxY, width: view.frame.width, height: 50)
        emptyLabel.frame = CGRect(x: 10, y: view.frame.height/3, width: view.frame.width-20, height: 20)
        emptyImage.frame = CGRect(x: (view.frame.width/2)-((view.frame.width/3)/2), y: emptyLabel.frame.maxY, width: view.frame.width/3, height: view.frame.width/3)
        
        collectionView?.frame = CGRect(x: 0, y: dayOption.frame.maxY, width: view.frame.width, height: view.frame.height-dayOption.frame.height)
    }
    
    private func setupFloat() {
        if "\((ApiService().myID()["user_type"])!)" == "teacher" {
            view.addSubview(blackView)
            
            view.addSubview(floatSeating)
            floatSeating.addSubview(seatingImage)
            floatSeating.addSubview(seatingLabel)
            
            view.addSubview(floatScan)
            floatScan.addSubview(scanImage)
            floatScan.addSubview(scanLabel)
            
            view.addSubview(floatUpload)
            floatUpload.addSubview(uploadImage)
            floatUpload.addSubview(uploadLabel)
            
            view.addSubview(floatExam)
            floatExam.addSubview(examImage)
            floatExam.addSubview(examLabel)
            
            view.addSubview(floatMenu)
            floatMenu.addSubview(floatImage1)
            floatMenu.addSubview(floatImage2)
            
            floatScan.alpha = 0
            floatSeating.alpha = 0
            floatExam.alpha = 0
            floatUpload.alpha = 0
            blackView.alpha = 0
            floatMenu.alpha = 0
            
            blackView.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height)
            
            floatMenu.frame = CGRect(x: view.frame.width-80, y: view.frame.height-150, width: 60, height: 60)
            floatImage1.frame = CGRect(x: 18, y: 25, width: 25, height: 1.5)
            floatImage2.frame = CGRect(x: 18, y: floatImage1.frame.maxY+8, width: 25, height: 1.5)
            
            floatScan.frame = CGRect(x: view.frame.width-75, y: view.frame.height-150, width: 50, height: 50)
            scanImage.frame = CGRect(x: 12.5, y: 12.5, width: 25, height: 25)
            scanLabel.frame = CGRect(x: 0, y: 12.5, width: 0, height: 25)
            
            floatSeating.frame = CGRect(x: view.frame.width-75, y: view.frame.height-150, width: 50, height: 50)
            seatingImage.frame = CGRect(x: 12.5, y: 12.5, width: 25, height: 25)
            seatingLabel.frame = CGRect(x: 0, y: 12.5, width: 0, height: 25)
            
            floatUpload.frame = CGRect(x: view.frame.width-75, y: view.frame.height-150, width: 50, height: 50)
            uploadImage.frame = CGRect(x: 12.5, y: 12.5, width: 25, height: 25)
            uploadLabel.frame = CGRect(x: 0, y: 12.5, width: 0, height: 25)
            
            floatExam.frame = CGRect(x: view.frame.width-75, y: view.frame.height-150, width: 50, height: 50)
            examImage.frame = CGRect(x: 12.5, y: 12.5, width: 25, height: 25)
            examLabel.frame = CGRect(x: 0, y: 12.5, width: 0, height: 25)
            
            floatMenu.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(openFloat)))
            blackView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(closeFloat)))
            floatScan.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(goToScanner)))
            floatUpload.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(openUpload)))
            floatExam.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(openExam)))
            floatSeating.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(goToSeatingPlan)))
        }
    }
    
    func documentMenu(_ documentMenu: UIDocumentPickerViewController, didPickDocumentPicker documentPicker: UIDocumentPickerViewController) {
        print(" didPickDocumentPicker ", documentPicker)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        print(" didFinishPickingMediaWithInfo ", info[UIImagePickerControllerReferenceURL], info)
        var fileExtension = "jpg"
        let image = info[UIImagePickerControllerOriginalImage] as? UIImage
        self.titleText.text = ""
        self.thumbnailView.image = image
        
        
        
        // make sure to update file extension
        if info[UIImagePickerControllerReferenceURL] != nil {
            let kindOfFile = "\((info[UIImagePickerControllerReferenceURL])!)"
            fileExtension = kindOfFile.suffix(3).lowercased()
        } else {
            //            self.thumbnailView.image = view.resizeImage(image: image!, newWidth: 600)
        }
        
        UIView.animate(withDuration: 0.1, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.libraryController.dismiss(animated: true, completion: nil)
            self.cameraPickerController.dismiss(animated: true, completion: nil)
            self.documentController.dismiss(animated: true, completion: nil)
        }, completion: { (Bool) in
            //            self.open()
        })
        
        let alertController = UIAlertController(title: "Picture Selected", message: "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n", preferredStyle: UIAlertControllerStyle.alert)
        alertController.view.frame = CGRect(x: (view.frame.width-(view.frame.width-20))/2, y: 50, width: view.frame.width-20, height: 270)
        alertController.view.addSubview(thumbnailView)
        alertController.view.addSubview(titleText)
        
        
        self.titleText.frame =  CGRect(x: 15, y: 60, width: 240, height: 40)
        self.thumbnailView.frame = CGRect(x: 15, y: titleText.frame.maxY+10, width: 240, height: 240)
        
        alertController.addAction(UIAlertAction(title: NSLocalizedString("Upload", comment: "Default action"), style: UIAlertActionStyle.default, handler: { _ in
            let title = self.titleText.text?.capitalized
            if self.titleText.text == "" {
                self.titleText.becomeFirstResponder()
            } else {
                UploadService().postModule(imageName: title!.replacingOccurrences(of: " ", with: "_"), image : image!, fileExtension: fileExtension, subj_code: self.subjectSelected.subject_code)
            }
        }))
        
        alertController.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: UIAlertActionStyle.cancel, handler: { _ in
            
        }))
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return classes.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let data = classes[indexPath.item] as Subjects
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! SubjectCell
        let condition = Int(Date().timeIntervalSince1970) >= Int(data.time_end_long)!
        
        cell.backgroundColor = UIColor.white
        cell.layer.shadowColor = UIColor.gray.cgColor
        cell.layer.shadowOpacity = 0.5
        cell.layer.shadowOffset = CGSize(width: 0, height: 1)
        cell.layer.shadowRadius = 1
        cell.layer.cornerRadius = 10
        cell.layer.borderColor = UIColor.ThemeDark(alpha: 1.0).cgColor
        cell.subject = classes[indexPath.item]
        
        if self.subjectSelected.subject_code == "\((data.subject_code)!)".uppercased() && self.subjectSelected.time_start == "\((data.time_start)!)"{
            cell.layer.borderWidth = 2
        } else {
            if condition {
                cell.backgroundColor = UIColor.rgba(red: 223, green: 223, blue: 223, alpha: 1.0)
            }
            cell.layer.borderWidth = 0
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//        let classToday = classes[indexPath.item] as Subjects
        //        print(" class today : ", classToday.subj_code)
//        if classToday.subj_code == "LUNCH"{
//            return CGSize(width: view.frame.width-20, height: 70)
//        }
        return CGSize(width: view.frame.width-20, height: 110)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        subjectSelected = classes[indexPath.item]
        //        SubjectDetail().subjectSelected = subjectSelected
        
        if "\((subjectSelected.subject)!)".capitalized != "Lunch" {
            let titleLabel : UILabel = {
                let label = UILabel()
                label.textColor = UIColor.white
                label.font = UIFont(name: "GillSans-Bold", size: UIFont.systemFontSize)!
                label.font = label.font.withSize(15)
                label.text = "\((subjectSelected.subject)!)".uppercased()
                return label
            }()
            
            let documentID = "CLASS_SEAT_\((self.subjectSelected.subject_code)!)_GR:\((self.subjectSelected.grade_lvl)!)_SEC:\((self.subjectSelected.section)!)"
            let subjectDetail = SubjectDetail()
            navigationController?.pushViewController(subjectDetail, animated: true)
            
            subjectDetail.navigationItem.title = "\((subjectSelected.subject_code)!) \(documentID)".uppercased()
            subjectDetail.navigationItem.titleView = titleLabel
        }
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.currentLabel.frame = CGRect(x: 0, y: self.currentLabel.frame.minY, width: self.view.frame.width, height: 0)
            self.collectionView?.frame = CGRect(x: 0, y: self.currentLabel.frame.maxY, width: self.view.frame.width, height: (self.collectionView?.frame.height)!+self.dayOption.frame.height)
        }, completion: { (Bool) in})
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.getSchedule), userInfo: nil, repeats: true)
        setupFloat()
        setupView()
        setupConstraints()
        watchSchedule()
        setupDayOption()
        
//        print(" classes : ",ApiService().classes())
        
        //logout test
//        ApiService().reset()
    }
    
}

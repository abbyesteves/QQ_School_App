//
//  HomeTabBarController.swift
//  QQ_School_App
//
//  Created by Abby Esteves on 10/07/2018.
//  Copyright © 2018 Abby Esteves. All rights reserved.
//

import UIKit
import Reachability
class HomeTabBarController: UITabBarController, UITabBarControllerDelegate {
    
    var connection = String()
    var reachability = Reachability()!
    let menuImage = UIImage(named: "ic_menu")?.withRenderingMode(.alwaysOriginal)
    let scanImage = UIImage(named: "ic_home_nav")?.withRenderingMode(.alwaysOriginal)
    //    let scanImage = UIImage(named: "qr_code")?.withRenderingMode(.alwaysOriginal)
    
    lazy var sideMenuLauncher: SideMenuLauncher = {
        let launcher = SideMenuLauncher()
        launcher.navigation = self
        return launcher
    }()
    
    lazy var profileController: ProfileController = {
        let launcher = ProfileController()
        return launcher
    }()
    
    let statusBarBackground: UIView = {
        let view = UIView()
        //        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor.ThemeDark(alpha: 1.0)
        return view
    }()
    
    let statusBarConnection: UIView = {
        let view = UIView()
        view.backgroundColor = .clear
        return view
    }()
    
    let connectionLabel: UILabel = {
        let label = UILabel()
        label.text = "Opps.. cannot connect at the moment"
        label.textColor = UIColor.white
        label.numberOfLines = 1
        label.textAlignment = .center
        label.font = label.font.withSize(11)
        label.backgroundColor = UIColor.Alert(alpha: 1.0)
        label.alpha = 0.8
        return label
    }()
    
    private func setupView() {
        
        let notesTab = NotesController(collectionViewLayout: UICollectionViewFlowLayout())
        notesTab.tabBarItem = UITabBarItem(title: "notes".capitalized, image: UIImage(named: "ic_notes"), selectedImage: UIImage(named: "ic_notes"))
        
        let announcementTab = AnnouncementController(collectionViewLayout: UICollectionViewFlowLayout())
        announcementTab.tabBarItem = UITabBarItem(title: "Announcements".capitalized, image: UIImage(named: "ic_announcement"), selectedImage: UIImage(named: "ic_announcement"))
        
        let teacherHomeTab = TeacherHomeController()
        teacherHomeTab.tabBarItem = UITabBarItem(title: "Home".capitalized, image: UIImage(named: "ic_home"), selectedImage: UIImage(named: "ic_home"))
        
        let studentHomeTab = HomeController()
        studentHomeTab.tabBarItem = UITabBarItem(title: "Home".capitalized, image: UIImage(named: "ic_home"), selectedImage: UIImage(named: "ic_home"))
        
        let calendarTab = CalendarController()
        calendarTab.tabBarItem = UITabBarItem(title: "calendar".capitalized, image: UIImage(named: "ic_calendar"), selectedImage: UIImage(named: "ic_calendar"))
        
        let taskTab = TasksController(collectionViewLayout: UICollectionViewFlowLayout())
        taskTab.tabBarItem = UITabBarItem(title: "tasks".capitalized, image: UIImage(named: "ic_task"), selectedImage: UIImage(named: "ic_task"))
        
        if "\((ApiService().myID()["user_type"])!)" == "teacher" {
            self.viewControllers = [notesTab, announcementTab, teacherHomeTab, calendarTab, taskTab]
            self.selectedIndex = 2
        } else if "\((ApiService().myID()["user_type"])!)" == "student" {
            self.viewControllers = [announcementTab, studentHomeTab, calendarTab]
            self.selectedIndex = 1
        }
        
        tabBar.isTranslucent = false
        tabBar.tintColor = UIColor.Theme(alpha: 1.0)
        moreNavigationController.navigationBar.isHidden = true
        
    }
    
    @objc func handleMenu() {
        self.sideMenuLauncher.showMenu()
    }
    
//    @objc func handleScan() {
//        let scanQRLauncher = ScanQRLauncher()
//        self.navigationController?.pushViewController(scanQRLauncher, animated: true)
//    }
    
    @objc func handleScan() {
        let homeTabBarController = HomeTabBarController()
        navigationController?.pushViewController(homeTabBarController, animated: true)
    }
    
    //Navigation
    func showController(menu: Menu){
        print(" showController ", menu.label)
        let layout = UICollectionViewFlowLayout()
        
        //News
        if sideMenuLauncher.menuLabels[1].label == menu.label{
            let homeTabBarController = HomeTabBarController()
            self.navigationController?.pushViewController(homeTabBarController, animated: true)
            
            //My Profile
        } else if sideMenuLauncher.menuLabels[2].label == menu.label {
            profileController.showProfile(window: true)
            //            let profileController = ProfileController()
            //            present(profileController, animated: true, completion: nil)
            
            //Announcements
            //        }
            //        else if sideMenuLauncher.menuLabels[3].label == menu.label {
            //            let announcementController = AnnouncementController(collectionViewLayout: layout)
            //            setupGoTo(ctrl: announcementController, title: "Announcements")
            //
            //
            //School Classes
        }
        else if sideMenuLauncher.menuLabels[3].label == menu.label {
            let subjectsController = SubjectsController(collectionViewLayout: layout)
            setupGoTo(to: subjectsController, title: "\(sideMenuLauncher.menuLabels[3].label)", from: selectedViewController!)
            
            //            callScanner()
            //            let profileController = ProfileController()
            //            setupGoTo(ctrl: profileController, title: "")
            
            //My friends
            //        } else if sideMenuLauncher.menuLabels[4].label == menu.label {
            //            let friendsController = FriendsController()
            //            setupGoTo(ctrl: friendsController, title: "\(sideMenuLauncher.menuLabels[4].label)")
            
            //My Calendar
            //        }
            //        else if sideMenuLauncher.menuLabels[6].label == menu.label {
            //            let calendarController = CalendarController()
            //            setupGoTo(ctrl: calendarController, title: "School Calendar")
            //
            //My School
        }
        else if sideMenuLauncher.menuLabels[4].label == menu.label {
            let schoolController = SchoolController()
            setupGoTo(to: schoolController, title: "\(sideMenuLauncher.menuLabels[4].label)", from: selectedViewController!)
            
            //Notes
        } else if sideMenuLauncher.menuLabels[5].label == menu.label {
            let notesController = NotesController(collectionViewLayout: layout)
            setupGoTo(to: notesController, title: "\(sideMenuLauncher.menuLabels[5].label)", from: selectedViewController!)
            
            //My Task
            //        } else if sideMenuLauncher.menuLabels[7].label == menu.label {
            //            let tasksController = TasksController(collectionViewLayout: layout)
            //            setupGoTo(ctrl: tasksController, title: "\(sideMenuLauncher.menuLabels[7].label)")
            
            //My Badges
        }
            //        else if sideMenuLauncher.menuLabels[7].label == menu.label {
            //            let badgesController = BadgesController(collectionViewLayout: layout)
            //            setupGoTo(ctrl: badgesController, title: "\(sideMenuLauncher.menuLabels[7].label)")
            //
            //        }
        else if sideMenuLauncher.menuLabels[sideMenuLauncher.menuLabels.count-1].label == menu.label {
            
            let alertController = UIAlertController(title: "Logout", message: "Reset App's Status & Delete all data?", preferredStyle: UIAlertControllerStyle.actionSheet)
            
            alertController.addAction(UIAlertAction(title: NSLocalizedString("Yes I am sure", comment: ""), style: UIAlertActionStyle.default, handler: { _ in
                //logout test
                //                ApiService().reset()
                HomeTabBarController().statusBarBackground.removeFromSuperview()
                HomeTabBarController().statusBarBackground.removeFromSuperview()
                
                let loginController = LoginController()
                self.navigationController?.pushViewController(loginController, animated: true)
            }))
            
            alertController.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: "Default action"), style: UIAlertActionStyle.cancel, handler: { _ in
                
            }))
            
            let alertWindow = UIWindow(frame: UIScreen.main.bounds)
            alertWindow.rootViewController = UIViewController()
            alertWindow.windowLevel = UIWindowLevelAlert + 1;
            alertWindow.makeKeyAndVisible()
            alertWindow.rootViewController?.present(alertController, animated: true, completion: nil)
        }
    }
    
    
    func setupGoTo(to : UIViewController, title : String, from: UIViewController) {
        print(" setupGoTo ", to, from)
        let menuBarItem = UIBarButtonItem(image: menuImage, style: .plain, target: self, action: #selector(handleMenu))
        let optionBarItem = UIBarButtonItem(image: scanImage, style: .plain, target: self, action: #selector(handleScan))
        
        let titleLabel : UILabel = {
            let label = UILabel()
            label.textColor = UIColor.white
            label.font = UIFont(name: "GillSans-Bold", size: UIFont.systemFontSize)!
            label.font = label.font.withSize(15)
            label.text = "\(title)".uppercased()
            return label
        }()
        
        from.navigationController?.pushViewController(to, animated: true)
        
        to.navigationItem.title = ""
        to.navigationItem.titleView = titleLabel
        to.navigationItem.leftBarButtonItems = [menuBarItem]
        to.navigationItem.rightBarButtonItems = [optionBarItem]
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupStatusBar()
        setupHeaderNavbar()
    }
    
    override func tabBar(_ tabBar: UITabBar, didSelect item: UITabBarItem) {
        let titleLabel : UILabel = {
            let label = UILabel()
            label.text = item.title! == "home".capitalized ? "\((ApiService().configuration()["alias"])!)".uppercased() : "My \(item.title!)"
            label.font = UIFont(name: "GillSans-Bold", size: UIFont.systemFontSize)!
            label.font = label.font.withSize(15)
            label.textColor = UIColor.white
            return label
        }()
        
        self.navigationItem.titleView = titleLabel
    }
    
    func setupStatusBar(){
        self.navigationController?.isNavigationBarHidden = false
        UIApplication.shared.isStatusBarHidden = false
        DispatchQueue.main.async(execute: {
            if let window = UIApplication.shared.keyWindow {
                window.addSubview(self.statusBarBackground)
                window.addSubview(self.statusBarConnection)
                //                self.statusBarBackground.topAnchor.constraint(equalTo: window.topAnchor).isActive = true
                //                self.statusBarBackground.widthAnchor.constraint(equalTo: window.widthAnchor).isActive = true
                //                self.statusBarBackground.heightAnchor.constraint(equalTo: self.statusBarBackground.heightAnchor, multiplier: 20).isActive = true
                self.statusBarBackground.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: UIApplication.shared.statusBarFrame.height)
            }
        })
    }
    
    func setupReachability() {
        //NOTE * reachability object for online offline
        reachability.whenReachable = { reachability in
            if reachability.connection == .wifi {
                print("Reachable via WiFi")
                if self.connection == "no" {
                    DispatchQueue.main.async(execute: {
                        self.connectionLabel.text = "Connected via wifi"
                        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                            self.statusBarConnection.backgroundColor = UIColor.Accept(alpha: 1.0)
                            self.connectionLabel.backgroundColor = UIColor.Accept(alpha: 1.0)
                        }, completion: { (Bool) in
                            UIView.animate(withDuration: 0.5, delay: 2, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                                self.statusBarConnection.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: 20)
                                self.connectionLabel.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: 0)
                            }, completion: { (Bool) in
                                self.connection = "wifi"
                                UIView.animate(withDuration: 0.5, delay: 2, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                                    self.statusBarConnection.backgroundColor = .clear
                                }, completion: { (Bool) in })
                            })
                        })
                    })
                }
            } else {
                print("Reachable via Cellular")
                if self.connection == "no" {
                    DispatchQueue.main.async(execute: {
                        self.connectionLabel.text = "Connected via cellular"
                        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                            self.statusBarConnection.backgroundColor = UIColor.Accept(alpha: 1.0)
                            self.connectionLabel.backgroundColor = UIColor.Accept(alpha: 1.0)
                        }, completion: { (Bool) in
                            UIView.animate(withDuration: 0.5, delay: 2, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                                self.connectionLabel.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: 0)
                            }, completion: { (Bool) in
                                self.connection = "cellular"
                            })
                        })
                    })
                }
            }
        }
        reachability.whenUnreachable = { _ in
            print("Not reachable")
            self.connection = "no"
            DispatchQueue.main.async(execute: {
                UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                    self.statusBarConnection.backgroundColor = UIColor.Alert(alpha: 1.0)
                    UIView.animate(withDuration: 0.5, delay: 2, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                        self.statusBarConnection.layer.cornerRadius = 5
                        self.statusBarConnection.frame = CGRect(x: self.view.frame.width-60, y: self.statusBarBackground.frame.maxY+10, width: 10, height: 10)
                    }, completion: { (Bool) in })
                }, completion: { (Bool) in })
            })
        }
        
        do {
            try reachability.startNotifier()
        } catch {
            print("Unable to start notifier")
        }
    }
    
    func setupHeaderNavbar() {
        self.navigationController?.navigationBar.barTintColor = UIColor.Theme(alpha: 0.1)
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.isTranslucent = false
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        self.navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: UIBarButtonItemStyle.plain, target: nil, action: nil)
        
        let titleLabel : UILabel = {
            let label = UILabel()
            label.text = "\((ApiService().configuration()["alias"])!)".uppercased()
            label.font = UIFont(name: "GillSans-Bold", size: UIFont.systemFontSize)!
            label.font = label.font.withSize(15)
            label.textColor = UIColor.white
            return label
        }()
        
        let menuBarItem = UIBarButtonItem(image: menuImage, style: .plain, target: self, action: #selector(handleMenu))
        let optionBarItem = UIBarButtonItem(image: scanImage, style: .plain, target: self, action: #selector(handleScan))
        
        self.navigationItem.titleView = titleLabel
        self.navigationItem.leftBarButtonItems = [menuBarItem]
        self.navigationItem.rightBarButtonItems = [optionBarItem]
    }
    
    func checkInternetConnection(){
        //check
        let google = URL(string: "https://www.google.com.ph/")!
        var request = URLRequest(url: google)
        request.timeoutInterval = 5
        let session = URLSession.shared
        session.dataTask(with: request) { (data, response, error) in
            if let response = response {
                print(" replicate repsone ",response)
                DispatchQueue.main.async(execute: {
                    UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                        self.connectionLabel.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: 0)
                    }, completion: { (Bool) in })
                })
            }
            if let error = error {
                print(" error catch replicate ", error)
                self.connectionLabel.text = "No Internet Connection"
                DispatchQueue.main.async(execute: {
                    TeacherHomeController().getApi {
                        DispatchQueue.main.async(execute: {
                            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                                self.connectionLabel.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: 20)
                            }, completion: { (Bool) in
                                UIView.animate(withDuration: 0.5, delay: 2, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                                    self.connectionLabel.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: 0)
                                }, completion: { (Bool) in
                                    TeacherHomeController().appendNews()
                                })
                            })
                        })
                    }
                })
            }
            }.resume()
    }
    
    func checkCouchConnection(){
        //check
        var request = URLRequest(url: ApiService().myURL())
        request.timeoutInterval = 5
        let session = URLSession.shared
        session.dataTask(with: request) { (data, response, error) in
            if let response = response {
                print(" replicate repsone ",response)
                DispatchQueue.main.async(execute: {
                    UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                        self.connectionLabel.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: 0)
                    }, completion: { (Bool) in })
                })
            }
            if let error = error {
                print(" error catch replicate ", error)
                self.connectionLabel.text = "Opps.. cannot connect at the moment"
                DispatchQueue.main.async(execute: {
                    TeacherHomeController().getApi {
                        DispatchQueue.main.async(execute: {
                            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                                self.connectionLabel.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: 20)
                            }, completion: { (Bool) in
                                UIView.animate(withDuration: 0.5, delay: 2, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                                    self.connectionLabel.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: 0)
                                }, completion: { (Bool) in
                                    TeacherHomeController().appendNews()
                                })
                            })
                        })
                    }
                })
            }
            }.resume()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.Background(alpha: 1.0)
        self.tabBarController?.delegate = self
        
        //check connection
        checkInternetConnection()
        checkCouchConnection()
        //
        setupView()
    }
}

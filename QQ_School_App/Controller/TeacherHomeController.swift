//
//  TeacherHomeController.swift
//  QQ_School_App
//
//  Created by Abby Esteves on 09/05/2018.
//  Copyright © 2018 Abby Esteves. All rights reserved.
//

import Foundation
import MarqueeLabel

class TeacherHomeController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UIScrollViewDelegate, UITextFieldDelegate, UITextViewDelegate{
    
    let manager = CBLManager.sharedInstance()
    var activityIndicator : UIActivityIndicatorView = UIActivityIndicatorView()
    var ArticlesArray = Array<Any>()
    var NewsArray = [News]()
    var subject_codes = [String]()
    var currentSubject_Code = String()
    var currentSubject = Subjects()
    var cellId = "cellId"
    var timerNews : Timer?
    var timer : Timer?
    var MainScrollView = UIScrollView()
    var pageNumber = 0
    var countNews = 0
    var countNextSubject = 0
    var schedule = ApiService().scheduleList()
    var scheduleToday = ApiService().schduleToday()
    var classes = ApiService().classes(day: FileService().getDateTime(format: "EEEE"))
    var attendances = ApiService().attendanceList()
    var attendanceScroll = UIScrollView()
    var didLoaded = false
    var titleName = String()
    let navbarStatusHeight = UIApplication.shared.statusBarFrame.height
    let bottomSpacing = UIApplication.shared.keyWindow?.safeAreaInsets.bottom
    let tabBarHeight = UITabBarController.init().tabBar.frame.height
    
    lazy var sideMenuLauncher: SideMenuLauncher = {
        let launcher = SideMenuLauncher()
        return launcher
    }()
    
    lazy var webDetail : WebDetail = {
        let launcher = WebDetail()
        return launcher
    }()
    
    lazy var profileController: ProfileController = {
        let launcher = ProfileController()
        return launcher
    }()
    
    lazy var newsDetail: NewsDetail = {
        let launcher = NewsDetail()
        return launcher
    }()
    
    var keyboardHeight = CGFloat()
    
    let collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = 30
        layout.scrollDirection = .vertical
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.backgroundColor = .clear
        return cv
    }()
    
    let WelcomeLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.ThemeDark(alpha: 1.0)//rgba(red: 85, green: 22, blue: 145, alpha: 1.0)
        label.numberOfLines = 3
        label.textAlignment = .right
        label.font = UIFont.boldSystemFont(ofSize: label.font.pointSize)
        label.font = label.font.withSize(20)
        return label
    }()
    
    let welcomeView : UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.white
        view.layer.shadowOpacity = 0.5
        view.layer.shadowOffset = CGSize(width: 0, height: 1)
        view.layer.shadowRadius = 1
        view.layer.cornerRadius = 3
        return view
    }()
    
    let SchedView : UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.layer.shadowOpacity = 0.5
        view.layer.shadowOffset = CGSize(width: 0, height: 1)
        view.layer.shadowRadius = 1
        view.layer.cornerRadius = 3
        return view
    }()
    
    let scanView : UIView = {
        let view = UIView()
        view.backgroundColor = .clear
        return view
    }()
    
    let totalStudentsLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.lightGray
        label.numberOfLines = 2
        label.textAlignment = .center
        label.text = "Students"
        label.font = label.font.withSize(11)
        return label
    }()
    
    let absentStudentsLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.lightGray
        label.numberOfLines = 2
        label.textAlignment = .center
        label.text = "Absent"
        label.font = label.font.withSize(11)
        return label
    }()
    
    let presentStudentsLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.lightGray
        label.numberOfLines = 2
        label.textAlignment = .center
        label.text = "Present"
        label.font = label.font.withSize(11)
        return label
    }()
    
    let classroomTotalLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.Warning(alpha: 1.0)
        label.numberOfLines = 2
        label.textAlignment = .center
        label.font = label.font.withSize(30)
        label.adjustsFontSizeToFitWidth = true
        label.text = "0"
        return label
    }()
    
    let classroomAbsentLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.Alert(alpha: 1.0)
        label.numberOfLines = 2
        label.textAlignment = .center
        label.font = label.font.withSize(30)
        label.adjustsFontSizeToFitWidth = true
        label.text = "0"
        return label
    }()
    
    let classroomPresentLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.Accept(alpha: 1.0)
        label.numberOfLines = 2
        label.textAlignment = .center
        label.font = label.font.withSize(30)
        label.adjustsFontSizeToFitWidth = true
        label.text = "0"
        return label
    }()
    
    let classroomTitleLabel: UILabel = {
        let label = UILabel()
        label.text = "Classroom View"
        label.textColor = UIColor.darkGray
        label.numberOfLines = 2
        label.font = label.font.withSize(30)
        label.adjustsFontSizeToFitWidth = true
        return label
    }()
    
    let classroomDescLabel: UITextView = {
        let text = UITextView()
        text.text = "Look into class seating"
        text.textColor = UIColor.gray
        text.isSelectable = false
        text.isEditable = false
        text.isScrollEnabled = false
        text.font = text.font?.withSize(13)
        return text
    }()
    
    let classroomImage: UIImageView = {
        let image = UIImageView()
        image.image = UIImage(named: "classroom")
        return image
    }()
    
    let classroomView : UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.white
        view.layer.shadowOpacity = 0.5
        view.layer.shadowOffset = CGSize(width: 0, height: 1)
        view.layer.shadowRadius = 1
        view.layer.cornerRadius = 3
        return view
    }()
    
    let uploadTitleLabel: UILabel = {
        let label = UILabel()
        label.text = "Upload Modules"
        label.textColor = UIColor.darkGray
        label.numberOfLines = 2
        label.font = label.font.withSize(30)
        label.adjustsFontSizeToFitWidth = true
        return label
    }()
    
    let uploadDescLabel: UITextView = {
        let text = UITextView()
        text.text = "Upload reviewers and quizzes"
        text.textColor = UIColor.gray
        text.isSelectable = false
        text.isEditable = false
        text.isScrollEnabled = false
        text.font = text.font?.withSize(13)
        return text
    }()
    
    let uploadImage: UIImageView = {
        let image = UIImageView()
        image.image = UIImage(named: "uploaddownload")
        return image
    }()
    
    let uploadView : UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.white
        view.layer.shadowOpacity = 0.5
        view.layer.shadowOffset = CGSize(width: 0, height: 1)
        view.layer.shadowRadius = 1
        view.layer.cornerRadius = 3
        return view
    }()
    
    let createExamTitleLabel: UILabel = {
        let label = UILabel()
        label.text = "Create Exams"
        label.textColor = UIColor.darkGray
        label.numberOfLines = 2
        label.font = label.font.withSize(30)
        label.adjustsFontSizeToFitWidth = true
        return label
    }()
    
    let createExamDescLabel: UITextView = {
        let text = UITextView()
        text.text = "Online exam creation"
        text.textColor = UIColor.gray
        text.isSelectable = false
        text.isEditable = false
        text.isScrollEnabled = false
        text.font = text.font?.withSize(13)
        return text
    }()
    
    let createExamImage: UIImageView = {
        let image = UIImageView()
        image.image = UIImage(named: "exam")
        return image
    }()
    
    let createExamView : UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.white
        view.layer.shadowOpacity = 0.5
        view.layer.shadowOffset = CGSize(width: 0, height: 1)
        view.layer.shadowRadius = 1
        view.layer.cornerRadius = 3
        return view
    }()
    
    let attendanceLabel: UILabel = {
        let label = UILabel()
        label.text = "Attendance".uppercased()
        label.textColor = UIColor.gray
        label.numberOfLines = 2
        label.textAlignment = .center
        label.font = label.font.withSize(13)
        return label
    }()
    
    let scanImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.clipsToBounds = true
        imageView.backgroundColor = .clear
        imageView.image = UIImage(named: "ic_qr")
        imageView.layer.masksToBounds = true
        return imageView
    }()
    
//    let timeLabel: UILabel = {
//        let label = UILabel()
//        label.textColor = UIColor.gray
//        label.numberOfLines = 2
//        label.textAlignment = .center
//        label.text = "\(ApiService().getDateTime(format : "h:mm a").uppercased())".uppercased()
//        label.font = label.font.withSize(11)
//        return label
//    }()
    
    let dayLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.lightGray
        label.alpha = 0.8
        label.numberOfLines = 2
        label.textAlignment = .left
        label.text = "\(FileService().getDateTime(format : "EEEE"))".uppercased()
        label.font = label.font.withSize(13)
        return label
    }()
    
    let monthLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.gray
        label.numberOfLines = 2
        label.textAlignment = .left
        label.font = label.font.withSize(30)
        label.text = "\(FileService().getDateTime(format : "MMM dd"))".uppercased()
        return label
    }()
    
    let dateLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.rgba(red: 25, green: 118, blue: 210, alpha: 1.0)
        label.numberOfLines = 2
        label.textAlignment = .center
//        label.font = UIFont.boldSystemFont(ofSize: label.font.pointSize)
        label.font = label.font.withSize(30)
        label.text = "\(FileService().getDateTime(format : "dd"))"
        return label
    }()
    
    let timerLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.lightGray
        label.numberOfLines = 2
        label.alpha = 0.8
        label.textAlignment = .right
        label.font = label.font.withSize(11)
        return label
    }()
    
    let ifCurrentLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.lightGray
        label.text = "Current Class"
        label.numberOfLines = 2
        label.alpha = 0.8
        label.font = label.font.withSize(11)
        return label
    }()
    
    let timerCountLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.ThemeDarkGreen(alpha: 1.0)
        label.numberOfLines = 2
        label.textAlignment = .right
        label.font = label.font.withSize(28)
        return label
    }()
    
    let headerView : UIView = {
        let view = UIView()
        view.backgroundColor = .black
        view.alpha = 0.5
        return view
    }()
    
    let HeaderLabel: UITextView = {
        let textView = UITextView()
        textView.text = "sample header"
        textView.textColor = UIColor.white
        textView.backgroundColor = UIColor.clear
        textView.isScrollEnabled = false
        textView.isEditable = false
        textView.font = textView.font?.withSize(15)
        return textView
    }()
    
    
    let currentNotesActivitiesText: UITextView = {
        let textView = UITextView()
        textView.backgroundColor = UIColor.white
        textView.textColor = UIColor.gray
        textView.layer.borderWidth = 1.0
        textView.layer.borderColor = UIColor.rgba(red: 215, green: 215, blue: 215, alpha: 0.8).cgColor
        textView.isEditable = false
        textView.isSelectable = false
        textView.isScrollEnabled = true
        textView.layer.cornerRadius = 5
        textView.font = textView.font?.withSize(13)
        return textView
    }()
    
    let currentLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.gray
        label.numberOfLines = 2
        label.textAlignment = .left
        label.font = label.font.withSize(13)
        return label
    }()
    
    let currentCodeLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.lightGray
        label.numberOfLines = 2
        label.font = label.font.withSize(12)
        label.font = UIFont.boldSystemFont(ofSize: label.font.pointSize)
        label.text = "no current subject".uppercased()
        return label
    }()

    let currentGradeSecLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.gray
        label.numberOfLines = 2
        label.textAlignment = .left
        label.font = label.font.withSize(13)
        return label
    }()

    let currentTimeLabel: UILabel = {
        let label = UILabel()
        label.text = ""
        label.textColor = UIColor.ThemeOrange(alpha: 1.0)
        label.numberOfLines = 2
        label.textAlignment = .right
        label.font = label.font.withSize(15)
        return label
    }()
    
    //@objc
    @objc func goToScanner() {
        DispatchQueue.main.async(execute: {
            let titleLabel : UILabel = {
                let label = UILabel()
                label.textColor = UIColor.white
                label.font = UIFont(name: "GillSans-Bold", size: UIFont.systemFontSize)!
                label.font = label.font.withSize(15)
                return label
            }()
            
            let scanQRLauncher = ScanQRLauncher()
            self.navigationController?.pushViewController(scanQRLauncher, animated: true)
//            print(" bug scann ", "\((ApiService().myID()["user_type"])!)")
            // current time subject code TEACHER
//                print(" currentSubject_Code ", self.currentSubject_Code)
            if "\(self.currentSubject_Code)" == "" {
                titleLabel.text = "scan qr".uppercased()
                scanQRLauncher.navigationItem.title = "no_subject".uppercased()
            } else {
                titleLabel.text = "\(self.currentSubject_Code) attendance".uppercased()
                scanQRLauncher.navigationItem.title = "\(self.currentSubject_Code)"
            }
            
            scanQRLauncher.navigationItem.titleView = titleLabel
        })
    }
    
    @objc func goToSubject() {
        let layout = UICollectionViewFlowLayout()
        let subjectsController = SubjectsController(collectionViewLayout: layout)
        HomeTabBarController().setupGoTo(to: subjectsController, title: "\(sideMenuLauncher.menuLabels[3].label)", from: self)
    }
    
    @objc func getTime() {
        classes = ApiService().classes(day: FileService().getDateTime(format: "EEEE"))
        monthLabel.text = "\(FileService().getDateTime(format : "MMM dd").uppercased())"
        dateLabel.text = "\(FileService().getDateTime(format : "dd"))"
//        dayLabel.text = "\(ApiService().getDateTime(format : "EEEE"))".uppercased()
        
        // get current and Next subjects
        if classes.count != 0 {
             for (index, subject) in classes.enumerated() {
                let minsAllotted = 1800 // second = 30 minutes
                let condition = Int(subject.time_start_long)! <= Int(Date().timeIntervalSince1970) && Int(Date().timeIntervalSince1970) <=  Int(subject.time_end_long)!
                
                var allotted = Int(subject.time_start_long)!-minsAllotted <= Int(Date().timeIntervalSince1970) && Int(Date().timeIntervalSince1970) <  Int(subject.time_start_long)!
                
                if index > 0 {
                    let next =  classes[Int(index-1)]
                    let minsBetween = Int(next.time_start_long)!-Int(subject.time_end_long)!
                    
                    allotted = Int(subject.time_start_long)!-minsAllotted <= Int(Date().timeIntervalSince1970) && Int(Date().timeIntervalSince1970) <  Int(subject.time_start_long)! && minsBetween <= minsAllotted
                }
                
                let classEnded = Int(classes[classes.count-1].time_end_long)! <= Int(Date().timeIntervalSince1970)
                
                let classNext = Int(subject.time_end_long)! <= Int(Date().timeIntervalSince1970) && index < classes.count-1
                
                let classFirst = Int(classes[0].time_start_long)!-minsAllotted > Int(Date().timeIntervalSince1970)
                
                if classNext {
                    let next =  classes[Int(index+1)]
                    
                    self.assign(subject: next)
                    self.getClassSeatingTotal()
                    self.getAttendance()
                    
                    currentCodeLabel.textColor = UIColor.lightGray
                    currentCodeLabel.font = currentCodeLabel.font.withSize(30)
                    ifCurrentLabel.font = currentCodeLabel.font.withSize(15)
                    ifCurrentLabel.textColor = UIColor.ThemeDarkGreen(alpha: 1.0)
                    currentTimeLabel.textColor = UIColor.lightGray
                    
                    ifCurrentLabel.text = "Prepare for Class"
                    
                    timerLabel.text = ""
                    timerCountLabel.text = ""
                    currentLabel.text = next.subject
                    currentCodeLabel.text = next.subject_code.uppercased()
                    currentTimeLabel.text = "\((next.time_start)!) - \((next.time_end)!)"
                    classroomDescLabel.text = "Grade: \((subject.grade_lvl)!) - Section: \((subject.section)!)"
                    self.currentTasksDues(subject_code : next.subject_code, prepare: true, end: false)
                }
                
                if classFirst {
                    let first = classes[0]
                    currentCodeLabel.textColor = UIColor.lightGray
                    currentCodeLabel.font = currentCodeLabel.font.withSize(30)
                    ifCurrentLabel.font = currentCodeLabel.font.withSize(15)
                    ifCurrentLabel.textColor = UIColor.ThemeDarkGreen(alpha: 1.0)
                    currentTimeLabel.textColor = UIColor.lightGray
                    
                    ifCurrentLabel.text = "First class today"
                    timerLabel.text = ""
                    currentLabel.text = first.subject
                    currentCodeLabel.text = first.subject_code.uppercased()
                    currentTimeLabel.text = "\((first.time_start)!) - \((first.time_end)!)"
                    self.currentTasksDues(subject_code : first.subject_code, prepare: false, end: true)
                }
                
                if allotted {

                    self.assign(subject: subject)
                    self.getClassSeatingTotal()
                    self.getAttendance()

                    currentCodeLabel.textColor = UIColor.lightGray
                    currentCodeLabel.font = currentCodeLabel.font.withSize(30)
                    ifCurrentLabel.font = currentCodeLabel.font.withSize(15)
                    ifCurrentLabel.textColor = UIColor.ThemeDarkGreen(alpha: 1.0)
                    currentTimeLabel.textColor = UIColor.lightGray

                    ifCurrentLabel.text = "Prepare for Class"

                    timerLabel.text = "Next Class starts in..."
                    currentLabel.text = subject.subject
                    currentCodeLabel.text = subject.subject_code.uppercased()
                    currentTimeLabel.text = "\((subject.time_start)!) - \((subject.time_end)!)"
                    classroomDescLabel.text = "Grade: \((subject.grade_lvl)!) - Section: \((subject.section)!)"
                    
                    self.timerCountDown(classEnd: Int(subject.time_start_long)!, color: UIColor.Warning(alpha: 1.0))
                    self.currentTasksDues(subject_code : subject.subject_code, prepare: true, end : false)
                }

                
                if classEnded {
                    self.currentSubject = Subjects()
                    currentSubject_Code = String()
                    var day = 86400
                    var isNext = "Tomorrow's"
                    for _ in schedule  {
                        let nextClasses = ApiService().classes(day: FileService().epochConvertString(date: "\(Int(Date().timeIntervalSince1970)+day)", format: "EEEE"))
                        
                        if nextClasses.count != 0 {
                            let firstClass = nextClasses[0]
                            currentCodeLabel.textColor = UIColor.lightGray
                            currentCodeLabel.font = currentCodeLabel.font.withSize(30)
                            ifCurrentLabel.font = currentCodeLabel.font.withSize(15)
                            ifCurrentLabel.textColor = UIColor.ThemeDarkGreen(alpha: 1.0)
                            currentTimeLabel.textColor = UIColor.lightGray
    
                            ifCurrentLabel.text = "\(isNext) First Class"
    
                            timerLabel.text = ""
                            timerCountLabel.text = ""
                            currentLabel.text = firstClass.subject
                            currentCodeLabel.text = firstClass.subject_code.uppercased()
                            currentTimeLabel.text = "\((firstClass.time_start)!) - \((firstClass.time_end)!)"
                            self.currentTasksDues(subject_code : firstClass.subject_code, prepare: true, end: false)
                        } else {
                            day = day + 86400
                            isNext = "\(FileService().epochConvertString(date: "\(Int(Date().timeIntervalSince1970)+day)", format: "EEEE"))'s"
                        }
                    }
                }
                
                if condition {
                    ifCurrentLabel.textColor = UIColor.lightGray
                    ifCurrentLabel.font = currentCodeLabel.font.withSize(11)
                    currentCodeLabel.textColor = UIColor.darkGray
                    currentCodeLabel.font = currentCodeLabel.font.withSize(30)
                    currentTimeLabel.textColor = UIColor.ThemeOrange(alpha: 1.0)
                    
                    if subject.subject_code != "LUNCH" {
                        
                        self.assign(subject: subject)
                        self.timerCountDown(classEnd: Int(subject.time_end_long)!, color: UIColor.ThemeDarkGreen(alpha: 1.0))
                        self.currentTasksDues(subject_code : subject.subject_code, prepare: false, end : false)
                        self.getClassSeatingTotal()
                        self.getAttendance()

                        
                        currentLabel.text = subject.subject
                        currentTimeLabel.text = "\((subject.time_start)!) - \((subject.time_end)!)"
                        currentCodeLabel.text = subject.subject_code.uppercased()
                        classroomDescLabel.text = "Grade: \((subject.grade_lvl)!) - Section: \((subject.section)!)"
                        timerLabel.text = "Remaining Time"
                        ifCurrentLabel.text = "Current Class"
                        
                    } else {
                        self.currentSubject = Subjects()
                        classroomDescLabel.text = ""
                        currentSubject_Code = String()
                        currentCodeLabel.text = "Lunch break".uppercased()
                        currentNotesActivitiesText.text = ""
                    }
                    
                    if index < classes.count-1 {
                        let next =  classes[Int(index+1)]
                        self.nextSubject(subject_code: " \((next.subject_code)!)", subject: "\((next.subject)!)", time: "\((next.time_start)!) / \((next.time_end)!)")
                    }

                }
                
                
            }
        } else {
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                self.ifCurrentLabel.text = "No schedule available"
                self.currentCodeLabel.text = "Go to your Configurator.".uppercased()
                self.currentLabel.text = "Setting up tips..."
                self.currentNotesActivitiesText.text = "1. Go to your Configuration, Check if your schedule is set.\n2. When you have already configured your Schedule, Click save. Your app would get your schedule momentarily.\n3. Check your connection.\n4. If configuration is not applied, Contact your administrator for assistance."
            }, completion: { (Bool) in })
        }
        countNextSubject = countNextSubject + 1
    }
    
    private func assign(subject: Subjects) {
        currentSubject_Code = subject.subject_code
        self.currentSubject.time_start = subject.time_start
        self.currentSubject.time_end = subject.time_end
        self.currentSubject.subject_code = subject.subject_code
        self.currentSubject.subject = subject.subject
        self.currentSubject.teacher = subject.teacher
        self.currentSubject.teacher_id = subject.teacher_id
        self.currentSubject.grade_lvl = subject.grade_lvl
        self.currentSubject.section = subject.section
    }
    
    private func currentTasksDues(subject_code : String, prepare: Bool, end : Bool) {
        let tasks = ApiService().taskList().filter({$0.subject_code == subject_code})
        var taskString = String()
        
        if prepare {
            currentNotesActivitiesText.text = "Preparations you can do...\n\n1. Check Attendances, Click scanner & scan student's QRs\n2. Monitor class Seating, Click Classroom view shows who's in class.\n3. Create online Exams, Give your students something to do at home.\n4. Upload Modules, Upload your reviewers."
        } else if end {
            currentNotesActivitiesText.text = "Preparations you can do...\n\n1. Create online Exams, Give your students something to do at home.\n2. Upload Modules, Upload your reviewers.\n3. Add task and notes for guidelines."
        } else {
            currentNotesActivitiesText.text = "No tasks or dues as of now... Go to task list and add"
        }
        
        
        if tasks.count > 0 {
            for (index, task) in tasks.enumerated() {
                taskString = "\(taskString)\(index+1). \((task.title)!), \((task.description)!)\n"
            }
            currentNotesActivitiesText.text = "Tasks / dues...\n\n\(taskString)"
//            if taskString != "" {
//                SchedView.addSubview(currentNotesActivitiesText)
//                currentNotesActivitiesText.frame = CGRect(x:  10, y: currentLabel.frame.maxY+10, width: SchedView.frame.width-20, height: SchedView.frame.height-(currentLabel.frame.height+currentCodeLabel.frame.height+currentGradeSecLabel.frame.height+currentTimeLabel.frame.height)-25)
//            }
        }
    }
    
    private func timerCountDown(classEnd: Int, color: UIColor) {
        let coutdown = FileService().countdown(classEnd: classEnd) as [String:Any]
//        print(" timerCountLabel.text ", coutdown)
        let hour = Int("\((coutdown["hours"])!)")
        let minute = Int("\((coutdown["minutes"])!)")
        
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.timerCountLabel.textColor = color
        }, completion: { (Bool) in })
        
        if color == UIColor.ThemeDarkGreen(alpha: 1.0) {
            if minute! < 10 && minute! > 15 && hour == 0 {
                UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                    self.timerCountLabel.textColor = UIColor.Warning(alpha: 1.0)
                }, completion: { (Bool) in })
            } else if minute! < 15 && hour == 0 {
                UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                    self.timerCountLabel.textColor = UIColor.Alert(alpha: 1.0)
                }, completion: { (Bool) in })
            }
        }
        
        
        timerCountLabel.text = "\((coutdown["hours"])!):\((coutdown["minutes"])!):\((coutdown["seconds"])!)"
    }
    
    private func nextSubject(subject_code: String, subject: String, time: String) {
        if countNextSubject == 60 {
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                self.WelcomeLabel.frame = CGRect(x: self.WelcomeLabel.frame.minX, y: self.WelcomeLabel.frame.minY, width: self.WelcomeLabel.frame.width, height: 0)
                self.dayLabel.frame = CGRect(x: self.dayLabel.frame.minX, y: self.dayLabel.frame.minY, width: self.dayLabel.frame.width, height: 0)
            }, completion: { (Bool) in
                UIView.animate(withDuration: 0.5, delay: 1, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                    self.WelcomeLabel.frame = CGRect(x: self.WelcomeLabel.frame.minX, y: self.WelcomeLabel.frame.minY, width: self.WelcomeLabel.frame.width, height: self.welcomeView.frame.height-10)
                    self.dayLabel.frame = CGRect(x: self.dayLabel.frame.minX, y: self.dayLabel.frame.minY, width: self.dayLabel.frame.width, height: 20)
                    self.dayLabel.text = "Next Class  —  "
                    self.dayLabel.textAlignment = .right
                    self.WelcomeLabel.textColor = UIColor.ThemeDarkGreen(alpha: 1.0)
                    self.WelcomeLabel.textAlignment = .left
                    self.WelcomeLabel.font = self.WelcomeLabel.font.withSize(13)
                    if subject_code == "LUNCH" {
                        self.WelcomeLabel.text = " \(subject) \n  \(time)"
                    } else {
                        self.WelcomeLabel.text = " \(subject_code) \n  \(subject) \n  \(time)"
                    }
                }, completion: { (Bool) in })
            })
        }
        else if countNextSubject == 70 {
            self.revertNextClass()
        }
        // highlight
//        if countNextSubject == 7 {
//            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
//                self.WelcomeLabel.backgroundColor = UIColor.Highlight(alpha: 1.0)
//            }, completion: { (Bool) in
//                UIView.animate(withDuration: 2, delay: 2, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
//                    self.WelcomeLabel.backgroundColor = UIColor.white
//                }, completion: { (Bool) in })
//            })
//        }
    }
    
    private func revertNextClass() {
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.WelcomeLabel.frame = CGRect(x: self.WelcomeLabel.frame.minX, y: self.WelcomeLabel.frame.minY, width: self.WelcomeLabel.frame.width, height: 0)
            self.dayLabel.frame = CGRect(x: self.dayLabel.frame.minX, y: self.dayLabel.frame.minY, width: self.dayLabel.frame.width, height: 0)
        }, completion: { (Bool) in
                UIView.animate(withDuration: 0.5, delay: 1, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                self.WelcomeLabel.frame = CGRect(x: self.WelcomeLabel.frame.minX, y: self.WelcomeLabel.frame.minY, width: self.WelcomeLabel.frame.width, height: self.welcomeView.frame.height-10)
                self.dayLabel.frame = CGRect(x: self.dayLabel.frame.minX, y: self.dayLabel.frame.minY, width: self.dayLabel.frame.width, height: 20)
                self.dayLabel.textAlignment = .left
                self.dayLabel.text = "\(FileService().getDateTime(format : "EEEE"))".uppercased()
                self.WelcomeLabel.backgroundColor = .clear
                self.WelcomeLabel.textColor = UIColor.ThemeDark(alpha: 1.0)
                self.WelcomeLabel.textAlignment = .right
                self.WelcomeLabel.font = UIFont.boldSystemFont(ofSize: self.WelcomeLabel.font.pointSize)
                self.WelcomeLabel.font = self.WelcomeLabel.font.withSize(20)
                self.WelcomeLabel.text = "Welcome, \(self.titleName). \((ApiService().myID()["lastname"])!)"
            }, completion: { (Bool) in
                self.countNextSubject = 0
            })
        })
    }
    
    @objc func handleMenu(){
        self.sideMenuLauncher.showMenu()
    }
    
    @objc func handleHome() {
        if "\((ApiService().myQR()["t"])!)" == "1" {
            let teacherHomeController = TeacherHomeController()
            navigationController?.pushViewController(teacherHomeController, animated: true)
        } else {
            let homeController = HomeController()
            navigationController?.pushViewController(homeController, animated: true)
        }
    }
    
    @objc func autoScroll() {
        //        print(" index ", pageNumber, ArticlesArray.count)
        if pageNumber == ArticlesArray.count-1 {
            let database = try! manager.databaseNamed("news")
            pageNumber = 0
            countNews = Int(database.documentCount)
            let indexPath = NSIndexPath(item: pageNumber, section: 0)
            collectionView.scrollToItem(at: indexPath as IndexPath, at: [], animated: false)
        } else {
            pageNumber = pageNumber + 1
            countNews = countNews - 1
            let indexPath = NSIndexPath(item: pageNumber, section: 0)
            collectionView.scrollToItem(at: indexPath as IndexPath, at: [], animated: true)
        }
        
    }
    
    @objc func goToCreateExam() {
        if currentSubject_Code != "" {
            let titleLabel : UILabel = {
                let label = UILabel()
                label.textColor = UIColor.white
                label.font = UIFont(name: "GillSans-Bold", size: UIFont.systemFontSize)!
                label.font = label.font.withSize(15)
                label.text = "create exam for \(self.currentSubject_Code)".uppercased()
                return label
            }()
            
            let createExamDetail = CreateExamDetail()
            self.navigationController?.pushViewController(createExamDetail, animated: true)
            
            createExamDetail.navigationItem.title = "\(self.currentSubject_Code) DocumentId".uppercased()
            createExamDetail.navigationItem.titleView = titleLabel
        }
    }
    
    
    @objc func goToSeatingPlan() {
        if currentSubject_Code != "" {
            let manager = CBLManager.sharedInstance()
            let database = try! manager.databaseNamed("seatplan")
            
            let document = database.document(withID: "CLASS_SEAT_\(self.currentSubject_Code.uppercased())_GR:\((self.currentSubject.grade_lvl)!)_SEC:\((self.currentSubject.section)!)")!
            let properties = document.properties
            
            if properties != nil {
                
                DispatchQueue.main.async(execute: {
                    let titleLabel : UILabel = {
                        let label = UILabel()
                        label.textColor = UIColor.white
                        label.font = UIFont(name: "GillSans-Bold", size: UIFont.systemFontSize)!
                        label.font = label.font.withSize(15)
                        label.text = "\(self.currentSubject_Code) classroom view".uppercased()
                        return label
                    }()
                    
                    let seatPlanDetail = SeatPlanDetail()
                    self.navigationController?.pushViewController(seatPlanDetail, animated: true)
                    seatPlanDetail.navigationItem.title = "\(self.currentSubject_Code) \(document.documentID)"
                    seatPlanDetail.navigationItem.titleView = titleLabel
                })
            } else {
                view.showToast(message: "Go to your Configuration and add students for class \(self.currentSubject_Code.uppercased()).")
//                DispatchQueue.main.async(execute: {
//                    let titleLabel : UILabel = {
//                        let label = UILabel()
//                        label.textColor = UIColor.white
//                        label.font = UIFont(name: "GillSans-Bold", size: UIFont.systemFontSize)!
//                        label.font = label.font.withSize(15)
//                        label.text = "\(self.currentSubject_Code) class seating".uppercased()
//                        return label
//                    }()
//
//                    let createSeatPlanDetail = CreateSeatPlanDetail()
//                    self.navigationController?.pushViewController(createSeatPlanDetail, animated: true)
//                    createSeatPlanDetail.navigationItem.title = "\(self.currentSubject_Code)".uppercased()
//                    createSeatPlanDetail.navigationItem.titleView = titleLabel
//                })
            }
        }
    }
    
    private func getAttendance() {
        let present = attendances.filter { $0.scan_subj == "parent" && $0.scan_date == FileService().getDateTime(format: "MM/dd/yyyy")}.count
        classroomPresentLabel.text = "\(present)"
        classroomAbsentLabel.text = "\(Int(classroomTotalLabel.text!)!-present)"
//        print(" getAttendance ", present)
    }
    
    private func getClassSeatingTotal() {
        let manager = CBLManager.sharedInstance()
        let database = try! manager.databaseNamed("seatplan")
        let document = database.document(withID: "CLASS_SEAT_\(self.currentSubject_Code.uppercased())_GR:\((self.currentSubject.grade_lvl)!)_SEC:\((self.currentSubject.section)!)")!
        let properties = document.properties
        
        if properties != nil {
            let row =  properties!["seat_row"]!
            let column =  properties!["seat_column"]!
            let estimateTotal = Int("\(row)")!*Int("\(column)")!
            var total = 0
            
            for num in 1...estimateTotal+1 {
                if properties!["\(num)"] != nil {
                    total = num
                }
            }
//            print(" getClassSeatingTotal ",  total)
            classroomTotalLabel.text = "\(total)"
        } else {
            classroomTotalLabel.text = "0"
        }
    }
    
    private func watch() {
        //schedule
        let mydata = try! manager.databaseNamed("mydata")
        NotificationCenter.default.addObserver(forName: NSNotification.Name.cblDatabaseChange, object: mydata, queue: nil) {
            (notification) -> Void in
            self.schedule = ApiService().scheduleList()
            if "\((ApiService().myID()["status"])!)" == "0" {
                let loginController = LoginController()
                self.navigationController?.pushViewController(loginController, animated: true)
            }
            
            if let changes = notification.userInfo!["changes"] as? [CBLDatabaseChange] {
                for change in changes {
                    print("schedule , revision ID '%@'", change.description)
                }
            }
        }
        
        //news
        self.news(channel: ["QQSCHOOL_NEWS"], database: "news")
        
        //attendance
        let attendance = try! manager.databaseNamed("attendance")
        NotificationCenter.default.addObserver(forName: NSNotification.Name.cblDatabaseChange, object: attendance, queue: nil) {
            (notification) -> Void in
            if let changes = notification.userInfo!["changes"] as? [CBLDatabaseChange] {
                for change in changes {
                    print("attendance , revision ID '%@'", change.description)
                    self.attendances = ApiService().attendanceList()
                }
            }
        }
        
        //status
        self.watchStatus()
    }
    
    func watchStatus() {
        let manager = CBLManager.sharedInstance()
        let database = try! manager.databaseNamed("mydata")
        NotificationCenter.default.addObserver(forName: NSNotification.Name.cblDatabaseChange, object: database, queue: nil) {
            (notification) -> Void in
            //            if let changes = notification.userInfo!["changes"] as? [CBLDatabaseChange] {
            if "\((ApiService().myID()["status"])!)" == "0" {
                if let window = UIApplication.shared.keyWindow {
                    window.showToast(message: "Session Expired.\nLogging out...")
                }
                HomeTabBarController().statusBarBackground.removeFromSuperview()
                HomeTabBarController().statusBarConnection.removeFromSuperview()
                
                self.timer?.invalidate()
                self.timerNews?.invalidate()
                
                let loginController = LoginController()
                self.navigationController?.pushViewController(loginController, animated: true)
            }
        }
    }
    
    private func setupView() {
        self.headerView.removeFromSuperview()
        titleName = (String("\((ApiService().myQR()["gen"])!)").fromBase64())! == "male" ? "Mr" : "Ms"
        WelcomeLabel.text = "Welcome, \(self.titleName). \((ApiService().myID()["lastname"])!)"
        
        collectionView.backgroundColor = UIColor.Background(alpha: 1.0)
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.isPagingEnabled = true
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.isScrollEnabled = false
        collectionView.register(NewsCell.self, forCellWithReuseIdentifier: cellId)
        
        activityIndicator.center = self.view.center
        activityIndicator.hidesWhenStopped = true
        activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.gray
        
        MainScrollView = UIScrollView(frame: view.bounds)
        MainScrollView.delegate = self
        MainScrollView.showsVerticalScrollIndicator = true
        MainScrollView.backgroundColor = UIColor.Background(alpha: 1.0)
        
        view.addSubview(MainScrollView)
        MainScrollView.addSubview(collectionView)
        MainScrollView.addSubview(activityIndicator)
        activityIndicator.startAnimating()
        
        MainScrollView.addSubview(SchedView)
        MainScrollView.addSubview(welcomeView)
        welcomeView.addSubview(WelcomeLabel)
        welcomeView.addSubview(dateLabel)
        welcomeView.addSubview(monthLabel)
        welcomeView.addSubview(dayLabel)
        
        MainScrollView.addSubview(classroomView)
        classroomView.addSubview(classroomImage)
        classroomView.addSubview(classroomTitleLabel)
        classroomView.addSubview(classroomDescLabel)
        
        classroomView.addSubview(classroomTotalLabel)
        classroomView.addSubview(totalStudentsLabel)
        classroomView.addSubview(classroomAbsentLabel)
        classroomView.addSubview(absentStudentsLabel)
        classroomView.addSubview(classroomPresentLabel)
        classroomView.addSubview(presentStudentsLabel)
        
        MainScrollView.addSubview(uploadView)
        uploadView.addSubview(uploadImage)
        uploadView.addSubview(uploadTitleLabel)
//        uploadView.addSubview(uploadDescLabel)
        
        MainScrollView.addSubview(createExamView)
        createExamView.addSubview(createExamImage)
        createExamView.addSubview(createExamTitleLabel)
//        createExamView.addSubview(createExamDescLabel)
        
        MainScrollView.addSubview(scanView)
        scanView.addSubview(scanImageView)
        
        //date format color
        let dateString = "\(FileService().getDateTime(format : "MMM dd").uppercased())" as NSString
        let dateMutable = NSMutableAttributedString(string: dateString as String, attributes: nil)
        dateMutable.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.rgba(red: 25, green: 118, blue: 210, alpha: 1.0), range: NSRange(location: "\(FileService().getDateTime(format : "MMM").uppercased())".count, length: "\(FileService().getDateTime(format : "dd"))".count+1))
        dateMutable.addAttribute(NSAttributedStringKey.font, value: UIFont.boldSystemFont(ofSize: 30), range: NSRange(location: "\(FileService().getDateTime(format : "MMM").uppercased())".count, length: "\(FileService().getDateTime(format : "dd"))".count+1))
        monthLabel.attributedText = dateMutable
    }
    
    
    
    private func setupConstraints() {
        var height = view.frame.height-(60+navbarStatusHeight+bottomSpacing!+tabBarHeight)
        MainScrollView.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height)

        scanView.frame = CGRect(x: view.frame.width-55, y: 0, width: 50, height: 50)
        scanImageView.frame = CGRect(x: 5, y: 5, width: 40, height: 40)
        
        collectionView.frame = CGRect(x: 0, y: 0, width: view.frame.width-scanView.frame.width-5, height: 50)
        activityIndicator.frame = CGRect(x: 0, y: collectionView.frame.height/2-10, width: view.frame.width, height: 20)
        welcomeView.frame = CGRect(x: 10, y: collectionView.frame.maxY+10, width: view.frame.width-20, height: 60)
        dayLabel.frame = CGRect(x: 10, y: 5, width: welcomeView.frame.width/3, height: 20)
        monthLabel.frame = CGRect(x: 10, y: dayLabel.frame.maxY, width: welcomeView.frame.width/3, height: welcomeView.frame.height-dayLabel.frame.height-10)
        WelcomeLabel.frame = CGRect(x: monthLabel.frame.maxX, y: 5, width: welcomeView.frame.width-monthLabel.frame.width-20, height: welcomeView.frame.height-10)
        
        height = height-collectionView.frame.height-(welcomeView.frame.height+120+100+40)
        
        // start adding in scroll main
        MainScrollView.contentSize = CGSize(width: view.frame.width, height: view.frame.height)
        
        SchedView.frame = CGRect(x: 10, y: welcomeView.frame.maxY+10, width: view.frame.width-20, height: height)
        
        classroomView.frame = CGRect(x: 10, y: SchedView.frame.maxY+10, width: view.frame.width-20, height: 120)
        classroomImage.frame = CGRect(x: 10, y: (classroomView.frame.height/2)-((classroomView.frame.width/3-60)/2), width: classroomView.frame.width/3-60, height: classroomView.frame.width/3-60)
        classroomTitleLabel.frame = CGRect(x: classroomImage.frame.maxX+10, y: 5, width: classroomView.frame.width-30-classroomImage.frame.width, height: classroomView.frame.height/2-25)
        classroomDescLabel.frame = CGRect(x: classroomImage.frame.maxX+10, y: classroomTitleLabel.frame.maxY-5, width: classroomView.frame.width-20-20-classroomImage.frame.width, height: 25)
        
        classroomTotalLabel.frame = CGRect(x: classroomImage.frame.maxX+10, y: classroomDescLabel.frame.maxY, width: ((classroomView.frame.width-classroomImage.frame.height)-20)/3, height: classroomView.frame.height-(classroomDescLabel.frame.height+classroomTitleLabel.frame.height+40)+10)
        classroomPresentLabel.frame = CGRect(x: classroomTotalLabel.frame.maxX, y: classroomDescLabel.frame.maxY, width: ((classroomView.frame.width-classroomImage.frame.height)-20)/3, height: classroomTotalLabel.frame.height)
        classroomAbsentLabel.frame = CGRect(x: classroomPresentLabel.frame.maxX, y: classroomDescLabel.frame.maxY, width: ((classroomView.frame.width-classroomImage.frame.height)-20)/3, height: classroomTotalLabel.frame.height)
       
        totalStudentsLabel.frame = CGRect(x: classroomImage.frame.maxX+10, y: classroomView.frame.height-30, width: classroomTotalLabel.frame.width, height: 20)
        presentStudentsLabel.frame = CGRect(x: totalStudentsLabel.frame.maxX, y: classroomView.frame.height-30, width: classroomPresentLabel.frame.width, height: 20)
        absentStudentsLabel.frame = CGRect(x: presentStudentsLabel.frame.maxX, y: classroomView.frame.height-30, width: classroomAbsentLabel.frame.width, height: 20)
    
        uploadView.frame = CGRect(x: 10, y: classroomView.frame.maxY+10, width: view.frame.width/2-15, height: 100)
        uploadImage.frame = CGRect(x: 10, y: (uploadView.frame.height/2)-((uploadView.frame.width/3-10)/2), width: uploadView.frame.width/3-10, height: uploadView.frame.width/3-10)
        uploadTitleLabel.frame = CGRect(x: uploadImage.frame.maxX+10, y: 0, width: uploadView.frame.width-30-uploadImage.frame.width, height: uploadView.frame.height)
//        uploadDescLabel.frame = CGRect(x: uploadImage.frame.maxX+10, y: uploadTitleLabel.frame.maxY-10, width: uploadView.frame.width-20-20-uploadImage.frame.width, height: uploadView.frame.height/2+5)
        
        createExamView.frame = CGRect(x: uploadView.frame.maxX+10, y: classroomView.frame.maxY+10, width: view.frame.width/2-15, height: 100)
        createExamImage.frame = CGRect(x: 10, y: (createExamView.frame.height/2)-((createExamView.frame.width/3-10)/2), width: createExamView.frame.width/3-10, height: createExamView.frame.width/3-10)
        createExamTitleLabel.frame = CGRect(x: createExamImage.frame.maxX+10, y: 0, width: createExamView.frame.width-30-createExamImage.frame.width, height: createExamView.frame.height)
//        createExamDescLabel.frame = CGRect(x: createExamImage.frame.maxX+10, y: createExamTitleLabel.frame.maxY-10, width: createExamView.frame.width-20-20-createExamImage.frame.width, height: createExamView.frame.height/2)
        
        
    }
    
    private func setupSchedule() {
        self.timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.getTime), userInfo: nil, repeats: true)

        
//        SchedView.addSubview(dateLabel)
//        SchedView.addSubview(monthLabel)
//        SchedView.addSubview(dayLabel)
//        SchedView.addSubview(timeLabel)
        SchedView.addSubview(timerCountLabel)
        SchedView.addSubview(timerLabel)
//        SchedView.addSubview(boarder)
        SchedView.addSubview(currentLabel)
        SchedView.addSubview(currentCodeLabel)
        SchedView.addSubview(ifCurrentLabel)
//        SchedView.addSubview(currentGradeSecLabel)
        SchedView.addSubview(currentTimeLabel)
        SchedView.addSubview(currentNotesActivitiesText)
        
        
//        dayLabel.frame = CGRect(x: 10, y: 10, width: 100, height: 15)
//        monthLabel.frame = CGRect(x: 10, y: dayLabel.frame.maxY+5, width: 100, height: 20)
//        dateLabel.frame = CGRect(x: 10, y: monthLabel.frame.maxY+5, width: 100, height: 30)
//        timeLabel.frame = CGRect(x: 10, y: dateLabel.frame.maxY+5, width: 70, height: 15)
    
        
//        let width = SchedView.frame.width-timerLabel.frame.width+40
        
//        boarder.frame = CGRect(x: 10, y: 10, width: 0.5, height: dayLabel.frame.height+dateLabel.frame.height+15)
        ifCurrentLabel.frame = CGRect(x: 10, y: 5, width: SchedView.frame.width/2, height: 20)
        currentCodeLabel.frame = CGRect(x: 10, y: ifCurrentLabel.frame.maxY+5, width: SchedView.frame.width/2, height: 25)
        currentTimeLabel.frame = CGRect(x: SchedView.frame.width/2, y: 10, width: SchedView.frame.width/2-10, height: 15)
        timerLabel.frame = CGRect(x: SchedView.frame.width/2, y: currentTimeLabel.frame.maxY+5, width: SchedView.frame.width/2-10, height: 15)
        timerCountLabel.frame = CGRect(x: SchedView.frame.width/2, y: timerLabel.frame.maxY+5, width: SchedView.frame.width/2-10, height: 23)
        
        currentLabel.frame = CGRect(x: 10, y: currentCodeLabel.frame.maxY, width: SchedView.frame.width-timerLabel.frame.width, height: 40)
//        currentGradeSecLabel.frame = CGRect(x: 10, y: currentLabel.frame.maxY+5, width: currentLabel.frame.width, height: 0)
        currentNotesActivitiesText.frame = CGRect(x:  10, y: currentLabel.frame.maxY, width: SchedView.frame.width-20, height: SchedView.frame.height-(currentLabel.frame.height+currentCodeLabel.frame.height+currentGradeSecLabel.frame.height+currentTimeLabel.frame.height)-25)
        
//        currentNotesActivitiesText.frame = CGRect(x:  10, y: currentTimeLabel.frame.maxY+10, width: SchedView.frame.width-(timerCountLabel.frame.width+taskBoarder.frame.width+35), height: taskBoarder.frame.height-15)
        
        //marquee
        let aFrame  = CGRect(x: 0, y: collectionView.frame.maxY, width: collectionView.frame.width, height: 20)
        let marqueeLabel = MarqueeLabel.init(frame: aFrame, rate: 30.0, fadeLength: 20.0)
        marqueeLabel.textColor = UIColor.gray
        marqueeLabel.numberOfLines = 2
        marqueeLabel.text = ""
        marqueeLabel.textAlignment = .left
        marqueeLabel.font = marqueeLabel.font.withSize(11)
        
//        MainScrollView.addSubview(marqueeLabel)
//        WelcomeLabel.frame = CGRect(x: 10, y: marqueeLabel.frame.maxY, width: view.frame.width-20, height: 22)
        
        // append event string
        let database = try! manager.databaseNamed("events")
        let query = database.createAllDocumentsQuery()
        
        do {
            let result = try query.run()
            if result != nil {
                while let row = result.nextRow(){
                    let document = row.document?.properties
                    if "\((document!["type"])!)" == "event" {
                        let formatToDate = DateFormatter()
                        formatToDate.dateFormat = "MM/dd/yyyy"
                        formatToDate.locale = Locale.current
                        let formatter = DateFormatter()
                        formatter.dateFormat = "MMMM d"
                        formatter.locale = Locale.current
                        let date = formatToDate.date(from: "\((document!["date_from"])!)")
                        
                        var marqueeText = "\((marqueeLabel.text)!)"
                        marqueeText.append("\(formatter.string(from: date!)) - \((document!["title"])!)   •  ".uppercased())
                        //                        print(" document event : ", (document!["date_from"])!, formatter.string(from: date!))
                        marqueeLabel.text = marqueeText
                    }
                }
            }
            var marqueeText = "\((marqueeLabel.text)!)"
            marqueeText.append("april 28 - School App releasing   •   april 13 - Lailatul Isra Wal Mi Raj   •   May 1 - Labor Day   •   June 12 - Independence Day   •   June 16 - Eidul-Fitar   •   June 16 - June Solstice   •   August 21 - Ninoy Aquino day   •   August 22 - Eid al-Adha (Feast of the Sacrifice)   •".uppercased())
            marqueeLabel.text = marqueeText
            
            
        } catch {
            print(" error getting announcements :", error)
        }
    }
    
    private func setupGestures(){
        SchedView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(goToSubject)))
        scanView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(goToScanner)))
        createExamView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(goToCreateExam)))
        classroomView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(goToSeatingPlan)))
    }
    

    func getApi(completion: @escaping () -> Void) {
        DispatchQueue.global().asyncAfter(deadline: .now()) {
            let urlString = "https://newsapi.org/v2/everything?domains=rappler.com&apiKey=5b58e21f40ff43128da960349a77b934"
            guard let url = URL(string: urlString) else { return }
            
            URLSession.shared.dataTask(with: url) { (data, response, err) in
                guard let data = data else { return }
                
                do {
                    let headlines = try JSONDecoder().decode(Headlines.self, from: data)
                    self.ArticlesArray = headlines.articles
                    completion()
                } catch let jsonErr{
                    print("ERROR Serializing: ", jsonErr)
                }
                
                }.resume()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return ArticlesArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! NewsCell
        cell.backgroundColor = UIColor.white
        cell.layer.shadowColor = UIColor.gray.cgColor
        cell.layer.shadowOpacity = 0.5
        cell.layer.shadowOffset = CGSize(width: 0, height: 3)
        cell.layer.shadowRadius = 2
        cell.layer.cornerRadius = 2
        cell.article = ArticlesArray[indexPath.item] as? Articles
        if self.countNews > 0 {
            cell.news = ArticlesArray[indexPath.item] as? News
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width, height: collectionView.frame.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if self.countNews > 0 {
            let news = ArticlesArray[indexPath.item] as! News
            newsDetail.showProfile(news : news)
        } else {
            let data = ArticlesArray[indexPath.item] as! Articles
            webDetail.open(url: data.url!, sourceName: data.source.name)
        }
    }
    
    func closeNewsTitle(){
        UIView.animate(withDuration: 0.5, delay: 7, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.headerView.frame = CGRect(x: 0, y: 0, width: self.collectionView.frame.width, height: 0)
            self.HeaderLabel.frame = CGRect(x: 10, y: 0, width: self.collectionView.frame.width-20, height: 0)
        }, completion: { (Bool) in
            self.headerView.removeFromSuperview()
            self.HeaderLabel.removeFromSuperview()
        })
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        //        print(" didEndDisplaying ", countNews, pageNumber )
        
//        if self.countNews > 0 {
//            let article = self.ArticlesArray[pageNumber] as! News
//        } else {
//            let article = self.ArticlesArray[pageNumber] as! Articles
//        }
        
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        self.closeNewsTitle()
    }
    
   
    @objc func keyboardWillShow(notification: NSNotification) {
        let height = self.navigationController?.navigationBar.frame.height
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            keyboardHeight = keyboardSize.height
            
            self.MainScrollView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height-keyboardSize.height)
            
            //scroll to bottom
            let topOffset = CGPoint(x : 0, y : 0)
            MainScrollView.setContentOffset(topOffset, animated: true)
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        let height = self.navigationController?.navigationBar.frame.height
        
        self.MainScrollView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
        self.MainScrollView.contentSize = CGSize(width: self.view.frame.width, height: self.view.frame.height)
       
    }
    
    func news(channel: [String], database: String){
        let database = try! manager.databaseNamed("\(database)")
        let pull = database.createPullReplication(ApiService().myURL())
        pull.channels = channel
        pull.continuous = true
        pull.start()
        
        NotificationCenter.default.addObserver(forName: NSNotification.Name.cblReplicationChange, object: pull, queue: nil) {
            (notification) -> Void in
            //determins if replication is over
            let active = pull.status != CBLReplicationStatus.active
            if active {
//                HomeTabBarController().checkCouchConnection()
                self.getNews()
                self.countNews = 0
                self.pageNumber = 0
                self.timerNews?.invalidate()
            }
        }
    }
    
    func appendNews(){
        let database = try! manager.databaseNamed("news")
        let query = database.createAllDocumentsQuery()
        countNews = Int(database.documentCount)
        //        print(" countNews ", countNews)
        do {
            //            try database.delete()
            let result = try query.run()
            if result != nil {
                while let row = result.nextRow(){
                    let document = row.document?.properties
                    
                    var news = News()
                    news.title = "\((document!["title"])!)"
                    news.desc = "\((document!["desc"])!)"
                    news.date = "\((document!["date"])!)"
                    news.b64 = "\((document!["b64"])!)"
                    news.author = "\((document!["author"])!)"
                    
                    ArticlesArray.insert(news, at: 0)
                    //                    ArticlesArray.insert(news, at: Int(ArticlesArray.count/2))
                }
            }
            self.collectionView.reloadData()
            
            UIView.animate(withDuration: 0.1, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                self.activityIndicator.stopAnimating()
                self.collectionView.reloadData()
                DispatchQueue.main.async(execute: {
                    //                    print(" News data : ", self.countNews)
                    if self.countNews > 0 {
//                        let article = self.ArticlesArray[0] as! News
//                        self.showNewsTitle(article: article.title)
                    } else {
//                        let article = self.ArticlesArray[0] as! Articles
//                        self.showNewsTitle(article: article.title)
                    }
                })
            }, completion:  { (Bool) in
                self.timerNews = Timer.scheduledTimer(timeInterval: 8, target: self, selector: #selector(self.autoScroll), userInfo: nil, repeats: true)
            })
            
        } catch {
            print(" error getting announcements :", error)
        }
        
    }
    
    private func getNews(){
        getApi {
            DispatchQueue.main.async(execute: {
                self.appendNews()
            })
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if didLoaded && NewsArray.count > 0{
            let database = try! manager.databaseNamed("news")
            countNews = Int(database.documentCount)
            pageNumber = 0
            let indexPath = NSIndexPath(item: pageNumber, section: 0)
            collectionView.scrollToItem(at: indexPath as IndexPath, at: [], animated: false)
            self.timerNews = Timer.scheduledTimer(timeInterval: 8, target: self, selector: #selector(self.autoScroll), userInfo: nil, repeats: true)
            self.timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.getTime), userInfo: nil, repeats: true)
        }

    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        //did load all
        didLoaded = true
        if didLoaded && NewsArray.count > 0 {
            DispatchQueue.main.async(execute: {
                self.timerNews?.invalidate()
                self.timer?.invalidate()
            })
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.white
        self.navigationController?.isNavigationBarHidden = false
        
        setupView()
        setupConstraints()
        setupGestures()
        watch()
        setupSchedule()
        
        NotificationCenter.default.addObserver(self, selector: #selector(HomeController.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(HomeController.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
}

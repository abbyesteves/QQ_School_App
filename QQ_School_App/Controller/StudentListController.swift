//
//  StudentListController.swift
//  QQ_School_App
//
//  Created by Abby Esteves on 11/05/2018.
//  Copyright © 2018 Abby Esteves. All rights reserved.
//

import UIKit

class StudentListController: UICollectionViewController, UICollectionViewDelegateFlowLayout, UITextViewDelegate {
    
    let manager = CBLManager.sharedInstance()
    var cellId = "cellId"
    var studentList = ApiService().studentList()
    var student_id = String()
    var subject_code = String()
    var MainScrollView = UIScrollView()
    var expanded = true
    var selectedDocumentID = String()
    var selectedIndexPath = IndexPath()
    let sorts = ["Seat Number", "Name A to Z", "Name Z to A", "Merits high to low", "Merits low to high", "Demerits high to low", "Demerits low to high"]
    var filters = [String]()
    var sortSelected = "Seat Number"
    var loaded = false
    var indexSelected = Int()
    var filterSelected = false
    
    let blackView : UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(white: 0, alpha: 0.6)
        return view
    }()
    
    let menu : UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.white
        view.layer.shadowColor = UIColor.gray.cgColor
        view.layer.shadowOpacity = 0.5
        view.layer.shadowOffset = CGSize(width: 0, height: 1)
        view.layer.shadowRadius = 1
        return view
    }()
    
    let optionView : UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.white
        view.layer.shadowColor = UIColor.gray.cgColor
        view.layer.shadowOpacity = 0.5
        view.layer.shadowOffset = CGSize(width: 0, height: 1)
        view.layer.shadowRadius = 1
        return view
    }()
    
    let filterImage: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.clipsToBounds = true
        imageView.backgroundColor = .clear
        imageView.layer.masksToBounds = true
        imageView.image = UIImage(named: "ic_filter")?.withRenderingMode(.alwaysTemplate)
        imageView.tintColor = UIColor.darkGray
        return imageView
    }()
    
    let filterCountLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.darkGray
        label.numberOfLines = 1
        label.layer.backgroundColor = UIColor.Background(alpha: 1.0).cgColor
        label.layer.cornerRadius = 10
        label.textAlignment = .center
        label.font = label.font.withSize(11)
        return label
    }()
    
    let filterLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.darkGray
        label.numberOfLines = 1
        label.font = label.font.withSize(13)
        label.text = "Filter"
        return label
    }()
    
    let filterView : UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.white
        return view
    }()
    
    let sortImage: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.clipsToBounds = true
        imageView.backgroundColor = .clear
        imageView.layer.masksToBounds = true
        imageView.image = UIImage(named: "ic_caret")?.withRenderingMode(.alwaysTemplate)
        imageView.tintColor = UIColor.gray
        return imageView
    }()
    
    let sortLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.darkGray
        label.numberOfLines = 1
        label.font = label.font.withSize(13)
        label.text = "Sort by Seat Number"
        return label
    }()
    
    let sortView : UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.white
        return view
    }()
    
    @objc func meritAdd() {
        let database = try! manager.databaseNamed("merits")
        let document = database.document(withID: selectedDocumentID)!
        var properties = document.properties
        let merits = Int("\((properties!["merits"])!)")!
        properties!["merits"] = "\(merits+1)"
        
        //update layout
        self.updateLayout(properties: properties!)

        do {
            try document.putProperties(properties!)
        } catch {
            print("Can't save update document in database")
            return
        }
    }
    
    @objc func meritMinus() {
        let database = try! manager.databaseNamed("merits")
        let document = database.document(withID: selectedDocumentID)!
        var properties = document.properties
        let merits = Int("\((properties!["merits"])!)")!

        if merits > 0  {
            properties!["merits"] = "\(merits-1)"
            
            //update layout
            self.updateLayout(properties: properties!)
        }

        do {
            try document.putProperties(properties!)
        } catch {
            print("Can't save update document in database")
            return
        }
    }
    
    @objc func demeritAdd() {
        let database = try! manager.databaseNamed("merits")
        let document = database.document(withID: selectedDocumentID)!
        var properties = document.properties
        let demerits = Int("\((properties!["demerits"])!)")!
        properties!["demerits"] = "\(demerits+1)"
        
        //update layout
        self.updateLayout(properties: properties!)

        do {
            try document.putProperties(properties!)
        } catch {
            print("Can't save update document in database")
            return
        }
    }

    @objc func demeritMinus() {
        let database = try! manager.databaseNamed("merits")
        let document = database.document(withID: selectedDocumentID)!
        var properties = document.properties
        let demerits = Int("\((properties!["demerits"])!)")!

        if demerits > 0  {
            properties!["demerits"] = "\(demerits-1)"
            
            //update layout
            self.updateLayout(properties: properties!)
        }

        do {
            try document.putProperties(properties!)
        } catch {
            print("Can't save update document in database")
            return
        }
    }
    
    @objc func sort() {
        removeMenuSubview()
        filterSelected = false
        menu.frame = CGRect(x: 0, y: optionView.frame.maxY+(0.5), width: optionView.frame.width, height: 0)
        var y = CGFloat(0)
        var height = CGFloat(0)
        
        for (index, sort) in sorts.enumerated() {
            let menuView : UIView = {
                let view = UIView()
                view.backgroundColor = UIColor.white
                view.layer.shadowColor = UIColor.gray.cgColor
                view.layer.shadowOpacity = 0.5
                view.layer.shadowOffset = CGSize(width: 0, height: 1)
                view.layer.shadowRadius = 1
                return view
            }()
            
            let menuLabel: UILabel = {
                let label = UILabel()
                label.textColor = UIColor.darkGray
                label.numberOfLines = 1
                label.font = label.font.withSize(13)
                label.text = sort
                return label
            }()
            
            if sort == sortSelected {
                menuLabel.textColor = UIColor.ThemeOrange(alpha: 1.0)
            }
            
            menu.addSubview(menuView)
            menuView.addSubview(menuLabel)
            
            menuView.frame = CGRect(x: 0, y: Int(y), width: Int(optionView.frame.width), height: 40)
            menuLabel.frame = CGRect(x: 20, y: 0, width: menuView.frame.width-40, height: menuView.frame.height)
            
            y = y + menuView.frame.height
            height = height + menuView.frame.height
            
            menuView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.selectorSort(sender:))))
            
            if index == sorts.count-1 {
                UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                    self.blackView.alpha = 1
                    self.sortImage.transform = CGAffineTransform(rotationAngle: 3.2)
                    self.menu.frame = CGRect(x: 0, y: self.menu.frame.minY, width: self.menu.frame.width, height: height)
                }, completion: { (Bool) in })
            }
        }
    }
    
    @objc func selectorSort(sender: UITapGestureRecognizer) {
        let label = sender.view!.subviews[0] as? UILabel
        self.resetSort()
        self.sortLabel.text = "Sort by \((label?.text)!)"
        self.sortSelected = "\((label?.text)!)"
        label?.textColor = UIColor.ThemeOrange(alpha: 1.0)
        self.close()
        self.sortApply(sort: "\((label?.text)!)")
    }
    
    private func resetSort() {
        for views in menu.subviews {
            let label = views.subviews[0] as? UILabel
            label?.textColor = UIColor.darkGray
        }
    }
    
    private func removeMenuSubview() {
        for views in menu.subviews {
            views.removeFromSuperview()
        }
    }
    
    @objc func filter() {
        removeMenuSubview()
        filterSelected = true
        menu.frame = CGRect(x: view.frame.width, y: optionView.frame.maxY+(0.5), width: (view.frame.width/2)+30, height: view.frame.height-optionView.frame.height)
        
        let genderLabel: UILabel = {
            let label = UILabel()
            label.textColor = UIColor.lightGray
            label.numberOfLines = 1
            label.font = label.font.withSize(13)
            label.text = "Gender"
            return label
        }()
        
        let maleButton: UIButton = {
            let button = UIButton()
            button.backgroundColor = filters.contains("male".capitalized) ? UIColor.Highlight(alpha: 0.3) : UIColor.Background(alpha: 1.0)
            button.layer.borderWidth = filters.contains("male".capitalized) ? 1 : 0
            button.layer.borderColor = UIColor.ThemeDark(alpha: 1.0).cgColor
            button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 15)
            button.setTitleColor(UIColor.darkGray, for: .normal)
            button.setTitle("male".capitalized, for: .normal)
            return button
        }()
        
        let femaleButton: UIButton = {
            let button = UIButton()
            button.backgroundColor = filters.contains("female".capitalized) ? UIColor.Highlight(alpha: 0.3) : UIColor.Background(alpha: 1.0)
            button.layer.borderWidth = filters.contains("female".capitalized) ? 1 : 0
            button.layer.borderColor = UIColor.ThemeDark(alpha: 1.0).cgColor
            button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 15)
            button.setTitleColor(UIColor.darkGray, for: .normal)
            button.setTitle("female".capitalized, for: .normal)
            return button
        }()
        
        let boarder1 : UIView = {
            let view = UIView()
            view.backgroundColor = UIColor.Background(alpha: 1.0)
            return view
        }()
        
        let topLabel: UILabel = {
            let label = UILabel()
            label.textColor = UIColor.lightGray
            label.numberOfLines = 1
            label.font = label.font.withSize(13)
            label.text = "Top 10"
            return label
        }()
        
        let meritsButton: UIButton = {
            let button = UIButton()
            button.backgroundColor = filters.contains("merit".capitalized) ? UIColor.Highlight(alpha: 0.3) : UIColor.Background(alpha: 1.0)
            button.layer.borderWidth = filters.contains("merit".capitalized) ? 1 : 0
            button.layer.borderColor = UIColor.ThemeDark(alpha: 1.0).cgColor
            button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 15)
            button.setTitleColor(UIColor.darkGray, for: .normal)
            button.setTitle("merit".capitalized, for: .normal)
            return button
        }()
        
        let demeritsButton: UIButton = {
            let button = UIButton()
            button.backgroundColor = filters.contains("demerit".capitalized) ? UIColor.Highlight(alpha: 0.3) : UIColor.Background(alpha: 1.0)
            button.layer.borderWidth = filters.contains("demerit".capitalized) ? 1 : 0
            button.layer.borderColor = UIColor.ThemeDark(alpha: 1.0).cgColor
            button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 15)
            button.setTitleColor(UIColor.darkGray, for: .normal)
            button.setTitle("demerit".capitalized, for: .normal)
            return button
        }()
        
        let boarder2 : UIView = {
            let view = UIView()
            view.backgroundColor = UIColor.Background(alpha: 1.0)
            return view
        }()
        
        let attendanceLabel: UILabel = {
            let label = UILabel()
            label.textColor = UIColor.lightGray
            label.numberOfLines = 1
            label.font = label.font.withSize(13)
            label.text = "Attendance"
            return label
        }()
        
        let absentButton: UIButton = {
            let button = UIButton()
            button.backgroundColor = filters.contains("absent".capitalized) ? UIColor.Highlight(alpha: 0.3) : UIColor.Background(alpha: 1.0)
            button.layer.borderWidth = filters.contains("absent".capitalized) ? 1 : 0
            button.layer.borderColor = UIColor.ThemeDark(alpha: 1.0).cgColor
            button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 15)
            button.setTitleColor(UIColor.darkGray, for: .normal)
            button.setTitle("absent".capitalized, for: .normal)
            return button
        }()
        
        let presentButton: UIButton = {
            let button = UIButton()
            button.backgroundColor = filters.contains("present".capitalized) ? UIColor.Highlight(alpha: 0.3) : UIColor.Background(alpha: 1.0)
            button.layer.borderWidth = filters.contains("present".capitalized) ? 1 : 0
            button.layer.borderColor = UIColor.ThemeDark(alpha: 1.0).cgColor
            button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 15)
            button.setTitleColor(UIColor.darkGray, for: .normal)
            button.setTitle("present".capitalized, for: .normal)
            return button
        }()
        
        let excusedButton: UIButton = {
            let button = UIButton()
            button.backgroundColor = filters.contains("absence with reason".capitalized) ? UIColor.Highlight(alpha: 0.3) : UIColor.Background(alpha: 1.0)
            button.layer.borderWidth = filters.contains("absence with reason".capitalized) ? 1 : 0
            button.layer.borderColor = UIColor.ThemeDark(alpha: 1.0).cgColor
            button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 15)
            button.setTitleColor(UIColor.darkGray, for: .normal)
            button.setTitle("absence with reason".capitalized, for: .normal)
            return button
        }()
        
        menu.addSubview(genderLabel)
        menu.addSubview(femaleButton)
        menu.addSubview(maleButton)
        menu.addSubview(boarder1)
        menu.addSubview(topLabel)
        menu.addSubview(meritsButton)
        menu.addSubview(demeritsButton)
        menu.addSubview(boarder2)
        menu.addSubview(attendanceLabel)
        menu.addSubview(presentButton)
        menu.addSubview(absentButton)
        menu.addSubview(excusedButton)
        
        
        
        genderLabel.frame = CGRect(x: 20, y: 20, width: menu.frame.width-40, height: 30)
        femaleButton.frame = CGRect(x: 20, y: genderLabel.frame.maxY+10, width: 70, height: 30)
        maleButton.frame = CGRect(x: femaleButton.frame.maxX+5, y: genderLabel.frame.maxY+10, width: 55, height: 30)
        
        boarder1.frame = CGRect(x: 0, y: maleButton.frame.maxY+20, width: menu.frame.width, height: 1)
        
        topLabel.frame = CGRect(x: 20, y: femaleButton.frame.maxY+30, width: menu.frame.width-40, height: 30)
        meritsButton.frame = CGRect(x: 20, y: topLabel.frame.maxY+10, width: 55, height: 30)
        demeritsButton.frame = CGRect(x: meritsButton.frame.maxX+5, y: topLabel.frame.maxY+10, width: 70, height: 30)
        
        boarder2.frame = CGRect(x: 0, y: demeritsButton.frame.maxY+20, width: menu.frame.width, height: 1)
        
        attendanceLabel.frame = CGRect(x: 20, y: demeritsButton.frame.maxY+30, width: menu.frame.width-40, height: 30)
        presentButton.frame = CGRect(x: 20, y: attendanceLabel.frame.maxY+10, width: 70, height: 30)
        absentButton.frame = CGRect(x: presentButton.frame.maxX+10, y: attendanceLabel.frame.maxY+10, width: 70, height: 30)
        excusedButton.frame = CGRect(x: 20, y: absentButton.frame.maxY+10, width: 180, height: 30)
        
        femaleButton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(filterAdd(sender:))))
        maleButton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(filterAdd(sender:))))
        demeritsButton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(filterAdd(sender:))))
        meritsButton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(filterAdd(sender:))))
        presentButton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(filterAdd(sender:))))
        absentButton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(filterAdd(sender:))))
        excusedButton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(filterAdd(sender:))))
        
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.blackView.alpha = 1
            self.menu.frame = CGRect(x: self.view.frame.width-self.menu.frame.width, y: self.menu.frame.minY, width: self.menu.frame.width, height: self.menu.frame.height)
        }, completion: { (Bool) in})
    }
    
    @objc func filterAdd(sender: UITapGestureRecognizer) {
        let button = sender.view as! UIButton
        let filter = (button.titleLabel?.text)!
        
        let closeImage: UIImageView = {
            let imageView = UIImageView()
            imageView.contentMode = .scaleAspectFit
            imageView.clipsToBounds = true
            imageView.backgroundColor = UIColor.ThemeDark(alpha: 1.0)
            imageView.layer.cornerRadius = 5
            imageView.layer.masksToBounds = true
            imageView.image = UIImage(named: "ic_close")?.withRenderingMode(.alwaysTemplate)
            imageView.tintColor = UIColor.white
            return imageView
        }()
        
        button.addSubview(closeImage)
        
        if filters.contains(filter) {
            button.subviews[1].removeFromSuperview()
            button.layer.borderWidth = 0
            button.layer.backgroundColor = UIColor.Background(alpha: 1.0).cgColor
            filters = filters.filter { $0 != filter }
        } else {
            closeImage.frame = CGRect(x: button.frame.width-5, y: -5, width: 10, height: 10)
            button.layer.borderColor = UIColor.ThemeDark(alpha: 1.0).cgColor
            button.layer.borderWidth = 1
            button.layer.backgroundColor = UIColor.Highlight(alpha: 0.3).cgColor
            filters.append(filter)
        }
        
        filterCountLabel.text = "\(filters.count)"
        self.seeFilterCount()
        self.filterApply(filter : filter)
    }
    private func filterApply(filter : String) {
        if filter == "Female" {
            studentList = ApiService().studentList().filter { $0.gender == "female" }
        } else if filter == "Male" {
            studentList = ApiService().studentList().filter { $0.gender == "male" }
        }
        
    }
    
    private func seeFilterCount() {
        if filters.count == 0 {
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                self.filterCountLabel.alpha = 0
            }, completion: { (Bool) in})
        } else {
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                self.filterCountLabel.alpha = 1
            }, completion: { (Bool) in})
        }
    }
    
    @objc func close() {
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.removeSort()
        }, completion: { (Bool) in
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                self.blackView.alpha = 0
                self.sortImage.transform = CGAffineTransform(rotationAngle: 0)
                if self.filterSelected {
                    self.menu.frame = CGRect(x: self.view.frame.width, y: self.menu.frame.minY, width: self.menu.frame.width, height: self.menu.frame.height)
                } else {
                    self.menu.frame = CGRect(x: 0, y: self.menu.frame.minY, width: self.menu.frame.width, height: 0)
                }
            }, completion: { (Bool) in })
        })
    }
    
    private func removeSort() {
        for views in menu.subviews {
            views.removeFromSuperview()
        }
    }
    
    private func getStudentList() {
        studentList = ApiService().studentList().filter { $0.subj_code == subject_code }
        //seat pos
        studentList = studentList.sorted(by: { $0.seat_pos.compare($1.seat_pos) == .orderedAscending })
    }
    
    private func sortApply(sort: String) {
        studentList = ApiService().studentList().filter { $0.subj_code == subject_code }
        if sort == "Seat Number" {
            //seat pos
            studentList = studentList.sorted(by: { $0.seat_pos.compare($1.seat_pos) == .orderedAscending })
        } else if sort == "Merits low to high" {
            //Merits low to high
            studentList = studentList.sorted(by: { $0.merits.compare($1.merits) == .orderedAscending })
        } else if sort == "Merits high to low" {
            //Merits high to low
            studentList = studentList.sorted(by: { $0.merits.compare($1.merits) == .orderedDescending })
        } else if sort == "Demerits low to high" {
            //Demerits low to high
            studentList = studentList.sorted(by: { $0.demerits.compare($1.demerits) == .orderedAscending })
        } else if sort == "Demerits high to low" {
            //Demerits high to low
            studentList = studentList.sorted(by: { $0.demerits.compare($1.demerits) == .orderedDescending })
        } else if sort == "Name Z to A" {
            //Name Z to A
            studentList = studentList.sorted(by: { $0.name.compare($1.name) == .orderedDescending })
        } else if sort == "Name A to Z" {
            //Name A to Z
            studentList = studentList.sorted(by: { $0.name.compare($1.name) == .orderedAscending })
        }
        collectionView?.reloadData()
    }
    
    func textViewDidChange(_ textView: UITextView) {
        let database = try! manager.databaseNamed("merits")
        let document = database.document(withID: selectedDocumentID)!
        var properties = document.properties
        properties!["note"] = textView.text
        
        //update layout
        self.updateLayout(properties : properties!)

        do {
            try document.putProperties(properties!)
        } catch {
            print("Can't save update document in database")
            return
        }
    }
    
    private func setupView(){
        collectionView?.scrollsToTop = true
        collectionView?.contentInset = UIEdgeInsetsMake(20, 0, 20, 0)
        collectionView?.backgroundColor = UIColor.Background(alpha: 1.0)
        collectionView?.register(StudentListCell.self, forCellWithReuseIdentifier: cellId)
        
        self.seeFilterCount()
        self.filterCountLabel.text = "\(filters.count)"
        
        view.addSubview(blackView)
        view.addSubview(optionView)
        view.addSubview(filterView)
        view.addSubview(menu)
        filterView.addSubview(filterLabel)
        filterView.addSubview(filterImage)
        filterView.addSubview(filterCountLabel)
        view.addSubview(sortView)
        sortView.addSubview(sortLabel)
        sortView.addSubview(sortImage)
        
        blackView.alpha = 0
    }
    
    private func setupConstraints(){
        optionView.frame = CGRect(x: 0, y: 0, width: view.frame.height, height: 50)
        sortView.frame = CGRect(x: 0, y: 0, width: 200, height: optionView.frame.height)
        sortLabel.frame = CGRect(x: 15, y: 0, width: sortView.frame.width-20, height: sortView.frame.height)
        sortImage.frame = CGRect(x: sortLabel.frame.maxX+10, y: (sortView.frame.height/2)-(10), width: 20, height: 20)
        
        filterView.frame = CGRect(x: optionView.frame.width-100, y: 0, width: 100, height: optionView.frame.height)
        filterImage.frame = CGRect(x: 20, y: (filterView.frame.height/2)-(10), width: 20, height: 20)
        filterLabel.frame = CGRect(x: filterImage.frame.maxX+10, y: 0, width: filterView.frame.width-(filterImage.frame.width+20), height: filterView.frame.height)
        filterCountLabel.frame = CGRect(x: filterImage.frame.maxX-10, y: filterImage.frame.minY-10, width: 20, height: 20)
        
        collectionView?.frame = CGRect(x: 0, y: optionView.frame.maxY, width: view.frame.width, height: view.frame.height-optionView.frame.height)
        
        blackView.frame = CGRect(x: 0, y: optionView.frame.maxY, width: view.frame.height, height: view.frame.width)
    }
    
    private func setupGestures(){
        filterView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(filter)))
        sortView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(sort)))
        blackView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(close)))
    }
    
//    private func watchStudentList() {
//        let merits = try! manager.databaseNamed("merits")
//        getStudentList()
//        NotificationCenter.default.addObserver(forName: NSNotification.Name.cblDatabaseChange, object: merits, queue: nil) {
//            (notification) -> Void in
////            if let changes = notification.userInfo!["changes"] as? [CBLDatabaseChange] {
////                self.projectChanges(changes: changes)
////            }
//        }
//    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return studentList.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! StudentListCell
        cell.backgroundColor = UIColor.white
        cell.layer.shadowColor = UIColor.gray.cgColor
        cell.layer.shadowOpacity = 0.5
        cell.layer.shadowOffset = CGSize(width: 0, height: 1)
        cell.layer.shadowRadius = 1
        cell.layer.cornerRadius = 10
        cell.layer.borderColor = UIColor.ThemeDark(alpha: 1.0).cgColor
        cell.student = studentList[indexPath.item]
        
        if cell.student?.qq_id == student_id {
            cell.layer.borderWidth = 2
            if !loaded {
                let indexPath = NSIndexPath(item: 2, section: 0)
                collectionView.scrollToItem(at: indexPath as IndexPath, at: [], animated: true)
                
                UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                    cell.backgroundColor = UIColor.Highlight(alpha: 0.3)
                }, completion: { (Bool) in
                    UIView.animate(withDuration: 2, delay: 2, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                        cell.backgroundColor = UIColor.white
                    }, completion: { (Bool) in
                        self.loaded = true
                    })
                })
            }

        } else {
            cell.layer.borderWidth = 0
        }
        
        self.layout(cell: cell, indexPath: indexPath)
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        
        if studentList[indexPath.item].isExpanded! {
            return CGSize(width: view.frame.width-20, height: 270)
        }
        return CGSize(width: view.frame.width-20, height: 130)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 15
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if indexSelected != indexPath.item {
            indexSelected = indexPath.item
            selectedDocumentID = studentList[indexPath.item].documentID
            selectedIndexPath = indexPath
            self.expand(indexPath: indexPath)
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
    }
    
    func updateLayout(properties : [String: Any]) {
        //update layout
        let remarks = "\((properties["note"])!)" == "" ? "Everyone is smart in their own way" : "\((properties["note"])!)"
        let cell = collectionView?.cellForItem(at: selectedIndexPath) as! StudentListCell
        cell.meritsDemeritsLabel.attributedText = meritsDemeritsFormat(text : "Merits \((properties["merits"])!) / Demerits \((properties["demerits"])!)", indexPath: selectedIndexPath)
        cell.remarksLabel.text = "— \"\(remarks)\""
        
        //update badge
        self.updateBadge(cell: cell, merits: "\((properties["merits"])!)")
        
        // update array
        studentList[selectedIndexPath.item].note = "\((properties["note"])!)"
        studentList[selectedIndexPath.item].merits = "\((properties["merits"])!)"
        studentList[selectedIndexPath.item].demerits = "\((properties["demerits"])!)"
    }
    
    func updateBadge(cell: StudentListCell, merits: String) {
        //update badge
        if Int("\(merits)")! >= 0 && Int("\(merits)")! <= 9 {
            cell.badgeImage.image = UIImage(named: "badge_level_1")
            cell.badgeLabel.attributedText = badgeFormat(text: "Good \nLevel 1", indexPath: selectedIndexPath)
        } else if Int("\(merits)")! >= 10 && Int("\(merits)")! <= 15 {
            cell.badgeImage.image = UIImage(named: "badge_level_2")
            cell.badgeLabel.attributedText = badgeFormat(text: "Great \nLevel 2", indexPath: selectedIndexPath)
        } else if Int("\(merits)")! >= 16 && Int("\(merits)")! <= 25 {
            cell.badgeImage.image = UIImage(named: "badge_level_3")
            cell.badgeLabel.attributedText = badgeFormat(text: "Excellent \nLevel 3", indexPath: selectedIndexPath)
        }
    }
    
    func meritsDemeritsFormat(text : String, indexPath: IndexPath) -> NSMutableAttributedString {
        //merits format
        let meritsString = text as NSString
        let meritsMutable = NSMutableAttributedString(string: meritsString as String, attributes: nil)
        meritsMutable.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.Accept(alpha: 1.0), range: NSRange(location: 7, length: studentList[indexPath.item].merits.count))
        meritsMutable.addAttribute(NSAttributedStringKey.font, value: UIFont.boldSystemFont(ofSize: 21), range: NSRange(location: 7, length: studentList[indexPath.item].merits.count))
        //demerits format
        meritsMutable.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.AlertDark(alpha: 1.0), range: NSRange(location: text.count-studentList[indexPath.item].demerits.count, length: studentList[indexPath.item].demerits.count))
        meritsMutable.addAttribute(NSAttributedStringKey.font, value: UIFont.boldSystemFont(ofSize: 21), range: NSRange(location: text.count-studentList[indexPath.item].demerits.count, length: studentList[indexPath.item].demerits.count))
        return meritsMutable
    }
    
    func badgeFormat(text : String, indexPath: IndexPath) -> NSMutableAttributedString {
        //badge levels format
        let badgeString = text as NSString
        let badgeMutable = NSMutableAttributedString(string: badgeString as String, attributes: nil)
        badgeMutable.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.ThemeOrange(alpha: 1.0), range: NSRange(location: text.count-1, length: 1))
        badgeMutable.addAttribute(NSAttributedStringKey.font, value: UIFont.boldSystemFont(ofSize: 20), range: NSRange(location: text.count-1, length: 1))
        return badgeMutable
    }
    
    func layout(cell: StudentListCell, indexPath: IndexPath) {
        
        cell.meritsLabel.alpha = 0
        cell.demeritsLabel.alpha = 0
        cell.boarderHorizontalView.alpha = 0
        cell.boarderVerticalView.alpha = 0
        cell.meritsAddView.alpha = 0
        cell.meritsMinusView.alpha = 0
        cell.demeritsAddView.alpha = 0
        cell.demeritsMinusView.alpha = 0
        cell.remarksText.alpha = 0
        cell.remarkLabel.alpha = 0

        if studentList[indexPath.item].isExpanded! {
            cell.remarksText.delegate = self
            cell.meritsAddView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(meritAdd)))
            cell.meritsMinusView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(meritMinus)))
            cell.demeritsAddView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(demeritAdd)))
            cell.demeritsMinusView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(demeritMinus)))
            
            UIView.animate(withDuration: 1, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                cell.expandImage.transform = CGAffineTransform(rotationAngle: 3.2)
                cell.meritsAddView.alpha = 1
                cell.meritsLabel.alpha = 1
                cell.demeritsLabel.alpha = 1
                cell.boarderHorizontalView.alpha = 1
                cell.boarderVerticalView.alpha = 1
                cell.meritsMinusView.alpha = 1
                cell.demeritsAddView.alpha = 1
                cell.demeritsMinusView.alpha = 1
                cell.remarksText.alpha = 1
                cell.remarkLabel.alpha = 1
            }, completion: { (Bool) in })
        }
        
        // text attribute
        let comma : Character = ","
        let name = "\((cell.nameLabel.text)!)"
        let merits = "\((cell.meritsDemeritsLabel.text)!)"
        let seat = "\((cell.seatLabel.text)!)"
        let badge = "\((cell.badgeLabel.text)!)"
        if let idx = name.characters.index(of: comma) {
            let pos = name.characters.distance(from: name.startIndex, to: idx)
            
            //name format
            let nameString = name as NSString
            let nameMutable = NSMutableAttributedString(string: nameString as String, attributes: nil)
            nameMutable.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.darkGray, range: NSRange(location: 0, length: pos+1))
            nameMutable.addAttribute(NSAttributedStringKey.font, value: UIFont.boldSystemFont(ofSize: 23), range: NSRange(location: 0, length: pos+1))
            cell.nameLabel.attributedText = nameMutable
            
            //seat number format
            let seatString = seat as NSString
            let seatMutable = NSMutableAttributedString(string: seatString as String, attributes: nil)
            seatMutable.addAttribute(NSAttributedStringKey.font, value: UIFont.boldSystemFont(ofSize: 20), range: NSRange(location: seat.count-(studentList[indexPath.item].seat_pos.count), length: studentList[indexPath.item].seat_pos.count))
            cell.seatLabel.attributedText = seatMutable
            
            cell.badgeLabel.attributedText = badgeFormat(text : badge, indexPath: indexPath)
            
            cell.meritsDemeritsLabel.attributedText = meritsDemeritsFormat(text: merits, indexPath: indexPath)
        }
        
        // gate attendance indication
        if ApiService().getGateAttendance(studentID: (cell.student?.qq_id)!) {
            cell.thumbnailView.layer.borderColor = UIColor.Accept(alpha: 1.0).cgColor
        }
        
        //update badge
        self.updateBadge(cell: cell, merits: studentList[indexPath.item].merits)
    }
    
    func expand(indexPath: IndexPath) {
        let cell = collectionView?.cellForItem(at: indexPath) as! StudentListCell
        
        for (index, _) in studentList.enumerated() {
            if index == indexPath.item {
                studentList[index].isExpanded = true
            } else {
                studentList[index].isExpanded = false
            }
        }
        
        collectionView?.reloadData()
//        cell.meritsLabel.alpha = 0
//        cell.demeritsLabel.alpha = 0
//        cell.boarderHorizontalView.alpha = 0
//        cell.boarderVerticalView.alpha = 0
//        cell.meritsAddView.alpha = 0
//        cell.meritsMinusView.alpha = 0
//        cell.demeritsAddView.alpha = 0
//        cell.demeritsMinusView.alpha = 0
//        cell.remarksText.alpha = 0
//        cell.remarkLabel.alpha = 0
        
//        collectionView?.reloadItems(at: [indexPath])
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.Background(alpha: 1.0)
        
        subject_code = String(self.navigationItem.title!.dropFirst("\((ApiService().myQR()["i"])!)".count+1))
        student_id = String(self.navigationItem.title!.dropLast(subject_code.count+1))
        
        print(" subject_code :", subject_code)
        print(" student_id :", student_id)
        
        setupView()
        setupConstraints()
        setupGestures()
//        watchStudentList()
        getStudentList()
    }
}

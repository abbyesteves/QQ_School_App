//
//  HomeController.swift
//  QQ_School_App
//
//  Created by Abby Esteves on 01/03/2018.
//  Copyright © 2018 Abby Esteves. All rights reserved.
//

import UIKit
import MarqueeLabel

class HomeController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UIScrollViewDelegate, UITextFieldDelegate, UITextViewDelegate{
    
    static var currentChannel = "CHAT_\((ApiService().myID()["grade"])!)_\((ApiService().myID()["section"])!)"
    let manager = CBLManager.sharedInstance()
    var activityIndicator : UIActivityIndicatorView = UIActivityIndicatorView()
    var ArticlesArray = Array<Any>()
    var NewsArray = [News]()
    var ChatRooms = [String]()
    var Channels = [String]()
    var subject_codes = [String]()
    var currentSubject_Code = String()
    var cellId = "cellId"
    var timerNews : Timer?
    var timer : Timer?
    var MainScrollView = UIScrollView()
    var pageNumber = 0
    var countNews = 0
    var countIfGateAttendance = 0
    var didExpandChat = true
    var messages = [Messages]()
    var schedule = ApiService().scheduleList()//(ApiService().myID()["schedule"])! as! [String : Any]
    var scheduleToday = ApiService().schduleToday()
    var classes = ApiService().classes(day: FileService().getDateTime(format: "EEEE"))
    var chatScroll = UIScrollView()
    var attendanceScroll = UIScrollView()
    var chatChannelScroll = UIScrollView()
    var didLoaded = false
    var navbarStatusHeight = Int(UIApplication.shared.statusBarFrame.height)
    
    lazy var sideMenuLauncher: SideMenuLauncher = {
        let launcher = SideMenuLauncher()
        return launcher
    }()
    
    lazy var webDetail : WebDetail = {
        let launcher = WebDetail()
        return launcher
    }()
    
    lazy var profileController: ProfileController = {
        let launcher = ProfileController()
        return launcher
    }()
    
    lazy var newsDetail: NewsDetail = {
        let launcher = NewsDetail()
        return launcher
    }()
    
    var keyboardHeight = CGFloat()
    
    let collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = 30
        layout.scrollDirection = .horizontal
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.backgroundColor = .clear
        return cv
    }()
    
    let QRView : UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.layer.shadowOpacity = 0.5
        view.layer.shadowOffset = CGSize(width: 0, height: 1)
        view.layer.shadowRadius = 1
        view.layer.cornerRadius = 3
        return view
    }()
    
    let printButton: UIButton = {
        let button = UIButton()
        button.backgroundColor = UIColor.ThemeDarkGreen(alpha : 1.0)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 13)
        button.setTitleColor(UIColor.white, for: .normal)
        button.setTitle("print qr".uppercased(), for: .normal)
        return button
    }()
    
    let SchedView : UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.layer.shadowOpacity = 0.5
        view.layer.shadowOffset = CGSize(width: 0, height: 1)
        view.layer.shadowRadius = 1
        view.layer.cornerRadius = 3
        return view
    }()
    
    let QRImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.clipsToBounds = true
        imageView.backgroundColor = .clear
        imageView.layer.masksToBounds = true
        return imageView
    }()
    
    let scanView : UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.layer.shadowOpacity = 0.5
        view.layer.shadowOffset = CGSize(width: 0, height: 1)
        view.layer.shadowRadius = 1
        view.layer.cornerRadius = 3
        return view
    }()
    
    let scanLabel: UILabel = {
        let label = UILabel()
        label.text = "scan qr".uppercased()
        label.textColor = UIColor.gray
        label.numberOfLines = 10
        label.textAlignment = .center
        label.font = label.font.withSize(13)
        return label
    }()
    
    let attendanceView : UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.white
        view.layer.shadowOpacity = 0.5
        view.layer.shadowOffset = CGSize(width: 0, height: 1)
        view.layer.shadowRadius = 1
        view.layer.cornerRadius = 3
        return view
    }()
    
    let attendanceLabel: UILabel = {
        let label = UILabel()
        label.text = "Attendance".uppercased()
        label.textColor = UIColor.gray
        label.numberOfLines = 2
        label.textAlignment = .center
        label.font = label.font.withSize(13)
        return label
    }()
    
    let scanImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.clipsToBounds = true
        imageView.backgroundColor = .clear
        imageView.layer.masksToBounds = true
        return imageView
    }()
    
    let blackView : UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(white: 0, alpha: 0.6)
        return view
    }()
    
    let chatView : UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.rgba(red: 245, green: 245, blue: 255, alpha: 1.0)
        view.layer.shadowOpacity = 0.5
        view.layer.shadowOffset = CGSize(width: 0, height: 1)
        view.layer.shadowRadius = 1
        return view
    }()
    
    let chatText: UITextField = {
        let text = UITextField()
        text.borderStyle = .none
        text.backgroundColor = .white
        text.textColor = UIColor.gray
        text.font = .systemFont(ofSize: 13)
        var placeholder = NSMutableAttributedString()
        placeholder = NSMutableAttributedString(attributedString: NSAttributedString(string: "Your message...", attributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 13.0), .foregroundColor: UIColor.gray]))
        text.setLeftPadding(space: 8)
        text.attributedPlaceholder = placeholder
        return text
    }()
    
  
    let sendView : UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.white
        return view
    }()
    
    let sendImage: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.clipsToBounds = true
        imageView.backgroundColor = .clear
        imageView.layer.masksToBounds = true
        imageView.image = UIImage(named: "ic_send")?.withRenderingMode(.alwaysTemplate)
        imageView.tintColor = UIColor.ThemeDark(alpha: 1.0)
        return imageView
    }()
    
    let expandImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.clipsToBounds = true
        imageView.backgroundColor = UIColor.rgba(red: 215, green: 215, blue: 229, alpha: 1.0)
        imageView.layer.masksToBounds = true
        imageView.image = UIImage(named: "ic_maximize")?.withRenderingMode(.alwaysTemplate)
        imageView.tintColor = UIColor.darkGray
        return imageView
    }()
    
    let expandView : UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.rgba(red: 215, green: 215, blue: 229, alpha: 1.0)
        return view
    }()
    
    let createChatImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.clipsToBounds = true
        imageView.backgroundColor = UIColor.rgba(red: 215, green: 215, blue: 229, alpha: 1.0)
        imageView.layer.masksToBounds = true
        imageView.image = UIImage(named: "ic_add")?.withRenderingMode(.alwaysTemplate)
        imageView.tintColor = UIColor.darkGray
        return imageView
    }()
    
    let createChatView : UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.rgba(red: 215, green: 215, blue: 229, alpha: 1.0)
        return view
    }()
    
    let timeLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.gray
        label.numberOfLines = 2
        label.textAlignment = .center
        label.text = "\(FileService().getDateTime(format : "h:mm a").uppercased())".uppercased()
        label.font = label.font.withSize(11)
        return label
    }()
    
    let monthYearLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.gray
        label.numberOfLines = 2
        label.textAlignment = .center
        label.text = "\(FileService().getDateTime(format : "MMM dd,yy").uppercased())".uppercased()
        label.font = label.font.withSize(11)
        return label
    }()
    
    let dateLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.rgba(red: 25, green: 118, blue: 210, alpha: 1.0)
        label.numberOfLines = 2
        label.textAlignment = .center
        label.font = UIFont.boldSystemFont(ofSize: label.font.pointSize)
        label.font = label.font.withSize(23)
        label.text = "\(FileService().getDateTime(format : "E"))"
        return label
    }()
    
    let classNameText: UITextField = {
        let text = UITextField()
        text.borderStyle = .none
        text.backgroundColor = UIColor.Background(alpha: 1.0)
        text.textColor = UIColor.gray
        text.layer.shadowOpacity = 0.5
        text.layer.shadowOffset = CGSize(width: 0, height: 1)
        text.layer.shadowRadius = 1
        text.font = .systemFont(ofSize: 13)
        var placeholder = NSMutableAttributedString()
        placeholder = NSMutableAttributedString(attributedString: NSAttributedString(string: "Channel Name...", attributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 13.0), .foregroundColor: UIColor.gray]))
        text.setLeftPadding(space: 8)
        text.attributedPlaceholder = placeholder
        return text
    }()
    
    let createStatusLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.gray
        label.numberOfLines = 2
        label.textAlignment = .center
        label.font = label.font.withSize(13)
        return label
    }()
    
    let createLabel: UILabel = {
        let label = UILabel()
        label.text = "Names must be lowercased, without spaces or periods and must be shorter than 22 characters."
        label.textColor = UIColor.gray
        label.numberOfLines = 2
        label.font = label.font.withSize(13)
        return label
    }()
    
    let headerView : UIView = {
        let view = UIView()
        view.backgroundColor = .black
        view.alpha = 0.5
        return view
    }()
    
    let HeaderLabel: UITextView = {
        let textView = UITextView()
        textView.text = "sample header"
        textView.textColor = UIColor.white
        textView.backgroundColor = UIColor.clear
        textView.isScrollEnabled = false
        textView.isEditable = false
        textView.font = textView.font?.withSize(15)
        return textView
    }()
    
    let currentLabel: UILabel = {
        let label = UILabel()
        label.text = ". no subject".uppercased()
        label.textColor = UIColor.gray
        label.numberOfLines = 2
        label.textAlignment = .left
        label.font = label.font.withSize(11)
        return label
    }()
    
    let currentTimeLabel: UILabel = {
        let label = UILabel()
        label.text = ""
        label.textColor = UIColor.gray
        label.numberOfLines = 2
        label.textAlignment = .left
        label.font = label.font.withSize(11)
        return label
    }()
    
    let currentNotesLabel: UITextView = {
        let textView = UITextView()
        textView.text = "No tasks or activities set"
        textView.backgroundColor = UIColor.white
        textView.textColor = UIColor.darkGray
        textView.layer.borderWidth = 1.0
        textView.layer.borderColor = UIColor.lightGray.cgColor
        textView.isEditable = false
        textView.isSelectable = false
        textView.isScrollEnabled = false
        textView.layer.cornerRadius = 5
        textView.font = textView.font?.withSize(10)
        return textView
    }()
    
    let nextLabel: UILabel = {
        let label = UILabel()
        label.text = ". no subject".uppercased()
        label.textColor = UIColor.gray
        label.numberOfLines = 2
        label.textAlignment = .left
        label.font = label.font.withSize(11)
        return label
    }()
    
    let nextTimeLabel: UILabel = {
        let label = UILabel()
        label.text = ""
        label.textColor = UIColor.gray
        label.numberOfLines = 2
        label.textAlignment = .left
        label.font = label.font.withSize(11)
        return label
    }()
    
    let currentSubjectLabel: UILabel = {
        let label = UILabel()
        label.text = "Current Subject"
        label.textColor = UIColor.gray
        label.numberOfLines = 2
        label.textAlignment = .left
        label.font = label.font.withSize(11)
        return label
    }()
    
    //attendance
    
    let emptyAttendanceView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.clipsToBounds = true
        imageView.backgroundColor = .clear
        imageView.layer.masksToBounds = true
        imageView.image = UIImage(named: "ic_empty")?.withRenderingMode(.alwaysTemplate)
        imageView.tintColor = UIColor.lightGray
        imageView.alpha = 0.2
        return imageView
    }()
    
    let todaysAttendance : UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.rgba(red: 250, green: 250, blue: 250, alpha: 1.0)
        view.layer.shadowColor = UIColor.gray.cgColor
        view.layer.shadowOpacity = 0.5
        view.layer.shadowOffset = CGSize(width: 0, height: 1)
        view.layer.shadowRadius = 1
        return view
    }()
    
    let todaysLabel: UILabel = {
        let label = UILabel()
        label.text = "Gate Attendance".uppercased()
        label.textColor = UIColor.darkGray
        label.numberOfLines = 2
        label.font = UIFont.boldSystemFont(ofSize: label.font.pointSize)
        label.font = label.font.withSize(12)
        label.minimumScaleFactor = 0.5
        label.adjustsFontSizeToFitWidth = true
        return label
    }()
    
    let inTodaysLabel: UILabel = {
        let label = UILabel()
        label.text = "Not in school yet..."
        label.textColor = UIColor.gray
        label.numberOfLines = 2
        label.font = label.font.withSize(13)
        return label
    }()
    
    let stateTodayLabel: UITextView = {
        let textView = UITextView()
        textView.textAlignment = .center
        textView.isEditable = false
        textView.isScrollEnabled = false
        textView.isSelectable = false
        textView.font = UIFont.systemFont(ofSize: 12.0)
        textView.layer.cornerRadius = 15
        textView.layer.shadowOpacity = 0.5
        textView.layer.shadowOffset = CGSize(width: 0, height: 1)
        textView.layer.shadowRadius = 1
        textView.textColor = .white
        textView.text = "absent".uppercased()
        textView.backgroundColor = UIColor.AlertDark(alpha: 1.0)
        return textView
    }()

    
    //@objc
    @objc func goToScanner() {
        DispatchQueue.main.async(execute: {
            let titleLabel : UILabel = {
                let label = UILabel()
                label.textColor = UIColor.white
                label.font = UIFont(name: "GillSans-Bold", size: UIFont.systemFontSize)!
                label.font = label.font.withSize(15)
                return label
            }()
            
            let scanQRLauncher = ScanQRLauncher()
            self.navigationController?.pushViewController(scanQRLauncher, animated: true)
            
//            if "\((ApiService().myID()["user_type"])!)" == "teacher" {
//                // current time subject code TEACHER
//                if "\(self.currentSubject_Code)" == "" {
//                    titleLabel.text = "scan qr".uppercased()
//                    scanQRLauncher.navigationItem.title = "no_subject".uppercased()
//                } else {
//                    titleLabel.text = "\(self.currentSubject_Code) attendance".uppercased()
//                    scanQRLauncher.navigationItem.title = "\(self.currentSubject_Code)"
//                }
//            } else if "\((ApiService().myID()["user_type"])!)" == "student" {
                // change to indicate time to scan attendance STUDENT
            titleLabel.text = "My Attendance".uppercased()
            scanQRLauncher.navigationItem.title = "parent"
//            }
            scanQRLauncher.navigationItem.titleView = titleLabel
        })
    }
    
    @objc func goToSubject() {
        let layout = UICollectionViewFlowLayout()
        let subjectsController = SubjectsController(collectionViewLayout: layout)
        HomeTabBarController().setupGoTo(to: subjectsController, title: "\(sideMenuLauncher.menuLabels[3].label)", from: self)
    }
    
    
    @objc func getTime() {
        classes = ApiService().classes(day: FileService().getDateTime(format: "EEEE"))
        countIfGateAttendance = countIfGateAttendance + 1
        self.loadGateAttendance()
        
        timeLabel.text = "\(FileService().getDateTime(format : "h:mm a").uppercased())".uppercased()
        monthYearLabel.text = "\(FileService().getDateTime(format : "MMM dd,yy").uppercased())".uppercased()
        dateLabel.text = "\(FileService().getDateTime(format : "E"))"
        
        
        // get current and Next subjects
//        if classes.count != 0 {
//            for (index, subject) in classes.enumerated() {
//                let condition = Int(subject.time_start_long)! <= Int(Date().timeIntervalSince1970) && Int(Date().timeIntervalSince1970) <=  Int(subject.time_end_long)!
//                
//                var allotted = Int(subject.time_start_long)!-1800 <= Int(Date().timeIntervalSince1970) && Int(Date().timeIntervalSince1970) <  Int(subject.time_start_long)!
//                
//                if index > 0 {
//                    let next =  classes[Int(index-1)]
//                    
//                    allotted = Int(subject.time_start_long)!-1800 <= Int(Date().timeIntervalSince1970) && Int(Date().timeIntervalSince1970) <  Int(subject.time_start_long)! && next.time_end != subject.time_start
//                }
//                
//                let classEnded = Int(classes[classes.count-1].time_end_long)! <= Int(Date().timeIntervalSince1970)
//                
//                let classNext = Int(subject.time_end_long)! <= Int(Date().timeIntervalSince1970) && index < classes.count-1
//                
//                let classFirst = Int(classes[0].time_start_long)!-1800 > Int(Date().timeIntervalSince1970)
//                
//                if classNext {
//                }
//                
//                if classFirst {
//                    nextLabel.text = ". \((subject.subject)!)".uppercased()
//                    nextTimeLabel.text = ". \((subject.time_start)!) / \((subject.time_end)!)"
//                }
//                if allotted {
//                    
//                }
//                if classEnded {
//                    nextLabel.text = ". \((subject.subject)!)".uppercased()
//                    nextTimeLabel.text = ". \((subject.time_start)!) / \((subject.time_end)!)"
//                    currentLabel.text = ". no subject".uppercased()
//                    currentTimeLabel.text = ""
//                    currentSubjectLabel.text = "Current Subject"
//                    scanLabel.text = "scan qr".uppercased()
//                    currentSubject_Code = String()
//                }
//                if condition {
//                    
//                }
//                
//                if countIfGateAttendance == 60 {
//                    countIfGateAttendance = 0
//                    //highlight attendance today if not yet in school
//                    if inTodaysLabel.text == "Not in school yet..." {
//                        UIView.animate(withDuration: 0.5, delay: 2, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
//                            self.todaysAttendance.backgroundColor = UIColor.Highlight(alpha: 1.0)
//                            self.scanView.backgroundColor = UIColor.Highlight(alpha: 1.0)
//                        }, completion: { (Bool) in
//                            UIView.animate(withDuration: 2, delay: 2, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
//                                self.todaysAttendance.backgroundColor = UIColor.rgba(red: 250, green: 250, blue: 250, alpha: 1.0)
//                                self.scanView.backgroundColor = UIColor.white
//                            }, completion: { (Bool) in })
//                        })
//                    }
//                }
//                
//            }
//        }
        
        
        
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-dd-MM h:mm a"
        formatter.locale = Locale.current
        
        // get current and Next subjects
        if schedule.count != 0 {
            for subject in schedule {
                if FileService().getDateTime(format: "EEEE") == subject.key.capitalized {
                    let subjects = (subject.value) as! [[String : Any]]
                    if subjects.count > 0 {
                        let first =  subjects[0]
                        let last =  subjects[Int(subjects.count-1)]
                        let firstStart = formatter.date(from: "\(FileService().getDateTime(format: "yyyy-dd-MM")) \((first["time_start"])!)")
                        let lastEnd = formatter.date(from: "\(FileService().getDateTime(format: "yyyy-dd-MM")) \((last["time_end"])!)")
                        
                        if Int(firstStart!.timeIntervalSince1970) > Int(Date().timeIntervalSince1970) {
                            nextLabel.text = ". \((first["subject"])!)".uppercased()
                            nextTimeLabel.text = ". \((first["time_start"])!) / \((first["time_end"])!)"

                        } else if Int(lastEnd!.timeIntervalSince1970) <= Int(Date().timeIntervalSince1970) {
                            nextLabel.text = ". no subject".uppercased()
                            nextTimeLabel.text = ""
                            currentLabel.text = ". no subject".uppercased()
                            currentTimeLabel.text = ""
                            currentSubjectLabel.text = "Current Subject"
                            scanLabel.text = "scan qr".uppercased()
                            currentSubject_Code = String()
                        }
//                        else {
                        for (index, subject) in subjects.enumerated() {
                            let classStart = formatter.date(from: "\(FileService().getDateTime(format: "yyyy-dd-MM")) \((subject["time_start"])!)")
                            let classEnd = formatter.date(from: "\(FileService().getDateTime(format: "yyyy-dd-MM")) \((subject["time_end"])!)")
                            let condition = (Int(classStart!.timeIntervalSince1970) <= Int(Date().timeIntervalSince1970) && Int(Date().timeIntervalSince1970) <=  Int(classEnd!.timeIntervalSince1970))
                            
//                                if "\((ApiService().myID()["user_type"])!)" == "student" {
                                if Int(classStart!.timeIntervalSince1970) <= Int(Date().timeIntervalSince1970) {
                                    if !(subject_codes.contains("\((subject["subject_code"])!)")) &&  "\((subject["subject_code"])!)" != "LUNCH" {
                                        subject_codes.append("\((subject["subject_code"])!)")
                                        loadAttendance()
                                    }
                                    
                                    if countIfGateAttendance == 60 {
                                        countIfGateAttendance = 0
                                        //highlight attendance today if not yet in school
                                        if inTodaysLabel.text == "Not in school yet..." {
                                            UIView.animate(withDuration: 0.5, delay: 2, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                                                self.todaysAttendance.backgroundColor = UIColor.Highlight(alpha: 1.0)
                                                self.scanView.backgroundColor = UIColor.Highlight(alpha: 1.0)
                                            }, completion: { (Bool) in
                                                UIView.animate(withDuration: 2, delay: 2, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                                                    self.todaysAttendance.backgroundColor = UIColor.rgba(red: 250, green: 250, blue: 250, alpha: 1.0)
                                                    self.scanView.backgroundColor = UIColor.white
                                                }, completion: { (Bool) in })
                                            })
                                        }
                                    }
                                }
//                                }
                            
                            
                            if condition {
                                if "\((subject["subject_code"])!)" != "LUNCH" {
                                    currentSubject_Code = "\((subject["subject_code"])!)"
                                    if "\((ApiService().myID()["user_type"])!)" == "teacher" {
                                        scanLabel.text = "scan for \((subject["subject"])!) attendance".uppercased()
                                    } else {
                                        scanLabel.text = "scan for my attendance".uppercased()
                                    }
                                } else {
                                    currentSubject_Code = String()
                                    scanLabel.text = "scan for my attendance".uppercased()
//                                        scanLabel.text = "scan qr".uppercased()
                                }
                                
                                currentLabel.text = ". \((subject["subject"])!)".uppercased()
                                currentTimeLabel.text = ". \((subject["time_start"])!) / \((subject["time_end"])!)"
                                // get next schedule
                                //                                print(" current sched to display :", index, subjects.count-1)
                                if index < subjects.count-1 {
                                    let next =  subjects[Int(index+1)]
                                    nextLabel.text = ". \((next["subject"])!)".uppercased()
                                    nextTimeLabel.text = ". \((next["time_start"])!) / \((next["time_end"])!)"
                                }
                            }
                        }
                    }
                }
            }
        }
    }
    
    @objc func handleMenu(){
        self.sideMenuLauncher.showMenu()
//        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
//            if "\((ApiService().myID()["user_type"])!)" == "teacher" {
//                self.didExpandChat = false
//                self.expandChat()
//            }
//        }, completion: { (Bool) in
//            self.sideMenuLauncher.showMenu()
//        })
    }
    
    @objc func handleOptions() {
        let text = "Share about your School"
        let url:NSURL = NSURL(string: "http://www.schoolsite.com/")!
        let link: NSURL = NSURL(string: "inquire@Inhs.edu.ph")!
        let activityVC = UIActivityViewController(activityItems: [text, url, link], applicationActivities: [])
        activityVC.popoverPresentationController?.sourceView = self.view
        self.present(activityVC, animated: true, completion: nil)
//        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
//            if "\((ApiService().myID()["user_type"])!)" == "teacher" {
//                self.didExpandChat = false
//                self.expandChat()
//            }
//        }, completion: { (Bool) in
//            let text = "Share about your School"
//            let url:NSURL = NSURL(string: "http://www.schoolsite.com/")!
//            let link: NSURL = NSURL(string: "inquire@Inhs.edu.ph")!
//            let activityVC = UIActivityViewController(activityItems: [text, url, link], applicationActivities: [])
//            activityVC.popoverPresentationController?.sourceView = self.view
//            self.present(activityVC, animated: true, completion: nil)
//        })
    }
    
    @objc func autoScroll() {
//        print(" index ", pageNumber, ArticlesArray.count)
        if pageNumber == ArticlesArray.count-1 {
            let database = try! manager.databaseNamed("news")
            pageNumber = 0
            countNews = Int(database.documentCount)
            let indexPath = NSIndexPath(item: pageNumber, section: 0)
            collectionView.scrollToItem(at: indexPath as IndexPath, at: [], animated: false)
        } else {
            pageNumber = pageNumber + 1
            countNews = countNews - 1
            let indexPath = NSIndexPath(item: pageNumber, section: 0)
            collectionView.scrollToItem(at: indexPath as IndexPath, at: [], animated: true)
        }
        
    }
    
    @objc func printQR(){
//        UIView.animate(withDuration: 0.1, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
//            self.hideQR()
//        }, completion: { (Bool) in
            let image = self.QRImageView.image
            let print = UISimpleTextPrintFormatter(text: "sample")
            let activityVC = UIActivityViewController(activityItems: [image, print], applicationActivities: [])
            activityVC.popoverPresentationController?.sourceView = self.view
        
            let alertWindow = UIWindow(frame: UIScreen.main.bounds)
            alertWindow.rootViewController = UIViewController()
            alertWindow.windowLevel = UIWindowLevelAlert + 1;
            alertWindow.makeKeyAndVisible()
            alertWindow.rootViewController?.present(activityVC, animated: true, completion: nil)
//            self.present(activityVC, animated: true, completion: nil)
//        })
    }
    
    @objc func hideQR(){
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
           
            self.blackView.alpha = 0
            self.printButton.alpha = 0
        
        }, completion: { (Bool) in
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                self.MainScrollView.addSubview(self.QRView)
                self.QRView.addSubview(self.QRImageView)
                
                self.QRView.frame = CGRect(x: self.view.frame.width-(self.view.frame.width/3)-10, y: self.SchedView.frame.maxY+10, width: self.view.frame.width/3, height: self.view.frame.width/3)
                self.QRImageView.frame = CGRect(x: 10, y: 10, width: self.QRView.frame.width-20, height: self.QRView.frame.height-20)
                
            }, completion: { (Bool) in })
        })
    }
    
    @objc func showQR(){
        if let window = UIApplication.shared.keyWindow {
            window.addSubview(blackView)
            window.addSubview(QRView)
            QRView.addSubview(QRImageView)
            QRView.addSubview(printButton)
            
            blackView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(hideQR)))
            printButton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(printQR)))
            
            blackView.frame = CGRect(x: 0, y: 0, width: window.frame.width, height: window.frame.height)
            self.printButton.frame = CGRect(x: 10, y: 300-60, width: window.frame.width-20-20, height: 50)
            
            blackView.alpha = 0
            printButton.alpha = 0
            
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                self.blackView.alpha = 1
                self.QRView.frame = CGRect(x: (window.frame.width-(window.frame.width-20))/2, y: (window.frame.height-270)/2, width: window.frame.width-20, height: 300)
                self.QRImageView.frame = CGRect(x: self.QRView.frame.width/2-100, y: 10, width: 200, height: 200)
            }, completion: { (Bool) in
                UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                    self.printButton.alpha = 1
                }, completion: { (Bool) in})
            })
        }
    }
    
    @objc func goToCreateExam() {
        let titleLabel : UILabel = {
            let label = UILabel()
            label.textColor = UIColor.white
            label.font = UIFont(name: "GillSans-Bold", size: UIFont.systemFontSize)!
            label.font = label.font.withSize(15)
            label.text = "create exam for \(self.currentSubject_Code)".uppercased()
            return label
        }()
        
        let examDetail = ExamDetail()
        self.navigationController?.pushViewController(examDetail, animated: true)
        
        examDetail.navigationItem.title = "\(self.currentSubject_Code)".uppercased()
        examDetail.navigationItem.titleView = titleLabel
    }
    
    
    
    @objc func goToSeatingPlan() {
        let manager = CBLManager.sharedInstance()
        let database = try! manager.databaseNamed("seatplan")
        let document = database.document(withID: "CLASS_SEAT_\(self.currentSubject_Code)_GR:\((ApiService().myID()["grade"])!)_SEC:\((ApiService().myID()["section"])!)")
        let properties = document?.properties
        
        if properties != nil {
            
            DispatchQueue.main.async(execute: {
                let titleLabel : UILabel = {
                    let label = UILabel()
                    label.textColor = UIColor.white
                    label.font = UIFont(name: "GillSans-Bold", size: UIFont.systemFontSize)!
                    label.font = label.font.withSize(15)
                    label.text = "\(self.currentSubject_Code) classroom view".uppercased()
                    return label
                }()
                
                let seatPlanDetail = SeatPlanDetail()
                self.navigationController?.pushViewController(seatPlanDetail, animated: true)
                seatPlanDetail.navigationItem.title = "\(self.currentSubject_Code)".uppercased()
                seatPlanDetail.navigationItem.titleView = titleLabel
            })
        } else {
            DispatchQueue.main.async(execute: {
                let titleLabel : UILabel = {
                    let label = UILabel()
                    label.textColor = UIColor.white
                    label.font = UIFont(name: "GillSans-Bold", size: UIFont.systemFontSize)!
                    label.font = label.font.withSize(15)
                    label.text = "\(self.currentSubject_Code) class seating".uppercased()
                    return label
                }()
                
                let createSeatPlanDetail = CreateSeatPlanDetail()
                self.navigationController?.pushViewController(createSeatPlanDetail, animated: true)
                createSeatPlanDetail.navigationItem.title = "\(self.currentSubject_Code)".uppercased()
                createSeatPlanDetail.navigationItem.titleView = titleLabel
            })
        }
    }
    
    @objc func quickAccessTapped(sender: UITapGestureRecognizer) {
        let title = sender.view!.subviews[1] as? UILabel
//        print(" quickAccessTapped ", (title?.text)!)
        
        if (title?.text)! == "Classroom View" && self.currentSubject_Code != "" {
            self.goToSeatingPlan()
        } else if (title?.text)! == "Upload Modules" {
            
        } else if (title?.text)! == "Create Exams" && self.currentSubject_Code != "" {
            self.goToCreateExam()
        }
    }
    
    @objc func sendChat(){
        UIView.animate(withDuration: 0.1, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            ApiService().sendChat(message : self.chatText.text!, channel : HomeController.currentChannel )
        }, completion: { (Bool) in
            self.chatText.text = ""
        })
    }
    
    @objc func hideCreateChat(){
        createChatView.backgroundColor = UIColor.rgba(red: 215, green: 215, blue: 229, alpha: 1.0)
        createChatView.layer.cornerRadius = 0
        for view in createChatView.subviews {
            view.removeFromSuperview()
        }
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.blackView.alpha = 0
            
            self.chatView.addSubview(self.createChatView)
            self.createChatView.addSubview(self.createChatImageView)
            
            self.createChatView.frame = CGRect(x: self.expandView.frame.maxX-30-30, y: 0, width: 30, height: 30)
            self.createChatImageView.frame = CGRect(x: 5, y: 5, width: 20, height: 20)
        }, completion: { (Bool) in })
    }
    
    @objc func createChat() {
        createChatView.backgroundColor = .white
        createChatView.layer.cornerRadius = 3
        createStatusLabel.textColor = UIColor.gray
        createStatusLabel.font = createStatusLabel.font.withSize(13)
        createLabel.textColor = UIColor.gray
        createLabel.font = createLabel.font.withSize(13)
        for view in createChatView.subviews {
            view.removeFromSuperview()
        }
        
        let boarderView : UIView = {
            let view = UIView()
            view.backgroundColor = UIColor.gray
            return view
        }()
        
        let createButton: UIButton = {
            let button = UIButton()
            button.backgroundColor = UIColor.Theme(alpha: 1.0)
            button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 13)
            button.setTitleColor(UIColor.white, for: .normal)
            button.setTitle("create".uppercased(), for: .normal)
            button.layer.shadowOpacity = 0.5
            button.layer.shadowOffset = CGSize(width: 0, height: 1)
            button.layer.shadowRadius = 1
            return button
        }()
        
        if let window = UIApplication.shared.keyWindow {
            window.addSubview(blackView)
            window.addSubview(createChatView)
            createChatView.addSubview(classNameText)
            createChatView.addSubview(createLabel)
            createChatView.addSubview(boarderView)
            createChatView.addSubview(createButton)
            
            blackView.alpha = 0
            
            blackView.frame = CGRect(x: 0, y: 0, width: window.frame.width, height: window.frame.height)
            createChatView.frame = CGRect(x: expandView.frame.maxX-30-30, y: chatView.frame.height+30+30, width: 30, height: 30)
            classNameText.frame = CGRect(x: 0, y: 0, width: 0, height: 0)
            createLabel.frame = CGRect(x: 0, y: 0, width: 0, height: 0)
            boarderView.frame = CGRect(x: 0, y: 0, width: 0, height: 0)
            createButton.frame = CGRect(x: 0, y: 0, width: 0, height: 0)
            
            
            if ApiService().friendsList(database : "friends").count == 0 {
                createStatusLabel.text = "No one is in your friends list."
                createChatView.addSubview(createStatusLabel)
                createStatusLabel.frame = CGRect(x: 0, y: 0, width: 0, height: 0)
            }
            
            
            blackView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(hideCreateChat)))
            createButton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(createChatButton)))
            
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                self.blackView.alpha = 1
                self.createChatView.frame = CGRect(x: 10, y: window.frame.height/2-115, width: window.frame.width-20, height: 230)
                self.classNameText.frame = CGRect(x: 0, y: 0, width: self.createChatView.frame.width, height: 50)
                self.createLabel.frame = CGRect(x: 10, y: self.classNameText.frame.maxY+10, width: self.createChatView.frame.width-20, height: 40)
                self.createStatusLabel.frame = CGRect(x: 10, y: self.createLabel.frame.maxY+20, width: self.createChatView.frame.width-20, height: 40)
                boarderView.frame = CGRect(x: 0, y: self.createChatView.frame.height-60, width: self.createChatView.frame.width, height: 0.5)
                createButton.frame = CGRect(x: self.createChatView.frame.width-90, y: boarderView.frame.maxY+10, width: 80, height: 40)
            }, completion: { (Bool) in })
        }
    }
    
    @objc func createChatButton(){
        let count = ApiService().friendsList(database : "friends").count
        if (classNameText.text != "") && (count != 0) {
            ApiService().createChat(name : "\((classNameText.text)!)", sendTo : [])
        }
        if (count == 0) {
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                self.createStatusLabel.textColor = UIColor.Alert(alpha: 1.0)
                self.createStatusLabel.font = UIFont.boldSystemFont(ofSize: self.createStatusLabel.font.pointSize)
            }, completion: { (Bool) in })
        }
        if classNameText.text != "" {
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                self.classNameText.becomeFirstResponder()
                self.createLabel.textColor = UIColor.Alert(alpha: 1.0)
                self.createLabel.font = UIFont.boldSystemFont(ofSize: self.createStatusLabel.font.pointSize)
            }, completion: { (Bool) in })
        }
    }
    
    @objc func expandChat() {
        
        self.chatText.resignFirstResponder()
        let height = self.navigationController?.navigationBar.frame.height
        if let window = UIApplication.shared.keyWindow {
            
            if didExpandChat {
                
                window.addSubview(chatView)
                expandImageView.image = UIImage(named: "ic_minimize")
                chatView.frame = CGRect(x: 10, y: SchedView.frame.maxY+10, width: view.frame.width-30-(QRView.frame.width), height: scanView.frame.height+QRView.frame.height-10-30)
                
                UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                    self.chatView.frame = CGRect(x: 0, y: 20+height!, width: self.view.frame.width, height: self.view.frame.height-height!)
                    self.chatText.frame = CGRect(x: 0, y: self.chatView.frame.height-50, width: self.chatView.frame.width-50, height: 50)
                    self.sendView.frame = CGRect(x: self.chatView.frame.width-50, y: self.chatView.frame.height-50, width: 50, height: 50)
                    self.sendImage.frame = CGRect(x: 12.5, y: 12.5, width: 25, height: 25)
                    self.expandView.frame = CGRect(x: self.chatView.frame.width-30, y: 0, width: 30, height: 30)
                    self.createChatView.frame = CGRect(x: self.expandView.frame.maxX-30-30, y: 0, width: 30, height: 30)
                    self.createChatImageView.frame = CGRect(x: 5, y: 5, width: 20, height: 20)
                    
                    self.chatScroll.frame = CGRect(x: 0, y: 30, width: self.chatView.frame.width, height: self.chatView.frame.height-30-50)
                    self.chatScroll.contentSize = CGSize(width: self.chatView.frame.width, height: self.chatView.frame.height-30-50)
                    
                    self.chatChannelScroll.frame = CGRect(x: 0, y: 0, width: self.chatView.frame.width-30-30, height: 30)
                    self.chatChannelScroll.contentSize = CGSize(width: self.chatChannelScroll.frame.width, height: self.chatChannelScroll.frame.height)
                    self.removeMessages()
                }, completion: { (Bool) in
                    self.didExpandChat = false
                    self.addMessages()
                })
            } else {
                self.expandImageView.image = UIImage(named: "ic_maximize")
                
                self.MainScrollView.addSubview(self.chatView)
                self.chatView.addSubview(self.chatScroll)
                self.chatView.addSubview(self.expandView)
                self.chatView.addSubview(self.sendView)
                self.sendView.addSubview(self.sendImage)
                self.chatView.addSubview(self.chatText)
                self.expandView.addSubview(expandImageView)

                UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                    self.chatView.frame = CGRect(x: 10, y: self.SchedView.frame.maxY+10, width: self.view.frame.width-30-(self.QRView.frame.width), height: self.scanView.frame.height+self.QRView.frame.height+10)
                    self.chatText.frame = CGRect(x: 0, y: self.chatView.frame.height-50, width: self.chatView.frame.width-50, height: 50)
                    self.sendView.frame = CGRect(x: self.chatView.frame.width-50, y: self.chatView.frame.height-50, width: 50, height: 50)
                    self.sendImage.frame = CGRect(x: 12.5, y: 12.5, width: 25, height: 25)
                    self.expandView.frame = CGRect(x: self.chatView.frame.width-30, y: 0, width: 30, height: 30)
                    self.expandImageView.frame = CGRect(x: 5, y: 5, width: 20, height: 20)
                    self.createChatView.frame = CGRect(x: self.expandView.frame.maxX-30-30, y: 0, width: 30, height: 30)
                    self.createChatImageView.frame = CGRect(x: 5, y: 5, width: 20, height: 20)
                    
                    self.chatScroll.frame = CGRect(x: 0, y: 30, width: self.chatView.frame.width, height: self.chatView.frame.height-30-50)
                    self.chatScroll.contentSize = CGSize(width: self.chatView.frame.width, height: self.chatView.frame.height-30-50)
                    
                    self.chatChannelScroll.frame = CGRect(x: 0, y: 0, width: self.chatView.frame.width-30-30, height: 30)
                    self.chatChannelScroll.contentSize = CGSize(width: self.chatChannelScroll.frame.width, height: self.chatChannelScroll.frame.height)
                    self.removeMessages()
                }, completion: { (Bool) in
                    self.didExpandChat = true
                    self.addMessages()
                    
                })
                
            }
        }
    }
    
    @objc func chatSelect(sender: UITapGestureRecognizer) {
        resetChatSelect()
        
        let label = sender.view!.subviews[0] as? UILabel
        label?.backgroundColor = UIColor.rgba(red: 245, green: 245, blue: 255, alpha: 1.0)
        label?.font = UIFont.boldSystemFont(ofSize: 13)
        label?.textColor = UIColor.ThemeDarkGreen(alpha: 1.0)
        //selecting of channel
//        print(" chatrooms : ", ChatRooms, (label!.text)!)
        let index = (ChatRooms.index(of: (label!.text)!))!
        HomeController.currentChannel = Channels[index]

        //load chat
        UIView.animate(withDuration: 0.1, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.removeMessages()
        }, completion: { (Bool) in
            self.loadChat(chnlSelect: HomeController.currentChannel)
        })
    }
    
    //func
    
    private func selectedChatIndex() {
        let selected = 0
        let view = chatChannelScroll.subviews[selected] as? UIView
        let label = view?.subviews[selected] as? UILabel
        label?.backgroundColor = UIColor.rgba(red: 245, green: 245, blue: 255, alpha: 1.0)
        label?.font = UIFont.boldSystemFont(ofSize: 13)
        label?.textColor = UIColor.ThemeDarkGreen(alpha: 1.0)
        let index = (ChatRooms.index(of: (label?.text)!))!
        HomeController.currentChannel = Channels[index]
        
        //load chat
        UIView.animate(withDuration: 0.1, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.removeMessages()
        }, completion: { (Bool) in
            self.loadChat(chnlSelect: HomeController.currentChannel)
        })
//        print(" selectedChatIndex ", index)
//        let image = view.subviews[1] as? UIImageView
//        image?.tintColor = UIColor.Theme(alpha: 1.0)
    }
    
    private func resetChatSelect() {
        for view in chatChannelScroll.subviews {
            let count = view.subviews.count
            if count != 0 {
                let view = view as? UIView
                let label = view?.subviews[0] as? UILabel
                label?.backgroundColor = UIColor.rgba(red: 226, green: 226, blue: 240, alpha: 1.0)
                label?.font = UIFont(name: "Helvetica", size: 13)
                label?.textColor = UIColor.gray
            }
        }
    }
    
    private func loadGateAttendance() {
        var yChild = 0
        let manager = CBLManager.sharedInstance()
        let database = try! manager.databaseNamed("attendance")
        let query = database.createAllDocumentsQuery()
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-dd-MM h:mm a"
        formatter.locale = Locale.current
        
        let gateIndication: UILabel = {
            let label = UILabel()
            label.text = "parent"
            label.textColor = UIColor.lightGray
            label.numberOfLines = 2
            label.font = label.font.withSize(13)
            return label
        }()
        
        // set up todays gate attendance
        attendanceScroll.addSubview(todaysAttendance)
        todaysAttendance.addSubview(gateIndication)
        todaysAttendance.addSubview(todaysLabel)
        todaysAttendance.addSubview(inTodaysLabel)
        todaysAttendance.addSubview(stateTodayLabel)
        
        todaysAttendance.frame = CGRect(x: 0, y:  yChild, width: Int(self.attendanceScroll.frame.width), height: 60)
        stateTodayLabel.frame = CGRect(x: todaysAttendance.frame.width-80-10, y: todaysAttendance.frame.height/2-15, width: 80, height: 30)
        todaysLabel.frame = CGRect(x: 10, y: 10, width: todaysAttendance.frame.width-stateTodayLabel.frame.width-15, height: 20)
        inTodaysLabel.frame = CGRect(x: 10, y: todaysLabel.frame.maxY+2, width: todaysAttendance.frame.width-stateTodayLabel.frame.width, height: 20)
        
        yChild = yChild + Int(todaysAttendance.frame.height) + 2
        
        // check attendance if Gate absent
        do {
            let result = try query.run()
            if result != nil {
                while let row = result.nextRow(){
                    let document = row.document?.properties
                    
                    let date = "\(FileService().epochConvertString(date: "\((document!["scan_datetime"])!)", format: "MMMM dd, yyyy"))"
                    
                    if date == FileService().getDateTime(format: "MMMM dd, yyyy") && "\((document!["scan_subj"])!)" == "parent" {
                        stateTodayLabel.text = "present".uppercased()
                        stateTodayLabel.backgroundColor = UIColor.ThemeDarkGreen(alpha: 1.0)
                        inTodaysLabel.text = "In : \(FileService().epochConvertString(date: "\((document!["scan_datetime"])!)", format: "h:mm a"))"
                    }
                }
            }
        } catch {
            print(" cannot view data ", error)
        }
    }
    
    
    private func loadAttendance() {
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.emptyAttendanceView.alpha = 0
        }, completion: { (Bool) in })
        // reset scroll view
        for view in attendanceScroll.subviews {
            view.removeFromSuperview()
        }
        
        var yChild = Int(todaysAttendance.frame.height) + 2
        
        let manager = CBLManager.sharedInstance()
        let database = try! manager.databaseNamed("attendance")
        let query = database.createAllDocumentsQuery()
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-dd-MM h:mm a"
        formatter.locale = Locale.current
        
        //
        if schedule.count != 0 {
            for subject in schedule {
                if FileService().getDateTime(format: "EEEE") == subject.key.capitalized {
                    let subjects = (subject.value) as! [[String : Any]]
                    for subject in subjects {
//                        let classStart = formatter.date(from: "\(ApiService().getDateTime(format: "yyyy-dd-MM")) \((subject["time_start"])!)")
                        let classEnd = formatter.date(from: "\(FileService().getDateTime(format: "yyyy-dd-MM")) \((subject["time_end"])!)")
                        let condition = Int(classEnd!.timeIntervalSince1970) <= Int(Date().timeIntervalSince1970)
                        
                        if condition && "\((subject["subject_code"])!)" != "LUNCH" {
                            
                            print(" if class ", subject)
                            let expandAttendance : UIView = {
                                let view = UIView()
                                view.backgroundColor = UIColor.rgba(red: 250, green: 250, blue: 250, alpha: 1.0)
                                view.layer.shadowColor = UIColor.gray.cgColor
                                view.layer.shadowOpacity = 0.5
                                view.layer.shadowOffset = CGSize(width: 0, height: 1)
                                view.layer.shadowRadius = 1
                                return view
                            }()
                            
                            let dateLabel: UILabel = {
                                let label = UILabel()
                                label.text = "\((subject["subject"])!)"
                                label.textColor = UIColor.darkGray
                                label.numberOfLines = 2
                                label.font = UIFont.boldSystemFont(ofSize: label.font.pointSize)
                                label.font = label.font.withSize(12)
                                return label
                            }()
                            
                            let inLabel: UILabel = {
                                let label = UILabel()
                                label.text = "In : \((subject["time_start"])!)"
                                label.textColor = UIColor.gray
                                label.numberOfLines = 2
                                label.font = label.font.withSize(13)
                                return label
                            }()
                            
                            let noteLabel: UILabel = {
                                let label = UILabel()
                                label.textColor = UIColor.lightGray
                                label.numberOfLines = 2
                                label.font = label.font.withSize(13)
                                return label
                            }()
                            
                            let indication: UILabel = {
                                let label = UILabel()
                                label.text = "\((subject["subject_code"])!)"
                                label.textColor = UIColor.lightGray
                                label.numberOfLines = 2
                                label.font = label.font.withSize(13)
                                return label
                            }()
                            
                            let stateLabel: UITextView = {
                                let textView = UITextView()
                                textView.textAlignment = .center
                                textView.isEditable = false
                                textView.isScrollEnabled = false
                                textView.font = UIFont.systemFont(ofSize: 12.0)
                                textView.layer.cornerRadius = 15
                                textView.layer.shadowOpacity = 0.5
                                textView.layer.shadowOffset = CGSize(width: 0, height: 1)
                                textView.layer.shadowRadius = 1
                                textView.textColor = .white
                                textView.text = "on time".uppercased()
                                textView.backgroundColor = UIColor.Accept(alpha: 1.0)
                                return textView
                            }()
                            
                            attendanceScroll.addSubview(expandAttendance)
                            expandAttendance.addSubview(indication)
                            expandAttendance.addSubview(dateLabel)
                            expandAttendance.addSubview(inLabel)
                            expandAttendance.addSubview(stateLabel)
                            expandAttendance.addSubview(noteLabel)
                            
                            if stateTodayLabel.text == "absent".uppercased() {
                                stateLabel.text = "absent".uppercased()
                                stateLabel.backgroundColor = UIColor.Alert(alpha: 1.0)
                            }
                            
                            expandAttendance.frame = CGRect(x: 0, y:  yChild, width: Int(attendanceScroll.frame.width), height: 0)
                            
                            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                                expandAttendance.frame = CGRect(x: 0, y:  yChild, width: Int(self.attendanceScroll.frame.width), height: 60)
                                stateLabel.frame = CGRect(x: expandAttendance.frame.width-70-10, y: expandAttendance.frame.height/2-15, width: 70, height: 30)
                                dateLabel.frame = CGRect(x: 10, y: 10, width: expandAttendance.frame.width-stateLabel.frame.width, height: 20)
                                inLabel.frame = CGRect(x: 10, y: dateLabel.frame.maxY+2, width: expandAttendance.frame.width-stateLabel.frame.width, height: 20)
                            }, completion: { (Bool) in })
                            
                            // check attendance if absent
                            do {
                                let result = try query.run()
                                if result != nil {
                                    while let row = result.nextRow(){
                                        let document = row.document?.properties
                                        
                                        let date = "\(FileService().epochConvertString(date: "\((document!["scan_datetime"])!)", format: "MMMM dd, yyyy"))"
                                        
//                                        if date == ApiService().getDateTime(format: "MMMM dd, yyyy") {
                                            print(" call attendance : ", date, "\((document!["scan_subj"])!)", "\((document!["scan_datetime"])!)")
//                                        }
                                        if  date == FileService().getDateTime(format: "MMMM dd, yyyy") && "\((document!["scan_subj"])!)" != "parent" {
//                                            print(" if class ", document!)// "\((document!["scan_datetime"])!)" "\((document!["is_on_time"])!)"
                                            if "\((document!["scan_subj"])!)" == "\((subject["subject_code"])!)" {
                                                inLabel.text = "In : \(FileService().epochConvertString(date: "\((document!["scan_datetime"])!)", format: "h:mm a"))"
                                                if "\((document!["is_on_time"])!)" == "1" {
                                                    stateLabel.text = "on time".uppercased()
                                                    stateLabel.backgroundColor = UIColor.Accept(alpha: 1.0)
                                                } else if "\((document!["is_on_time"])!)" == "2"{
                                                    stateLabel.text = "late".uppercased()
                                                    stateLabel.backgroundColor = UIColor.Warning(alpha: 1.0)
                                                } else if "\((document!["is_on_time"])!)" == "3" {
                                                    stateLabel.text = "absent".uppercased()
                                                    stateLabel.backgroundColor = UIColor.Alert(alpha: 1.0)
                                                } else if "\((document!["is_on_time"])!)" == "4" {
                                                    noteLabel.text = document!["note"] == nil ? "" : "\((document!["note"])!)"
                                                    stateLabel.text = "excused".uppercased()
                                                    stateLabel.backgroundColor = UIColor.ThemeOrange(alpha: 1.0)
                                                    
                                                    if noteLabel.text != "" {
                                                        let size = CGSize(width: Int(self.attendanceScroll.frame.width), height: Int(self.attendanceScroll.frame.height))
                                                        let options = NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin)
                                                        let estimatedNoteFrame = NSString(string: "\((noteLabel.text)!)").boundingRect(with: size, options: options, attributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 13)], context: nil)
                                                        
                                                        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                                                            expandAttendance.frame = CGRect(x: 0, y:  yChild, width: Int(self.attendanceScroll.frame.width), height: 60+Int(estimatedNoteFrame.height))
                                                            stateLabel.frame = CGRect(x: expandAttendance.frame.width-70-10, y: expandAttendance.frame.height/2-15, width: 70, height: 30)
                                                            noteLabel.frame = CGRect(x: 10, y: inLabel.frame.maxY+2, width: expandAttendance.frame.width-stateLabel.frame.width, height: estimatedNoteFrame.height)
                                                        }, completion: { (Bool) in })
                                                        
                                                    }
                                                    
                                                }
                                            }
                                        }
                                    }
                                }
                            } catch {
                                print(" cannot view data ", error)
                            }
                            
                            yChild = yChild + Int(expandAttendance.frame.height) + 2
                            
                            if CGFloat(yChild) > attendanceScroll.frame.height {
                                attendanceScroll.contentSize = CGSize(width: attendanceScroll.frame.width, height: CGFloat(yChild))
                            }
                            //scroll to bottom
                            let bottomOffset = CGPoint(x : 0, y : attendanceScroll.contentSize.height-attendanceScroll.bounds.size.height+attendanceScroll.contentInset.bottom)
                            attendanceScroll.setContentOffset(bottomOffset, animated: false)
                            
                        }
                        
                        
                    }
                }
            }
        }
    }
    
    private func highlight(changes: [CBLDatabaseChange]) {
//        print(" highlight 1", changes)
        let manager = CBLManager.sharedInstance()
        let database = try! manager.databaseNamed("attendance")
        let viewsArray = attendanceScroll.subviews
        
        for views in viewsArray {
//            print(" highlight 2", views)
            let subject_code = views.subviews[0] as? UILabel
            let inLabel = views.subviews[2] as? UILabel
            let stateLabel = views.subviews[3] as? UITextView
            let noteLabel = views.subviews[4] as? UILabel
            
            for change in changes {
//                print(" highlight 3", change)
                let document = database.document(withID: "\(change.documentID)")
                let properties = document?.properties
                let size = CGSize(width: Int(self.attendanceScroll.frame.width), height: Int(self.attendanceScroll.frame.height))
                let options = NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin)

                if "\((subject_code?.text)!)" == "\((properties!["scan_subj"])!)" {
                    //highlight attendance today if not yet in school
                    if "\((properties!["scan_subj"])!)" == "parent" {
//                        if inTodaysLabel.text == "Not in school yet..." {
                            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                                self.todaysAttendance.backgroundColor = UIColor.Highlight(alpha: 1.0)
                                self.stateTodayLabel.text = "present".uppercased()
                                self.stateTodayLabel.backgroundColor = UIColor.ThemeDarkGreen(alpha: 1.0)
                                self.inTodaysLabel.text = "In : \(FileService().epochConvertString(date: "\((properties!["scan_datetime"])!)", format: "h:mm a"))"
                            }, completion: { (Bool) in
                                UIView.animate(withDuration: 2, delay: 2, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                                    self.todaysAttendance.backgroundColor = UIColor.rgba(red: 250, green: 250, blue: 250, alpha: 1.0)
                                }, completion: { (Bool) in })
                            })
//                        }
                    } else {
                        UIView.animate(withDuration: 0.5, delay: 0.5, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                            stateLabel?.alpha = 0
                            inLabel?.alpha = 0
                        }, completion: { (Bool) in
                            //                        inLabel?.text = "In : \(ApiService().epochConvertString(date: "\((properties!["scan_datetime"])!)", format: "h:mm a"))"
                            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                                views.frame = CGRect(x: 0, y:  Int(views.frame.minY), width: Int(self.attendanceScroll.frame.width), height: 60)
                                stateLabel?.frame = CGRect(x: views.frame.width-70-10, y: views.frame.height/2-15, width: 70, height: 30)
                                noteLabel?.frame = CGRect(x: 10, y: (inLabel?.frame.maxY)!+2, width: views.frame.width-(stateLabel?.frame.width)!, height: 0)
                            }, completion: { (Bool) in })
                            
                            if "\((properties!["is_on_time"])!)" == "1" {
                                stateLabel?.text = "on time".uppercased()
                                stateLabel?.backgroundColor = UIColor.Accept(alpha: 1.0)
                            } else if "\((properties!["is_on_time"])!)" == "2"{
                                stateLabel?.text = "late".uppercased()
                                stateLabel?.backgroundColor = UIColor.Warning(alpha: 1.0)
                            } else if "\((properties!["is_on_time"])!)" == "3" {
                                stateLabel?.text = "absent".uppercased()
                                stateLabel?.backgroundColor = UIColor.Alert(alpha: 1.0)
                            } else if "\((properties!["is_on_time"])!)" == "4" {
                                stateLabel?.text = "excused".uppercased()
                                stateLabel?.backgroundColor = UIColor.ThemeOrange(alpha: 1.0)
                                
                                let estimatedNoteFrame = NSString(string: "\((properties!["note"])!)").boundingRect(with: size, options: options, attributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 13)], context: nil)
                                
                                noteLabel?.text = "\((properties!["note"])!)"
                                print(" properties ", "\((properties!["note"])!)")
                                
                                if noteLabel?.text != "" {
                                    UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                                        views.frame = CGRect(x: 0, y:  Int(views.frame.minY), width: Int(self.attendanceScroll.frame.width), height: 60+Int(estimatedNoteFrame.height))
                                        stateLabel?.frame = CGRect(x: views.frame.width-70-10, y: views.frame.height/2-15, width: 70, height: 30)
                                        noteLabel?.frame = CGRect(x: 10, y: (inLabel?.frame.maxY)!+2, width: views.frame.width-(stateLabel?.frame.width)!, height: estimatedNoteFrame.height)
                                    }, completion: { (Bool) in })
                                }
                                
                            }
                            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                                stateLabel?.alpha = 1
                                inLabel?.alpha = 1
                            }, completion: { (Bool) in })
                        })
                        
                        UIView.animate(withDuration: 0.5, delay: 2, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                            inLabel?.text = "In : \(FileService().epochConvertString(date: "\((properties!["scan_datetime"])!)", format: "h:mm a"))"
                            views.backgroundColor = UIColor.Highlight(alpha: 1.0)
                        }, completion: { (Bool) in
                            UIView.animate(withDuration: 2, delay: 2, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                                views.backgroundColor = UIColor.rgba(red: 250, green: 250, blue: 250, alpha: 1.0)
                            }, completion: { (Bool) in })
                        })
                    }
                    //adjust
//                    if index >= viewsArray.count-1 {
//                        let nextView = viewsArray[index+1]
//                         print(" index ", viewsArray[index+1])
//                        nextView.frame = CGRect(x: 0, y:  Int(views.frame.minY), width: Int(self.attendanceScroll.frame.width), height: 60+Int(estimatedNoteFrame.height))
//                    }
                }
            }
//            let nextView = viewsArray[index-1]
//            views.frame = CGRect(x: 0, y:  Int(nextView.frame.minY), width: Int(self.attendanceScroll.frame.width), height: Int(views.frame.height))
        }
    }
    
    private func watch() {
        // schedule
        let schedule = try! manager.databaseNamed("schedule")
        NotificationCenter.default.addObserver(forName: NSNotification.Name.cblDatabaseChange, object: schedule, queue: nil) {
            (notification) -> Void in
            if let changes = notification.userInfo!["changes"] as? [CBLDatabaseChange] {
                for change in changes {
                    print("schedule , revision ID '%@'", change.description)
                    self.schedule = ApiService().scheduleList()
                }
            }
        }
        
        
        //chat class
        let chat = try! manager.databaseNamed("chat")
        NotificationCenter.default.addObserver(forName: NSNotification.Name.cblDatabaseChange, object: chat, queue: nil) {
            (notification) -> Void in
            if let changes = notification.userInfo!["changes"] as? [CBLDatabaseChange] {
                for change in changes {
                    print("sent chat, revision ID '%@'", change.description)
                    UIView.animate(withDuration: 0.1, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                        self.removeMessages()
                    }, completion: { (Bool) in
                        self.loadChat(chnlSelect: HomeController.currentChannel)
                    })
                }
            }
        }
        
        //attendance
        let attendance = try! manager.databaseNamed("attendance")
        print(" attendance count ", attendance.documentCount)
        NotificationCenter.default.addObserver(forName: NSNotification.Name.cblDatabaseChange, object: attendance, queue: nil) {
            (notification) -> Void in
            if let changes = notification.userInfo!["changes"] as? [CBLDatabaseChange] {
                self.highlight(changes: changes)
            }
        }
        
        //chat request
        let chatRequest = try! manager.databaseNamed("chatrequest")
//        print("there is request : ", chatRequest.documentCount)
        let query = chatRequest.createAllDocumentsQuery()
        do {
            let result = try query.run()
            if result != nil {
                while let row = result.nextRow(){
                    let document = row.document?.properties
                    ApiService().addChannelToChatList(channel: "\((document!["group_id"])!)", chatroom: "\((document!["group_name"])!)")
                }
            }
        } catch let error as NSError {
            print("ERROR ", error)
        }
        
        NotificationCenter.default.addObserver(forName: NSNotification.Name.cblDatabaseChange, object: chatRequest, queue: nil) {
            (notification) -> Void in
            if let changes = notification.userInfo!["changes"] as? [CBLDatabaseChange] {
                for change in changes {
                    print("chat request, revision ID '%@'", change.description)
                    
                    let query = chatRequest.createAllDocumentsQuery()
                    do {
                        let result = try query.run()
                        if result != nil {
                            while let row = result.nextRow(){
                                let document = row.document?.properties
                                ApiService().addChannelToChatList(channel: "\((document!["group_id"])!)", chatroom: "\((document!["group_name"])!)")
                            }
                        }
                    } catch let error as NSError {
                        print("ERROR ", error)
                    }
                }
            }
        }
        //messages
        let chatlist = try! manager.databaseNamed("chatlist")
        let documentin = chatlist.document(withID:"USER_\((ApiService().myID()["qq_id"])!)")!
        let chatrooms = documentin["chatrooms"] as! [String]
        let channels = documentin["channels"] as! [String]
        let filterChannels = channels.filter { $0 != "USER_\((ApiService().myID()["qq_id"])!)" }
        ChatRooms = chatrooms
        Channels = filterChannels
        self.addExistingChannels(chatrooms: chatrooms)
        
        NotificationCenter.default.addObserver(forName: NSNotification.Name.cblDatabaseChange, object: chatlist, queue: nil) {
            (notification) -> Void in
            if let changes = notification.userInfo!["changes"] as? [CBLDatabaseChange] {
                for change in changes {
                    print("chat list, revision ID '%@'", change.description)
                }
            }
        }
        
        //news
        self.news(channel: ["QQSCHOOL_NEWS"], database: "news")
        
        //status
        watchStatus()
    }
    
    func watchStatus() {
        let manager = CBLManager.sharedInstance()
        let database = try! manager.databaseNamed("mydata")
        NotificationCenter.default.addObserver(forName: NSNotification.Name.cblDatabaseChange, object: database, queue: nil) {
            (notification) -> Void in
            //            if let changes = notification.userInfo!["changes"] as? [CBLDatabaseChange] {
            if "\((ApiService().myID()["status"])!)" == "0" {
                if let window = UIApplication.shared.keyWindow {
                    window.showToast(message: "Session Expired.\nLogging out...")
                }

                HomeTabBarController().statusBarBackground.removeFromSuperview()
                HomeTabBarController().statusBarConnection.removeFromSuperview()
                
                self.timer?.invalidate()
                self.timerNews?.invalidate()
                
                let loginController = LoginController()
                self.navigationController?.pushViewController(loginController, animated: true)
            }
        }
    }
    
    private func addExistingChannels(chatrooms : [Any]) {
//        print("all chat channel : ", chatrooms)
        var x = 0
        for chatroom in chatrooms {
            let size = CGSize(width: self.chatChannelScroll.frame.width, height: self.chatChannelScroll.frame.height)
            let options = NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin)
            let estimatedFrame = NSString(string: "\(chatroom)").boundingRect(with: size, options: options, attributes: [NSAttributedStringKey.font: UIFont(name: "Helvetica", size: 13)!], context: nil)
            var width = estimatedFrame.width
            
            let chatroomView : UIView = {
                let view = UIView()
                view.backgroundColor = UIColor.rgba(red: 226, green: 226, blue: 240, alpha: 1.0)
                return view
            }()
            
            let chatroomLabel: UILabel = {
                let label = UILabel()
                label.text = "\(chatroom)"
                label.textColor = UIColor.gray
                label.numberOfLines = 2
                label.textAlignment = .center
                label.font = UIFont(name: "Helvetica", size: 13)
                return label
            }()
            
            chatChannelScroll.addSubview(chatroomView)
            chatroomView.addSubview(chatroomLabel)
            
            if width < 60 {
                width = 60
            } else {
                width = width+15
            }
            chatroomView.frame = CGRect(x: CGFloat(x), y: 0, width: width, height: 30)
            chatroomLabel.frame = CGRect(x: 0, y: 0, width: chatroomView.frame.width+10, height: chatroomView.frame.height)
            x = x + Int(width)+1
            
            chatroomView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.chatSelect(sender:))))
                
        }
        self.selectedChatIndex()
    }
    
    private func loadChat(chnlSelect : String){
        let database = try! manager.databaseNamed("chat")
        let query = database.createAllDocumentsQuery()
        messages.removeAll()
        
        do {
            let result = try query.run()
            if result != nil {
                while let row = result.nextRow(){
                    let document = row.document?.properties
                    let channel = (document!["channels"])! as! [String]
                    
                    if "\(chnlSelect)" == "\(channel[0])" {
                        var message = Messages()
                        message.sender_qq_id = "\((document!["sender_qq_id"])!)"
                        message.sender_name = "\((document?["sender_name"])!)"
                        message.message = "\((document?["message"])!)"
                        message.datetime = "\((document?["datetime"])!)"

                        messages.append(message)
                    }
                }
            }
            addMessages()
        } catch let error as NSError {
            print("ERROR ", error)
        }
    }
    
    private func removeMessages(){
        chatScroll.frame = CGRect(x: 0, y: 30, width: chatView.frame.width, height: chatView.frame.height-30-50)
        chatScroll.contentSize = CGSize(width: chatView.frame.width, height: chatView.frame.height-30-50)
        for view in chatScroll.subviews {
            view.removeFromSuperview()
        }
    }
    
    private func addMessages(){
        var y = 20
        messages = messages.sorted(by: { $0.datetime.compare($1.datetime) == .orderedAscending })
        
        for (index, text) in messages.enumerated(){
            let size = CGSize(width: self.chatView.frame.width-30, height: self.chatView.frame.height)
            let options = NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin)
            let estimatedFrame = NSString(string: "\((text.message)!)").boundingRect(with: size, options: options, attributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 13.83)], context: nil)
            
            let nameLabel: UILabel = {
                let label = UILabel()
                label.text = "\((text.sender_name)!)"
                label.textColor = UIColor.lightGray
                label.numberOfLines = 2
                label.font = label.font.withSize(10)
                return label
            }()
            
            let dateTimeLabel: UILabel = {
                let label = UILabel()
                label.textColor = UIColor.lightGray
                label.text = FileService().epochConvertString(date: "\((text.datetime)!)", format: "dd MMM, h:mm a").uppercased()
                label.numberOfLines = 2
                label.textAlignment = .center
                label.font = label.font.withSize(10)
                label.font = UIFont.boldSystemFont(ofSize: label.font.pointSize)
                return label
            }()
            
            let messageLabel: UITextView = {
                let textView = UITextView()
                textView.isEditable = false
                textView.isScrollEnabled = false
                textView.font = UIFont.systemFont(ofSize: 13.0)
                textView.text = "\((text.message)!)"
                textView.textAlignment = .left//.justified
                textView.sizeToFit()
                textView.layer.cornerRadius = 10
                textView.layer.shadowColor = UIColor.gray.cgColor
                textView.layer.shadowOpacity = 0.5
                textView.layer.shadowOffset = CGSize(width: 0, height: 1)
                textView.layer.shadowRadius = 1
                return textView
            }()
            
            chatScroll.addSubview(messageLabel)
            // change Today if date is today
            if FileService().getDateTime(format: "dd MMM").uppercased() == FileService().epochConvertString(date: "\((text.datetime)!)", format: "dd MMM").uppercased() {
                dateTimeLabel.text = "Today, \(FileService().epochConvertString(date: "\((text.datetime)!)", format: "h:mm a").uppercased())"
            }
            
            // adding date in messages
            if index > 0 {
                let datePrev = Date(timeIntervalSince1970: Double(messages[index-1].datetime)!)
                let dateSent = Date(timeIntervalSince1970: Double(text.datetime)!)
                let elapsed = dateSent.timeIntervalSince(datePrev)
                //15 mins from prev date
                if Int(elapsed/60) > 15 {
                    chatScroll.addSubview(dateTimeLabel)
//                    y = y + 20
                    dateTimeLabel.frame = CGRect(x: 0, y: y, width: Int(chatScroll.frame.width), height: 20)
                    y = y+Int(dateTimeLabel.frame.height)
                    if "\((ApiService().myID()["qq_id"])!)" != "\((text.sender_qq_id)!)" {
                        chatScroll.addSubview(nameLabel)
                        nameLabel.frame = CGRect(x: 8, y: y, width: Int(self.chatScroll.frame.width-16), height: 20)
                        y = y+Int(nameLabel.frame.height)
                    }
                }
            } else if index == 0 {
                chatScroll.addSubview(dateTimeLabel)
                dateTimeLabel.frame = CGRect(x: 0, y: y, width: Int(chatScroll.frame.width), height: 20)
                y = y+Int(dateTimeLabel.frame.height)
                if "\((ApiService().myID()["qq_id"])!)" != "\((text.sender_qq_id)!)" {
                    chatScroll.addSubview(nameLabel)
                    nameLabel.frame = CGRect(x: 8, y: y, width: Int(self.chatScroll.frame.width-16), height: 20)
                    y = y+Int(nameLabel.frame.height)
                }
            }
            
            // switch if sent by me
            if "\((ApiService().myID()["qq_id"])!)" == "\((text.sender_qq_id)!)" {
                
                let width = Int(estimatedFrame.width)
                let height = Int(estimatedFrame.height)
                let x = Int(chatView.frame.width-estimatedFrame.width)-20
                
                messageLabel.textColor = UIColor.ThemeDark(alpha: 1.0)
                messageLabel.backgroundColor = UIColor.white
                
                messageLabel.frame = CGRect(x: x, y: y, width: width, height: 0)

                UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                    messageLabel.frame = CGRect(x: x, y: y, width: width+10, height: height+15)
                }, completion: { (Bool) in})
                
            } else {
                let width = Int(estimatedFrame.width)
                let height = Int(estimatedFrame.height)
                
                messageLabel.textColor = UIColor.white
                messageLabel.backgroundColor = UIColor.Theme(alpha: 1.0)//UIColor.rgba(red: 215, green: 215, blue: 229, alpha: 1.0)
                
                messageLabel.frame = CGRect(x: 5, y: y, width: width, height: 0)
                
                // add name if
                if index > 0 {
                    //15 mins from prev date
                    let datePrev = Date(timeIntervalSince1970: Double(messages[index-1].datetime)!)
                    let dateSent = Date(timeIntervalSince1970: Double(text.datetime)!)
                    let elapsed = dateSent.timeIntervalSince(datePrev)

                    let namePrev = (messages[index-1].sender_name)!
                    if namePrev != nameLabel.text && Int(elapsed/60) < 15{
                        chatScroll.addSubview(nameLabel)
                        nameLabel.frame = CGRect(x: 8, y: y, width: Int(self.chatScroll.frame.width-16), height: 20)
                        y = y+Int(nameLabel.frame.height)
                    }
                }
                
                UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                    messageLabel.frame = CGRect(x: 5, y: y, width: width+10, height: height+15)
                }, completion: { (Bool) in})

            }
            y = y+Int(messageLabel.frame.height)+3
            
            if CGFloat(y) > CGFloat(chatView.frame.height-30-50) {
                //adjust scroll
                chatScroll.contentSize = CGSize(width: chatView.frame.width, height: CGFloat(y)+20)
            }
            
        }
        //scroll to bottom
        let bottomOffset = CGPoint(x : 0, y : chatScroll.contentSize.height-chatScroll.bounds.size.height+chatScroll.contentInset.bottom)
        chatScroll.setContentOffset(bottomOffset, animated: false)
    }
   
    private func setupQuickAccess() {
        let shortcutsIcons = ["classroom", "uploaddownload", "exam"]
        let shortcutsTitle = ["Classroom View", "Upload Modules", "Create Exams"]
        let shortcutsDesc = ["Look into class seating", "Upload reviewers and quizzes", "Online exam creation"]
        var y = CGFloat(0)
        
        for (index, icon) in shortcutsIcons.enumerated(){
            print(" setupQuickAccess ", icon)
            
            let accessView : UIView = {
                let view = UIView()
                view.backgroundColor = UIColor.white
                view.layer.shadowColor = UIColor.gray.cgColor
                view.layer.shadowOpacity = 0.5
                view.layer.shadowOffset = CGSize(width: 0, height: 1)
                view.layer.shadowRadius = 1
                return view
            }()
            
            let titleLabel: UILabel = {
                let label = UILabel()
                label.text = "\(shortcutsTitle[index])"
                label.textColor = UIColor.darkGray
                label.numberOfLines = 2
                label.font = UIFont.boldSystemFont(ofSize: label.font.pointSize)
                label.font = label.font.withSize(12)
                return label
            }()
            
            let descLabel: UITextView = {
                let text = UITextView()
                text.text = "\(shortcutsDesc[index])"
                text.textColor = UIColor.gray
                text.isSelectable = false
                text.isEditable = false
                text.font = text.font?.withSize(13)
                return text
            }()
            
            
            let iconImage: UIImageView = {
                let image = UIImageView()
                image.image = UIImage(named: icon)
                return image
            }()
            
            attendanceView.addSubview(accessView)
            accessView.addSubview(iconImage)
            accessView.addSubview(titleLabel)
            accessView.addSubview(descLabel)
            
            accessView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.quickAccessTapped(sender:))))
            
            accessView.frame = CGRect(x: CGFloat(0), y: y, width: attendanceView.frame.width, height: attendanceView.frame.height/CGFloat(shortcutsIcons.count)-2)
            iconImage.frame = CGRect(x: 20, y: (accessView.frame.height/2)-((accessView.frame.width/3-30)/2), width: accessView.frame.width/3-30, height: accessView.frame.width/3-30)
            
            titleLabel.frame = CGRect(x: iconImage.frame.maxX+20, y: (accessView.frame.height/2)-((accessView.frame.width/3-30)/2), width: attendanceView.frame.width-iconImage.frame.width-50, height: 20)
            descLabel.frame = CGRect(x: iconImage.frame.maxX+15, y: titleLabel.frame.maxY-5, width: attendanceView.frame.width-iconImage.frame.width-45, height: accessView.frame.height-titleLabel.frame.maxY-10)
            
            y = y + accessView.frame.height + 1
        }
    }
    
    private func setupView() {
        self.headerView.removeFromSuperview()
        
        collectionView.backgroundColor = UIColor.Background(alpha: 1.0)
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.isPagingEnabled = true
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.isScrollEnabled = false
        collectionView.register(NewsCell.self, forCellWithReuseIdentifier: cellId)
        
        activityIndicator.center = self.view.center
        activityIndicator.hidesWhenStopped = true
        activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.gray
        
        MainScrollView = UIScrollView(frame: view.bounds)
        MainScrollView.delegate = self
        MainScrollView.showsVerticalScrollIndicator = true
        MainScrollView.backgroundColor = UIColor.Background(alpha: 1.0)
            
        chatScroll = UIScrollView(frame: view.bounds)
        chatScroll.delegate = self
        chatScroll.showsVerticalScrollIndicator = true
        
        attendanceScroll = UIScrollView(frame: view.bounds)
        attendanceScroll.delegate = self
        attendanceScroll.showsVerticalScrollIndicator = true
        
        chatChannelScroll = UIScrollView(frame: view.bounds)
        chatChannelScroll.delegate = self
        chatChannelScroll.showsVerticalScrollIndicator = false
        chatChannelScroll.backgroundColor = UIColor.rgba(red: 215, green: 215, blue: 229, alpha: 1.0)

        QRImageView.image = ApiService().generateQRCode(from: ApiService().myQRString(type: "\((ApiService().myQR()["t"])!)"))
        scanImageView.image = UIImage(named: "ic_qr")
//            ?.withRenderingMode(.alwaysTemplate)as UIImage?
//        scanImageView.tintColor = UIColor.ThemeDarkGreen(alpha: 1.0)
        
        view.addSubview(MainScrollView)
        MainScrollView.addSubview(collectionView)
        MainScrollView.addSubview(activityIndicator)
        activityIndicator.startAnimating()
        
        MainScrollView.addSubview(SchedView)
        MainScrollView.addSubview(QRView)
        QRView.addSubview(QRImageView)
        
        MainScrollView.addSubview(scanView)
        scanView.addSubview(scanLabel)
        scanView.addSubview(scanImageView)
        
    }
    
   
    
    private func setupConstraints() {
        var height = view.frame.height-20-20
        let collectionViewHeight = view.frame.height/4
        
        collectionView.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: collectionViewHeight)
        activityIndicator.frame = CGRect(x: 0, y: collectionView.frame.height/2-10, width: view.frame.width, height: 20)
        
        height = height-collectionView.frame.height
        // start adding in scroll main

        MainScrollView.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height)
        MainScrollView.contentSize = CGSize(width: view.frame.width, height: view.frame.height)
        
        SchedView.frame = CGRect(x: 10, y: collectionView.frame.maxY+10, width: view.frame.width-20, height: 90)
        QRView.frame = CGRect(x: view.frame.width-(view.frame.width/3)-10, y: SchedView.frame.maxY+10, width: view.frame.width/3, height: view.frame.width/3)
        height = height-SchedView.frame.height-QRView.frame.height
        
        QRImageView.frame = CGRect(x: 10, y: 10, width: QRView.frame.width-20, height: QRView.frame.height-20)
        scanView.frame = CGRect(x: view.frame.width-(view.frame.width/3)-10, y: QRView.frame.maxY+10, width: view.frame.width/3, height: height)
        view.addConstraintsFormat(format: "H:|-(\(scanView.frame.width/2-((scanView.frame.width-80)/2)))-[v0(\(scanView.frame.width-80))]|", views: scanImageView)
        view.addConstraintsFormat(format: "V:|-20-[v0(\(scanView.frame.width-80))][v1]-5-|", views: scanImageView, scanLabel)
        view.addConstraintsFormat(format: "H:|-5-[v0]-5-|", views: scanLabel)
//        scanImageView.frame = CGRect(x: scanView.frame.width/2-((scanView.frame.width-80)/2), y: scanView.frame.height/2-((scanView.frame.width-80)/2)-30, width: scanView.frame.width-80, height: scanView.frame.width-80)
//        scanLabel.frame = CGRect(x: 5, y: scanImageView.frame.maxX+10, width: scanView.frame.width-10, height: 50)
        
        MainScrollView.addSubview(attendanceView)
        attendanceView.frame = CGRect(x: 10, y: SchedView.frame.maxY+10, width: view.frame.width-30-(QRView.frame.width), height: scanView.frame.height+QRView.frame.height+10)
        if "\((ApiService().myID()["user_type"])!)" == "student" {
            attendanceView.addSubview(attendanceLabel)
            attendanceView.addSubview(attendanceScroll)
            attendanceView.addSubview(emptyAttendanceView)
            
            emptyAttendanceView.frame = CGRect(x: attendanceView.frame.width/2-((attendanceView.frame.width/2)/2), y: attendanceView.frame.height/3, width: attendanceView.frame.width/2, height: attendanceView.frame.width/2)
            attendanceLabel.frame = CGRect(x: 0, y: 0, width: attendanceView.frame.width, height: 30)
            attendanceScroll.frame = CGRect(x:0, y:  attendanceLabel.frame.maxY, width: attendanceView.frame.width, height: attendanceView.frame.height-attendanceLabel.frame.height)
            attendanceScroll.contentSize = CGSize(width: attendanceView.frame.width, height: attendanceView.frame.height-40)
            
        } else if "\((ApiService().myID()["user_type"])!)" == "teacher" {
            self.setupQuickAccess()
        }
       
        
        //chat view
//        if "\((ApiService().myID()["user_type"])!)" == "" {
//            MainScrollView.addSubview(chatView)
//            chatView.addSubview(chatChannelScroll)
//            chatView.addSubview(chatScroll)
//            chatView.addSubview(expandView)
//            expandView.addSubview(expandImageView)
//            chatView.addSubview(sendView)
//            sendView.addSubview(sendImage)
//            chatView.addSubview(chatText)
//            chatView.addSubview(createChatView)
//            createChatView.addSubview(createChatImageView)
//
//            chatView.frame = CGRect(x: 10, y: SchedView.frame.maxY+10, width: view.frame.width-30-(QRView.frame.width), height: scanView.frame.height+QRView.frame.height+10)
//            chatText.frame = CGRect(x: 0, y: chatView.frame.height-50, width: chatView.frame.width-50, height: 50)
//            sendView.frame = CGRect(x: chatView.frame.width-50, y: chatView.frame.height-50, width: 50, height: 50)
//            sendImage.frame = CGRect(x: 12.5, y: 12.5, width: 25, height: 25)
//            expandView.frame = CGRect(x: chatView.frame.width-30, y: 0, width: 30, height: 30)
//            expandImageView.frame = CGRect(x: 5, y: 5, width: 20, height: 20)
//            createChatView.frame = CGRect(x: expandView.frame.maxX-30-30, y: 0, width: 30, height: 30)
//            createChatImageView.frame = CGRect(x: 5, y: 5, width: 20, height: 20)
//
//            chatScroll.frame = CGRect(x: 0, y: 30, width: chatView.frame.width, height: chatView.frame.height-30-50)
//            chatScroll.contentSize = CGSize(width: chatView.frame.width, height: chatView.frame.height-30-50)
//
//            chatChannelScroll.frame = CGRect(x: 0, y: 0, width: chatView.frame.width-30-30, height: 30)
//            chatChannelScroll.contentSize = CGSize(width: chatChannelScroll.frame.width, height: chatChannelScroll.frame.height)
//        }
    }
    
    private func setupSchedule() {
        self.timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.getTime), userInfo: nil, repeats: true)
        
        let boarder : UIView = {
            let view = UIView()
            view.backgroundColor = UIColor.Boarder(alpha: 1.0)
            return view
        }()
        
        let nextSubjectLabel: UILabel = {
            let label = UILabel()
            label.text = "Next Subject"
            label.textColor = UIColor.gray
            label.numberOfLines = 2
            label.textAlignment = .left
            label.font = label.font.withSize(11)
            return label
        }()
        
        SchedView.addSubview(dateLabel)
        SchedView.addSubview(monthYearLabel)
        SchedView.addSubview(timeLabel)
        SchedView.addSubview(boarder)
        SchedView.addSubview(currentSubjectLabel)
        SchedView.addSubview(currentLabel)
        SchedView.addSubview(currentTimeLabel)
        SchedView.addSubview(nextSubjectLabel)
        SchedView.addSubview(nextLabel)
        SchedView.addSubview(nextTimeLabel)
        
        monthYearLabel.frame = CGRect(x: 10, y: 5, width: 60, height: 15)
        dateLabel.frame = CGRect(x: 10, y: monthYearLabel.frame.maxY, width: 60, height: 30)
        timeLabel.frame = CGRect(x: 10, y: dateLabel.frame.maxY, width: 60, height: 15)
        boarder.frame = CGRect(x: monthYearLabel.frame.maxX+10, y: 5, width: 0.5, height: monthYearLabel.frame.height+dateLabel.frame.height+5)
        currentSubjectLabel.frame = CGRect(x: boarder.frame.maxX+10, y: 5, width: 90, height: 15)
        currentLabel.frame = CGRect(x: currentSubjectLabel.frame.maxX+10, y: 5, width: SchedView.frame.width-monthYearLabel.frame.width-10-currentSubjectLabel.frame.width-10, height: 15)
        currentTimeLabel.frame = CGRect(x: currentSubjectLabel.frame.maxX+10, y: currentLabel.frame.maxY, width: SchedView.frame.width-monthYearLabel.frame.width-10-currentSubjectLabel.frame.width-10, height: 15)
        
        nextSubjectLabel.frame = CGRect(x: boarder.frame.maxX+10, y: currentTimeLabel.frame.maxY, width: 90, height: 15)
        nextLabel.frame = CGRect(x: nextSubjectLabel.frame.maxX+10, y: currentTimeLabel.frame.maxY, width: SchedView.frame.width-monthYearLabel.frame.width-10-currentSubjectLabel.frame.width-10, height: 15)
        nextTimeLabel.frame = CGRect(x: nextSubjectLabel.frame.maxX+10, y: nextLabel.frame.maxY, width: SchedView.frame.width-monthYearLabel.frame.width-10-currentSubjectLabel.frame.width-10, height: 15)
        
        //marquee
        let aFrame  = CGRect(x: 0, y: SchedView.frame.height-20, width: SchedView.frame.width, height: 20)
        let marqueeLabel = MarqueeLabel.init(frame: aFrame, rate: 30.0, fadeLength: 20.0)
        marqueeLabel.textColor = UIColor.gray
        marqueeLabel.numberOfLines = 2
        marqueeLabel.text = ""
        marqueeLabel.textAlignment = .left
        marqueeLabel.font = marqueeLabel.font.withSize(11)
        
        // append event string
        let database = try! manager.databaseNamed("events")
        let query = database.createAllDocumentsQuery()
        
        do {
            let result = try query.run()
            if result != nil {
                while let row = result.nextRow(){
                    let document = row.document?.properties
                    if "\((document!["type"])!)" == "event" {
                        let formatToDate = DateFormatter()
                        formatToDate.dateFormat = "MM/dd/yyyy"
                        formatToDate.locale = Locale.current
                        let formatter = DateFormatter()
                        formatter.dateFormat = "MMMM d"
                        formatter.locale = Locale.current
                        let date = formatToDate.date(from: "\((document!["date_from"])!)")
                        
                        var marqueeText = "\((marqueeLabel.text)!)"
                        marqueeText.append("\(formatter.string(from: date!)) - \((document!["title"])!)   •  ".uppercased())
//                        print(" document event : ", (document!["date_from"])!, formatter.string(from: date!))
                        marqueeLabel.text = marqueeText
                    }
                }
            }
            var marqueeText = "\((marqueeLabel.text)!)"
            marqueeText.append("april 28 - School App releasing   •   april 13 - Lailatul Isra Wal Mi Raj   •   May 1 - Labor Day   •   June 12 - Independence Day   •   June 16 - Eidul-Fitar   •   June 16 - June Solstice   •   August 21 - Ninoy Aquino day   •   August 22 - Eid al-Adha (Feast of the Sacrifice)   •".uppercased())
            marqueeLabel.text = marqueeText

            SchedView.addSubview(marqueeLabel)
        } catch {
            print(" error getting announcements :", error)
        }
        
        
    }
    
    private func setupGestures(){
        QRView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(showQR)))
        sendView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(sendChat)))
        expandView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(expandChat)))
        createChatView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(createChat)))
        SchedView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(goToSubject)))
        scanView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(goToScanner)))
    }
    
    func getApi(completion: @escaping () -> Void){
        DispatchQueue.global().asyncAfter(deadline: .now()) {
            let urlString = "https://newsapi.org/v2/everything?domains=rappler.com&apiKey=5b58e21f40ff43128da960349a77b934"
            guard let url = URL(string: urlString) else { return }

            URLSession.shared.dataTask(with: url) { (data, response, err) in
                guard let data = data else { return }

                do {
                    let headlines = try JSONDecoder().decode(Headlines.self, from: data)
                    self.ArticlesArray = headlines.articles
                    completion()
                } catch let jsonErr{
                    print("ERROR Serializing: ", jsonErr)
                }

            }.resume()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return ArticlesArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! NewsCell
        cell.backgroundColor = UIColor.white
        cell.layer.shadowColor = UIColor.gray.cgColor
        cell.layer.shadowOpacity = 0.5
        cell.layer.shadowOffset = CGSize(width: 0, height: 3)
        cell.layer.shadowRadius = 2
        cell.layer.cornerRadius = 2
        cell.article = ArticlesArray[indexPath.item] as? Articles
        if self.countNews > 0 {
            cell.news = ArticlesArray[indexPath.item] as? News
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width, height: collectionView.frame.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if self.countNews > 0 {
            let news = ArticlesArray[indexPath.item] as! News
            newsDetail.showProfile(news : news)
        } else {
            let data = ArticlesArray[indexPath.item] as! Articles
            webDetail.open(url: data.url!, sourceName: data.source.name)
        }
    }
    
    func showNewsTitle(article : String) {
        
        if self.countNews > 0 {
            HeaderLabel.text = article.capitalized
            self.headerView.backgroundColor = UIColor.ThemeDarkGreen(alpha: 1.0)
            self.headerView.alpha = 0.8
        } else {
            HeaderLabel.text = article
            self.headerView.backgroundColor = UIColor.black
            self.headerView.alpha = 0.5
        }
        
        
        MainScrollView.addSubview(headerView)
        MainScrollView.addSubview(HeaderLabel)
        
        HeaderLabel.frame = CGRect(x: 10, y: collectionView.frame.height, width: collectionView.frame.width-20, height: 0)
        headerView.frame = CGRect(x: 0, y: collectionView.frame.height, width: collectionView.frame.width, height: 0)
        
        
        UIView.animate(withDuration: 0.5, delay: 1, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.headerView.frame = CGRect(x: 0, y: self.collectionView.frame.height-30, width: self.view.frame.width, height: 30)
            self.HeaderLabel.frame = CGRect(x: 10, y: self.collectionView.frame.height-30, width: self.view.frame.width-20, height: 30)
        }, completion: { (Bool) in
            self.closeNewsTitle()
        })
    }
    
    func closeNewsTitle(){
        UIView.animate(withDuration: 0.5, delay: 5, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.headerView.frame = CGRect(x: 0, y: self.collectionView.frame.height, width: self.view.frame.width, height: 0)
            self.HeaderLabel.frame = CGRect(x: 10, y: self.collectionView.frame.height, width: self.view.frame.width-20, height: 0)
        }, completion: { (Bool) in
            self.headerView.removeFromSuperview()
            self.HeaderLabel.removeFromSuperview()
        })
    }

    
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
//        print(" didEndDisplaying ", countNews, pageNumber )
        
        if self.countNews > 0 {
            let article = self.ArticlesArray[pageNumber] as! News
            self.showNewsTitle(article: article.title)
        } else {
            let article = self.ArticlesArray[pageNumber] as! Articles
            self.showNewsTitle(article: article.title)
        }
        
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        self.closeNewsTitle()
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        if textField == chatText {
            self.chatText.resignFirstResponder()
        } else if textField == classNameText {
            self.classNameText.resignFirstResponder()
        }
        return false
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        let height = self.navigationController?.navigationBar.frame.height
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            keyboardHeight = keyboardSize.height
            
            if didExpandChat {
                self.MainScrollView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height-keyboardSize.height)
                
                //scroll to bottom
                let bottomOffset = CGPoint(x : 0, y : MainScrollView.contentSize.height-MainScrollView.bounds.size.height+MainScrollView.contentInset.bottom)
                MainScrollView.setContentOffset(bottomOffset, animated: true)
            } else {
                self.chatView.frame = CGRect(x: 0, y: 20+height!, width: self.view.frame.width, height: self.view.frame.height-keyboardSize.height)
                chatText.frame = CGRect(x: 0, y: chatView.frame.height-50, width: chatView.frame.width-50, height: 50)
                sendView.frame = CGRect(x: chatView.frame.width-50, y: chatView.frame.height-50, width: 50, height: 50)
                sendImage.frame = CGRect(x: 12.5, y: 12.5, width: 25, height: 25)
                
                //scroll to bottom
                let bottomOffset = CGPoint(x : 0, y : chatScroll.contentSize.height-chatScroll.bounds.size.height+chatScroll.contentInset.bottom)
                chatScroll.setContentOffset(bottomOffset, animated: true)
            }
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        let height = self.navigationController?.navigationBar.frame.height
        if didExpandChat {
            self.MainScrollView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
            self.MainScrollView.contentSize = CGSize(width: self.view.frame.width, height: self.view.frame.height)
        } else {
            self.chatView.frame = CGRect(x: 0, y: 20+height!, width: self.view.frame.width, height: self.view.frame.height)
            chatText.frame = CGRect(x: 0, y: chatView.frame.height-50, width: chatView.frame.width-50, height: 50)
            sendView.frame = CGRect(x: chatView.frame.width-50, y: chatView.frame.height-50, width: 50, height: 50)
            sendImage.frame = CGRect(x: 12.5, y: 12.5, width: 25, height: 25)
        }
    }
    
    func news(channel: [String], database: String){
        let database = try! manager.databaseNamed("\(database)")
        let pull = database.createPullReplication(ApiService().myURL())
        pull.channels = channel
        pull.continuous = true
        pull.start()
        
        NotificationCenter.default.addObserver(forName: NSNotification.Name.cblReplicationChange, object: pull, queue: nil) {
            (notification) -> Void in
            //determins if replication is over
            let active = pull.status != CBLReplicationStatus.active
            print(" replication status if idle ", active)
            if active {
                self.getNews()
                self.countNews = 0
                self.pageNumber = 0
                self.timerNews?.invalidate()
            }
        }
    }
    
    private func appendNews(){
        let database = try! manager.databaseNamed("news")
        let query = database.createAllDocumentsQuery()
        countNews = Int(database.documentCount)
//        print(" countNews ", countNews)
        do {
//            try database.delete()
            let result = try query.run()
            if result != nil {
                while let row = result.nextRow(){
                    let document = row.document?.properties

                    var news = News()
                    news.title = "\((document!["title"])!)"
                    news.desc = "\((document!["desc"])!)"
                    news.date = "\((document!["date"])!)"
                    news.b64 = "\((document!["b64"])!)"
                    news.author = "\((document!["author"])!)"

                    ArticlesArray.insert(news, at: 0)
//                    ArticlesArray.insert(news, at: Int(ArticlesArray.count/2))
                }
            }
            self.collectionView.reloadData()
            
            UIView.animate(withDuration: 0.1, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                self.activityIndicator.stopAnimating()
                self.collectionView.reloadData()
                DispatchQueue.main.async(execute: {
//                    print(" News data : ", self.countNews)
                    if self.countNews > 0 {
                        let article = self.ArticlesArray[0] as! News
                        self.showNewsTitle(article: article.title)
                    } else {
                        let article = self.ArticlesArray[0] as! Articles
                        self.showNewsTitle(article: article.title)
                    }
                })
            }, completion:  { (Bool) in
                self.timerNews = Timer.scheduledTimer(timeInterval: 8, target: self, selector: #selector(self.autoScroll), userInfo: nil, repeats: true)
            })
            
        } catch {
            print(" error getting announcements :", error)
        }
        
    }
    
    private func getNews(){
        getApi {
            DispatchQueue.main.async(execute: {
                self.appendNews()
            })
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if didLoaded && NewsArray.count > 0 {
            let database = try! manager.databaseNamed("news")
            countNews = Int(database.documentCount)
            pageNumber = 0
            let indexPath = NSIndexPath(item: pageNumber, section: 0)
            collectionView.scrollToItem(at: indexPath as IndexPath, at: [], animated: false)
            let article = self.ArticlesArray[pageNumber] as! Articles
            self.showNewsTitle(article: article.title)
            
            self.timerNews = Timer.scheduledTimer(timeInterval: 8, target: self, selector: #selector(self.autoScroll), userInfo: nil, repeats: true)
            self.timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.getTime), userInfo: nil, repeats: true)
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        //did load all
        didLoaded = true
        if didLoaded && NewsArray.count > 0 {
            DispatchQueue.main.async(execute: {
                self.timerNews?.invalidate()
                self.timer?.invalidate()
            })
        }
    }

//    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
//        if UIDevice.current.orientation.isLandscape {
//            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
//                self.statusBarBackground.alpha = 0
//            }, completion: { (Bool) in })
//        } else {
//            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
//                self.statusBarBackground.alpha = 1
//            }, completion: { (Bool) in })
//        }
//    }
//
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.white
        self.navigationController?.isNavigationBarHidden = false
        
        chatText.delegate = self
        
//        let database = try! manager.databaseNamed("news")
//        if Int(database.documentCount) == 0 {
//            self.getApi {
//                DispatchQueue.main.async(execute: {
//                    UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
//                        self.activityIndicator.stopAnimating()
//                        self.collectionView.reloadData()
//                        let article = self.ArticlesArray[0] as! Articles
//                        self.showNewsTitle(article: article.title)
//                    }, completion:  { (Bool) in
//                        self.timerNews = Timer.scheduledTimer(timeInterval: 8, target: self, selector: #selector(self.autoScroll), userInfo: nil, repeats: true)
//                    })
//                })
//            }
//        }
//        self.timerNews?.invalidate()
        

        setupView()
        setupConstraints()
        setupGestures()
//        loadChat(chnlSelect: HomeController.currentChannel)
        watch()
        setupSchedule()
        
        NotificationCenter.default.addObserver(self, selector: #selector(HomeController.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(HomeController.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
}

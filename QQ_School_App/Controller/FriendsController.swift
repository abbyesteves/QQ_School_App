//
//  FriendsController.swift
//  QQ_School_App
//
//  Created by Abby Esteves on 08/03/2018.
//  Copyright © 2018 Abby Esteves. All rights reserved.
//

import UIKit

class FriendsController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout{
    
    let cellId = "cellId"
    let cellRequestId = "cellRequestId"
    let cellFriendId = "cellFriendId"
    var activityIndicator : UIActivityIndicatorView = UIActivityIndicatorView()
    let inviteButton: UIButton = {
        let button = UIButton()
        button.backgroundColor = UIColor.Theme(alpha: 1.0)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 13)
        button.setTitleColor(UIColor.white, for: .normal)
        button.layer.borderColor = UIColor.Theme(alpha: 1.0).cgColor
        button.layer.borderWidth = 0.5
        button.setTitle("Invite a Friend", for: .normal)
        return button
    }()
    
    let friendsButton: UIButton = {
        let button = UIButton()
        button.backgroundColor = UIColor.white
        button.layer.borderColor = UIColor.Theme(alpha: 1.0).cgColor
        button.layer.borderWidth = 0.5
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 13)
        button.setTitleColor(UIColor.Theme(alpha: 1.0), for: .normal)
        button.setTitle("My Friends", for: .normal)
        return button
    }()
    
    let requestButton: UIButton = {
        let button = UIButton()
        button.backgroundColor = UIColor.white
        button.layer.borderColor = UIColor.Theme(alpha: 1.0).cgColor
        button.layer.borderWidth = 0.5
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 13)
        button.setTitleColor(UIColor.Theme(alpha: 1.0), for: .normal)
        button.setTitle("Friend Requests", for: .normal)
        return button
    }()
    
    let pendingView : UIView = {
        let view = UIView()
        view.backgroundColor = .red
        return view
    }()
    
    let collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.backgroundColor = UIColor.white
        return cv
    }()
    
    let FriendRequestView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.backgroundColor = UIColor.white
        return cv
    }()
    
    let FriendView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.backgroundColor = UIColor.white
        return cv
    }()
    
    var requests = [Friends]()
    var friends = [Friends]()
    
    //mock up friends list
    var users = [Users]()
    var invites: [Friends] = {
        
        var friend1 = Friends()
        friend1.qq_id = "QQuser004"
        friend1.name = "Mark Barrientos"
        friend1.grade = "12"
        friend1.section = "1st"
        
        var friend2 = Friends()
        friend2.qq_id = "QQuser005"
        friend1.name = "Mars Sulit"
        friend2.grade = "12"
        friend2.section = "1st"
        
        var friend3 = Friends()
        friend3.qq_id = "QQuser006"
        friend1.name = "Jan Relente"
        friend3.grade = "12"
        friend3.section = "1st"
        
        var friend4 = Friends()
        friend4.qq_id = "QQuser007"
        friend1.name = "Clairol Salazar"
        friend4.grade = "12"
        friend4.section = "1st"
        
        var friend5 = Friends()
        friend5.qq_id = "QQuser008"
        friend1.name = "Carlo Nunag"
        friend5.grade = "12"
        friend5.section = "1st"
        
        return [friend1, friend2, friend3, friend4, friend5]
    }()
    
    @objc func inviteFriends() {
        for view in pendingView.subviews {
            view.removeFromSuperview()
        }
        requests.removeAll()
        FriendRequestView.reloadData()
        view.addSubview(collectionView)
        view.addSubview(pendingView)
        collectionView.frame = CGRect(x: 0, y: friendsButton.frame.maxY+20, width: view.frame.width, height: view.frame.height-50)
        collectionView.reloadData()
        
        watchRequest()

        buttonSet(button: inviteButton)
        buttonReset(button: friendsButton)
        buttonReset(button: requestButton)
        
    }
    
    @objc func requestFriends() {
//        for view in pendingView.subviews {
//            view.removeFromSuperview()
//        }
//        collectionView.removeFromSuperview()
        view.addSubview(FriendRequestView)
        FriendRequestView.frame = CGRect(x: 0, y: friendsButton.frame.maxY+20, width: view.frame.width, height: view.frame.height-50)
        
        buttonReset(button: inviteButton)
        buttonReset(button: friendsButton)
        buttonSet(button: requestButton)
        
        let requests = ApiService().friendsList(database : "request")
        self.addRequest(requests : requests)
    }
    
    @objc func myFriends() {
        
        view.addSubview(FriendView)
        FriendView.frame = CGRect(x: 0, y: friendsButton.frame.maxY+20, width: view.frame.width, height: view.frame.height-50)
        
        buttonReset(button: inviteButton)
        buttonSet(button: friendsButton)
        buttonReset(button: requestButton)
    }

    //func
    
    private func checkType(type : String){
        let manager = CBLManager.sharedInstance()
        let database = try! manager.databaseNamed("request")
        let query = database.createAllDocumentsQuery()
        //        print("all data in database receive : ", database.documentCount)
        
        do {
            let result = try query.run()
            if result != nil {
                while let row = result.nextRow(){
                    let document = row.document?.properties
//                    print(" row doc ", (document!["img"])!)
                    if type == "request" {
                        if "\((document!["req_id"])!)" == "\((ApiService().myID()["qq_id"])!)" {
                            var friend = Friends()
                            friend.qq_id = "\((document!["qq_id"])!)"
                            friend.name = "\((document!["firstname"])!) \((document!["middlename"])!) \((document!["lastname"])!)"
                            friend.grade = "\((document!["grade"])!)"
                            friend.section = "\((document!["section"])!)"
                            friend.img = "\((document!["img"])!)"
                            friend.type = "request"
                            invites.insert(friend, at: 0)
                        }
                    } else {
                        if "\((document!["req_id"])!)" != "\((ApiService().myID()["qq_id"])!)" {
                            var friend = Friends()
                            friend.qq_id = "\((document!["qq_id"])!)"
                            friend.name = "\((document!["firstname"])!) \((document!["middlename"])!) \((document!["lastname"])!)"
                            friend.grade = "\((document!["grade"])!)"
                            friend.section = "\((document!["section"])!)"
//                            friend.img = "\((document!["img"])!)"
                            friend.type = "pending"
                            invites.insert(friend, at: 0)
                        }
                    }
                    
                }
                collectionView.reloadData()
            }
        } catch let error as NSError {
            print("ERROR ", error)
        }
        
    }
    
    func buttonSet(button : UIButton) {
        button.backgroundColor = UIColor.Theme(alpha: 1.0)
        button.setTitleColor(UIColor.white, for: .normal)
    }
    
    func buttonReset(button : UIButton){
        button.backgroundColor = UIColor.white
        button.setTitleColor(UIColor.Theme(alpha: 1.0), for: .normal)
    }
    
    private func setupView(){
        activityIndicator.center = self.view.center
        activityIndicator.hidesWhenStopped = true
        activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.gray
        
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.scrollsToTop = true
        collectionView.contentInset = UIEdgeInsetsMake(10, 0, 80, 0)
        collectionView.backgroundColor = UIColor.Background(alpha: 1.0)
        collectionView.register(FriendsInviteCell.self, forCellWithReuseIdentifier: cellId)
        
        
        FriendRequestView.delegate = self
        FriendRequestView.dataSource = self
        FriendRequestView.scrollsToTop = true
        FriendRequestView.contentInset = UIEdgeInsetsMake(10, 0, 80, 0)
        FriendRequestView.backgroundColor = UIColor.Background(alpha: 1.0)
        FriendRequestView.register(FriendRequestCell.self, forCellWithReuseIdentifier: cellRequestId)
        
        
        FriendView.delegate = self
        FriendView.dataSource = self
        FriendView.scrollsToTop = true
        FriendView.contentInset = UIEdgeInsetsMake(10, 0, 80, 0)
        FriendView.backgroundColor = UIColor.Background(alpha: 1.0)
        FriendView.register(FriendsCell.self, forCellWithReuseIdentifier: cellFriendId)
        
        
        view.addSubview(friendsButton)
        view.addSubview(inviteButton)
        view.addSubview(requestButton)
        view.addSubview(pendingView)
        view.addSubview(activityIndicator)
        
        view.addSubview(collectionView)
        
        activityIndicator.startAnimating()
        
        friendsButton.frame = CGRect(x: 10, y: 10, width: view.frame.width/3-(6.66), height: 40)
        inviteButton.frame = CGRect(x: friendsButton.frame.maxX, y: 10, width: view.frame.width/3-(6.66), height: 40)
        requestButton.frame = CGRect(x: inviteButton.frame.maxX, y: 10, width: view.frame.width/3-(6.66), height: 40)
        activityIndicator.frame = CGRect(x: 0, y: 100, width: view.frame.width, height: 20)
        collectionView.frame = CGRect(x: 0, y: friendsButton.frame.maxY+20, width: view.frame.width, height: view.frame.height-50)
        
        inviteButton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(inviteFriends)))
        friendsButton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(myFriends)))
        requestButton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(requestFriends)))
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == FriendRequestView {
            return requests.count
        } else if collectionView == FriendView {
            return friends.count
        }
        return users.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == FriendRequestView {
            let cellRequest = collectionView.dequeueReusableCell(withReuseIdentifier: cellRequestId, for: indexPath) as! FriendRequestCell
            cellRequest.backgroundColor = UIColor.white
            cellRequest.layer.shadowColor = UIColor.gray.cgColor
            cellRequest.layer.shadowOpacity = 0.5
            cellRequest.layer.shadowOffset = CGSize(width: 0, height: 1)
            cellRequest.layer.shadowRadius = 1
            cellRequest.friend = requests[indexPath.item]
            return cellRequest
            
        } else if collectionView == FriendView {
            let cellfriend = collectionView.dequeueReusableCell(withReuseIdentifier: cellFriendId, for: indexPath) as! FriendsCell
            cellfriend.backgroundColor = UIColor.white
            cellfriend.layer.shadowColor = UIColor.gray.cgColor
            cellfriend.layer.shadowOpacity = 0.5
            cellfriend.layer.shadowOffset = CGSize(width: 0, height: 1)
            cellfriend.layer.shadowRadius = 1
            cellfriend.friend = friends[indexPath.item]
            return cellfriend
        }
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! FriendsInviteCell
        cell.backgroundColor = UIColor.white
        cell.layer.shadowColor = UIColor.gray.cgColor
        cell.layer.shadowOpacity = 0.5
        cell.layer.shadowOffset = CGSize(width: 0, height: 1)
        cell.layer.shadowRadius = 1
        cell.user = users[indexPath.item]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width, height: 80)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {

        if collectionView == FriendRequestView {
            let request = requests[indexPath.item] as Friends
            
            let alertController = UIAlertController(title: "Respond to friend request", message: "from \((request.name)!)", preferredStyle: UIAlertControllerStyle.actionSheet)
            alertController.view.frame = CGRect(x: (view.frame.width-(view.frame.width-20))/2, y: (view.frame.height-270)/2, width: view.frame.width-20, height: 270)
            
            alertController.addAction(UIAlertAction(title: NSLocalizedString("Confirm", comment: "Default action"), style: UIAlertActionStyle.default, handler: { _ in
                ApiService().updateFriendRequest(friend : request, status : "accept")
            }))
            
            alertController.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: UIAlertActionStyle.cancel, handler: { _ in
                
            }))
            
            alertController.addAction(UIAlertAction(title: NSLocalizedString("Reject", comment: ""), style: UIAlertActionStyle.destructive, handler: { _ in
                ApiService().deleteFriendRequest(friend : request)
            }))
            
            let alertWindow = UIWindow(frame: UIScreen.main.bounds)
            alertWindow.rootViewController = UIViewController()
            alertWindow.windowLevel = UIWindowLevelAlert + 1;
            alertWindow.makeKeyAndVisible()
            alertWindow.rootViewController?.present(alertController, animated: true, completion: nil)
            
        } else if collectionView == FriendView {
//            let friend = friends[indexPath.item] as Friends
//            let alertController = UIAlertController(title: "Message \((friend.name)!)", message: "would you want to send a message?", preferredStyle: UIAlertControllerStyle.actionSheet)
//            alertController.view.frame = CGRect(x: (view.frame.width-(view.frame.width-20))/2, y: (view.frame.height-270)/2, width: view.frame.width-20, height: 270)
//
//            alertController.addAction(UIAlertAction(title: NSLocalizedString("Confirm", comment: "Default action"), style: UIAlertActionStyle.default, handler: { _ in
//                
//            }))
//
//            alertController.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: UIAlertActionStyle.destructive, handler: { _ in
//
//            }))
//
//            let alertWindow = UIWindow(frame: UIScreen.main.bounds)
//            alertWindow.rootViewController = UIViewController()
//            alertWindow.windowLevel = UIWindowLevelAlert + 1;
//            alertWindow.makeKeyAndVisible()
//            alertWindow.rootViewController?.present(alertController, animated: true, completion: nil)
//
        } else {
            let friend = users[indexPath.item] as Users
            let alertController = UIAlertController(title: "Add \((friend.firstname)!)", message: "would you want to send a request?", preferredStyle: UIAlertControllerStyle.actionSheet)
            alertController.view.frame = CGRect(x: (view.frame.width-(view.frame.width-20))/2, y: (view.frame.height-270)/2, width: view.frame.width-20, height: 270)
            
            alertController.addAction(UIAlertAction(title: NSLocalizedString("Yes", comment: "Default action"), style: UIAlertActionStyle.default, handler: { _ in
                ApiService().sendFriendRequest(friend: friend, status: "pending")
            }))
            
            alertController.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: UIAlertActionStyle.destructive, handler: { _ in
                
            }))
            
            let alertWindow = UIWindow(frame: UIScreen.main.bounds)
            alertWindow.rootViewController = UIViewController()
            alertWindow.windowLevel = UIWindowLevelAlert + 1;
            alertWindow.makeKeyAndVisible()
            alertWindow.rootViewController?.present(alertController, animated: true, completion: nil)
        }
    }
    
    private func watchRequest(){
        let manager = CBLManager.sharedInstance()
        
        //request
        let request = try! manager.databaseNamed("request")
        //get all request from database
        self.callFriends()
        let requests = ApiService().friendsList(database : "request")
        self.addInviteRequest(requests : requests)
        self.addFriendRequest(requests : requests)
        self.addRequest(requests: requests)
        self.addFriend()
        
        NotificationCenter.default.addObserver(forName: NSNotification.Name.cblDatabaseChange, object: request, queue: nil) {
            (notification) -> Void in
            if let changes = notification.userInfo!["changes"] as? [CBLDatabaseChange] {
                for change in changes {
                    print("friend request, revision ID '%@'", change.description)
                    UIView.animate(withDuration: 0.1, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                        //get all request from database
                        self.checkFriendsList()
                        self.filterMeOut()
                        let requests = ApiService().friendsList(database : "request")
                        self.addInviteRequest(requests : requests)
                        self.addFriendRequest(requests : requests)
                        self.addRequest(requests: requests)
                        self.addFriend()
                    }, completion: { (Bool) in })
                }
            }
        }
    }
    
    private func checkFriendsList() {
        let currents = ApiService().friendsList(database : "request") as? [Friends]
        for current in currents! {
            self.users = self.users.filter { $0.qq_id != "\((current.qq_id)!)"}
        }
    }
    
    private func filterMeOut(){
        let me = "\((ApiService().myID()["qq_id"])!)"
        for user in users {
            self.users = self.users.filter { $0.qq_id != me}
        }
    }
    
    private func addFriend(){
        friends = ApiService().friendsList(database : "friends")
    }
    
    private func addFriendRequest(requests : [Friends]){
        if requests.count != 0 {
            for request in requests {
                if "\((request.status)!)" == "accept"{
                    ApiService().addFriend(friend : request)
                }
            }
        }
    }
    
    private func addRequest(requests : [Friends]){
        if requests.count != 0 {
            for request in requests {
                
                if "\((request.status)!)" == "pending"{
                    if "\((request.req_id)!)" != "\((ApiService().myID()["qq_id"])!)"{
                        self.requests.removeAll()
                        var friend1 = Friends()
                        friend1.qq_id = "\((request.req_id)!)"
                        friend1.name = "\((request.req_name)!)"
                        friend1.grade = "\((request.req_grade)!)"
                        friend1.section = "\((request.req_section)!)"
                        self.requests.append(friend1)
                        self.FriendRequestView.reloadData()
                    }
                }
            }
        }
    }
    
    private func addInviteRequest(requests : [Friends]){
        if requests.count != 0 {
            var y = inviteButton.frame.maxY+20
            
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                
                for request in requests {
                    
                    let requestView : UIView = {
                        let view = UIView()
                        view.backgroundColor = .white
                        view.layer.shadowOpacity = 0.5
                        view.layer.shadowOffset = CGSize(width: 0, height: 1)
                        view.layer.shadowRadius = 1
                        return view
                    }()
                    
                    let thumbnailView: UIImageView = {
                        let imageView = UIImageView()
                        imageView.contentMode = .scaleAspectFill
                        imageView.clipsToBounds = true
                        imageView.backgroundColor = .clear
                        imageView.layer.masksToBounds = true
                        imageView.image = UIImage(named: "base64")
                        imageView.layer.cornerRadius = 25
                        return imageView
                    }()
                    
                    let nameLabel: UILabel = {
                        let label = UILabel()
                        label.text = "\((request.name)!)"
                        label.textColor = UIColor.black
                        label.numberOfLines = 2
                        label.font = label.font.withSize(15)
                        return label
                    }()
                    
                    let gradeSectionLabel: UILabel = {
                        let label = UILabel()
                        label.text = "Grade: \((request.grade)!) • Section: \((request.section)!)"
                        label.textColor = UIColor.gray
                        label.numberOfLines = 2
                        label.font = label.font.withSize(13)
                        return label
                    }()
                    
                    let statusView: UIImageView = {
                        let imageView = UIImageView()
                        imageView.contentMode = .scaleAspectFill
                        imageView.clipsToBounds = true
                        imageView.backgroundColor = .clear
                        imageView.layer.masksToBounds = true
                        imageView.image = UIImage(named: "ic_pending")?.withRenderingMode(.alwaysTemplate)
                        imageView.tintColor = UIColor.Theme(alpha: 1.0)
                        return imageView
                    }()
                    
                    if let image = request.b_64 {
                        if image != "" {
                            thumbnailView.image = UIView.convertBase64ToImage(imageString: "\(image)" )
                        }
                    }
                    
                    if "\((request.req_id)!)" == "\((ApiService().myID()["qq_id"])!)"{
                        if "\((request.status)!)" == "pending"{
                            self.invites = self.invites.filter { $0.qq_id != "\((request.qq_id)!)"}
                            
                            self.pendingView.addSubview(requestView)
                            requestView.addSubview(thumbnailView)
                            requestView.addSubview(nameLabel)
                            requestView.addSubview(gradeSectionLabel)
                            requestView.addSubview(statusView)
                            
                            requestView.frame = CGRect(x: 0, y: y, width: self.view.frame.width, height: 80)
                            self.view.addConstraintsFormat(format: "H:|-20-[v0(50)]-20-[v1]-20-|", views: thumbnailView, nameLabel)
                            self.view.addConstraintsFormat(format: "H:|-\(self.view.frame.width-30-20)-[v0(30)]-20-|", views: statusView)
                            self.view.addConstraintsFormat(format: "V:|-25-[v0(30)]-25-|", views: statusView)
                            self.view.addConstraintsFormat(format: "V:|-20-[v0][v1]-20-|", views: nameLabel, gradeSectionLabel)
                            self.view.addConstraintsFormat(format: "H:|-90-[v0]-20-|", views: gradeSectionLabel)
                            self.view.addConstraintsFormat(format: "V:|-15-[v0(50)]-15-|", views: thumbnailView)
                            
                            y = y+requestView.frame.height+1
                        }
                    }
                }
                
            }, completion: { (Bool) in
                UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                    self.collectionView.frame = CGRect(x: 0, y: y, width: self.view.frame.width, height: self.view.frame.height-50)
                }, completion: { (Bool) in
                    self.collectionView.reloadData()
                })
            })
        }
        
    }
    

    func getFriends(completion: @escaping () -> Void){
        DispatchQueue.global().asyncAfter(deadline: .now()) {
            var urlString = String()
//            var url = URL()
            if "\((ApiService().myID()["user_type"])!)" == "teacher" {
//                url = ApiService().friendsTeacherURL
                urlString = "http://192.168.1.110/local-school-backend/backend/get_list_teachers"
            } else {
//                url = ApiService().friendsStudentURL
                urlString = "http://192.168.1.110/local-school-backend/backend/get_list_students"
            }
            
            guard let url = URL(string: urlString) else { return }
            
            URLSession.shared.dataTask(with: url) { (data, response, err) in
                guard let data = data else { return }
                
                do {
                    let json = try JSONDecoder().decode(FriendList.self, from: data)
                    self.users = json.users
                    completion()
                } catch let jsonErr{
                    print("ERROR Serializing: ", jsonErr)
                }
                
            }.resume()
        }
    }
    
    func callFriends(){
        getFriends {
            DispatchQueue.main.async(execute: {
                UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                    self.checkFriendsList()
                    self.filterMeOut()
                    self.activityIndicator.stopAnimating()
                    self.collectionView.reloadData()
                }, completion:  { (Bool) in })
            })
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.Background(alpha: 1.0)
        
        callFriends()
        setupView()
        watchRequest() // if someone sent me a request or respond to my request
    }
}

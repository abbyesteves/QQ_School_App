//
//  ClassesController.swift
//  QQ_School_App
//
//  Created by Abby Esteves on 02/03/2018.
//  Copyright © 2018 Abby Esteves. All rights reserved.
//

import UIKit

class SubjectsController: UICollectionViewController, UICollectionViewDelegateFlowLayout {
    var cellId = "cellId"
    var classes: [Classes] = {
        var data1 = Classes()
        data1.subject = "English"
        data1.time = "07:00 AM - 09:00 AM"
        data1.topic = "Literature"
        
        var data2 = Classes()
        data2.subject = "Filipino"
        data2.time = "09:00 AM - 10:30 AM"
        data2.topic = "El Filibusterismo"
        
        return [data1, data2]
    }()
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return classes.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! ClassesCell
        cell.backgroundColor = UIColor.white
        cell.layer.shadowColor = UIColor.gray.cgColor
        cell.layer.shadowOpacity = 0.5
        cell.layer.shadowOffset = CGSize(width: 0, height: 1)
        cell.layer.shadowRadius = 1
        cell.layer.cornerRadius = 10
        cell.classList = classes[indexPath.item]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width-20, height: 100)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        collectionView?.scrollsToTop = true
        collectionView?.contentInset = UIEdgeInsetsMake(10, 0, 10, 0)
        collectionView?.backgroundColor = UIColor.Background(alpha: 1.0)
        collectionView?.register(ClassesCell.self, forCellWithReuseIdentifier: cellId)
        
    }
    
}

//
//  NotesController.swift
//  QQ_School_App
//
//  Created by Abby Esteves on 02/03/2018.
//  Copyright © 2018 Abby Esteves. All rights reserved.
//

import UIKit

class NotesController: UICollectionViewController, UICollectionViewDelegateFlowLayout {
    
    var cellId = "cellId"
    let manager = CBLManager.sharedInstance()
    var notes = ApiService().noteList()
    var didAddNote = false
    var keyboardHeight = CGFloat()
    let navbarStatusHeight = UIApplication.shared.statusBarFrame.height
    let bottomSpacing = UIApplication.shared.keyWindow?.safeAreaInsets.bottom
    let tabBarHeight = UITabBarController.init().tabBar.frame.height
    
//    var notes : [Notes] = {
//        var data1 = Notes()
//        data1.title = "Create a lesson plan"
//        data1.content = "Deadline March 03, 2018"
//        data1.date = "February 26,2018"
//
//        return [data1]
//    }()
    
    let bgNote : UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(white: 0, alpha: 0.6)
        return view
    }()
    
    let addMainView : UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.white
        view.layer.cornerRadius = 10
        return view
    }()
    
    let addNoteImage: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "ic_add")?.withRenderingMode(.alwaysTemplate)
        imageView.tintColor = UIColor.white
        return imageView
    }()
    
    let addNoteView : UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.ThemeDarkGreen(alpha: 1.0)
        view.layer.shadowColor = UIColor.gray.cgColor
        view.layer.shadowOpacity = 0.5
        view.layer.shadowOffset = CGSize(width: 0, height: 2)
        view.layer.shadowRadius = 3
        view.layer.cornerRadius = 25
        return view
    }()
    
    let titleText: UITextField = {
        let text = UITextField()
        text.layer.borderWidth = 1.0
        text.layer.borderColor = UIColor.lightGray.cgColor
        text.backgroundColor = .clear//UIColor.rgba(red: 255, green: 255, blue: 255, alpha: 0.5)
        text.textColor = UIColor.darkGray
        var placeholder = NSMutableAttributedString()
        placeholder = NSMutableAttributedString(attributedString: NSAttributedString(string: "Title", attributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 11.0), .foregroundColor: UIColor.lightGray]))
        text.setLeftPadding(space: 8)
        text.font = text.font?.withSize(11)
        text.attributedPlaceholder = placeholder
        text.layer.cornerRadius = 5
        return text
    }()
    
    let contentText: UITextView = {
        let textView = UITextView()
        textView.backgroundColor = UIColor.clear
        textView.textColor = UIColor.darkGray
        textView.layer.borderWidth = 1.0
        textView.layer.borderColor = UIColor.lightGray.cgColor
        textView.isEditable = true
        textView.isSelectable = true
        textView.isScrollEnabled = true
        textView.layer.cornerRadius = 5
        textView.font = textView.font?.withSize(10)
        return textView
    }()
    
    let saveButton: UIButton = {
        let button = UIButton()
        button.backgroundColor = UIColor.ThemeDarkGreen(alpha: 1.0)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 13)
        button.setTitleColor(UIColor.white, for: .normal)
        button.setTitle("SAVE", for: .normal)
        return button
    }()
    
    let cancelButton: UIButton = {
        let button = UIButton()
        button.backgroundColor = UIColor.ThemeDarkGreen(alpha: 1.0)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 13)
        button.setTitleColor(UIColor.white, for: .normal)
        button.setTitle("SAVE", for: .normal)
        return button
    }()
    
    let emptyLabel: UILabel = {
        let label = UILabel()
        label.text = "No Notes"
        label.textColor = UIColor.lightGray
        label.numberOfLines = 2
        label.textAlignment = .center
        label.font = UIFont.boldSystemFont(ofSize: label.font.pointSize)
        label.font = label.font.withSize(12)
        label.alpha = 0.5
        return label
    }()
    
    let emptyImage: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.clipsToBounds = true
        imageView.backgroundColor = .clear
        imageView.layer.masksToBounds = true
        imageView.image = UIImage(named: "ic_empty")?.withRenderingMode(.alwaysTemplate)
        imageView.tintColor = UIColor.lightGray
        imageView.alpha = 0.2
        return imageView
    }()
    
    @objc func closeNote() {
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.bgNote.alpha = 0
            self.addMainView.frame = CGRect(x: (self.view.frame.width)/2, y: (self.view.frame.height)/2, width: 0, height: 0)
            self.titleText.frame = CGRect(x: 0, y: 0, width: 0, height: 0)
            self.contentText.frame = CGRect(x: 0, y: 0, width: 0, height: 0)
        }, completion: { (Bool) in })
    }
    
    @objc func addNote() {
        goToNoteLauncher(documentID: "")
    }
    
    private func goToNoteLauncher(documentID: String) {
        let notesLauncher = NotesLauncher()
        
        let titleLabel : UILabel = {
            let label = UILabel()
            label.textColor = UIColor.white
            label.font = UIFont(name: "GillSans-Bold", size: UIFont.systemFontSize)!
            label.font = label.font.withSize(15)
            label.text = "Add Note".uppercased()
            return label
        }()
        
        navigationController?.pushViewController(notesLauncher, animated: true)
        
        notesLauncher.navigationItem.title = "\(documentID)"
        notesLauncher.navigationItem.titleView = titleLabel
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return notes.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! NotesCell
        cell.backgroundColor = UIColor.white
        cell.layer.shadowColor = UIColor.gray.cgColor
        cell.layer.shadowOpacity = 0.5
        cell.layer.shadowOffset = CGSize(width: 0, height: 1)
        cell.layer.shadowRadius = 1
//        cell.layer.cornerRadius = 10
        cell.note = notes[indexPath.item]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
//        let data = notes[indexPath.item]
//        let heightEstimation = view.heightEstimation(text: data.content, width: view.frame.width-20, size: 13, defaultHeight: 20)
//        print( " content : ", data.content, heightEstimation )
//        return CGSize(width: view.frame.width-20, height: 80+heightEstimation)
        return CGSize(width: view.frame.width, height: 100)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let data = notes[indexPath.item] as Notes
        goToNoteLauncher(documentID: "\((data.documentID)!)")
    }
    
    private func setupView(){
        collectionView?.scrollsToTop = true
        if "\((ApiService().myID()["user_type"])!)" == "teacher" {
            collectionView?.contentInset = UIEdgeInsetsMake(20, 0, 110, 0)
        } else if "\((ApiService().myID()["user_type"])!)" == "student" {
            collectionView?.contentInset = UIEdgeInsetsMake(20, 0, 20, 0)
        }
        collectionView?.backgroundColor = UIColor.Background(alpha: 1.0)
        collectionView?.register(NotesCell.self, forCellWithReuseIdentifier: cellId)
        collectionView?.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height)
        
        view.addSubview(addNoteView)
        addNoteView.addSubview(addNoteImage)
        
        view.addSubview(emptyImage)
        view.addSubview(emptyLabel)
    }
    
    private func setupConstraints(){
        addNoteView.frame = CGRect(x: view.frame.width-70, y: CGFloat((collectionView?.frame.height)!)-(navbarStatusHeight+bottomSpacing!+tabBarHeight+110), width: 50, height: 50)
        addNoteImage.frame = CGRect(x: 15, y: 15, width: 20, height: 20)
        
        emptyLabel.frame = CGRect(x: 10, y: view.frame.height/3, width: view.frame.width-20, height: 20)
        emptyImage.frame = CGRect(x: (view.frame.width/2)-((view.frame.width/3)/2), y: emptyLabel.frame.maxY, width: view.frame.width/3, height: view.frame.width/3)
    }
    
    private func setupGestures(){
        addNoteView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(addNote)))
        addMainView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(closeNote)))
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            keyboardHeight = keyboardSize.height
            
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            
        }
    }
    
    private func watchNotes() {
        let notes = try! manager.databaseNamed("notes")
        if notes.documentCount == 0 {
            addNote()
        }
        self.getNotes()
        NotificationCenter.default.addObserver(forName: NSNotification.Name.cblDatabaseChange, object: notes, queue: nil) {
            (notification) -> Void in
            if let changes = notification.userInfo!["changes"] as? [CBLDatabaseChange] {
                for change in changes {
                    print("announcement, revision ID '%@'", change.description)
                    self.getNotes()
                }
            }
        }
    }
    
    private func getNotes(){
        notes = ApiService().noteList()
        if notes.count > 0 {
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                self.emptyLabel.alpha = 0
                self.emptyImage.alpha = 0
            }, completion:  { (Bool) in })
        }
        collectionView?.reloadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        if didAddNote {
            getNotes()
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillAppear(animated)
        didAddNote = true
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
        setupConstraints()
        setupGestures()
        watchNotes()
        
        NotificationCenter.default.addObserver(self, selector: #selector(NotesController.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(NotesController.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
    }
    
}

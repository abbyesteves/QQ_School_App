//
//  TaskController.swift
//  QQ_School_App
//
//  Created by Abby Esteves on 05/03/2018.
//  Copyright © 2018 Abby Esteves. All rights reserved.
//

import UIKit
import AnyFormatKit

class TasksController: UICollectionViewController, UICollectionViewDelegateFlowLayout, UITextFieldDelegate, UITextViewDelegate {
    
    var cellId = "cellId"
    var tasks = ApiService().taskList()
    var didOpenDropDown = true
    let manager = CBLManager.sharedInstance()
    let dateFormatter = TextFormatter(textPattern: "##/##/####")
    let navbarStatusHeight = UIApplication.shared.statusBarFrame.height
    let bottomSpacing = UIApplication.shared.keyWindow?.safeAreaInsets.bottom
    let tabBarHeight = UITabBarController.init().tabBar.frame.height
    
    let createView : UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.white
        view.layer.cornerRadius = 10
        return view
    }()
    
    let blackView : UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(white: 0, alpha: 0.6)
        return view
    }()
    
    let subjectLabel: UILabel = {
        let label = UILabel()
        label.text = "Subject"
        label.textColor = UIColor.gray
        label.numberOfLines = 2
        label.font = label.font.withSize(15)
        return label
    }()
    
    let selectSubjectLabel: UILabel = {
        let label = UILabel()
        label.text = ""
        label.textColor = UIColor.gray
        label.numberOfLines = 2
        label.font = label.font.withSize(15)
        return label
    }()
    
    let selectSubjectImage: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.backgroundColor = .clear
        imageView.layer.masksToBounds = true
        imageView.image = UIImage(named: "ic_caret")?.withRenderingMode(.alwaysTemplate)
        imageView.tintColor = UIColor.lightGray
        return imageView
    }()
    
    let selectSubjectView : UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.white
        view.layer.borderWidth = 1.0
        view.layer.borderColor = UIColor.lightGray.cgColor
        return view
    }()
    
    let dropDownView : UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.Background(alpha: 1.0)
        view.layer.shadowColor = UIColor.gray.cgColor
        view.layer.shadowOpacity = 0.5
        view.layer.shadowOffset = CGSize(width: 0, height: 1)
        view.layer.shadowRadius = 1
        view.layer.borderWidth = 1.0
        view.layer.borderColor = UIColor.lightGray.cgColor
        return view
    }()
    
    let taskLabel: UILabel = {
        let label = UILabel()
        label.text = "Task"
        label.textColor = UIColor.gray
        label.numberOfLines = 2
        label.font = label.font.withSize(15)
        return label
    }()
    
    let taskText: UITextField = {
        let text = UITextField()
        text.text = ""
        text.borderStyle = .none
        text.backgroundColor = .white
        text.textColor = UIColor.darkGray
        text.font = .systemFont(ofSize: 13)
        text.layer.borderWidth = 1.0
        text.layer.borderColor = UIColor.lightGray.cgColor
        text.autocorrectionType = .no
        text.setLeftPadding(space: 8)
        return text
    }()
    
    let dueLabel: UILabel = {
        let label = UILabel()
        label.text = "Exam Due :"
        label.textColor = UIColor.gray
        label.numberOfLines = 2
        label.font = label.font.withSize(15)
        return label
    }()
    
    let taskDueTextLabel: UILabel = {
        let label = UILabel()
        label.text = "MM/DD/YYYY"
        label.textColor = UIColor.gray
        label.numberOfLines = 2
        label.font = label.font.withSize(15)
        return label
    }()
    
    let taskDueText: UITextView = {
        let text = UITextView()
        text.text = ""
        text.keyboardType = .numberPad
        text.backgroundColor = UIColor.clear
        text.textColor = UIColor.clear
        text.font = .systemFont(ofSize: 15)
        text.layer.borderWidth = 1.0
        text.layer.borderColor = UIColor.lightGray.cgColor
        text.autocorrectionType = .no
        return text
    }()
    
//    let taskDueText: UITextField = {
//        let text = UITextField()
//        text.text = ""
//        text.keyboardType = .numberPad
//        text.borderStyle = .none
//        text.backgroundColor = .white
//        text.textColor = UIColor.darkGray
//        text.font = .systemFont(ofSize: 13)
//        text.layer.borderWidth = 1.0
//        text.layer.borderColor = UIColor.lightGray.cgColor
//        text.autocorrectionType = .no
//        var placeholder = NSMutableAttributedString()
//        placeholder = NSMutableAttributedString(attributedString: NSAttributedString(string: "MM/DD/YYYY", attributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 13.0), .foregroundColor: UIColor.lightGray]))
//        text.setLeftPadding(space: 8)
//        text.attributedPlaceholder = placeholder
//        return text
//    }()
    
    let saveTask: UIButton = {
        let button = UIButton()
        button.backgroundColor = UIColor.ThemeDarkGreen(alpha: 1.0)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 13)
        button.setTitleColor(UIColor.white, for: .normal)
        button.setTitle("Add Task".uppercased(), for: .normal)
        return button
    }()
    
    let detailLabel: UILabel = {
        let label = UILabel()
        label.text = "Details"
        label.textColor = UIColor.gray
        label.numberOfLines = 2
        label.font = label.font.withSize(15)
        return label
    }()
    
    let detailText: UITextField = {
        let text = UITextField()
        text.text = ""
        text.borderStyle = .none
        text.backgroundColor = .white
        text.textColor = UIColor.darkGray
        text.font = .systemFont(ofSize: 13)
        text.layer.borderWidth = 1.0
        text.layer.borderColor = UIColor.lightGray.cgColor
        text.autocorrectionType = .no
        text.setLeftPadding(space: 8)
        return text
    }()
    
    let addNoteView : UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.ThemeDarkGreen(alpha: 1.0)
        view.layer.shadowColor = UIColor.gray.cgColor
        view.layer.shadowOpacity = 0.5
        view.layer.shadowOffset = CGSize(width: 0, height: 2)
        view.layer.shadowRadius = 3
        view.layer.cornerRadius = 25
        return view
    }()
    
    let addNoteImage: UIImageView = {
        let imageView = UIImageView()
        imageView.image = UIImage(named: "ic_add")?.withRenderingMode(.alwaysTemplate)
        imageView.tintColor = UIColor.white
        return imageView
    }()
    
    let emptyLabel: UILabel = {
        let label = UILabel()
        label.text = "No Task"
        label.textColor = UIColor.lightGray
        label.numberOfLines = 2
        label.textAlignment = .center
        label.font = UIFont.boldSystemFont(ofSize: label.font.pointSize)
        label.font = label.font.withSize(12)
        label.alpha = 0.5
        return label
    }()
    
    let emptyImage: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.clipsToBounds = true
        imageView.backgroundColor = .clear
        imageView.layer.masksToBounds = true
        imageView.image = UIImage(named: "ic_empty")?.withRenderingMode(.alwaysTemplate)
        imageView.tintColor = UIColor.lightGray
        imageView.alpha = 0.2
        return imageView
    }()
    
    @objc func getSubjectCodes() {
        if didOpenDropDown {
            didOpenDropDown = false
            var y = 0
            var subject_code = String()
            for schedule in ApiService().scheduleList() {
                let subjects = (schedule.value) as! [[String : Any]]
                if subjects.count > 0 {
                    for subject in subjects {
                        
                        if "\((subject["subject_code"])!)" != subject_code {
                            subject_code = "\((subject["subject_code"])!)"
                            
                            let subjCodeLabel: UILabel = {
                                let label = UILabel()
                                label.text = "\((subject["subject_code"])!)"
                                label.textColor = UIColor.darkGray
                                label.numberOfLines = 2
                                label.font = label.font.withSize(18)
                                label.font = UIFont.boldSystemFont(ofSize: label.font.pointSize)
                                return label
                            }()
                            
                            //                        let subjCodeDescLabel: UILabel = {
                            //                            let label = UILabel()
                            //                            label.text = "\((subject["day"])!) • Grade: \((subject["grade_lvl"])!) - Section: \((subject["section"])!)"
                            //                            label.textColor = UIColor.lightGray
                            //                            label.numberOfLines = 2
                            //                            label.font = label.font.withSize(13)
                            //                            return label
                            //                        }()
                            //
                            let subjCodeView : UIView = {
                                let view = UIView()
                                view.backgroundColor = UIColor.white
                                view.layer.shadowColor = UIColor.gray.cgColor
                                view.layer.shadowOpacity = 0.5
                                view.layer.shadowOffset = CGSize(width: 0, height: 1)
                                view.layer.shadowRadius = 1
                                return view
                            }()
                            
                            dropDownView.addSubview(subjCodeView)
                            subjCodeView.addSubview(subjCodeLabel)
                            //                        subjCodeView.addSubview(subjCodeDescLabel)
                            
                            subjCodeView.frame = CGRect(x: 0, y: y+2, width: Int(dropDownView.frame.width), height: 50)
                            subjCodeLabel.frame = CGRect(x: 10, y: 5, width: subjCodeView.frame.width-20, height: subjCodeView.frame.height-10)
                            //                        subjCodeDescLabel.frame = CGRect(x: 10, y: subjCodeLabel.frame.maxY+5, width: subjCodeView.frame.width-20, height: (subjCodeView.frame.height/2)-10)
                            
                            subjCodeView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(getSubjectCode(sender:))))
                            
                            y = y + Int(subjCodeView.frame.height + 2)
                            
                        }
                    }
                }
            }
            
            
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                self.dropDownView.frame = CGRect(x: self.dropDownView.frame.minX, y:  self.dropDownView.frame.minY, width:  self.dropDownView.frame.width, height: CGFloat(y))
            }, completion: { (Bool) in })
        } else {
            self.closeDropDown()
        }
    }
    
    @objc func createTask() {
        if (taskText.text)! != "" && (detailText.text)! != "" && (selectSubjectLabel.text)! != "" && (taskDueText.text)! != "" {
            ApiService().createTasks(title: (taskText.text)!, details: (detailText.text)!, subject_code: (selectSubjectLabel.text)!, due_date: (taskDueTextLabel.text)!)
            closeTask()
        } else if (taskText.text)! == "" {
            taskText.becomeFirstResponder()
            view.showToast(message: "Complete title")
        } else if (detailText.text)! == "" {
            detailText.becomeFirstResponder()
            view.showToast(message: "Complete detail")
        } else if (selectSubjectLabel.text)! == "" {
            selectSubjectLabel.becomeFirstResponder()
            view.showToast(message: "Select subject for task")
        } else if (taskDueText.text)! == "" {
            taskDueText.becomeFirstResponder()
            view.showToast(message: "Complete due date")
        }
    }
    
    @objc func getSubjectCode(sender: UITapGestureRecognizer) {
        let subject_code = sender.view!.subviews[0] as? UILabel
        selectSubjectLabel.text = "\((subject_code?.text)!)"
        self.closeDropDown()
    }
    
    @objc  func closeDropDown(){
        didOpenDropDown = true
        
        for ddView in dropDownView.subviews {
            ddView.removeFromSuperview()
        }
        
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.dropDownView.frame = CGRect(x: self.dropDownView.frame.minX, y:  self.dropDownView.frame.minY, width:  self.dropDownView.frame.width, height: 0)
        }, completion: { (Bool) in })
    }
    
    @objc func openTask() {
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.blackView.alpha = 1
            self.createView.frame = CGRect(x: 10, y: 50, width: self.blackView.frame.width-20, height:0)
            self.taskLabel.frame = CGRect(x: 10, y: 10, width: self.createView.frame.width-20, height: 20)
            self.taskText.frame = CGRect(x: 10, y: self.taskLabel.frame.maxY+10, width: self.createView.frame.width-20, height: 30)
            self.detailLabel.frame = CGRect(x: 10, y: self.taskText.frame.maxY+10, width: self.createView.frame.width-20, height: 20)
            self.detailText.frame = CGRect(x: 10, y: self.detailLabel.frame.maxY+10, width: self.createView.frame.width-20, height: 30)
            self.subjectLabel.frame = CGRect(x: 10, y: self.detailText.frame.maxY+10, width: self.createView.frame.width-20, height: 20)
            self.selectSubjectView.frame = CGRect(x: 10, y: self.subjectLabel.frame.maxY+10, width: self.createView.frame.width-20, height: 30)
            self.selectSubjectLabel.frame = CGRect(x: 5, y: 0, width: self.selectSubjectView.frame.width-self.selectSubjectView.frame.height, height: self.selectSubjectView.frame.height)
            self.selectSubjectImage.frame = CGRect(x: self.selectSubjectView.frame.width-self.selectSubjectView.frame.height, y: 0, width: self.selectSubjectView.frame.height, height: self.selectSubjectView.frame.height)
            self.dropDownView.frame = CGRect(x: 10, y: self.selectSubjectView.frame.maxY, width: self.createView.frame.width-20, height: 0)
            self.dueLabel.frame = CGRect(x: 10, y: self.selectSubjectView.frame.maxY+10, width: self.createView.frame.width-20, height: 20)
            self.taskDueText.frame = CGRect(x: 10, y: self.dueLabel.frame.maxY+10, width: self.createView.frame.width-20, height: 30)
            self.taskDueTextLabel.frame = CGRect(x: 5, y: 0, width: self.taskDueText.frame.width-10, height: self.taskDueText.frame.height)
            self.saveTask.frame = CGRect(x: 10, y: self.taskDueText.frame.maxY+20, width: self.createView.frame.width-20, height: 50)
            
            let height = self.taskLabel.frame.height +  self.taskText.frame.height + self.detailLabel.frame.height + self.detailText.frame.height + self.subjectLabel.frame.height + self.selectSubjectView.frame.height + self.dueLabel.frame.height + self.taskDueText.frame.height + self.saveTask.frame.height + 110
            
            self.createView.frame = CGRect(x: 10, y: 50, width: self.blackView.frame.width-20, height:height)
        }, completion: { (Bool) in })
    }
    
    @objc func closeTask() {
        exitKeyboard()
        taskText.text = ""
        detailText.text = ""
        selectSubjectLabel.text = ""
        taskDueText.text = ""
        
        self.closeDropDown()
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.blackView.alpha = 0
            self.createView.frame = CGRect(x: self.createView.frame.width/2, y: self.createView.frame.height/2, width: 0, height: 0)
            self.taskLabel.frame = CGRect(x: self.createView.frame.width/2, y: self.createView.frame.height/2, width: 0, height: 0)
            self.taskText.frame = CGRect(x: self.createView.frame.width/2, y: self.createView.frame.height/2, width: 0, height: 0)
            self.detailLabel.frame = CGRect(x: self.createView.frame.width/2, y: self.createView.frame.height/2, width: 0, height: 0)
            self.detailText.frame = CGRect(x: self.createView.frame.width/2, y: self.createView.frame.height/2, width: 0, height: 0)
            self.subjectLabel.frame = CGRect(x: self.createView.frame.width/2, y: self.createView.frame.height/2, width: 0, height: 0)
            self.selectSubjectView.frame = CGRect(x: self.createView.frame.width/2, y: self.createView.frame.height/2, width: 0, height: 0)
            self.selectSubjectLabel.frame = CGRect(x: self.createView.frame.width/2, y: self.createView.frame.height/2, width: 0, height: 0)
            self.dueLabel.frame = CGRect(x: self.createView.frame.width/2, y: self.createView.frame.height/2, width: 0, height: 0)
            self.taskDueText.frame = CGRect(x: self.createView.frame.width/2, y: self.createView.frame.height/2, width: 0, height: 0)
            self.taskDueTextLabel.frame = CGRect(x: self.createView.frame.width/2, y: self.createView.frame.height/2, width: 0, height: 0)
            self.saveTask.frame = CGRect(x: self.createView.frame.width/2, y: self.createView.frame.height/2, width: 0, height: 0)
        }, completion: { (Bool) in })
    }
    
    @objc func exitKeyboard() {
        view.endEditing(true)
        self.closeDropDown()
    }
    
    func textViewDidChange(_ textView: UITextView) {
        if textView == taskDueText {
            let formatted = (dateFormatter.formattedText(from: "\((taskDueText.text)!)"))!
            taskDueTextLabel.text = formatted
        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
//        if textField == taskDueText {
//            let formatted = (dateFormatter.formattedText(from: "\((taskDueText.text)!)"))!
//            taskDueText.text = formatted
//            print(" dueDate ", taskDueText.text )
//        }
    }
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return tasks.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! TasksCell
        cell.backgroundColor = UIColor.white
        cell.layer.shadowColor = UIColor.gray.cgColor
        cell.layer.shadowOpacity = 0.5
        cell.layer.shadowOffset = CGSize(width: 0, height: 1)
        cell.layer.shadowRadius = 1
        cell.layer.cornerRadius = 10
        cell.task = tasks[indexPath.item]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width-20, height: 110)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let about = tasks[indexPath.item].title
        let description = tasks[indexPath.item].description
    }
    
    private func setupView() {
        taskDueText.delegate = self
        
        collectionView?.scrollsToTop = true
        if "\((ApiService().myID()["user_type"])!)" == "teacher" {
            collectionView?.contentInset = UIEdgeInsetsMake(20, 0, 110, 0)
        } else if "\((ApiService().myID()["user_type"])!)" == "student" {
            collectionView?.contentInset = UIEdgeInsetsMake(20, 0, 20, 0)
        }
        collectionView?.backgroundColor = UIColor.Background(alpha: 1.0)
        collectionView?.register(TasksCell.self, forCellWithReuseIdentifier: cellId)
        collectionView?.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height)
        
        view.addSubview(emptyImage)
        view.addSubview(emptyLabel)
        view.addSubview(addNoteView)
        addNoteView.addSubview(addNoteImage)
        view.addSubview(blackView)
        view.addSubview(createView)
        createView.addSubview(taskLabel)
        createView.addSubview(taskText)
        createView.addSubview(detailLabel)
        createView.addSubview(detailText)
        createView.addSubview(subjectLabel)
        createView.addSubview(selectSubjectView)
        selectSubjectView.addSubview(selectSubjectLabel)
        selectSubjectView.addSubview(selectSubjectImage)
        createView.addSubview(dueLabel)
        createView.addSubview(taskDueText)
        taskDueText.addSubview(taskDueTextLabel)
        createView.addSubview(saveTask)
        createView.addSubview(dropDownView)
        
        blackView.alpha = 0
    }
    
    private func setupConstraints() {
        addNoteView.frame = CGRect(x: view.frame.width-70, y: CGFloat((collectionView?.frame.height)!)-(navbarStatusHeight+bottomSpacing!+tabBarHeight+110), width: 50, height: 50)
        addNoteImage.frame = CGRect(x: 15, y: 15, width: 20, height: 20)
        
        emptyLabel.frame = CGRect(x: 10, y: view.frame.height/3, width: view.frame.width-20, height: 20)
        emptyImage.frame = CGRect(x: (view.frame.width/2)-((view.frame.width/3)/2), y: emptyLabel.frame.maxY, width: view.frame.width/3, height: view.frame.width/3)
        
        blackView.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height)
        createView.frame = CGRect(x: view.frame.width/2, y: view.frame.height/2, width: 0, height: 0)
        taskLabel.frame = CGRect(x: createView.frame.width/2, y: createView.frame.height/2, width: 0, height: 0)
        taskLabel.frame = CGRect(x: createView.frame.width/2, y: createView.frame.height/2, width: 0, height: 0)
        detailLabel.frame = CGRect(x: createView.frame.width/2, y: createView.frame.height/2, width: 0, height: 0)
        detailText.frame = CGRect(x: createView.frame.width/2, y: createView.frame.height/2, width: 0, height: 0)
        subjectLabel.frame = CGRect(x: createView.frame.width/2, y: createView.frame.height/2, width: 0, height: 0)
        selectSubjectView.frame = CGRect(x: createView.frame.width/2, y: createView.frame.height/2, width: 0, height: 0)
        dropDownView.frame = CGRect(x: createView.frame.width/2, y: createView.frame.height/2, width: 0, height: 0)
        selectSubjectImage.frame = CGRect(x: createView.frame.width/2, y: createView.frame.height/2, width: 0, height: 0)
        dueLabel.frame = CGRect(x: createView.frame.width/2, y: createView.frame.height/2, width: 0, height: 0)
        taskDueText.frame = CGRect(x: createView.frame.width/2, y: createView.frame.height/2, width: 0, height: 0)
        saveTask.frame = CGRect(x: createView.frame.width/2, y: createView.frame.height/2, width: 0, height: 0)
        taskDueTextLabel.frame = CGRect(x: createView.frame.width/2, y: createView.frame.height/2, width: 0, height: 0)
    }
    
    private func setupGestures() {
        createView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(exitKeyboard)))
        addNoteView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(openTask)))
        blackView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(closeTask)))
        selectSubjectView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(getSubjectCodes)))
        saveTask.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(createTask)))
    }
    
    private func watchTask() {
        let task = try! manager.databaseNamed("tasks")
        self.getTasks()
        NotificationCenter.default.addObserver(forName: NSNotification.Name.cblDatabaseChange, object: task, queue: nil) {
            (notification) -> Void in
            if let changes = notification.userInfo!["changes"] as? [CBLDatabaseChange] {
                for change in changes {
                    print("task, revision ID '%@'", change.description)
                    self.getTasks()
                }
            }
        }
    }
    
    private func getTasks() {
        self.tasks = ApiService().taskList()
        collectionView?.reloadData()
        if tasks.count > 0 {
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                self.emptyLabel.alpha = 0
                self.emptyImage.alpha = 0
            }, completion:  { (Bool) in })
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        setupView()
        setupConstraints()
        setupGestures()
        watchTask()
    }
    
}

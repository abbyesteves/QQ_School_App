//
//  TeacherHomeController.swift
//  QQ_School_App
//
//  Created by Abby Esteves on 09/05/2018.
//  Copyright © 2018 Abby Esteves. All rights reserved.
//

import Foundation
import MarqueeLabel
import Reachability

class TeacherHomeController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UIScrollViewDelegate, UITextFieldDelegate, UITextViewDelegate{
    
    var reachability = Reachability()!
    let manager = CBLManager.sharedInstance()
    var activityIndicator : UIActivityIndicatorView = UIActivityIndicatorView()
    var ArticlesArray = Array<Any>()
    var NewsArray = [News]()
    var subject_codes = [String]()
    var currentSubject_Code = String()
    var cellId = "cellId"
    var timerNews : Timer?
    var timer : Timer?
    var MainScrollView = UIScrollView()
    var pageNumber = 0
    var countNews = 0
    let icons = ["ic_notes", "ic_announcement", "ic_home", "ic_calendar", "ic_task"]
    let navbarTitle = ["Notes", "Announcements", "Home", "Calendar", "Task"]
    var schedule = ApiService().scheduleList()
    var scheduleToday = ApiService().schduleToday()
    var attendanceScroll = UIScrollView()
    var connection = String()
    
    let statusBarBackground: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        view.backgroundColor = UIColor.ThemeDark(alpha: 1.0)
        return view
    }()
    
    let statusBarConnection: UIView = {
        let view = UIView()
        view.backgroundColor = .clear
        return view
    }()
    
    let menuImage = UIImage(named: "ic_menu")?.withRenderingMode(.alwaysOriginal)
    let optionImage = UIImage(named: "ic_share")?.withRenderingMode(.alwaysOriginal)
    
    lazy var sideMenuLauncher: SideMenuLauncher = {
        let launcher = SideMenuLauncher()
        launcher.teacherHomeController = self
        return launcher
    }()
    
    lazy var webDetail : WebDetail = {
        let launcher = WebDetail()
        return launcher
    }()
    
    lazy var profileController: ProfileController = {
        let launcher = ProfileController()
        return launcher
    }()
    
    lazy var newsDetail: NewsDetail = {
        let launcher = NewsDetail()
        return launcher
    }()
    
    var keyboardHeight = CGFloat()
    
    let collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = 30
        layout.scrollDirection = .vertical
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.backgroundColor = .clear
        return cv
    }()
    
    let optionView : UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.layer.shadowColor = UIColor.gray.cgColor
        view.layer.shadowOpacity = 0.5
        view.layer.shadowOffset = CGSize(width: 0, height: 2)
        view.layer.shadowRadius = 3
        return view
    }()
    
    let SchedView : UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.layer.shadowOpacity = 0.5
        view.layer.shadowOffset = CGSize(width: 0, height: 1)
        view.layer.shadowRadius = 1
        view.layer.cornerRadius = 3
        return view
    }()
    
    let scanView : UIView = {
        let view = UIView()
        view.backgroundColor = .clear
        return view
    }()
    
    let classroomTitleLabel: UILabel = {
        let label = UILabel()
        label.text = "Classroom View"
        label.textColor = UIColor.darkGray
        label.numberOfLines = 2
        label.font = UIFont.boldSystemFont(ofSize: label.font.pointSize)
        label.font = label.font.withSize(23)
        return label
    }()
    
    let classroomDescLabel: UITextView = {
        let text = UITextView()
        text.text = "Look into class seating"
        text.textColor = UIColor.gray
        text.isSelectable = false
        text.isEditable = false
        text.isScrollEnabled = false
        text.font = text.font?.withSize(13)
        return text
    }()
    
    let classroomImage: UIImageView = {
        let image = UIImageView()
        image.image = UIImage(named: "classroom")
        return image
    }()
    
    let classroomView : UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.white
        view.layer.shadowOpacity = 0.5
        view.layer.shadowOffset = CGSize(width: 0, height: 1)
        view.layer.shadowRadius = 1
        view.layer.cornerRadius = 3
        return view
    }()
    
    let uploadTitleLabel: UILabel = {
        let label = UILabel()
        label.text = "Upload Modules"
        label.textColor = UIColor.darkGray
        label.numberOfLines = 2
        label.font = UIFont.boldSystemFont(ofSize: label.font.pointSize)
        label.font = label.font.withSize(18)
        return label
    }()
    
    let uploadDescLabel: UITextView = {
        let text = UITextView()
        text.text = "Upload reviewers and quizzes"
        text.textColor = UIColor.gray
        text.isSelectable = false
        text.isEditable = false
        text.isScrollEnabled = false
        text.font = text.font?.withSize(13)
        return text
    }()
    
    let uploadImage: UIImageView = {
        let image = UIImageView()
        image.image = UIImage(named: "uploaddownload")
        return image
    }()
    
    let uploadView : UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.white
        view.layer.shadowOpacity = 0.5
        view.layer.shadowOffset = CGSize(width: 0, height: 1)
        view.layer.shadowRadius = 1
        view.layer.cornerRadius = 3
        return view
    }()
    
    let createExamTitleLabel: UILabel = {
        let label = UILabel()
        label.text = "Create Exams"
        label.textColor = UIColor.darkGray
        label.numberOfLines = 2
        label.font = UIFont.boldSystemFont(ofSize: label.font.pointSize)
        label.font = label.font.withSize(18)
        return label
    }()
    
    let createExamDescLabel: UITextView = {
        let text = UITextView()
        text.text = "Online exam creation"
        text.textColor = UIColor.gray
        text.isSelectable = false
        text.isEditable = false
        text.isScrollEnabled = false
        text.font = text.font?.withSize(13)
        return text
    }()
    
    let createExamImage: UIImageView = {
        let image = UIImageView()
        image.image = UIImage(named: "exam")
        return image
    }()
    
    let createExamView : UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.white
        view.layer.shadowOpacity = 0.5
        view.layer.shadowOffset = CGSize(width: 0, height: 1)
        view.layer.shadowRadius = 1
        view.layer.cornerRadius = 3
        return view
    }()
    
    let attendanceLabel: UILabel = {
        let label = UILabel()
        label.text = "Attendance".uppercased()
        label.textColor = UIColor.gray
        label.numberOfLines = 2
        label.textAlignment = .center
        label.font = label.font.withSize(13)
        return label
    }()
    
    let scanImageView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.clipsToBounds = true
        imageView.backgroundColor = .clear
        imageView.image = UIImage(named: "ic_qr")
        imageView.layer.masksToBounds = true
        return imageView
    }()
    
//    let timeLabel: UILabel = {
//        let label = UILabel()
//        label.textColor = UIColor.gray
//        label.numberOfLines = 2
//        label.textAlignment = .center
//        label.text = "\(ApiService().getDateTime(format : "h:mm a").uppercased())".uppercased()
//        label.font = label.font.withSize(11)
//        return label
//    }()
    
    let dayLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.gray
        label.numberOfLines = 2
        label.textAlignment = .center
        label.text = "\(ApiService().getDateTime(format : "EEEE"))"
        label.font = label.font.withSize(11)
        return label
    }()
    
    let monthLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.gray
        label.numberOfLines = 2
        label.textAlignment = .center
        label.font = label.font.withSize(30)
        label.text = "\(ApiService().getDateTime(format : "MMM"))".uppercased()
        return label
    }()
    
    let dateLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.rgba(red: 25, green: 118, blue: 210, alpha: 1.0)
        label.numberOfLines = 2
        label.textAlignment = .center
        label.font = UIFont.boldSystemFont(ofSize: label.font.pointSize)
        label.font = label.font.withSize(50)
        label.text = "\(ApiService().getDateTime(format : "dd"))"
        return label
    }()
    
    let timerLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.lightGray
        label.numberOfLines = 2
        label.textAlignment = .center
        label.text = "Remaining Time"
        label.font = label.font.withSize(11)
        return label
    }()
    
    let timerCountLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.ThemeDarkGreen(alpha: 1.0)
        label.backgroundColor = UIColor.Highlight(alpha: 1.0)
        label.numberOfLines = 2
        label.textAlignment = .center
        label.font = UIFont.boldSystemFont(ofSize: label.font.pointSize)
        label.font = label.font.withSize(30)
        label.text = "00:00:00"
        return label
    }()
    
    let headerView : UIView = {
        let view = UIView()
        view.backgroundColor = .black
        view.alpha = 0.5
        return view
    }()
    
    let HeaderLabel: UITextView = {
        let textView = UITextView()
        textView.text = "sample header"
        textView.textColor = UIColor.white
        textView.backgroundColor = UIColor.clear
        textView.isScrollEnabled = false
        textView.isEditable = false
        textView.font = textView.font?.withSize(15)
        return textView
    }()
    
    let currentNotesLabel: UITextView = {
        let textView = UITextView()
        textView.text = "No tasks or activities set"
        textView.backgroundColor = UIColor.white
        textView.textColor = UIColor.darkGray
        textView.layer.borderWidth = 1.0
        textView.layer.borderColor = UIColor.lightGray.cgColor
        textView.isEditable = false
        textView.isSelectable = false
        textView.isScrollEnabled = true
        textView.layer.cornerRadius = 5
        textView.font = textView.font?.withSize(10)
        return textView
    }()
    
    let currentLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.darkGray
        label.numberOfLines = 2
        label.textAlignment = .left
        label.font = label.font.withSize(12)
        return label
    }()
    
    let currentCodeLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.darkGray
        label.numberOfLines = 2
        label.font = label.font.withSize(25)
        return label
    }()

    let currentGradeSecLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.darkGray
        label.numberOfLines = 2
        label.textAlignment = .left
        label.font = label.font.withSize(12)
        return label
    }()

    let currentTimeLabel: UILabel = {
        let label = UILabel()
        label.text = ""
        label.textColor = UIColor.darkGray
        label.numberOfLines = 2
        label.textAlignment = .left
        label.font = label.font.withSize(12)
        return label
    }()
    
    let connectionLabel: UILabel = {
        let label = UILabel()
        label.text = "Opps.. cannot connect at the moment"
        label.textColor = UIColor.white
        label.numberOfLines = 1
        label.textAlignment = .center
        label.font = label.font.withSize(11)
        label.backgroundColor = UIColor.Alert(alpha: 1.0)
        return label
    }()
    
    //@objc
    @objc func goToScanner() {
        DispatchQueue.main.async(execute: {
            let titleLabel : UILabel = {
                let label = UILabel()
                label.textColor = UIColor.white
                label.font = UIFont(name: "GillSans-Bold", size: UIFont.systemFontSize)!
                label.font = label.font.withSize(15)
                return label
            }()
            
            let scanQRLauncher = ScanQRLauncher()
            self.navigationController?.pushViewController(scanQRLauncher, animated: true)
//            print(" bug scann ", "\((ApiService().myID()["user_type"])!)")
            if "\((ApiService().myID()["user_type"])!)" == "teacher" {
                // current time subject code TEACHER
                print(" currentSubject_Code ", self.currentSubject_Code)
                if "\(self.currentSubject_Code)" == "" {
                    titleLabel.text = "scan qr".uppercased()
                    scanQRLauncher.navigationItem.title = "no_subject".uppercased()
                } else {
                    titleLabel.text = "\(self.currentSubject_Code) attendance".uppercased()
                    scanQRLauncher.navigationItem.title = "\(self.currentSubject_Code)".uppercased()
                }
            } else {
                // change to indicate time to scan attendance STUDENT
                titleLabel.text = "My Attendance".uppercased()
                scanQRLauncher.navigationItem.title = "parent".uppercased()
            }
            scanQRLauncher.navigationItem.titleView = titleLabel
        })
    }
    
    @objc func goToSubject() {
        let layout = UICollectionViewFlowLayout()
        let subjectsController = SubjectsController(collectionViewLayout: layout)
        setupGoTo(ctrl: subjectsController, title: "\(sideMenuLauncher.menuLabels[3].label)")
    }
    
    @objc func getTime() {
        monthLabel.text = "\(ApiService().getDateTime(format : "MMM").uppercased())"
        dateLabel.text = "\(ApiService().getDateTime(format : "dd"))"
        dayLabel.text = "\(ApiService().getDateTime(format : "EEEE"))"
        let formatter = DateFormatter()
        formatter.dateFormat = "yyyy-dd-MM h:mm a"
        formatter.locale = Locale.current
        
        // get current and Next subjects
        //        if "\((ApiService().myID()["user_type"])!)" == "teacher" {
        if schedule.count != 0 {
            for subject in schedule {
                if ApiService().getDateTime(format: "EEEE") == subject.key.capitalized {
                    let subjects = (subject.value) as! [[String : Any]]
                    let first =  subjects[0]
                    let last =  subjects[Int(subjects.count-1)]
                    let firstStart = formatter.date(from: "\(ApiService().getDateTime(format: "yyyy-dd-MM")) \((first["time_start"])!)")
                    let lastEnd = formatter.date(from: "\(ApiService().getDateTime(format: "yyyy-dd-MM")) \((last["time_end"])!)")
                    
                    if Int(firstStart!.timeIntervalSince1970) > Int(Date().timeIntervalSince1970) {
                        print("· \((first["subject"])!)".uppercased())
                        print("· \((first["time_start"])!) / \((first["time_end"])!)")
                        
                    } else if Int(lastEnd!.timeIntervalSince1970) <= Int(Date().timeIntervalSince1970) {
                        print("• no subject".uppercased())
                        print("")
                        currentLabel.text = "• no subject".uppercased()
                        currentLabel.font = currentLabel.font.withSize(12)
                        currentTimeLabel.text = ""
                        currentSubject_Code = String()
                    }
                    //                        else {
                    for (index, subject) in subjects.enumerated() {
                        let classStart = formatter.date(from: "\(ApiService().getDateTime(format: "yyyy-dd-MM")) \((subject["time_start"])!)")
                        let classEnd = formatter.date(from: "\(ApiService().getDateTime(format: "yyyy-dd-MM")) \((subject["time_end"])!)")
                        let condition = (Int(classStart!.timeIntervalSince1970) <= Int(Date().timeIntervalSince1970) && Int(Date().timeIntervalSince1970) <=  Int(classEnd!.timeIntervalSince1970))
                        
                        if "\((ApiService().myID()["user_type"])!)" == "student" {
                            if Int(classStart!.timeIntervalSince1970) <= Int(Date().timeIntervalSince1970) {
                                if !(subject_codes.contains("\((subject["subject_code"])!)")) &&  "\((subject["subject_code"])!)" != "LUNCH" {
                                    subject_codes.append("\((subject["subject_code"])!)")
                                }
                            }
                        }
                        
                        
                        if condition {
                            if "\((subject["subject_code"])!)" != "LUNCH" {
                                currentSubject_Code = "\((subject["subject_code"])!)"
                            } else {
                                currentSubject_Code = String()
                            }
                            
                            currentLabel.text = "• \((subject["subject"])!)"
                            currentTimeLabel.text = "• \((subject["time_start"])!) / \((subject["time_end"])!)"
                            currentCodeLabel.text = "\((subject["subject_code"])!)".uppercased()
                            currentGradeSecLabel.text = "• 7 - Sample"//. \((subject["grade"])!) - \((subject["section"])!)
                            // get next schedule
                            //                                print(" current sched to display :", index, subjects.count-1)
                            if index < subjects.count-1 {
                                let next =  subjects[Int(index+1)]
                                print(". \((next["subject"])!)".uppercased())
                                print(". \((next["time_start"])!) / \((next["time_end"])!)")
                            }
                        }
                    }
                    //                        }
                }
            }
        }
        //        }
        
    }
    
    @objc func handleMenu(){
        self.sideMenuLauncher.showMenu()
    }
    
    @objc func handleOptions() {
        let text = "Share about your School"
        let url:NSURL = NSURL(string: "http://www.schoolsite.com/")!
        let link: NSURL = NSURL(string: "inquire@Inhs.edu.ph")!
        let activityVC = UIActivityViewController(activityItems: [text, url, link], applicationActivities: [])
        activityVC.popoverPresentationController?.sourceView = self.view
        self.present(activityVC, animated: true, completion: nil)
    }
    
    @objc func autoScroll() {
        //        print(" index ", pageNumber, ArticlesArray.count)
        if pageNumber == ArticlesArray.count-1 {
            let database = try! manager.databaseNamed("news")
            pageNumber = 0
            countNews = Int(database.documentCount)
            let indexPath = NSIndexPath(item: pageNumber, section: 0)
            collectionView.scrollToItem(at: indexPath as IndexPath, at: [], animated: false)
        } else {
            pageNumber = pageNumber + 1
            countNews = countNews - 1
            let indexPath = NSIndexPath(item: pageNumber, section: 0)
            collectionView.scrollToItem(at: indexPath as IndexPath, at: [], animated: true)
        }
        
    }
    
    @objc func goToCreateExam() {
        let titleLabel : UILabel = {
            let label = UILabel()
            label.textColor = UIColor.white
            label.font = UIFont(name: "GillSans-Bold", size: UIFont.systemFontSize)!
            label.font = label.font.withSize(15)
            label.text = "create exam for \(self.currentSubject_Code)".uppercased()
            return label
        }()
        
        let examDetail = ExamDetail()
        self.navigationController?.pushViewController(examDetail, animated: true)
        
        examDetail.navigationItem.title = "\(self.currentSubject_Code)".uppercased()
        examDetail.navigationItem.titleView = titleLabel
    }
    
    
    @objc func goToSeatingPlan() {
        let manager = CBLManager.sharedInstance()
        let database = try! manager.databaseNamed("seatplan")
        let document = database.document(withID: "CLASS_SEAT_\(self.currentSubject_Code)_GR:\((ApiService().myID()["grade"])!)_SEC:\((ApiService().myID()["section"])!)")
        let properties = document?.properties
        
        if properties != nil {
            
            DispatchQueue.main.async(execute: {
                let titleLabel : UILabel = {
                    let label = UILabel()
                    label.textColor = UIColor.white
                    label.font = UIFont(name: "GillSans-Bold", size: UIFont.systemFontSize)!
                    label.font = label.font.withSize(15)
                    label.text = "\(self.currentSubject_Code) classroom view".uppercased()
                    return label
                }()
                
                let seatPlanDetail = SeatPlanDetail()
                self.navigationController?.pushViewController(seatPlanDetail, animated: true)
                seatPlanDetail.navigationItem.title = "\(self.currentSubject_Code)".uppercased()
                seatPlanDetail.navigationItem.titleView = titleLabel
            })
        } else {
            DispatchQueue.main.async(execute: {
                let titleLabel : UILabel = {
                    let label = UILabel()
                    label.textColor = UIColor.white
                    label.font = UIFont(name: "GillSans-Bold", size: UIFont.systemFontSize)!
                    label.font = label.font.withSize(15)
                    label.text = "\(self.currentSubject_Code) class seating".uppercased()
                    return label
                }()
                
                let createSeatPlanDetail = CreateSeatPlanDetail()
                self.navigationController?.pushViewController(createSeatPlanDetail, animated: true)
                createSeatPlanDetail.navigationItem.title = "\(self.currentSubject_Code)".uppercased()
                createSeatPlanDetail.navigationItem.titleView = titleLabel
            })
        }
    }
    
    @objc func quickAccessTapped(sender: UITapGestureRecognizer) {
        let title = sender.view!.subviews[1] as? UILabel
        print(" quickAccessTapped ", (title?.text)!)
        
        if (title?.text)! == "Classroom View" && self.currentSubject_Code != "" {
            self.goToSeatingPlan()
        } else if (title?.text)! == "Upload Modules" {
            
        } else if (title?.text)! == "Create Exams" && self.currentSubject_Code != "" {
            self.goToCreateExam()
        }
    }
    
    @objc func selectorTapped(sender: UITapGestureRecognizer) {
        let image = sender.view!.subviews[1] as? UIImageView
        let label = sender.view!.subviews[0] as? UILabel
        //        print((label?.text)!)
        //reset color
        resetColor()
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            image?.tintColor = UIColor.Theme(alpha: 1.0)
        }, completion: { (Bool) in
            if (label?.text)! == "ic_announcement"{
                let layout = UICollectionViewFlowLayout()
                let announcementController = AnnouncementController(collectionViewLayout: layout)
                self.setupGoTo(ctrl: announcementController, title: "Announcements")
            } else if (label?.text)! == "ic_calendar" {
                let calendarController = CalendarController()
            }
        })
    }
    
    private func watch() {
        
        //news
        self.news(channel: ["QQSCHOOL_NEWS"], database: "news")
    }
    
    private func resetColor() {
        for view in optionView.subviews {
            let inImage = view.subviews[1] as? UIImageView
            inImage?.tintColor = UIColor.gray
        }
    }
    
    private func selectedIndex() {
        let selected = 2
        let view = optionView.subviews[selected]
        let image = view.subviews[1] as? UIImageView
        image?.tintColor = UIColor.Theme(alpha: 1.0)
    }
    
    //Navigation
    func showController(menu: Menu){
        let layout = UICollectionViewFlowLayout()
        
        //News
        if sideMenuLauncher.menuLabels[1].label == menu.label{
            let teacherHomeController = TeacherHomeController()
            navigationController?.pushViewController(teacherHomeController, animated: true)
            
            //My Friends
        }
            //        else if sideMenuLauncher.menuLabels[2].label == menu.label {
            //            let friendsController = FriendsController()
            //            setupGoTo(ctrl: friendsController, title: "\(sideMenuLauncher.menuLabels[2].label)")
            //
            //            //Announcements
            //        }
            //        else if sideMenuLauncher.menuLabels[3].label == menu.label {
            //            let announcementController = AnnouncementController(collectionViewLayout: layout)
            //            setupGoTo(ctrl: announcementController, title: "Announcements")
            //
            //            //Profile
            //        }
        else if sideMenuLauncher.menuLabels[2].label == menu.label {
            profileController.showProfile(window: true)
            //            callScanner()
            
            //            let profileController = ProfileController()
            //            setupGoTo(ctrl: profileController, title: "")
            
            //School Classes
        } else if sideMenuLauncher.menuLabels[3].label == menu.label {
            let subjectsController = SubjectsController(collectionViewLayout: layout)
            setupGoTo(ctrl: subjectsController, title: "\(sideMenuLauncher.menuLabels[3].label)")
            
            //My Calendar
        }
            //        else if sideMenuLauncher.menuLabels[6].label == menu.label {
            //            let calendarController = CalendarController()
            //            setupGoTo(ctrl: calendarController, title: "School Calendar")
            //
            //            //My School
            //        }
        else if sideMenuLauncher.menuLabels[4].label == menu.label {
            let schoolController = SchoolController()
            setupGoTo(ctrl: schoolController, title: "\(sideMenuLauncher.menuLabels[4].label)")
            
            //Notes
        } else if sideMenuLauncher.menuLabels[5].label == menu.label {
            let notesController = NotesController(collectionViewLayout: layout)
            setupGoTo(ctrl: notesController, title: "\(sideMenuLauncher.menuLabels[5].label)")
            
            //My Task
        } else if sideMenuLauncher.menuLabels[6].label == menu.label {
            let tasksController = TasksController(collectionViewLayout: layout)
            setupGoTo(ctrl: tasksController, title: "\(sideMenuLauncher.menuLabels[6].label)")
            
            //My Badges
        }
            //        else if sideMenuLauncher.menuLabels[7].label == menu.label {
            //            let badgesController = BadgesController(collectionViewLayout: layout)
            //            setupGoTo(ctrl: badgesController, title: "\(sideMenuLauncher.menuLabels[7].label)")
            //
            //        }
        else if sideMenuLauncher.menuLabels[7].label == menu.label {
            let alertController = UIAlertController(title: "Reset App", message: "for testing purposes.", preferredStyle: UIAlertControllerStyle.actionSheet)
            alertController.view.frame = CGRect(x: (view.frame.width-(view.frame.width-20))/2, y: (view.frame.height-270)/2, width: view.frame.width-20, height: 270)
            
            alertController.addAction(UIAlertAction(title: NSLocalizedString("Next Time", comment: "Default action"), style: UIAlertActionStyle.default, handler: { _ in
                
            }))
            
            alertController.addAction(UIAlertAction(title: NSLocalizedString("Yes I am sure", comment: ""), style: UIAlertActionStyle.destructive, handler: { _ in
                //                let window = UIWindow(frame: UIScreen.main.bounds)
                //                window.makeKeyAndVisible()
                //                window.rootViewController = UINavigationController(rootViewController: LoginController(collectionViewLayout: layout))
                
                let loginController = ScanQRLauncher()
                self.navigationController?.pushViewController(loginController, animated: true)
                
                //                ApiService().deleteDb()
            }))
            
            let alertWindow = UIWindow(frame: UIScreen.main.bounds)
            alertWindow.rootViewController = UIViewController()
            alertWindow.windowLevel = UIWindowLevelAlert + 1;
            alertWindow.makeKeyAndVisible()
            alertWindow.rootViewController?.present(alertController, animated: true, completion: nil)
        }
    }
    
    func setupGoTo(ctrl : UIViewController, title : String) {
        let menuBarItem = UIBarButtonItem(image: menuImage, style: .plain, target: self, action: #selector(handleMenu))
        let optionBarItem = UIBarButtonItem(image: optionImage, style: .plain, target: self, action: #selector(handleOptions))
        
        let titleLabel : UILabel = {
            let label = UILabel()
            label.textColor = UIColor.white
            label.font = UIFont(name: "GillSans-Bold", size: UIFont.systemFontSize)!
            label.font = label.font.withSize(15)
            label.text = "\(title)".uppercased()
            return label
        }()
        
        navigationController?.pushViewController(ctrl, animated: true)
        
        ctrl.navigationItem.title = ""
        ctrl.navigationItem.titleView = titleLabel
        ctrl.navigationItem.leftBarButtonItems = [menuBarItem]
        ctrl.navigationItem.rightBarButtonItems = [optionBarItem]
    }
    
    private func setupStatusBar(){
        UIApplication.shared.isStatusBarHidden = false
        if let window = UIApplication.shared.keyWindow {
            window.addSubview(statusBarBackground)
            window.addSubview(statusBarConnection)
            statusBarConnection.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: 20)
            statusBarBackground.topAnchor.constraint(equalTo: window.topAnchor).isActive = true
            statusBarBackground.widthAnchor.constraint(equalTo: window.widthAnchor).isActive = true
            statusBarBackground.heightAnchor.constraint(equalTo: statusBarBackground.heightAnchor, multiplier: 20).isActive = true
            window.addConstraintsFormat(format: "V:|[v0(20)]|", views: statusBarBackground)
        }
    }
    
    private func setupHeaderNavbar(ctrl: UIViewController){
        //setting up navigation menu and options\
        navigationController?.navigationBar.barTintColor = UIColor.Theme(alpha: 0.1)
        //        navigationController?.navigationBar.backgroundColor = UIColor.Theme(alpha: 0.5)
        navigationController?.navigationBar.tintColor = UIColor.white
        navigationController?.navigationBar.isTranslucent = false
        
        navigationItem.backBarButtonItem = UIBarButtonItem(title: "", style: UIBarButtonItemStyle.plain, target: nil, action: nil)
        navigationController?.navigationBar.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
        
        let titleLabel : UILabel = {
            let label = UILabel()
            label.text = "\((ApiService().configuration()["alias"])!)".uppercased()
            label.font = UIFont(name: "GillSans-Bold", size: UIFont.systemFontSize)!
            label.font = label.font.withSize(15)
            label.textColor = UIColor.white
            return label
        }()
        
        let menuBarItem = UIBarButtonItem(image: menuImage, style: .plain, target: self, action: #selector(handleMenu))
        let optionBarItem = UIBarButtonItem(image: optionImage, style: .plain, target: self, action: #selector(handleOptions))
        
        if (ctrl == self) {
            navigationItem.titleView = titleLabel
            navigationItem.leftBarButtonItems = [menuBarItem]
            navigationItem.rightBarButtonItems = [optionBarItem]
        } else {
            ctrl.navigationItem.titleView = titleLabel
            ctrl.navigationItem.leftBarButtonItems = [menuBarItem]
            ctrl.navigationItem.rightBarButtonItems = [optionBarItem]
        }
    }
    
    private func setupView() {
        self.headerView.removeFromSuperview()
        
        collectionView.backgroundColor = UIColor.Background(alpha: 1.0)
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.isPagingEnabled = true
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.isScrollEnabled = false
        collectionView.register(NewsCell.self, forCellWithReuseIdentifier: cellId)
        
        activityIndicator.center = self.view.center
        activityIndicator.hidesWhenStopped = true
        activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.gray
        
        MainScrollView = UIScrollView(frame: view.bounds)
        MainScrollView.delegate = self
        MainScrollView.showsVerticalScrollIndicator = true
        MainScrollView.backgroundColor = UIColor.Background(alpha: 1.0)
        
        view.addSubview(connectionLabel)
        view.addSubview(MainScrollView)
        MainScrollView.addSubview(collectionView)
        MainScrollView.addSubview(activityIndicator)
        activityIndicator.startAnimating()
        
        MainScrollView.addSubview(SchedView)
        
        MainScrollView.addSubview(classroomView)
        classroomView.addSubview(classroomImage)
        classroomView.addSubview(classroomTitleLabel)
        classroomView.addSubview(classroomDescLabel)
        
        MainScrollView.addSubview(uploadView)
        uploadView.addSubview(uploadImage)
        uploadView.addSubview(uploadTitleLabel)
        uploadView.addSubview(uploadDescLabel)
        
        MainScrollView.addSubview(createExamView)
        createExamView.addSubview(createExamImage)
        createExamView.addSubview(createExamTitleLabel)
        createExamView.addSubview(createExamDescLabel)
        
        MainScrollView.addSubview(scanView)
        scanView.addSubview(scanImageView)
        
        
        view.addSubview(optionView)
    }
    
    
    
    private func setupConstraints() {
        
        connectionLabel.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: 0)
        optionView.frame = CGRect(x: 0, y: view.frame.height-150, width: view.frame.width, height: 150)
        MainScrollView.frame = CGRect(x: 0, y: connectionLabel.frame.maxY, width: view.frame.width, height: view.frame.height-optionView.frame.height)
        
        var height = view.frame.height-20-20-20-optionView.frame.height
        
        collectionView.frame = CGRect(x: 0, y: 0, width: view.frame.width-50, height: 50)
        activityIndicator.frame = CGRect(x: 0, y: collectionView.frame.height/2-10, width: view.frame.width, height: 20)
        
        height = height-collectionView.frame.height-120
        
        scanView.frame = CGRect(x: view.frame.width-50, y: 0, width: 50, height: 50)
        scanImageView.frame = CGRect(x: 5, y: 5, width: 40, height: 40)
        
        // start adding in scroll main
        MainScrollView.contentSize = CGSize(width: view.frame.width, height: view.frame.height-optionView.frame.height)
        
        SchedView.frame = CGRect(x: 10, y: collectionView.frame.maxY+30, width: view.frame.width-20, height: height/2)
        
        classroomView.frame = CGRect(x: 10, y: SchedView.frame.maxY+10, width: view.frame.width-20, height: height/2)
        classroomImage.frame = CGRect(x: 10, y: (classroomView.frame.height/2)-((classroomView.frame.width/3-60)/2), width: classroomView.frame.width/3-60, height: classroomView.frame.width/3-60)
        classroomTitleLabel.frame = CGRect(x: classroomImage.frame.maxX+10, y: 10, width: classroomView.frame.width-20-classroomImage.frame.width, height: 23)
        classroomDescLabel.frame = CGRect(x: classroomImage.frame.maxX+10, y: classroomTitleLabel.frame.maxY+2, width: classroomView.frame.width-20-20-classroomImage.frame.width, height: 25)
    
        uploadView.frame = CGRect(x: 10, y: classroomView.frame.maxY+10, width: view.frame.width/2-15, height: 120)
        uploadImage.frame = CGRect(x: 10, y: (uploadView.frame.height/2)-((uploadView.frame.width/3-10)/2), width: uploadView.frame.width/3-10, height: uploadView.frame.width/3-10)
        uploadTitleLabel.frame = CGRect(x: uploadImage.frame.maxX+10, y: 0, width: uploadView.frame.width-30-uploadImage.frame.width, height: uploadView.frame.height/2)
        uploadDescLabel.frame = CGRect(x: uploadImage.frame.maxX+10, y: uploadTitleLabel.frame.maxY-10, width: uploadView.frame.width-20-20-uploadImage.frame.width, height: uploadView.frame.height/2+5)
        
        createExamView.frame = CGRect(x: uploadView.frame.maxX+10, y: classroomView.frame.maxY+10, width: view.frame.width/2-15, height: 120)
        createExamImage.frame = CGRect(x: 10, y: (createExamView.frame.height/2)-((createExamView.frame.width/3-10)/2), width: createExamView.frame.width/3-10, height: createExamView.frame.width/3-10)
        createExamTitleLabel.frame = CGRect(x: createExamImage.frame.maxX+10, y: 10, width: createExamView.frame.width-30-createExamImage.frame.width, height: createExamView.frame.height/2)
        createExamDescLabel.frame = CGRect(x: createExamImage.frame.maxX+10, y: createExamTitleLabel.frame.maxY-10, width: createExamView.frame.width-20-20-createExamImage.frame.width, height: createExamView.frame.height/2)
        
        
    }
    
    private func setupSchedule() {
        self.timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.getTime), userInfo: nil, repeats: true)
        
        let boarder : UIView = {
            let view = UIView()
            view.backgroundColor = UIColor.Boarder(alpha: 1.0)
            return view
        }()
        
        let currentSubjectLabel: UILabel = {
            let label = UILabel()
            label.text = "Current Subject"
            label.textColor = UIColor.gray
            label.numberOfLines = 2
            label.textAlignment = .right
            label.font = label.font.withSize(11)
            return label
        }()
        
        let nextSubjectLabel: UILabel = {
            let label = UILabel()
            label.text = "Tasks / Notes"
            label.textColor = UIColor.gray
            label.numberOfLines = 2
            label.textAlignment = .right
            label.font = label.font.withSize(11)
            return label
        }()
        
        
        SchedView.addSubview(dateLabel)
        SchedView.addSubview(monthLabel)
        SchedView.addSubview(dayLabel)
//        SchedView.addSubview(timeLabel)
        SchedView.addSubview(timerLabel)
        SchedView.addSubview(timerCountLabel)
        SchedView.addSubview(boarder)
        SchedView.addSubview(currentSubjectLabel)
        SchedView.addSubview(currentLabel)
        SchedView.addSubview(currentCodeLabel)
        SchedView.addSubview(currentGradeSecLabel)
        SchedView.addSubview(currentTimeLabel)
        SchedView.addSubview(nextSubjectLabel)
        
        dayLabel.frame = CGRect(x: 10, y: 10, width: 70, height: 15)
        monthLabel.frame = CGRect(x: 10, y: dayLabel.frame.maxY+5, width: 70, height: 30)
        dateLabel.frame = CGRect(x: 10, y: monthLabel.frame.maxY+5, width: 70, height: 40)
//        timeLabel.frame = CGRect(x: 10, y: dateLabel.frame.maxY+5, width: 70, height: 15)
        
        timerCountLabel.frame = CGRect(x: 10, y: SchedView.frame.height-40, width: 180, height: 30)
        timerLabel.frame = CGRect(x: 10, y: timerCountLabel.frame.minY-20, width: 100, height: 15)
        
        boarder.frame = CGRect(x: monthLabel.frame.maxX+10, y: 10, width: 0.5, height: dayLabel.frame.height+monthLabel.frame.height+dateLabel.frame.height+15)
        currentSubjectLabel.frame = CGRect(x: boarder.frame.maxX+10, y: 5, width: 90, height: 15)
        currentCodeLabel.frame = CGRect(x: currentSubjectLabel.frame.maxX+10, y: 5, width: SchedView.frame.width-monthLabel.frame.width-10-currentSubjectLabel.frame.width-10, height: 20)
        currentLabel.frame = CGRect(x: currentSubjectLabel.frame.maxX+10, y: currentCodeLabel.frame.maxY, width: SchedView.frame.width-monthLabel.frame.width-10-currentSubjectLabel.frame.width-10, height: 25)
        currentGradeSecLabel.frame = CGRect(x: currentSubjectLabel.frame.maxX+10, y: currentLabel.frame.maxY, width: SchedView.frame.width-monthLabel.frame.width-10-currentSubjectLabel.frame.width-10, height: 20)
        currentTimeLabel.frame = CGRect(x: currentSubjectLabel.frame.maxX+10, y: currentGradeSecLabel.frame.maxY, width: SchedView.frame.width-monthLabel.frame.width-10-currentSubjectLabel.frame.width-10, height: 20)
        
        nextSubjectLabel.frame = CGRect(x: boarder.frame.maxX+10, y: currentTimeLabel.frame.maxY, width: 90, height: 15)
        
        //marquee
        let aFrame  = CGRect(x: 0, y: collectionView.frame.maxY, width: view.frame.width, height: 20)
        let marqueeLabel = MarqueeLabel.init(frame: aFrame, rate: 30.0, fadeLength: 20.0)
        marqueeLabel.textColor = UIColor.gray
        marqueeLabel.numberOfLines = 2
        marqueeLabel.text = ""
        marqueeLabel.textAlignment = .left
        marqueeLabel.font = marqueeLabel.font.withSize(11)
        
        MainScrollView.addSubview(marqueeLabel)
        
        // append event string
        let database = try! manager.databaseNamed("events")
        let query = database.createAllDocumentsQuery()
        
        do {
            let result = try query.run()
            if result != nil {
                while let row = result.nextRow(){
                    let document = row.document?.properties
                    if "\((document!["type"])!)" == "event" {
                        let formatToDate = DateFormatter()
                        formatToDate.dateFormat = "MM/dd/yyyy"
                        formatToDate.locale = Locale.current
                        let formatter = DateFormatter()
                        formatter.dateFormat = "MMMM d"
                        formatter.locale = Locale.current
                        let date = formatToDate.date(from: "\((document!["date_from"])!)")
                        
                        var marqueeText = "\((marqueeLabel.text)!)"
                        marqueeText.append("\(formatter.string(from: date!)) - \((document!["title"])!)   •  ".uppercased())
                        //                        print(" document event : ", (document!["date_from"])!, formatter.string(from: date!))
                        marqueeLabel.text = marqueeText
                    }
                }
            }
            var marqueeText = "\((marqueeLabel.text)!)"
            marqueeText.append("april 28 - School App releasing   •   april 13 - Lailatul Isra Wal Mi Raj   •   May 1 - Labor Day   •   June 12 - Independence Day   •   June 16 - Eidul-Fitar   •   June 16 - June Solstice   •   August 21 - Ninoy Aquino day   •   August 22 - Eid al-Adha (Feast of the Sacrifice)   •".uppercased())
            marqueeLabel.text = marqueeText
            
            
        } catch {
            print(" error getting announcements :", error)
        }
        
        
    }
    
    private func setupMenu() {
        let width = (view.frame.width/CGFloat(icons.count))
        var x = 0
        
        for (index, icon) in icons.enumerated() {
            let iconImage: UIImageView = {
                let image = UIImageView()
                image.safeAreaInsetsDidChange()
                image.image = UIImage(named: "\(icon)")?.withRenderingMode(.alwaysTemplate)as UIImage?
                image.tintColor = UIColor.lightGray
                return image
            }()
            
            let titlelabel : UILabel = {
                let label = UILabel()
                label.text = "\(navbarTitle[index])"
                label.textColor = UIColor.lightGray
                label.font = label.font.withSize(13)
                label.textAlignment = .center
                return label
            }()
            
            let menuView: UIView = {
                let view = UIView()
                return view
            }()
            
            optionView.addSubview(menuView)
            menuView.addSubview(titlelabel)
            menuView.addSubview(iconImage)
            
            view.addConstraintsFormat(format: "H:|-(\(width/2-15))-[v0(35)]|", views: iconImage)
            view.addConstraintsFormat(format: "H:|-5-[v0]-5-|", views: titlelabel)
            view.addConstraintsFormat(format: "V:|-15-[v0(35)][v1(10)]-10-|", views: iconImage, titlelabel)
            
            menuView.frame = CGRect(x: x, y: 0, width: Int(width), height: Int(optionView.frame.height))
            menuView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.selectorTapped(sender:))))
            x = x+Int(width)
        }
    }
    
    
    private func setupGestures(){
        SchedView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(goToSubject)))
        scanView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(goToScanner)))
    }
    
    private func getApi(completion: @escaping () -> Void){
        DispatchQueue.global().asyncAfter(deadline: .now()) {
            let urlString = "https://newsapi.org/v2/everything?domains=rappler.com&apiKey=5b58e21f40ff43128da960349a77b934"
            guard let url = URL(string: urlString) else { return }
            
            URLSession.shared.dataTask(with: url) { (data, response, err) in
                guard let data = data else { return }
                
                do {
                    let headlines = try JSONDecoder().decode(Headlines.self, from: data)
                    self.ArticlesArray = headlines.articles
                    completion()
                } catch let jsonErr{
                    print("ERROR Serializing: ", jsonErr)
                }
                
                }.resume()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return ArticlesArray.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! NewsCell
        cell.backgroundColor = UIColor.white
        cell.layer.shadowColor = UIColor.gray.cgColor
        cell.layer.shadowOpacity = 0.5
        cell.layer.shadowOffset = CGSize(width: 0, height: 3)
        cell.layer.shadowRadius = 2
        cell.layer.cornerRadius = 2
        cell.article = ArticlesArray[indexPath.item] as? Articles
        if self.countNews > 0 {
            cell.news = ArticlesArray[indexPath.item] as? News
        }
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width, height: collectionView.frame.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if self.countNews > 0 {
            let news = ArticlesArray[indexPath.item] as! News
            newsDetail.showProfile(news : news)
        } else {
            let data = ArticlesArray[indexPath.item] as! Articles
            webDetail.open(url: data.url!, sourceName: data.source.name)
        }
    }
    
    func closeNewsTitle(){
        UIView.animate(withDuration: 0.5, delay: 7, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.headerView.frame = CGRect(x: 0, y: 0, width: self.collectionView.frame.width, height: 0)
            self.HeaderLabel.frame = CGRect(x: 10, y: 0, width: self.collectionView.frame.width-20, height: 0)
        }, completion: { (Bool) in
            self.headerView.removeFromSuperview()
            self.HeaderLabel.removeFromSuperview()
        })
    }
    
    
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        //        print(" didEndDisplaying ", countNews, pageNumber )
        
        if self.countNews > 0 {
            let article = self.ArticlesArray[pageNumber] as! News
        } else {
            let article = self.ArticlesArray[pageNumber] as! Articles
        }
        
    }
    
    func scrollViewWillBeginDragging(_ scrollView: UIScrollView) {
        self.closeNewsTitle()
    }
    
   
    @objc func keyboardWillShow(notification: NSNotification) {
        let height = self.navigationController?.navigationBar.frame.height
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            keyboardHeight = keyboardSize.height
            
            self.MainScrollView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height-keyboardSize.height)
            
            //scroll to bottom
            let topOffset = CGPoint(x : 0, y : 0)
            MainScrollView.setContentOffset(topOffset, animated: true)
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        let height = self.navigationController?.navigationBar.frame.height
        
        self.MainScrollView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
        self.MainScrollView.contentSize = CGSize(width: self.view.frame.width, height: self.view.frame.height)
       
    }
    
    func news(channel: [String], database: String){
        let database = try! manager.databaseNamed("\(database)")
        let pull = database.createPullReplication(ApiService().myURL())
        pull.channels = channel
        pull.continuous = true
        pull.start()
        
        NotificationCenter.default.addObserver(forName: NSNotification.Name.cblReplicationChange, object: pull, queue: nil) {
            (notification) -> Void in
            //determins if replication is over
            let active = pull.status != CBLReplicationStatus.active
            if active {
                self.getNews()
                self.countNews = 0
                self.pageNumber = 0
                self.timerNews?.invalidate()
            }
        }
    }
    
    private func appendNews(){
        let database = try! manager.databaseNamed("news")
        let query = database.createAllDocumentsQuery()
        countNews = Int(database.documentCount)
        //        print(" countNews ", countNews)
        do {
            //            try database.delete()
            let result = try query.run()
            if result != nil {
                while let row = result.nextRow(){
                    let document = row.document?.properties
                    
                    var news = News()
                    news.title = "\((document!["title"])!)"
                    news.desc = "\((document!["desc"])!)"
                    news.date = "\((document!["date"])!)"
                    news.b64 = "\((document!["b64"])!)"
                    news.author = "\((document!["author"])!)"
                    
                    ArticlesArray.insert(news, at: 0)
                    //                    ArticlesArray.insert(news, at: Int(ArticlesArray.count/2))
                }
            }
            self.collectionView.reloadData()
            
            UIView.animate(withDuration: 0.1, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                self.activityIndicator.stopAnimating()
                self.collectionView.reloadData()
                DispatchQueue.main.async(execute: {
                    //                    print(" News data : ", self.countNews)
                    if self.countNews > 0 {
                        let article = self.ArticlesArray[0] as! News
//                        self.showNewsTitle(article: article.title)
                    } else {
                        let article = self.ArticlesArray[0] as! Articles
//                        self.showNewsTitle(article: article.title)
                    }
                })
            }, completion:  { (Bool) in
                self.timerNews = Timer.scheduledTimer(timeInterval: 8, target: self, selector: #selector(self.autoScroll), userInfo: nil, repeats: true)
            })
            
        } catch {
            print(" error getting announcements :", error)
        }
        
    }
    
    private func getNews(){
        getApi {
            DispatchQueue.main.async(execute: {
                self.appendNews()
            })
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        //        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
        //            let article = self.ArticlesArray[0] as! Articles
        //            self.showNewsTitle(article: article.title)
        //        }, completion:  { (Bool) in
        //            self.timerNews = Timer.scheduledTimer(timeInterval: 8, target: self, selector: #selector(self.autoScroll), userInfo: nil, repeats: true)
        //        })
        
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        DispatchQueue.main.async(execute: {
            //            let database = try! self.manager.databaseNamed("news")
            //            self.pageNumber = 0
            //            self.countNews = Int(database.documentCount)
            //            let indexPath = NSIndexPath(item: self.pageNumber, section: 0)
            //            self.collectionView.scrollToItem(at: indexPath as IndexPath, at: [], animated: false)
            //            self.timerNews?.invalidate()
        })
    }
    
    func setupReachability() {
        //NOTE * reachability object for online offline
        reachability.whenReachable = { reachability in
            if reachability.connection == .wifi {
                print("Reachable via WiFi")
                if self.connection == "no" {
                    DispatchQueue.main.async(execute: {
                        self.connectionLabel.text = "Connected via wifi"
                        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                            self.statusBarConnection.backgroundColor = UIColor.Accept(alpha: 1.0)
                            self.connectionLabel.backgroundColor = UIColor.Accept(alpha: 1.0)
                        }, completion: { (Bool) in
                            UIView.animate(withDuration: 0.5, delay: 2, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                                self.statusBarConnection.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: 20)
                                self.connectionLabel.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: 0)
                                self.MainScrollView.frame = CGRect(x: 0, y: self.connectionLabel.frame.maxY, width: self.MainScrollView.frame.width, height: self.MainScrollView.frame.height+20)
                            }, completion: { (Bool) in
                                self.connection = "wifi"
                                UIView.animate(withDuration: 0.5, delay: 2, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                                    self.statusBarConnection.backgroundColor = .clear
                                }, completion: { (Bool) in })
                            })
                        })
                    })
                }
            } else {
                print("Reachable via Cellular")
                if self.connection == "no" {
                    DispatchQueue.main.async(execute: {
                        self.connectionLabel.text = "Connected via cellular"
                        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                            self.statusBarConnection.backgroundColor = UIColor.Accept(alpha: 1.0)
                            self.connectionLabel.backgroundColor = UIColor.Accept(alpha: 1.0)
                        }, completion: { (Bool) in
                            UIView.animate(withDuration: 0.5, delay: 2, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                                self.connectionLabel.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: 0)
                                self.MainScrollView.frame = CGRect(x: 0, y: self.connectionLabel.frame.maxY, width: self.MainScrollView.frame.width, height: self.MainScrollView.frame.height+20)
                            }, completion: { (Bool) in
                                self.connection = "cellular"
                            })
                        })
                    })
                }
            }
        }
        reachability.whenUnreachable = { _ in
            print("Not reachable")
            self.connection = "no"
            DispatchQueue.main.async(execute: {
                UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                    self.statusBarConnection.backgroundColor = UIColor.Alert(alpha: 1.0)
                    UIView.animate(withDuration: 0.5, delay: 2, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                        self.statusBarConnection.layer.cornerRadius = 5
                        self.statusBarConnection.frame = CGRect(x: self.view.frame.width-120, y: 5, width: 10, height: 10)
                    }, completion: { (Bool) in })
                }, completion: { (Bool) in })
            })
        }
        
        do {
            try reachability.startNotifier()
        } catch {
            print("Unable to start notifier")
        }
    }
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        if UIDevice.current.orientation.isLandscape {
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                self.statusBarBackground.alpha = 0
            }, completion: { (Bool) in })
        } else {
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                self.statusBarBackground.alpha = 1
            }, completion: { (Bool) in })
        }
    }
    
    func checkConnection(){
        //check
        var request = URLRequest(url: ApiService().myURL())
        request.timeoutInterval = 5
        let session = URLSession.shared
        session.dataTask(with: request) { (data, response, error) in
            if let response = response {
                print(" replicate repsone ",response)
            }
            if let error = error {
                print(" error catch replicate ", error)
                DispatchQueue.main.async(execute: {
                    UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                        self.connectionLabel.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: 20)
                        self.MainScrollView.frame = CGRect(x: 0, y: self.connectionLabel.frame.maxY, width: self.MainScrollView.frame.width, height: self.MainScrollView.frame.height-20)
                    }, completion: { (Bool) in })
                })
            }
        }.resume()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.white
        
        //check connection
        checkConnection()
        //
        
        setupView()
        setupConstraints()
        setupGestures()
        setupStatusBar()
        setupMenu()
        selectedIndex()
        setupHeaderNavbar(ctrl: self)
        watch()
        setupSchedule()
        setupReachability()
        
        NotificationCenter.default.addObserver(self, selector: #selector(HomeController.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(HomeController.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
}

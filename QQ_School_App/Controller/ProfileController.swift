//
//  ProfileController.swift
//  QQ_School_App
//
//  Created by Abby Esteves on 02/03/2018.
//  Copyright © 2018 Abby Esteves. All rights reserved.
//

import UIKit

class ProfileController: UIViewController, UIScrollViewDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    
    let libraryController = UIImagePickerController()
    let cameraPickerController = UIImagePickerController()
    var parentAttendances = [Attendances]()
    var attendanceDates = [String]()
    var schedule = ApiService().scheduleList()
    let mainView : UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.rgba(red: 245, green: 245, blue: 255, alpha: 1.0)
        return view
    }()
    let myQR = ApiService().myQR() as [String:Any]
    var MainScrollView = UIScrollView()
    var isExpanded = true
    
    let blackView : UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(white: 0, alpha: 0.6)
        return view
    }()
    
    let attendanceView : UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.Background(alpha: 1.0)
        view.layer.shadowColor = UIColor.gray.cgColor
        view.layer.shadowOpacity = 0.5
        view.layer.shadowOffset = CGSize(width: 0, height: 1)
        view.layer.shadowRadius = 1
        view.layer.cornerRadius = 3
        return view
    }()
    
    var AttendanceScrollView : UIScrollView = {
        let scroll = UIScrollView()
        scroll.layer.shadowColor = UIColor.gray.cgColor
        scroll.layer.shadowOpacity = 0.5
        scroll.layer.shadowOffset = CGSize(width: 0, height: 1)
        scroll.layer.shadowRadius = 1
        scroll.layer.cornerRadius = 3
        return scroll
    }()
    
    let contactView : UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.white
        view.layer.shadowColor = UIColor.gray.cgColor
        view.layer.shadowOpacity = 0.5
        view.layer.shadowOffset = CGSize(width: 0, height: 1)
        view.layer.shadowRadius = 1
        view.layer.cornerRadius = 3
        return view
    }()
    
    let thumbnailView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.backgroundColor = .clear
        imageView.layer.masksToBounds = true
        imageView.image = UIImage(named: "attractions_head")
        return imageView
    }()
    
    let maskView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleToFill
        imageView.clipsToBounds = true
        imageView.backgroundColor = .clear
        imageView.layer.masksToBounds = true
        return imageView
    }()
    
    let guardianView: UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.layer.cornerRadius = 3
        return view
    }()
    
    let guardianQR: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.clipsToBounds = true
        imageView.backgroundColor = .clear
        imageView.layer.masksToBounds = true
        imageView.image = UIImage(named: "ic_qr")
//            ?.withRenderingMode(.alwaysTemplate)as UIImage?
//        imageView.tintColor = UIColor.ThemeDarkGreen(alpha: 1.0)
        return imageView
    }()
    
    let QRView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.clipsToBounds = true
        imageView.backgroundColor = .white
        imageView.layer.masksToBounds = true
        return imageView
    }()
    
    let backView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.clear
        return view
    }()
    
    let backImage: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.backgroundColor = .clear
        imageView.layer.masksToBounds = true
        imageView.image = UIImage(named: "ic_back")?.withRenderingMode(.alwaysTemplate)as UIImage?
        imageView.tintColor = UIColor.white
        return imageView
    }()
    
    let pictureView: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.clear
        return view
    }()
    
    let pictureImage: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.backgroundColor = .clear
        imageView.layer.masksToBounds = true
        imageView.image = UIImage(named: "ic_takephoto")?.withRenderingMode(.alwaysTemplate)as UIImage?
        imageView.tintColor = UIColor.white
        return imageView
    }()
    
    let scanButton: UIButton = {
        let button = UIButton()
        button.backgroundColor = UIColor.ThemeDarkGreen(alpha : 1.0)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 13)
        button.setTitleColor(UIColor.white, for: .normal)
        button.setTitle("scan qr code".uppercased(), for: .normal)
        return button
    }()
    
    let nameLabel: UILabel = {
        let label = UILabel()
        label.text = "Student Juan Dela Cruz"
        label.textColor = UIColor.white
        label.numberOfLines = 2
        label.font = UIFont.boldSystemFont(ofSize: label.font.pointSize)
        label.font = label.font.withSize(15)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let studentNumLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.ThemeDark(alpha: 1.0)
        label.numberOfLines = 2
        label.font = label.font.withSize(13)
        return label
    }()
    
    let gradeLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.ThemeDark(alpha: 1.0)
        label.numberOfLines = 2
        label.font = label.font.withSize(13)
        return label
    }()
    
    let sectionLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.ThemeDark(alpha: 1.0)
        label.numberOfLines = 2
        label.font = label.font.withSize(13)
        return label
    }()
    
    let departmentLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.ThemeDark(alpha: 1.0)
        label.numberOfLines = 2
        label.text = "Department: Senior High School"
        label.font = label.font.withSize(13)
        return label
    }()
    
    let typeLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.white
        label.numberOfLines = 2
        label.font = label.font.withSize(13)
        return label
    }()
    
    let schoolyearLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.white
        label.numberOfLines = 2
        label.font = label.font.withSize(13)
        return label
    }()
    
    let emergencyLabel: UILabel = {
        let label = UILabel()
        label.text = "Person to Contact in case of Emergency"
        label.textColor = UIColor.gray
        label.numberOfLines = 2
        label.font = label.font.withSize(13)
        return label
    }()
    
    let emergencyNameLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.ThemeDark(alpha: 1.0)
        label.numberOfLines = 2
        label.font = label.font.withSize(13)
        return label
    }()
    
    let emergencyContactLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.ThemeDark(alpha: 1.0)
        label.numberOfLines = 2
        label.font = label.font.withSize(13)
        return label
    }()
    
    let attendanceLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.ThemeDark(alpha: 1.0)
        label.numberOfLines = 2
        label.font = UIFont.boldSystemFont(ofSize: label.font.pointSize)
        label.font = label.font.withSize(15)
        return label
    }()
    
    let printButton: UIButton = {
        let button = UIButton()
        button.backgroundColor = UIColor.ThemeDarkGreen(alpha : 1.0)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 13)
        button.setTitleColor(UIColor.white, for: .normal)
        button.setTitle("print qr".uppercased(), for: .normal)
        return button
    }()
    
    @objc func hideProfile(){
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.backView.alpha = 0
            self.pictureView.alpha = 0
        }, completion: { (Bool) in
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                self.mainView.frame = CGRect(x: 0, y: self.view.frame.height, width: self.view.frame.width, height: self.view.frame.height)
            }, completion: { (Bool) in })
        })
        
    }
    
    @objc func generateGuardian(){
        if let window = UIApplication.shared.keyWindow {
            
            guardianQR.image = ApiService().generateQRCode(from: ApiService().myQRString(type: "3"))
//                ?.withRenderingMode(.alwaysTemplate)as UIImage?
//            guardianQR.tintColor = UIColor.ThemeDarkGreen(alpha: 1.0)
            
            window.addSubview(blackView)
            window.addSubview(guardianView)
            
            blackView.alpha = 0
            
            guardianView.frame = CGRect(x: window.frame.width-30-50, y: contactView.frame.maxY-50, width: 50, height: 50)
            blackView.frame = CGRect(x: 0, y: 0, width: window.frame.width, height: window.frame.height)
            
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                self.blackView.alpha = 1
                self.guardianView.frame = CGRect(x: (window.frame.width-(window.frame.width-20))/2, y: (window.frame.height-270)/2, width: window.frame.width-20, height: 300)
                self.guardianQR.frame = CGRect(x: self.guardianView.frame.width/2-140, y: 10, width: 280, height: 280)
            }, completion: { (Bool) in
                self.blackView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.closeGenerateGuardian)))
            })
        }
    }
    
    @objc func closeGenerateGuardian(){
        guardianQR.image = UIImage(named: "ic_qr")
//        ?.withRenderingMode(.alwaysTemplate)as UIImage?
//        guardianQR.tintColor = UIColor.ThemeDarkGreen(alpha: 1.0)
        if "\((myQR["t"])!)" == "3" {
            view.addSubview(guardianView)
            guardianView.addSubview(guardianQR)
            
            guardianView.frame = CGRect(x: view.frame.width-30-30, y: 15, width: 30, height: 30)
        } else {
            contactView.addSubview(guardianView)
            guardianView.addSubview(guardianQR)
        }
        
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.blackView.alpha = 0
            if "\((self.myQR["t"])!)" == "3" {
                self.guardianView.frame = CGRect(x: self.view.frame.width-30-10, y: 20, width: 30, height: 30)
            } else {
                self.guardianView.frame = CGRect(x: self.view.frame.width-30-50, y: 15, width: 50, height: 50)
            }
            self.guardianQR.frame = CGRect(x: 0, y: 0, width: self.guardianView.frame.width, height: self.guardianView.frame.height)
        }, completion: { (Bool) in
        })
    }
    
    @objc func scan(){
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.backView.alpha = 0
        }, completion: { (Bool) in
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                self.mainView.frame = CGRect(x: 0, y: self.view.frame.height, width: self.view.frame.width, height: self.view.frame.height)
            }, completion: { (Bool) in
                ScanQRLauncher().setup()
            })
        })
    }
    
    @objc func openPictureOptions() {
        let alertController = UIAlertController(title: "Edit profile Photo", message: "Choose your prefered method...", preferredStyle: UIAlertControllerStyle.actionSheet)
        
        alertController.addAction(UIAlertAction(title: NSLocalizedString("Take a Photo", comment: "Default action"), style: UIAlertActionStyle.default, handler: { _ in
            
            self.cameraPickerController.delegate = self
            self.cameraPickerController.sourceType = .camera
            
            let alertWindow = UIWindow(frame: UIScreen.main.bounds)
            alertWindow.rootViewController = UIViewController()
            alertWindow.windowLevel = UIWindowLevelAlert + 1;
            alertWindow.makeKeyAndVisible()
            alertWindow.rootViewController?.present(self.cameraPickerController, animated: true, completion: nil)
        }))
        
        alertController.addAction(UIAlertAction(title: NSLocalizedString("Insert from library", comment: "Default action"), style: UIAlertActionStyle.default, handler: { _ in
            
            self.libraryController.delegate = self
            self.libraryController.sourceType = .photoLibrary
            self.libraryController.mediaTypes = UIImagePickerController.availableMediaTypes(for: .photoLibrary)!
            
            let alertWindow = UIWindow(frame: UIScreen.main.bounds)
            alertWindow.rootViewController = UIViewController()
            alertWindow.windowLevel = UIWindowLevelAlert + 1;
            alertWindow.makeKeyAndVisible()
            alertWindow.rootViewController?.present(self.libraryController, animated: true, completion: nil)
            
        }))
        
        alertController.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: "Default action"), style: UIAlertActionStyle.cancel, handler: { _ in
        }))
        
        let alertWindow = UIWindow(frame: UIScreen.main.bounds)
        alertWindow.rootViewController = UIViewController()
        alertWindow.windowLevel = UIWindowLevelAlert + 1;
        alertWindow.makeKeyAndVisible()
        alertWindow.rootViewController?.present(alertController, animated: true, completion: nil)
    }
    
    @objc func selectAttendance(sender: UITapGestureRecognizer){
        let dateSelected = sender.view!.subviews[0] as? UILabel
        let timeSelected = sender.view!.subviews[1] as? UILabel
        let manager = CBLManager.sharedInstance()
        let database = try! manager.databaseNamed("attendance")
        let query = database.createAllDocumentsQuery()
        let viewSelect = sender.view!
        var yChild = Int(viewSelect.frame.maxY)+2
        let formatter = DateFormatter()
        formatter.dateFormat = "MMMM dd, yyyy h:mm a"
        formatter.locale = Locale.current
        let time = String((timeSelected?.text)!.dropFirst(4))
        let date = formatter.date(from: "\((dateSelected?.text)!) \(time)")
        let day = "\(FileService().epochConvertString(date: "\(Int(date!.timeIntervalSince1970))", format: "EEEE"))"
        
        
        if isExpanded {
            isExpanded = false
            for subject in schedule {
                if day == subject.key.capitalized {
                    let subjects = (subject.value) as! [[String : Any]]
                    
                    for classes in subjects{
                        if "\((classes["subject"])!)" != "Lunch" {
                            print(" selectAttendance classes ", classes)
                            let expandAttendance : UIView = {
                                let view = UIView()
                                view.backgroundColor = UIColor.white
                                view.layer.shadowColor = UIColor.gray.cgColor
                                view.layer.shadowOpacity = 0.5
                                view.layer.shadowOffset = CGSize(width: 0, height: 1)
                                view.layer.shadowRadius = 1
                                return view
                            }()
                            
                            let dateLabel: UILabel = {
                                let label = UILabel()
                                label.text = "\((classes["subject"])!)"
                                label.textColor = UIColor.lightGray
                                label.numberOfLines = 2
                                label.font = UIFont.boldSystemFont(ofSize: label.font.pointSize)
                                label.font = label.font.withSize(14)
                                return label
                            }()
                            
                            let inLabel: UILabel = {
                                let label = UILabel()
                                label.text = "In: \((classes["time_start"])!)"
                                label.textColor = UIColor.lightGray
                                label.numberOfLines = 2
                                label.font = label.font.withSize(13)
                                return label
                            }()
                            
                            let subjectCodeLabel: UILabel = {
                                let label = UILabel()
                                label.text = "\((classes["subject_code"])!)"
                                label.textColor = UIColor.lightGray
                                label.numberOfLines = 2
                                label.font = label.font.withSize(13)
                                return label
                            }()
                            
                            let noteLabel: UILabel = {
                                let label = UILabel()
                                label.textColor = UIColor.lightGray
                                label.numberOfLines = 2
                                label.font = label.font.withSize(13)
                                label.text = ""
                                return label
                            }()
                            
                            let stateLabel: UILabel = {
                                let label = UILabel()
                                label.textAlignment = .center
                                label.font = UIFont.systemFont(ofSize: 13.0)
                                label.layer.cornerRadius = 15
                                label.layer.shadowOpacity = 0.5
                                label.layer.shadowOffset = CGSize(width: 0, height: 1)
                                label.layer.shadowRadius = 1
                                label.textColor = .white
                                label.text = "on time".uppercased()
                                label.backgroundColor = UIColor.Accept(alpha: 1.0)
                                return label
                            }()
                            
                            // check attendance if absent
                            do {
                                let result = try query.run()
                                if result != nil {
                                    while let row = result.nextRow(){
                                        let document = row.document?.properties
                                        let date = "\(FileService().epochConvertString(date: "\((document!["scan_datetime"])!)", format: "MMMM dd, yyyy"))"
                                        
                                        if  date == (dateSelected?.text)! && "\((document!["scan_subj"])!)" != "parent" {
//                                            print(" selectAttendance ", document?["note"])
                                            if "\((classes["subject_code"])!)" == "\((document!["scan_subj"])!)" {
                                                if "\((document!["is_on_time"])!)" == "1" {
                                                    stateLabel.text = "on time".uppercased()
                                                    stateLabel.backgroundColor = UIColor.Accept(alpha: 1.0)
                                                } else if "\((document!["is_on_time"])!)" == "2"{
                                                    stateLabel.text = "late".uppercased()
                                                    stateLabel.backgroundColor = UIColor.Warning(alpha: 1.0)
                                                } else if "\((document!["is_on_time"])!)" == "3" {
                                                    stateLabel.text = "absent".uppercased()
                                                    stateLabel.backgroundColor = UIColor.Alert(alpha: 1.0)
                                                } else if "\((document!["is_on_time"])!)" == "4" {
                                                    stateLabel.text = "excused".uppercased()
                                                    stateLabel.backgroundColor = UIColor.ThemeOrange(alpha: 1.0)
                                                    
                                                    if document?["note"] != nil {
                                                        print(" selectAttendance document ", (document?["note"]))
                                                        if "\(document?["note"])" != "" {
                                                            noteLabel.text = "Note: \((document!["note"])!)"
                                                        } else {
                                                            noteLabel.text = ""
                                                        }
                                                    } else {
                                                        noteLabel.text = ""
                                                    }
                                                    
                                                }
                                            }
//                                            print(" selectAttendance classes ", classes)
                                            inLabel.text = "In: \(FileService().epochConvertString(date: "\((document!["scan_datetime"])!)", format: "h:mm a"))"
                                            
                                        }
                                    }
                                }
                                AttendanceScrollView.addSubview(expandAttendance)
                                expandAttendance.addSubview(subjectCodeLabel)
                                expandAttendance.addSubview(dateLabel)
                                expandAttendance.addSubview(inLabel)
                                expandAttendance.addSubview(noteLabel)
                                expandAttendance.addSubview(stateLabel)
                                
                                expandAttendance.frame = CGRect(x: 10, y:  yChild, width: Int(AttendanceScrollView.frame.width-20), height: 0)
                                UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                                    if noteLabel.text != "" {
                                        expandAttendance.frame = CGRect(x: 10, y:  yChild, width: Int(self.AttendanceScrollView.frame.width-20), height: 80)
                                        expandAttendance.addConstraintsFormat(format: "H:|-15-[v0]-15-|", views: noteLabel)
                                        expandAttendance.addConstraintsFormat(format: "V:|-10-[v0][v1][v2]-10-|", views: dateLabel, inLabel, noteLabel)
                                    } else {
                                        expandAttendance.frame = CGRect(x: 10, y:  yChild, width: Int(self.AttendanceScrollView.frame.width-20), height: 60)
                                        expandAttendance.addConstraintsFormat(format: "V:|-10-[v0][v1]-10-|", views: dateLabel, inLabel)
                                    }
                                    expandAttendance.addConstraintsFormat(format: "V:|-15-[v0(30)]-15-|", views: stateLabel)
                                    expandAttendance.addConstraintsFormat(format: "H:|-15-[v0][v1(80)]-15-|", views: dateLabel, stateLabel)
                                    expandAttendance.addConstraintsFormat(format: "H:|-15-[v0]-15-|", views: inLabel)
                                }, completion: { (Bool) in })
                                
                                
                                yChild = yChild + 60
                                
                                if (CGFloat(yChild)-60) > CGFloat(AttendanceScrollView.frame.height) {
                                    //adjust scroll
                                    AttendanceScrollView.contentSize = CGSize(width: attendanceView.frame.width, height: attendanceView.frame.height + 60 )
                                }
                                
                                //scroll to bottom
                                let bottomOffset = CGPoint(x : 0, y : AttendanceScrollView.contentSize.height-AttendanceScrollView.bounds.size.height+AttendanceScrollView.contentInset.bottom)
                                AttendanceScrollView.setContentOffset(bottomOffset, animated: false)
                                
                            } catch {
                                print(" cannot view data ", error)
                            }
                            
                        }
                    }
                }
            }
        } else {
            isExpanded = true
            self.removeAttendanceChild()
        }
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
//        print(" didFinishPickingMediaWithInfo ", info[UIImagePickerControllerReferenceURL], info)
        let image = info[UIImagePickerControllerOriginalImage] as? UIImage
        self.thumbnailView.image = image
        
        self.libraryController.dismiss(animated: true, completion: nil)
        self.cameraPickerController.dismiss(animated: true, completion: nil)
        
        let alertController = UIAlertController(title: "Save profile Photo", message: "", preferredStyle: UIAlertControllerStyle.actionSheet)
        
        let save = UIAlertAction(title: "save".capitalized, style: UIAlertActionStyle.default, handler:{ _ in
            ApiService().saveProfilePicture(image: image!)
        })
        save.setValue(UIColor.Alert(alpha: 1.0), forKey: "titleTextColor")
        alertController.addAction(save)
        
        alertController.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: "Default action"), style: UIAlertActionStyle.cancel, handler: { _ in
            self.profilePhoto()
        }))
        
        let alertWindow = UIWindow(frame: UIScreen.main.bounds)
        alertWindow.rootViewController = UIViewController()
        alertWindow.windowLevel = UIWindowLevelAlert + 1;
        alertWindow.makeKeyAndVisible()
        alertWindow.rootViewController?.present(alertController, animated: true, completion: nil)
    }
    
    func removeAttendanceChild(){
        for view in AttendanceScrollView.subviews {
            if view is UILabel {
                view.alpha = 1
            } else {
                UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                    view.alpha = 0
                }, completion: { (Bool) in
                    view.removeFromSuperview()
                })
            }
            
        }
        self.addAttendanceView(alpha: 1)
    }
    
    func profilePhoto() {
        let image = ApiService().myID()["b64"] == nil ? "attractions_head" : "\((ApiService().myID()["b64"])!)"
        if image != "" && image != "attractions_head" {
            thumbnailView.image = UIView.convertBase64ToImage(imageString: "\(image)" )
        } else {
            thumbnailView.image = UIImage(named: "\(image)")
        }
    }
    
    func showProfile(window: Bool){
        AttendanceScrollView = UIScrollView(frame: view.bounds)
        AttendanceScrollView.delegate = self
        AttendanceScrollView.showsVerticalScrollIndicator = true
        AttendanceScrollView.backgroundColor = UIColor.Background(alpha: 1.0)
        
        self.schoolyearLabel.text = "S.Y. 2017-2018"
        self.nameLabel.text = "\((ApiService().myID()["firstname"])!) \((ApiService().myID()["middlename"])!) \((ApiService().myID()["lastname"])!)"
        
        var studentNum = String()
        if "\((ApiService().myID()["user_type"])!)" == "student" {
            self.typeLabel.text = "\((ApiService().myID()["user_type"])!)".uppercased()
            studentNum = "SN: \((ApiService().myID()["student_id"])!)"
        } else {
            self.typeLabel.text = "\((ApiService().myID()["user_type"])!)-\((ApiService().myID()["teacher_type"])!)".uppercased()
            studentNum = "SN: \((ApiService().myID()["school_id"])!)"
        }
        
        //profile photo
        self.profilePhoto()
        
        
        let studentNumString = studentNum as NSString
        let studentNumMutable = NSMutableAttributedString(string: studentNumString as String, attributes: nil)
        studentNumMutable.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.lightGray, range: NSRange(location: 3, length: studentNum.count-3))
        self.studentNumLabel.attributedText = studentNumMutable
        
        let name = "Name: \((ApiService().myID()["emg_person"])!)"
        let nameString = name as NSString
        let nameMutable = NSMutableAttributedString(string: nameString as String, attributes: nil)
        nameMutable.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.lightGray, range: NSRange(location: 4, length: name.count-4))
        self.emergencyNameLabel.attributedText = nameMutable
        
        let contact = "Contact No: \((ApiService().myID()["emg_contact"])!)"
        let contactString = contact as NSString
        let contactMutable = NSMutableAttributedString(string: contactString as String, attributes: nil)
        contactMutable.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.lightGray, range: NSRange(location: 11, length: contact.count-11))
        self.emergencyContactLabel.attributedText = contactMutable
        
        let grade = "Grade: \((ApiService().myID()["grade"])!)"
        let gradeString = grade as NSString
        let gradeMutable = NSMutableAttributedString(string: gradeString as String, attributes: nil)
        gradeMutable.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.lightGray, range: NSRange(location: 6, length: grade.count-6))
        self.gradeLabel.attributedText = gradeMutable
        
        let section = "Section: \((ApiService().myID()["section"])!)"
        let sectionString = section as NSString
        let sectionMutable = NSMutableAttributedString(string: sectionString as String, attributes: nil)
        sectionMutable.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.lightGray, range: NSRange(location: 8, length: section.count-8))
        self.sectionLabel.attributedText = sectionMutable
        
        let department = "Department: Senior High School"
        let departmentString = department as NSString
        let departmentMutable = NSMutableAttributedString(string: departmentString as String, attributes: nil)
        departmentMutable.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.lightGray, range: NSRange(location: 11, length: department.count-11))
        self.departmentLabel.attributedText = departmentMutable
        
        let use = window == true ? UIApplication.shared.keyWindow : view
        backView.alpha = 0
        pictureView.alpha = 0
        use?.addSubview(mainView)
        
        QRView.image = ApiService().generateQRCode(from: ApiService().myQRString(type: "\((ApiService().myQR()["t"])!)"))
        
        mainView.addSubview(thumbnailView)
        mainView.addSubview(maskView)
        mainView.addSubview(nameLabel)
        mainView.addSubview(schoolyearLabel)
//            mainView.addSubview(scanButton)
        mainView.addSubview(QRView)
        mainView.addSubview(typeLabel)
        mainView.addSubview(gradeLabel)
        mainView.addSubview(sectionLabel)
        mainView.addSubview(departmentLabel)
        mainView.addSubview(studentNumLabel)
        
        mainView.frame = CGRect(x: 0, y: (use?.frame.height)!, width: (use?.frame.width)!, height: (use?.frame.height)!)
        thumbnailView.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height/3+20)
        maskView.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height/3+20)
        QRView.frame = CGRect(x: view.frame.width-120, y: thumbnailView.frame.height-90, width: 100, height: 100)
        backView.frame = CGRect(x: 10, y: 25, width: 50, height: 50)
        backImage.frame = CGRect(x: 0, y: 0, width: 25, height: 25)
        pictureView.frame = CGRect(x: view.frame.width-70, y: 25, width: 50, height: 50)
        pictureImage.frame = CGRect(x: 25, y: 0, width: 25, height: 25)
//            scanButton.frame = CGRect(x: 10, y: view.frame.height-60, width: view.frame.width-20, height: 50)
//            studentNumLabel.frame = CGRect(x: 20, y: QRView.frame.maxY-20, width: view.frame.width-40, height: 20)
        
        gradeLabel.frame = CGRect(x: 20, y: QRView.frame.maxY+20, width: view.frame.width/2, height: 20)
        departmentLabel.frame = CGRect(x: 20, y: gradeLabel.frame.maxY+10, width: view.frame.width/2+50, height: 20)
        studentNumLabel.frame = CGRect(x: departmentLabel.frame.width, y: gradeLabel.frame.maxY+10, width: view.frame.width/2-50, height: 20)
        sectionLabel.frame = CGRect(x: departmentLabel.frame.width, y: QRView.frame.maxY+20, width: view.frame.width/2, height: 20)
        contactView.frame = CGRect(x: 10, y: studentNumLabel.frame.maxY+10, width: view.frame.width-20, height: 80)
        guardianView.frame = CGRect(x: view.frame.width-30-50, y: 15, width: 50, height: 50)
        guardianQR.frame = CGRect(x: 0, y: 0, width: guardianView.frame.width, height: guardianView.frame.height)
        
        if "\((ApiService().myID()["user_type"])!)" == "student" {
            mainView.addSubview(contactView)
            contactView.addSubview(emergencyLabel)
            contactView.addSubview(emergencyNameLabel)
            contactView.addSubview(emergencyContactLabel)
            
            mainView.addSubview(attendanceView)
            attendanceView.addSubview(AttendanceScrollView)
            AttendanceScrollView.addSubview(attendanceLabel)
            contactView.addSubview(guardianView)
            guardianView.addSubview(guardianQR)
            
            let height = maskView.frame.height + studentNumLabel.frame.height + contactView.frame.height + 50 + departmentLabel.frame.height + gradeLabel.frame.height
            
            attendanceView.frame = CGRect(x: 10, y: contactView.frame.maxY+10, width: view.frame.width-20, height: view.frame.height-height)
            AttendanceScrollView.frame = CGRect(x: 0, y: 0, width: attendanceView.frame.width, height: attendanceView.frame.height)
            AttendanceScrollView.contentSize = CGSize(width: attendanceView.frame.width, height: attendanceView.frame.height)
            attendanceLabel.frame = CGRect(x: 10, y: 5, width: AttendanceScrollView.frame.width-20, height: 20)
            
            use?.addConstraintsFormat(format: "H:|-10-[v0]-10-|", views: emergencyLabel)
            use?.addConstraintsFormat(format: "H:|-10-[v0]-10-|", views: emergencyNameLabel)
            use?.addConstraintsFormat(format: "H:|-10-[v0]-10-|", views: emergencyContactLabel)
            use?.addConstraintsFormat(format: "V:|-10-[v0(20)][v1]-5-[v2]-10-|", views: emergencyLabel, emergencyNameLabel, emergencyContactLabel)
        }
        
        use?.addConstraintsFormat(format: "H:|-20-[v0]-20-|", views: schoolyearLabel)
        use?.addConstraintsFormat(format: "H:|-20-[v0]-(\(QRView.frame.width+30))-|", views: nameLabel)
        use?.addConstraintsFormat(format: "H:|-20-[v0]-20-|", views: typeLabel)
        
        // for guardian
        if "\((myQR["t"])!)" == "3" {
            view.addSubview(guardianView)
            guardianView.addSubview(guardianQR)
            
            view.addSubview(backView)
            backView.addSubview(backImage)
            
            backView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(logout)))
            
            guardianView.frame = CGRect(x: view.frame.width-30-10, y: 30, width: 30, height: 30)
            guardianQR.frame = CGRect(x: 0, y: 0, width: guardianView.frame.width, height: guardianView.frame.height)
            
            self.typeLabel.text = "guardian of".uppercased()
            
            QRView.image = ApiService().generateQRCode(from: "{\"d\":\"\((myQR["d"])!)\",\"i\":\"\((myQR["i"])!)\",\"p\":\"\((myQR["p"])!)\",\"t\":\"2\",\"s\":\"\((myQR["s"])!)\"}")
            
            maskView.image = UIImage(named: "guardian_mask")
            attendanceView.frame = CGRect(x: 10, y: departmentLabel.frame.maxY+5, width: view.frame.width-20, height: attendanceView.frame.height+contactView.frame.height+5)
            use?.addConstraintsFormat(format: "V:|-(\(thumbnailView.frame.height-90))-[v0(18)]-5-[v1][v2(18)]", views: typeLabel,nameLabel, schoolyearLabel)
        } else {
            attendanceLabel.text = "My Attendance"
            use?.addSubview(backView)
            backView.addSubview(backImage)
            use?.addSubview(pictureView)
            pictureView.addSubview(pictureImage)
            maskView.image = UIImage(named: "mask")
            use?.addConstraintsFormat(format: "V:|-(\(thumbnailView.frame.height-90))-[v0]-5-[v1(18)][v2(18)]", views: nameLabel, schoolyearLabel, typeLabel)
            backView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(hideProfile)))
            pictureView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(openPictureOptions)))
        }
        
        
        scanButton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(scan)))
        guardianView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(generateGuardian)))
        
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.mainView.frame = CGRect(x: 0, y: 0, width: (use?.frame.width)!, height: (use?.frame.height)!)
        }, completion: { (Bool) in
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                self.backView.alpha = 1
                self.pictureView.alpha = 1
            }, completion: { (Bool) in
                if "\((ApiService().myID()["user_type"])!)" == "student" {
                    self.watchAttendance()
                }
            })
        })
        
    }
    
    @objc func logout() {
        let alertController = UIAlertController(title: "Logout", message: "Reset App's Status & Delete all data?", preferredStyle: UIAlertControllerStyle.actionSheet)
        
        alertController.addAction(UIAlertAction(title: NSLocalizedString("Yes I am sure", comment: ""), style: UIAlertActionStyle.default, handler: { _ in
            //logout test
            let loginController = LoginController()
            self.navigationController?.pushViewController(loginController, animated: true)
        }))
        
        alertController.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: "Default action"), style: UIAlertActionStyle.cancel, handler: { _ in
            
        }))
        
        let alertWindow = UIWindow(frame: UIScreen.main.bounds)
        alertWindow.rootViewController = UIViewController()
        alertWindow.windowLevel = UIWindowLevelAlert + 1;
        alertWindow.makeKeyAndVisible()
        alertWindow.rootViewController?.present(alertController, animated: true, completion: nil)
    }
    
    func addAttendanceView(alpha : Int) {
        mainView.addSubview(attendanceView)
        attendanceView.addSubview(AttendanceScrollView)
        AttendanceScrollView.addSubview(attendanceLabel)
        
        let height = maskView.frame.height + studentNumLabel.frame.height + contactView.frame.height + 50 + departmentLabel.frame.height + gradeLabel.frame.height
        
        attendanceView.frame = CGRect(x: 10, y: contactView.frame.maxY+10, width: view.frame.width-20, height: view.frame.height-height)
        AttendanceScrollView.frame = CGRect(x: 0, y: 0, width: attendanceView.frame.width, height: attendanceView.frame.height)
        AttendanceScrollView.contentSize = CGSize(width: attendanceView.frame.width, height: attendanceView.frame.height)
        attendanceLabel.frame = CGRect(x: 10, y: 5, width: AttendanceScrollView.frame.width-20, height: 20)
        
        let manager = CBLManager.sharedInstance()
        let database = try! manager.databaseNamed("attendance")
        let query = database.createAllDocumentsQuery()
        
        if database.documentCount == 0 {
            attendanceLabel.text = "No Attendance for View"
            attendanceLabel.textAlignment = .center
            attendanceLabel.textColor = UIColor.lightGray
        } else {
            attendanceLabel.textAlignment = .left
            attendanceLabel.textColor = UIColor.ThemeDark(alpha: 1.0)
            
            if "\((myQR["t"])!)" == "3" {
                attendanceLabel.text = "Attendance"
            } else {
                attendanceLabel.text = "My Attendance"
            }
        }
        
        do {
            let result = try query.run()
            if result != nil {
                while let row = result.nextRow(){
                    let document = row.document?.properties
                    print(" Attendance document ", document)
                    if !(attendanceDates.contains("\((document!["scan_date"])!)")) {
                        attendanceDates.append("\((document!["scan_date"])!)")
                        var attendance = Attendances()
                        attendance.is_on_time = "\((document!["is_on_time"])!)"
                        attendance.scan_date = "\((document!["scan_date"])!)"
                        attendance.scan_datetime = "\((document!["scan_datetime"])!)"
                        attendance.scan_id = "\((document!["scan_id"])!)"
                        attendance.scan_subj = "\((document!["scan_subj"])!)"
                        attendance.note = "\((document!["scan_subj"])!)"
                        
//                        let tryAppend = [
//                            "dateParent": "\((document!["scan_date"])!)",
//                            "attendance": attendance
//                        ] as [String:Any]
//
//                        print(" tryAppend ", tryAppend)
                       
                        parentAttendances.append(attendance)
                    }
                }
                setupParent(alpha : alpha)
            }
        } catch {
            print(" cannot view data ", error)
        }
    }
    
    
    private func setupParent(alpha : Int) {
        var y = 35
        for parent in parentAttendances {
            
            let mainView : UIView = {
                let view = UIView()
                view.backgroundColor = UIColor.white
                view.layer.shadowColor = UIColor.gray.cgColor
                view.layer.shadowOpacity = 0.5
                view.layer.shadowOffset = CGSize(width: 0, height: 1)
                view.layer.shadowRadius = 1
                view.layer.cornerRadius = 3
                return view
            }()

            let dateLabel: UILabel = {
                let label = UILabel()
                label.text = "\(FileService().epochConvertString(date: "\((parent.scan_datetime)!)", format: "MMMM dd, yyyy"))"
                label.textColor = UIColor.gray
                label.numberOfLines = 2
                label.font = UIFont.boldSystemFont(ofSize: label.font.pointSize)
                label.font = label.font.withSize(14)
                return label
            }()

            let inLabel: UILabel = {
                let label = UILabel()
                label.text = "In: \(FileService().epochConvertString(date: "\((parent.scan_datetime)!)", format: "h:mm a"))"
                label.textColor = UIColor.gray
                label.numberOfLines = 2
                label.font = label.font.withSize(13)
                return label
            }()
        
            let statusView: UIView = {
                let view = UIView()
                view.layer.cornerRadius = 7.5
                return view
            }()
            
            if "\((parent.is_on_time)!)" == "1"{
                statusView.backgroundColor = UIColor.Accept(alpha: 1.0)
            } else if "\((parent.is_on_time)!)" == "2"{
                statusView.backgroundColor = UIColor.Warning(alpha: 1.0)
            } else if "\((parent.is_on_time)!)" == "3" {
                statusView.backgroundColor = UIColor.Alert(alpha: 1.0)
            } else if "\((parent.is_on_time)!)" == "4" {
                statusView.backgroundColor = UIColor.ThemeOrange(alpha: 1.0)
            }
        
            mainView.alpha = CGFloat(alpha)

            AttendanceScrollView.addSubview(mainView)
            mainView.addSubview(dateLabel)
            mainView.addSubview(inLabel)
            mainView.addSubview(statusView)

            mainView.frame = CGRect(x: 10, y: y, width: Int(AttendanceScrollView.frame.width-20), height: 60)
            mainView.addConstraintsFormat(format: "V:|-22.5-[v0(15)]-22.5-|", views: statusView)
            mainView.addConstraintsFormat(format: "H:|-10-[v0][v1(15)]-10-|", views: dateLabel, statusView)
            mainView.addConstraintsFormat(format: "H:|-10-[v0]-10-|", views: inLabel)
            mainView.addConstraintsFormat(format: "V:|-10-[v0][v1]-10-|", views: dateLabel, inLabel)
            
            mainView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.selectAttendance(sender:))))
        
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                mainView.alpha = 1
                y = y + Int(mainView.frame.height) + 10
            }, completion: { (Bool) in })
            
            if CGFloat(y) > CGFloat(AttendanceScrollView.frame.height) {
                //adjust scroll
                AttendanceScrollView.contentSize = CGSize(width: attendanceView.frame.width, height: attendanceView.frame.height + 100)
            }
            
            //scroll to bottom
            let bottomOffset = CGPoint(x : 0, y : AttendanceScrollView.contentSize.height-AttendanceScrollView.bounds.size.height+AttendanceScrollView.contentInset.bottom)
            AttendanceScrollView.setContentOffset(bottomOffset, animated: false)

        }
    }
    
    private func watchAttendance(){
        let manager = CBLManager.sharedInstance()
        self.addAttendanceView(alpha: 0)
        let attendance = try! manager.databaseNamed("attendance")
        NotificationCenter.default.addObserver(forName: NSNotification.Name.cblDatabaseChange, object: attendance, queue: nil) {
            (notification) -> Void in
            if let changes = notification.userInfo!["changes"] as? [CBLDatabaseChange] {
                for change in changes {
                    print("attendance, revision ID '%@'", change.description)
                    self.removeAttendanceChild()
                }
            }
        }
        
        //attendance HIGHLIGHT
//        let attendance = try! manager.databaseNamed("attendance")
//        NotificationCenter.default.addObserver(forName: NSNotification.Name.cblDatabaseChange, object: attendance, queue: nil) {
//            (notification) -> Void in
//            if let changes = notification.userInfo!["changes"] as? [CBLDatabaseChange] {
//                self.highlight(changes: changes)
//            }
//        }
    }
    
    private func highlight(changes: [CBLDatabaseChange]) {
        let viewsArray = AttendanceScrollView.subviews
        for (index,views) in viewsArray.enumerated() {
            print(" view data ", views)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.Background(alpha: 1.0)
        navigationController?.isNavigationBarHidden = true
        
        if "\((myQR["t"])!)" == "3" {
            showProfile(window: false)
        }
    }
    
}

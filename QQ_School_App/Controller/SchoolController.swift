//
//  SchoolController.swift
//  QQ_School_App
//
//  Created by Abby Esteves on 02/03/2018.
//  Copyright © 2018 Abby Esteves. All rights reserved.
//

import UIKit
class SchoolController: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    var cellId = "cellId"
    let icons = ["ic_vision", "ic_about", "ic_mission"]
    var about: [MySchool] = {
        
//        var page0 = MySchool()
//        page0.thumbnail = ""
//        page0.name = "\((ApiService().configuration()["school_name"])!)"
//        page0.title = "About"
//        page0.description = "\((ApiService().configuration()["about"])!)"//"Lolomboy National High School is a public high school specifically located at in the municipality of Bocaue, in the province of bulacan, in the III.rd region of Central Luzon, Philippines. Believe it or not but this school was established on January 01, 1970, 47 yeas ago. Headed by Principal \"Edgrado E. Villanueva, III\""
        
        var page1 = MySchool()
        page1.thumbnail = "http://www.dyci.edu.ph/data/images1/college_library_01.jpg"
        page1.name = "\((ApiService().configuration()["school_name"])!)"
        page1.title = "Vision"
        page1.description = "\((ApiService().configuration()["vision"])!)"
        //"Our vision at Lolomboy Nation High School is to empower students to aquire, demonstrate, articulate and value knowledge and skills that will support them, as life-long learners, to participate in and contribute to the global world and practice the core vaues of the school: respect, tolerance, inclusion and excellence."
        
        var page2 = MySchool()
        page2.thumbnail = "http://statefields.edu.ph/admin/_files/photogallery/e8f55_canteen_1.jpg"
        page2.name = "\((ApiService().configuration()["school_name"])!)" //"Lolomboy National High School"
        page2.title = "About"
        page2.description = "\((ApiService().configuration()["about"])!) \n\n📞\((ApiService().configuration()["contact"])!) \n\n📧\((ApiService().configuration()["email"])!) \n\n🏫\((ApiService().configuration()["address_street"])!), \((ApiService().configuration()["address_municipality"])!), \((ApiService().configuration()["address_province"])!), \((ApiService().configuration()["address_zipcode"])!) \n\n🕗\((ApiService().configuration()["operation_hours"])!)"
        //"📞(442) 883 255 \n\n\n📧inquire@Inhs.edu.ph \n\n\n🏫L. Gonzales Extn Road, Bundukan, Bocaue, Bulacan"
        
        var page3 = MySchool()
        page3.thumbnail = "http://media.philstar.com/images/the-philippine-star/opinion/20150615/lila-shahani-philippines-basic-education1.jpg"
        page3.name = "\((ApiService().configuration()["school_name"])!)"
        page3.title = "Mission"
        page3.description = "\((ApiService().configuration()["mission"])!)"
        //"Our mission is to foster each child's well being as a foundation for academic and life success, by drawing on the strengths of the child's entire community including family, educators, medical and mental health providers."
        
//        var page4 = MySchool()
//        page4.name = "\((ApiService().configuration()["school_name"])!)"
//        page4.title = "Goals"
//        page4.description = "Govern and manage with probity, strategic, vision, financial control, ultilization of risk management. Effective monitoring of performance to enhance the professionalism and competence of the faculty. Teaching the promotion of meaningful program delivery, values formation and highly delivery values through highly functional research programs."
        
        return [page1, page2, page3]
    }()
    let collectionView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = 30
        layout.scrollDirection = .horizontal
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.backgroundColor = .clear
        return cv
    }()
    
    let optionView : UIView = {
        let view = UIView()
        view.backgroundColor = .white
        return view
    }()
    
    @objc func selectorTapped(sender: UITapGestureRecognizer) {
        let image = sender.view!.subviews[1] as? UIImageView
        let label = sender.view!.subviews[0] as? UILabel
        if let index = icons.index(of: (label?.text)!){
            //reset color
            resetColor()
            image?.tintColor = UIColor.Theme(alpha: 1.0)
            let indexPath = NSIndexPath(item: index, section: 0)
            collectionView.scrollToItem(at: indexPath as IndexPath, at: .centeredHorizontally, animated: true)
        }
    }
    
    private func resetColor() {
        for view in optionView.subviews {
            let inImage = view.subviews[1] as? UIImageView
            inImage?.tintColor = UIColor.gray
        }
    }
    
    private func setupView() {
        collectionView.backgroundColor = UIColor.Background(alpha: 1.0)
        collectionView.delegate = self
        collectionView.dataSource = self
        collectionView.isPagingEnabled = true
        collectionView.isScrollEnabled = false
        collectionView.contentInset = UIEdgeInsetsMake(50, 50, 50, 50)
        collectionView.scrollIndicatorInsets = UIEdgeInsetsMake(0, 50, 0, 50)
        collectionView.showsHorizontalScrollIndicator = false
        collectionView.register(SchoolCell.self, forCellWithReuseIdentifier: cellId)
        
        view.addSubview(collectionView)
        view.addSubview(optionView)
    }
    
    private func setupConstraints() {
        collectionView.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height-110)
        optionView.frame = CGRect(x: 0, y: view.frame.height-110, width: view.frame.width, height: 110)
    }
    
    private func selectedIndex() {
        let selected = 1
        let view = optionView.subviews[selected]
        let image = view.subviews[1] as? UIImageView
        image?.tintColor = UIColor.Theme(alpha: 1.0)

        let indexPath = NSIndexPath(item: selected, section: 0)
        collectionView.scrollToItem(at: indexPath as IndexPath, at: .centeredHorizontally, animated: true)
    }
    
    private func setupMenu() {
        let width = (view.frame.width/CGFloat(icons.count))
        var x = 0

        for icon in icons {
            let iconImage: UIImageView = {
                let image = UIImageView()
                image.safeAreaInsetsDidChange()
                image.image = UIImage(named: "\(icon)")?.withRenderingMode(.alwaysTemplate)as UIImage?
                image.tintColor = UIColor.gray
                return image
            }()
            
            let titlelabel : UILabel = {
                let label = UILabel()
                label.text = "\(icon)"
                return label
            }()

            let menuView: UIView = {
                let view = UIView()
                return view
            }()
            
            optionView.addSubview(menuView)
            menuView.addSubview(titlelabel)
            menuView.addSubview(iconImage)

            view.addConstraintsFormat(format: "H:|-(\(width/2-15))-[v0(30)]|", views: iconImage)
            view.addConstraintsFormat(format: "V:|-10-[v0(30)]-10-|", views: iconImage)

            menuView.frame = CGRect(x: x, y: 0, width: Int(width), height: 60)
            menuView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.selectorTapped(sender:))))
            x = x+Int(width)
        }
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return about.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! SchoolCell
        cell.backgroundColor = UIColor.white
        cell.layer.shadowColor = UIColor.gray.cgColor
        cell.layer.shadowOpacity = 0.5
        cell.layer.shadowOffset = CGSize(width: 0, height: 3)
        cell.layer.shadowRadius = 2
        cell.layer.cornerRadius = 2
        cell.about = about[indexPath.item]
        return cell
    }
    var indexDidend = Int()
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        let index = targetContentOffset.pointee.x / view.frame.width
        indexDidend = Int(index)
    }
    
    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
        if decelerate {
            print(decelerate, indexDidend)
            let indexPath = NSIndexPath(item: indexDidend, section: 0)
            collectionView.scrollToItem(at: indexPath as IndexPath, at: .centeredHorizontally, animated: true)
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width-100, height: collectionView.frame.height-120)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 20
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.Background(alpha: 1.0)
        
        setupView()
        setupConstraints()
        setupMenu()
        selectedIndex()
    }
}

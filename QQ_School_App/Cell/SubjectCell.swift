//
//  ClassesCell.swift
//  QQ_School_App
//
//  Created by Abby Esteves on 05/03/2018.
//  Copyright © 2018 Abby Esteves. All rights reserved.
//

import UIKit

class SubjectCell: BaseCell {
    var schedule = ApiService().scheduleList()
    var categories = ["Math","Science","English","Filipino","Mapeh","General"]
    var subject: Subjects? {
        didSet {
            
            gradeSecLabel.text = "\((ApiService().myID()["user_type"])!)" == "teacher" ? "Grade: \((subject?.grade_lvl)!) • Section: \((subject?.section)!)" : subject?.teacher
            schedLabel.text = "\((subject?.time_start)!) - \((subject?.time_end)!)"
            topicLabel.text = subject?.subject
            bgImage.image = categories.contains("\((subject?.subject)!)") ? UIImage(named: "\((subject?.subject)!)".lowercased()) : UIImage(named: "general")
            subjectLabel.text = subject?.subject_code.uppercased()
        }
    }
    
    let bgImage: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.image = UIImage(named: "general")?.withRenderingMode(.alwaysTemplate)
        imageView.tintColor = UIColor.lightGray
        imageView.backgroundColor = .clear
        return imageView
    }()
    
    let subjectLabel: UILabel = {
        let label = UILabel()
        label.text = "Title of the news and announcement"
        label.textColor = UIColor.Theme(alpha: 1.0)
        label.numberOfLines = 2
        label.font = UIFont.boldSystemFont(ofSize: label.font.pointSize)
        label.font = label.font.withSize(15)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let gradeSecLabel: UILabel = {
        let label = UILabel()
        label.text = "grade & section"
        label.textColor = UIColor.ThemeOrange(alpha: 1.0)
        label.numberOfLines = 2
        label.textAlignment = .right
        label.font = UIFont.boldSystemFont(ofSize: label.font.pointSize)
        label.font = label.font.withSize(15)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let schedLabel: UILabel = {
        let label = UILabel()
        label.text = "sched sample"
        label.textColor = UIColor.darkGray
        label.numberOfLines = 2
        label.font = label.font.withSize(35)
        label.adjustsFontSizeToFitWidth = true
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let topicLabel: UILabel = {
        let label = UILabel()
        label.text = "topic sample"
        label.textColor = UIColor.gray
        label.backgroundColor = UIColor.clear
        label.font = label.font?.withSize(13)
        return label
    }()
    
    
    private func setupView() {
        bgImage.alpha = 0.05
        
        addSubview(topicLabel)
        addSubview(subjectLabel)
        addSubview(schedLabel)
        addSubview(bgImage)
        addSubview(gradeSecLabel)
//        gradeSecLabel.alpha = 0
        
//        if "\((ApiService().myID()["user_type"])!)" == "teacher" {
//            gradeSecLabel.alpha = 1.0
//        }
        
        bgImage.transform = CGAffineTransform(rotationAngle: 0.08)
        
    }
    
    private func setupConstraints() {
        addConstraintsFormat(format: "H:|-20-[v0]-20-|", views: topicLabel)
        addConstraintsFormat(format: "H:|-20-[v0][v1]-20-|", views: subjectLabel, gradeSecLabel)
        addConstraintsFormat(format: "H:|-20-[v0]-20-|", views: schedLabel)
        addConstraintsFormat(format: "H:|-(\(frame.width-100))-[v0(90)]|", views: bgImage)
        addConstraintsFormat(format: "V:|-10-[v0(20)]-10-|", views: gradeSecLabel)
        addConstraintsFormat(format: "V:|-10-[v0(90)]|", views: bgImage)
        addConstraintsFormat(format: "V:|-10-[v0]-2-[v1]-5-[v2]-10-|", views: subjectLabel, topicLabel, schedLabel)
        
    }
    
    override func setupViews() {
        super.setupViews()
        
        setupView()
        setupConstraints()
    }
}

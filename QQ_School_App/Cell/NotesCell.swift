//
//  NotesCell.swift
//  QQ_School_App
//
//  Created by Abby Esteves on 05/03/2018.
//  Copyright © 2018 Abby Esteves. All rights reserved.
//

import UIKit

class NotesCell: BaseCell {
    var note: Notes? {
        didSet {
            //title format
            let contentString = note?.content as! NSString
            let contentMutable = NSMutableAttributedString(string: contentString as String, attributes: nil)
            contentMutable.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.ThemeDark(alpha: 1.0), range: NSRange(location: 0, length: (note?.title.count)!))
            contentMutable.addAttribute(NSAttributedStringKey.font, value: UIFont.boldSystemFont(ofSize: 13), range: NSRange(location: 0, length: (note?.title.count)!))
            contentLabel.attributedText = contentMutable
            
//            titleLabel.text = note?.title
            contentLabel.text = note?.content
            dateLabel.text = FileService().getRecentDate(date: (note?.date)!)
            
        }
    }
    
//    let titleLabel: UILabel = {
//        let label = UILabel()
//        label.text = "Title of the news and announcement"
//        label.textColor = UIColor.ThemeDark(alpha: 1.0)
//        label.numberOfLines = 1
//        label.font = UIFont.boldSystemFont(ofSize: label.font.pointSize)
//        label.font = label.font.withSize(13)
//        label.translatesAutoresizingMaskIntoConstraints = false
//        return label
//    }()
    
    let contentLabel: UILabel = {
        let label = UILabel()
        label.text = "sched sample"
        label.textColor = UIColor.gray
        label.numberOfLines = 3
        label.font = label.font.withSize(13)
        return label
    }()
    
    let dateLabel: UITextView = {
        let textView = UITextView()
        textView.text = "topic sample"
        textView.textColor = UIColor.gray
        textView.backgroundColor = UIColor.clear
        textView.isScrollEnabled = false
        textView.isEditable = false
        textView.isSelectable = false
        textView.font = textView.font?.withSize(11)
        return textView
    }()
    
    private func setupView() {
//        addSubview(titleLabel)
        addSubview(contentLabel)
        addSubview(dateLabel)
    }
    
    private func setupConstraints() {
//        addConstraintsFormat(format: "H:|-10-[v0]-10-|", views: titleLabel)
        addConstraintsFormat(format: "H:|-10-[v0]-10-|", views: contentLabel)
        addConstraintsFormat(format: "H:|-5-[v0]-5-|", views: dateLabel)
//        addConstraintsFormat(format: "V:|-10-[v0(20)]", views: titleLabel)
        addConstraintsFormat(format: "V:|-10-[v0]-5-[v1(20)]-10-|", views: contentLabel, dateLabel)
    }
    
    override func setupViews() {
        super.setupViews()
        
        setupView()
        setupConstraints()
    }
}

//
//  ModulesCell.swift
//  QQ_School_App
//
//  Created by Abby Esteves on 24/03/2018.
//  Copyright © 2018 Abby Esteves. All rights reserved.
//

import UIKit
class ModulesCell: BaseCell {
    var module: Modules? {
        didSet {
            if module?.type == "jpeg" || module?.type == "jpg"{
                thumbnailView.image = UIImage(named: "jpg")
            } else if module?.type == "docx" || module?.type == "doc"{
                thumbnailView.image = UIImage(named: "doc")
            } else {
                thumbnailView.image = UIImage(named: "\((module?.type)!)")
            }
            nameLabel.text = module?.name
            let dateFormat = FileService().epochConvertString(date : (module?.date)!, format : "MMMM dd,yyyy")
            dateLabel.text = dateToday() == dateFormat ? "Today, \(FileService().epochConvertString(date : (module?.date)!, format : "h:mm a"))" : FileService().epochConvertString(date : (module?.date)!, format : "MMMM dd,yyyy, h:mm a")
            
        }
    }
    
    let thumbnailView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.backgroundColor = .clear
        imageView.layer.masksToBounds = true
        imageView.image = UIImage(named: "base64")
        return imageView
    }()
    
    let nameLabel: UILabel = {
        let label = UILabel()
        label.text = "name sample"
        label.textColor = UIColor.darkGray
        label.numberOfLines = 1
        label.lineBreakMode = .byTruncatingHead
        label.adjustsFontSizeToFitWidth = false
        label.font = label.font.withSize(20)
        return label
    }()
    
    let dateLabel: UILabel = {
        let label = UILabel()
        label.text = "grade sec sample"
        label.textColor = UIColor.gray
        label.numberOfLines = 2
        label.font = label.font.withSize(12)
        return label
    }()
    
    private func setupView(){
        addSubview(thumbnailView)
        addSubview(nameLabel)
        addSubview(dateLabel)
    }
    
    private func setupConstraints(){
        addConstraintsFormat(format: "H:|-10-[v0(35)]-10-|", views: thumbnailView)
        addConstraintsFormat(format: "V:|-10-[v0(50)]-10-|", views: thumbnailView)
        addConstraintsFormat(format: "H:|-65-[v0]-10-|", views: nameLabel)
        addConstraintsFormat(format: "H:|-65-[v0]-10-|", views: dateLabel)
        addConstraintsFormat(format: "V:|-10-[v0][v1(20)]-10-|", views: nameLabel, dateLabel)
    }
    
    override func setupViews() {
        super.setupViews()
        
        setupView()
        setupConstraints()
    }
}

//
//  TasksCell.swift
//  QQ_School_App
//
//  Created by Abby Esteves on 05/03/2018.
//  Copyright © 2018 Abby Esteves. All rights reserved.
//

import UIKit

class TasksCell: BaseCell {
    var task: Tasks? {
        didSet {
            gradeSectionLabel.attributedText = imbedIcon(string : "  \((task?.subject_code)!)", iconImage : "ic_cap", iconSize : 25, textSize : 15.0)
            titleLabel.text = task?.title
            descriptionLabel.text = task?.description
            dateLabel.text = task?.due
//            dateLabel.attributedText = imbedIcon(string : "  \((task?.due)!)", iconImage : "ic_timer", iconSize : 20, textSize : 13.0)
        }
    }
    
    let titleLabel: UILabel = {
        let label = UILabel()
        label.text = "Title of the news and announcement"
        label.textColor = UIColor.darkGray
        label.numberOfLines = 2
        label.textColor = UIColor.darkGray
        label.adjustsFontSizeToFitWidth = false
        label.font = UIFont.boldSystemFont(ofSize: label.font.pointSize)
        label.font = label.font.withSize(20)
        return label
    }()
    
    let gradeSectionLabel: UITextView = {
        let textView = UITextView()
        textView.text = "Grade Section sample"
        textView.textColor = UIColor.black
        textView.backgroundColor = UIColor.clear
        textView.isScrollEnabled = false
        textView.isEditable = false
        textView.font = textView.font?.withSize(18)
        return textView
    }()
    
    let descriptionLabel: UITextView = {
        let textView = UITextView()
        textView.text = "description sample"
        textView.textColor = UIColor.lightGray
        textView.backgroundColor = UIColor.clear
        textView.isScrollEnabled = false
        textView.isEditable = false
        textView.font = textView.font?.withSize(15)
        return textView
    }()
    
    let dueImage: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.backgroundColor = .clear
        imageView.layer.masksToBounds = true
        imageView.image = UIImage(named: "ic_due")?.withRenderingMode(.alwaysTemplate)
        imageView.tintColor = UIColor.lightGray
        return imageView
    }()
    
    let dateLabel: UITextView = {
        let textView = UITextView()
        textView.text = "date sample"
        textView.textColor = UIColor.lightGray
        textView.backgroundColor = UIColor.clear
        textView.isScrollEnabled = false
        textView.isEditable = false
        textView.textAlignment = .right
        textView.font = textView.font?.withSize(14)
        return textView
    }()
    private func setupView(){
        addSubview(gradeSectionLabel)
        addSubview(titleLabel)
        addSubview(descriptionLabel)
        addSubview(dateLabel)
        dateLabel.addSubview(dueImage)
    }
    
    private func setupConstraints(){
        addConstraintsFormat(format: "V:|-5-[v0(20)]|", views: dueImage)
        addConstraintsFormat(format: "H:|[v0(20)]|", views: dueImage)
        
        addConstraintsFormat(format: "H:|-10-[v0]-5-[v1(100)]-10-|", views: gradeSectionLabel, dateLabel)
        addConstraintsFormat(format: "V:|-15-[v0(25)]", views: dateLabel)
        addConstraintsFormat(format: "H:|-15-[v0]-15-|", views: titleLabel)
        addConstraintsFormat(format: "H:|-10-[v0]-10-|", views: descriptionLabel)
        addConstraintsFormat(format: "V:|-5-[v0(30)]-10-[v1][v2]-10-|", views: gradeSectionLabel, titleLabel, descriptionLabel)
    }
    
    override func setupViews() {
        super.setupViews()
        
        setupView()
        setupConstraints()
    }
}

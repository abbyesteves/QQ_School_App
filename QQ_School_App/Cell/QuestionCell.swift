//
//  QuestionCell.swift
//  QQ_School_App
//
//  Created by Abby Esteves on 18/04/2018.
//  Copyright © 2018 Abby Esteves. All rights reserved.
//

import UIKit

class QuestionCell: BaseCell {
    var question: Questions? {
        didSet {
        }
    }
    
    let questionNoLabel: UILabel = {
        let label = UILabel()
        label.text = "Question # 1"
        label.textColor = UIColor.lightGray
        label.numberOfLines = 2
        label.font = UIFont.boldSystemFont(ofSize: label.font.pointSize)
        label.font = label.font.withSize(15)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let questionLabel: UILabel = {
        let label = UILabel()
        label.text = "Question :"
        label.textColor = UIColor.lightGray
        label.numberOfLines = 2
        label.font = label.font.withSize(15)
        return label
    }()
    
    let choicesLabel: UILabel = {
        let label = UILabel()
        label.text = "Choices :"
        label.textColor = UIColor.lightGray
        label.numberOfLines = 2
        label.font = label.font.withSize(15)
        return label
    }()
    
    let questionText: UITextView = {
        let textView = UITextView()
        textView.backgroundColor = UIColor.white
        textView.textColor = UIColor.darkGray
        textView.layer.borderWidth = 1.0
        textView.layer.borderColor = UIColor.lightGray.cgColor
        textView.isEditable = true
        textView.isSelectable = true
        textView.isScrollEnabled = true
        textView.layer.cornerRadius = 5
        textView.font = textView.font?.withSize(10)
        return textView
    }()
    
    let choice1: UITextField = {
        let text = UITextField()
        text.borderStyle = .none
        text.backgroundColor = .white
        text.textColor = UIColor.gray
        text.font = .systemFont(ofSize: 13)
//        var placeholder = NSMutableAttributedString()
//        placeholder = NSMutableAttributedString(attributedString: NSAttributedString(string: "", attributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 13.0), .foregroundColor: UIColor.gray]))
//        text.setLeftPadding(space: 8)
//        text.attributedPlaceholder = placeholder
        return text
    }()
    
    let choice2: UITextField = {
        let text = UITextField()
        text.borderStyle = .none
        text.backgroundColor = .white
        text.textColor = UIColor.gray
        text.font = .systemFont(ofSize: 13)
        return text
    }()
    
    let choice3: UITextField = {
        let text = UITextField()
        text.borderStyle = .none
        text.backgroundColor = .white
        text.textColor = UIColor.gray
        text.font = .systemFont(ofSize: 13)
        return text
    }()
    
    let choice4: UITextField = {
        let text = UITextField()
        text.borderStyle = .none
        text.backgroundColor = .white
        text.textColor = UIColor.gray
        text.font = .systemFont(ofSize: 13)
        return text
    }()
    
    let boarderView : UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.Boarder(alpha: 1.0)
        return view
    }()
    
    func setupView() {
        addSubview(questionText)
        addSubview(choice1)
        addSubview(choice2)
        addSubview(choice3)
        addSubview(choice4)
        addSubview(questionLabel)
        addSubview(choicesLabel)
        addSubview(questionNoLabel)
        addSubview(boarderView)
        
        addConstraintsFormat(format: "H:|-10-[v0]-10-|", views: questionNoLabel)
        addConstraintsFormat(format: "H:|-10-[v0]-10-|", views: boarderView)
        addConstraintsFormat(format: "H:|-10-[v0]-10-|", views: questionLabel)
        addConstraintsFormat(format: "H:|-10-[v0]-10-|", views: questionText)
        addConstraintsFormat(format: "V:|-10-[v0(50)][v1(1)]-20-[v2(20)][v3(50)]-5-[v4(20)]|", views: questionNoLabel, boarderView, questionLabel, questionText, choicesLabel)
    }
    
    override func setupViews() {
        super.setupViews()
        
        setupView()
    }
}


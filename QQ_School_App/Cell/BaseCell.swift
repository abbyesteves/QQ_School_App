//
//  BaseCell.swift
//  QQ_School_App
//
//  Created by Abby Esteves on 01/03/2018.
//  Copyright © 2018 Abby Esteves. All rights reserved.
//

import UIKit

class BaseCell: UICollectionViewCell{
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    
    func setupViews(){
        
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

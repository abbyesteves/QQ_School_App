//
//  CreateExamCell.swift
//  QQ_School_App
//
//  Created by Abby Esteves on 23/05/2018.
//  Copyright © 2018 Abby Esteves. All rights reserved.
//

import UIKit
class CreateExamCell: BaseCell {
    
    var exam: Exams? {
        didSet {
            nameLabel.text = exam?.exam_name.capitalized
            descLabel.text = exam?.exam_desc.capitalized == "Draft" ? "" : exam?.exam_desc
            timeLabel.text = "\((exam?.exam_due)!)  •  \((exam?.allotted_time)!)"
        }
    }
    
    let thumbnailView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.backgroundColor = .clear
        imageView.layer.masksToBounds = true
        imageView.image = UIImage(named: "exam")//?.withRenderingMode(.alwaysTemplate)
//        imageView.tintColor = UIColor.Theme(alpha: 1.0)
        return imageView
    }()
    
    let nameLabel: UILabel = {
        let label = UILabel()
        label.text = "name sample"
        label.textColor = UIColor.darkGray
        label.numberOfLines = 1
        label.adjustsFontSizeToFitWidth = false
        label.font = label.font.withSize(20)
        return label
    }()
    
    let descLabel: UILabel = {
        let label = UILabel()
        label.text = "grade sec sample"
        label.textColor = UIColor.gray
        label.numberOfLines = 2
        label.font = label.font.withSize(14)
        return label
    }()
    
    let timeLabel: UILabel = {
        let label = UILabel()
        label.text = "grade sec sample"
        label.textColor = UIColor.lightGray
        label.numberOfLines = 2
        label.font = label.font.withSize(13)
        return label
    }()
    
    private func setupView(){
        addSubview(thumbnailView)
        addSubview(nameLabel)
        addSubview(descLabel)
        addSubview(timeLabel)
    }
    
    private func setupConstraints() {
        addConstraintsFormat(format: "H:|-10-[v0(50)]-10-|", views: thumbnailView)
        addConstraintsFormat(format: "V:|-25-[v0]-25-|", views: thumbnailView)
        addConstraintsFormat(format: "H:|-75-[v0]-10-|", views: nameLabel)
        addConstraintsFormat(format: "H:|-75-[v0]-10-|", views: descLabel)
        addConstraintsFormat(format: "H:|-75-[v0]-10-|", views: timeLabel)
        addConstraintsFormat(format: "V:|-10-[v0]-5-[v1]-5-[v2]-10-|", views: nameLabel, descLabel, timeLabel)
    }
    
    override func setupViews() {
        super.setupViews()
        
        setupView()
        setupConstraints()
    }
}

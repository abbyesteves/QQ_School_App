//
//  NewsCell.swift
//  QQ_School_App
//
//  Created by Abby Esteves on 02/03/2018.
//  Copyright © 2018 Abby Esteves. All rights reserved.
//

import UIKit

class NewsCell: BaseCell {
    var heightHeaderLabelView: NSLayoutConstraint?
    var article: Articles? {
        didSet {
            if let urlImage = article?.urlToImage {
                if urlImage != "" {
                    thumbnailView.backgroundColor = .clear
                    ApiService().get_image(url_str: urlImage, thumbnailView: self.thumbnailView)
                } else {
                    thumbnailView.image = UIImage(named: "news1")
                }
            }
            if let title = article?.title {
                headerView.backgroundColor = .black
                HeaderLabel.text = title
            }
        }
    }
    
    var news: News? {
        didSet {
            if let urlImage = news?.b64 {
                if urlImage != "" {
                    thumbnailView.backgroundColor = UIColor.Background(alpha: 1.0)
                    thumbnailView.image = UIView.convertBase64ToImage(imageString: "\(urlImage)")
                } else {
                    thumbnailView.backgroundColor = .clear
                    thumbnailView.image = UIImage(named: "news1")
                }
            }
            if let title = news?.title {
                headerView.backgroundColor = UIColor.ThemeDarkGreen(alpha: 1.0)
                HeaderLabel.text = title
            }
        }
    }
    
    let headerView : UIView = {
        let view = UIView()
        view.backgroundColor = .black
        view.alpha = 0.5
        return view
    }()
    
    let HeaderLabel: UILabel = {
        let textView = UILabel()
        textView.text = "sample header"
        textView.textColor = UIColor.white
        textView.backgroundColor = UIColor.clear
        textView.numberOfLines = 2
        textView.font = textView.font?.withSize(15)
        return textView
    }()
    
    let thumbnailView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.backgroundColor = .clear
        imageView.layer.masksToBounds = true
        return imageView
    }()
    
    override func setupViews() {
        super.setupViews()
        
        addSubview(thumbnailView)
        if "\((ApiService().myQR()["t"])!)" == "1" {
            addSubview(headerView)
            addSubview(HeaderLabel)
            
            addConstraintsFormat(format: "H:|[v0]|", views: headerView)
            addConstraintsFormat(format: "V:|[v0]|", views: headerView)
            addConstraintsFormat(format: "H:|-50-[v0]-50-|", views: HeaderLabel)
            addConstraintsFormat(format: "V:|-5-[v0]-5-|", views: HeaderLabel)
        }
        
        addConstraintsFormat(format: "H:|[v0]|", views: thumbnailView)
        addConstraintsFormat(format: "V:|[v0]|", views: thumbnailView)
    }
}






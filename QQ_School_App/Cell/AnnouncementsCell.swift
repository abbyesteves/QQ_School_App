//
//  AnnouncementsCell.swift
//  QQ_School_App
//
//  Created by Abby Esteves on 05/03/2018.
//  Copyright © 2018 Abby Esteves. All rights reserved.
//

import UIKit

class AnnouncementsCell: BaseCell {
    var announcement: Announcements? {
        didSet {
            dateLabel.text =  "\(FileService().getRecentDate(date: (announcement?.date)!)) • \((announcement?.author)!)"//FileService().epochConvertString(date : (announcement?.date)!, format : "MM/dd/yyyy")
            titleLabel.text = announcement?.title
            descriptionLabel.text = announcement?.description
        }
    }
    
    let boarderView : UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.Background(alpha: 1.0)
        return view
    }()
    
//    let shareView: UIImageView = {
//        let imageView = UIImageView()
//        imageView.contentMode = .scaleAspectFill
//        imageView.clipsToBounds = true
//        imageView.backgroundColor = .clear
//        imageView.layer.masksToBounds = true
//        imageView.image = UIImage(named: "ic_more")
//        return imageView
//    }()
    
    let titleLabel: UILabel = {
        let label = UILabel()
        label.text = "Title of the news and announcement"
        label.textColor = UIColor.darkGray
        label.numberOfLines = 2
        label.font = UIFont.boldSystemFont(ofSize: label.font.pointSize)
        label.font = label.font.withSize(13)
        return label
    }()
    
    let dateLabel: UILabel = {
        let label = UILabel()
        label.text = "date sample"
        label.textColor = UIColor.gray
        label.numberOfLines = 1
        label.font = label.font.withSize(11)
        return label
    }()
    
    let descriptionLabel: UILabel = {
        let label = UILabel()
        label.text = "date sample"
        label.textColor = UIColor.darkGray
        label.numberOfLines = 5
        label.font = label.font.withSize(12)
        return label
    }()
    
//    let descriptionLabel: UITextView = {
//        let textView = UITextView()
//        textView.text = "date sample"
//        textView.textColor = UIColor.darkGray
//        textView.backgroundColor = UIColor.clear
//        textView.isScrollEnabled = false
//        textView.isEditable = false
//        textView.isSelectable = false
//        textView.font = textView.font?.withSize(12)
//        return textView
//    }()
    
    private func setupView(){
        addSubview(dateLabel)
        addSubview(titleLabel)
        addSubview(descriptionLabel)
//        addSubview(shareView)
        addSubview(boarderView)
    }
    
    private func setupConstraints() {
//        addConstraintsFormat(format: "H:|-10-[v0][v1(20)]-10-|", views: dateLabel, shareView)
        addConstraintsFormat(format: "H:|-10-[v0]-10-|", views: dateLabel)
//        addConstraintsFormat(format: "V:|-60-[v0(20)]-10-|", views: shareView)
        addConstraintsFormat(format: "H:|-15-[v0]-15-|", views: titleLabel)
        addConstraintsFormat(format: "H:|-15-[v0]-15-|", views: descriptionLabel)
        addConstraintsFormat(format: "H:|[v0]|", views: boarderView)
        addConstraintsFormat(format: "V:|-5-[v0][v1]-5-[v2(1)][v3(25)]-5-|", views: titleLabel, descriptionLabel, boarderView, dateLabel)
    }
    
    override func setupViews() {
        super.setupViews()
        
        setupView()
        setupConstraints()
    }
}

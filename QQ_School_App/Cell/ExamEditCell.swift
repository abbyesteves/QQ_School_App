//
//  ExamEditCell.swift
//  QQ_School_App
//
//  Created by Abby Esteves on 25/05/2018.
//  Copyright © 2018 Abby Esteves. All rights reserved.
//

import UIKit
class ExamEditCell: BaseCell {
    var question: Questions? {
        didSet {
            let answers = [answer1, answer2, answer3, answer4]
            let choices = [question?.choice1, question?.choice2, question?.choice3, question?.choice4]
            
            for (index, choice) in choices.enumerated() {
                if question?.answer == choice {
                    answers[index].backgroundColor = UIColor.ThemeDarkGreen(alpha: 1.0)
                } else {
                    answers[index].backgroundColor = UIColor.clear
                }
            }
            
            number1Label.text = "\((question?.number)!)1"
            number2Label.text = "\((question?.number)!)2"
            number3Label.text = "\((question?.number)!)3"
            number4Label.text = "\((question?.number)!)4"
            questionNoLabel.text = "Question \((question?.number)!)"
            questionText.text = question?.question
            choice1.text = question?.choice1
            choice2.text = question?.choice2
            choice3.text = question?.choice3
            choice4.text = question?.choice4
        }
    }
    
    let boarderView : UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.lightGray
        return view
    }()
    
    let questionNoLabel: UILabel = {
        let label = UILabel()
        label.text = "Question"
        label.textColor = UIColor.darkGray
        label.numberOfLines = 2
        label.font = UIFont.boldSystemFont(ofSize: label.font.pointSize)
        label.font = label.font.withSize(15)
        return label
    }()
    
    let questionLabel: UILabel = {
        let label = UILabel()
        label.text = "Question :"
        label.textColor = UIColor.gray
        label.numberOfLines = 2
        label.font = label.font.withSize(15)
        return label
    }()
    
    let choicesLabel: UILabel = {
        let label = UILabel()
        label.text = "Choices :"
        label.textColor = UIColor.gray
        label.numberOfLines = 2
        label.font = label.font.withSize(15)
        return label
    }()
    
    let number1Label: UILabel = {
        let label = UILabel()
        return label
    }()
    
    let number2Label: UILabel = {
        let label = UILabel()
        return label
    }()
    
    let number3Label: UILabel = {
        let label = UILabel()
        return label
    }()
    
    let number4Label: UILabel = {
        let label = UILabel()
        return label
    }()
    
    let answer1 : UIView = {
        let view = UIView()
        view.layer.borderWidth = 2
        view.layer.borderColor = UIColor.lightGray.cgColor
        view.layer.cornerRadius = 10
        return view
    }()
    
    let answer2 : UIView = {
        let view = UIView()
        view.layer.borderWidth = 2
        view.layer.borderColor = UIColor.lightGray.cgColor
        view.layer.cornerRadius = 10
        return view
    }()
    
    let answer3 : UIView = {
        let view = UIView()
        view.layer.borderWidth = 2
        view.layer.borderColor = UIColor.lightGray.cgColor
        view.layer.cornerRadius = 10
        return view
    }()
    
    let answer4 : UIView = {
        let view = UIView()
        view.layer.borderWidth = 2
        view.layer.borderColor = UIColor.lightGray.cgColor
        view.layer.cornerRadius = 10
        return view
    }()

    
    let questionText: UITextView = {
        let textView = UITextView()
        textView.text = ""
        textView.backgroundColor = UIColor.white
        textView.textColor = UIColor.darkGray
        textView.layer.borderWidth = 1.0
        textView.layer.borderColor = UIColor.lightGray.cgColor
        textView.isEditable = true
        textView.isSelectable = true
        textView.isScrollEnabled = true
        textView.layer.cornerRadius = 5
        textView.font = textView.font?.withSize(18)
        return textView
    }()
    
    
    let choice1: UITextField = {
        let text = UITextField()
        text.borderStyle = .none
        text.backgroundColor = .white
        text.textColor = UIColor.darkGray
        text.font = .systemFont(ofSize: 15)
        text.layer.borderWidth = 1.0
        text.layer.borderColor = UIColor.lightGray.cgColor
        text.layer.cornerRadius = 5
        text.setLeftPadding(space: 8)
        return text
    }()
    
    let choice2: UITextField = {
        let text = UITextField()
        text.borderStyle = .none
        text.backgroundColor = .white
        text.textColor = UIColor.darkGray
        text.font = .systemFont(ofSize: 15)
        text.layer.borderWidth = 1.0
        text.layer.borderColor = UIColor.lightGray.cgColor
        text.layer.cornerRadius = 5
        text.setLeftPadding(space: 8)
        return text
    }()
    
    let choice3: UITextField = {
        let text = UITextField()
        text.borderStyle = .none
        text.backgroundColor = .white
        text.textColor = UIColor.darkGray
        text.font = .systemFont(ofSize: 15)
        text.layer.borderWidth = 1.0
        text.layer.borderColor = UIColor.lightGray.cgColor
        text.layer.cornerRadius = 5
        text.setLeftPadding(space: 8)
        return text
    }()
    
    let choice4: UITextField = {
        let text = UITextField()
        text.borderStyle = .none
        text.backgroundColor = .white
        text.textColor = UIColor.darkGray
        text.font = .systemFont(ofSize: 15)
        text.layer.borderWidth = 1.0
        text.layer.borderColor = UIColor.lightGray.cgColor
        text.layer.cornerRadius = 5
        text.setLeftPadding(space: 8)
        return text
    }()
    
    private func setupView() {
        
        addSubview(answer1)
        answer1.addSubview(number1Label)
        addSubview(answer2)
        answer2.addSubview(number2Label)
        addSubview(answer3)
        answer3.addSubview(number3Label)
        addSubview(answer4)
        answer4.addSubview(number4Label)
        
        addSubview(choice1)
        addSubview(choice2)
        addSubview(choice3)
        addSubview(choice4)
        addSubview(questionNoLabel)
        addSubview(boarderView)
        addSubview(questionLabel)
        addSubview(questionText)
        addSubview(choicesLabel)
    }
    
    private func setupConstraints() {
        
        questionNoLabel.frame = CGRect(x: 10, y: 25, width: frame.width-20, height: 50)
        boarderView.frame = CGRect(x: 10, y: questionNoLabel.frame.maxY, width: frame.width-20, height: 1)
        questionLabel.frame = CGRect(x: 10, y: boarderView.frame.maxY+20, width: frame.width-20, height: 20)
        questionText.frame = CGRect(x: 10, y: questionLabel.frame.maxY, width: frame.width-20, height: 50)
        choicesLabel.frame = CGRect(x: 10, y: questionText.frame.maxY+10, width: frame.width-20, height: 20)
        answer1.frame = CGRect(x: 10, y: choicesLabel.frame.maxY+15, width: 20, height: 20)
        answer2.frame = CGRect(x: 10, y: answer1.frame.maxY+20, width: 20, height: 20)
        answer3.frame = CGRect(x: 10, y: answer2.frame.maxY+20, width: 20, height: 20)
        answer4.frame = CGRect(x: 10, y: answer3.frame.maxY+20, width: 20, height: 20)
        choice1.frame = CGRect(x: answer1.frame.maxX+10, y: choicesLabel.frame.maxY+10, width: frame.width-answer1.frame.maxX-20, height: 30)
        choice2.frame = CGRect(x: answer2.frame.maxX+10, y: choice1.frame.maxY+10, width: frame.width-answer2.frame.maxX-20, height: 30)
        choice3.frame = CGRect(x: answer3.frame.maxX+10, y: choice2.frame.maxY+10, width: frame.width-answer3.frame.maxX-20, height: 30)
        choice4.frame = CGRect(x: answer4.frame.maxX+10, y: choice3.frame.maxY+10, width: frame.width-answer4.frame.maxX-20, height: 30)
    }
    
    override func setupViews() {
        super.setupViews()
        
        setupView()
        setupConstraints()
    }
}

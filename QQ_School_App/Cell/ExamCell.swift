//
//  ExamCell.swift
//  QQ_School_App
//
//  Created by Abby Esteves on 23/05/2018.
//  Copyright © 2018 Abby Esteves. All rights reserved.
//

import UIKit
class ExamCell: BaseCell, UIScrollViewDelegate {
    var MainScrollView = UIScrollView()
    var question: Questions? {
        didSet {
            let size = CGSize(width: frame.width-20, height: frame.height)
            let options = NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin)
            let answers = [answer1, answer2, answer3, answer4]
            let choicesText = [choice1, choice2, choice3, choice4]
            let choices = [question?.choice1, question?.choice2, question?.choice3, question?.choice4]
            let estimatedQuestionFrame = NSString(string: "\((question?.question)!)").boundingRect(with: size, options: options, attributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 16)], context: nil)
            
            
            questionText.frame = CGRect(x: 10, y: questionLabel.frame.maxY, width: frame.width-20, height: estimatedQuestionFrame.height+5)
            choicesLabel.frame = CGRect(x: 10, y: questionText.frame.maxY+10, width: frame.width-20, height: 20)
            
            var y = choicesLabel.frame.maxY
            for (index, choice) in choices.enumerated() {
                if question?.answered == choice {
                    answers[index].backgroundColor = UIColor.ThemeDarkGreen(alpha: 1.0)
                } else {
                    answers[index].backgroundColor = UIColor.clear
                }
                let estimatedChoiceFrame = NSString(string: "\(choice)").boundingRect(with: size, options: options, attributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 16)], context: nil)
                
                answers[index].frame = CGRect(x: 10, y: y+10, width: 20, height: 20)
                choicesText[index].frame = CGRect(x: answers[index].frame.maxX+10, y: y, width: frame.width-answers[index].frame.maxX-20, height: estimatedChoiceFrame.height+5)
                y = y + choicesText[index].frame.height+10
                
//                if y > frame.height {
//                    MainScrollView.frame = CGRect(x: 0, y: 0, width: frame.width, height: self.MainScrollView.frame.height+choicesText[index].frame.height+10)
//                }
            }
            
            
            
            
            number1Label.text = "\((question?.number)!)1"
            number2Label.text = "\((question?.number)!)2"
            number3Label.text = "\((question?.number)!)3"
            number4Label.text = "\((question?.number)!)4"
            questionNoLabel.text = "Question \((question?.number)!)"
            questionText.text = question?.question
            choice1.text = question?.choice1
            choice2.text = question?.choice2
            choice3.text = question?.choice3
            choice4.text = question?.choice4
            
        }
    }
    
    let boarderView : UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.lightGray
        return view
    }()
    
    let questionNoLabel: UILabel = {
        let label = UILabel()
        label.text = "Question"
        label.textColor = UIColor.darkGray
        label.numberOfLines = 2
        label.font = UIFont.boldSystemFont(ofSize: label.font.pointSize)
        label.font = label.font.withSize(15)
        return label
    }()
    
    let questionLabel: UILabel = {
        let label = UILabel()
        label.text = "Question :"
        label.textColor = UIColor.gray
        label.numberOfLines = 2
        label.font = label.font.withSize(15)
        return label
    }()
    
    let choicesLabel: UILabel = {
        let label = UILabel()
        label.text = "Choices :"
        label.textColor = UIColor.gray
        label.numberOfLines = 2
        label.font = label.font.withSize(15)
        return label
    }()
    
    let number1Label: UILabel = {
        let label = UILabel()
        return label
    }()
    
    let number2Label: UILabel = {
        let label = UILabel()
        return label
    }()
    
    let number3Label: UILabel = {
        let label = UILabel()
        return label
    }()
    
    let number4Label: UILabel = {
        let label = UILabel()
        return label
    }()
    
    let answer1 : UIView = {
        let view = UIView()
        view.layer.borderWidth = 2
        view.layer.borderColor = UIColor.lightGray.cgColor
        view.layer.cornerRadius = 10
        return view
    }()
    
    let answer2 : UIView = {
        let view = UIView()
        view.layer.borderWidth = 2
        view.layer.borderColor = UIColor.lightGray.cgColor
        view.layer.cornerRadius = 10
        return view
    }()

    let answer3 : UIView = {
        let view = UIView()
        view.layer.borderWidth = 2
        view.layer.borderColor = UIColor.lightGray.cgColor
        view.layer.cornerRadius = 10
        return view
    }()

    let answer4 : UIView = {
        let view = UIView()
        view.layer.borderWidth = 2
        view.layer.borderColor = UIColor.lightGray.cgColor
        view.layer.cornerRadius = 10
        return view
    }()
    
    let questionText: UITextView = {
        let text = UITextView()
        text.text = "Question"
        text.textColor = UIColor.darkGray
        text.isScrollEnabled = false
        text.isEditable = false
        text.isSelectable = false
        text.font = text.font?.withSize(15)
        return text
    }()
    
    let choice1: UITextView = {
        let text = UITextView()
        text.text = "Question"
        text.textColor = UIColor.darkGray
        text.isScrollEnabled = false
        text.isEditable = false
        text.isSelectable = false
        text.font = text.font?.withSize(15)
        return text
    }()
    
    let choice2: UITextView = {
        let text = UITextView()
        text.text = "Question"
        text.textColor = UIColor.darkGray
        text.isScrollEnabled = false
        text.isEditable = false
        text.isSelectable = false
        text.font = text.font?.withSize(15)
        return text
    }()
    
    let choice3: UITextView = {
        let text = UITextView()
        text.text = "Question"
        text.textColor = UIColor.darkGray
        text.isScrollEnabled = false
        text.isEditable = false
        text.isSelectable = false
        text.font = text.font?.withSize(15)
        return text
    }()
    
    let choice4: UITextView = {
        let text = UITextView()
        text.text = "Question"
        text.textColor = UIColor.darkGray
        text.isScrollEnabled = false
        text.isEditable = false
        text.isSelectable = false
        text.font = text.font?.withSize(15)
        return text
    }()
    
    private func setupView() {
//        MainScrollView = UIScrollView(frame: bounds)
        MainScrollView.delegate = self
        MainScrollView.showsVerticalScrollIndicator = true
        MainScrollView.backgroundColor = UIColor.white//Background(alpha: 1.0)
        
        addSubview(MainScrollView)
        MainScrollView.addSubview(answer1)
        answer1.addSubview(number1Label)
        MainScrollView.addSubview(answer2)
        answer2.addSubview(number2Label)
        MainScrollView.addSubview(answer3)
        answer3.addSubview(number3Label)
        MainScrollView.addSubview(answer4)
        answer4.addSubview(number4Label)
        
        MainScrollView.addSubview(choice1)
        MainScrollView.addSubview(choice2)
        MainScrollView.addSubview(choice3)
        MainScrollView.addSubview(choice4)
        MainScrollView.addSubview(questionNoLabel)
        MainScrollView.addSubview(boarderView)
        MainScrollView.addSubview(questionLabel)
        MainScrollView.addSubview(questionText)
        MainScrollView.addSubview(choicesLabel)
    }
    
    private func setupConstraints() {
        MainScrollView.frame = CGRect(x: 0, y: 0, width: frame.width, height: frame.height)
        MainScrollView.contentSize = CGSize(width: frame.width, height: frame.height)
        
        questionNoLabel.frame = CGRect(x: 10, y: 0, width: frame.width-20, height: 50)
        boarderView.frame = CGRect(x: 10, y: questionNoLabel.frame.maxY, width: frame.width-20, height: 1)
        questionLabel.frame = CGRect(x: 10, y: boarderView.frame.maxY+20, width: frame.width-20, height: 20)
//        questionText.frame = CGRect(x: 10, y: questionLabel.frame.maxY, width: frame.width-20, height: 50)
        
    }
    
    override func setupViews() {
        super.setupViews()
        
        setupView()
        setupConstraints()
    }
}

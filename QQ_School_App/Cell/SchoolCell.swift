//
//  SchoolCell.swift
//  QQ_School_App
//
//  Created by Abby Esteves on 05/03/2018.
//  Copyright © 2018 Abby Esteves. All rights reserved.
//

import UIKit

class SchoolCell: BaseCell {
    var about: MySchool? {
        didSet {
            nameLabel.text = about?.name
            
            if let title = about?.title {
                titleLabel.text = title.uppercased()
                if title == "About"{
                    addConstraintsFormat(format: "H:|-30-[v0]-30-|", views: descriptionLabel)
                } else {
                    addConstraintsFormat(format: "H:|-20-[v0]-20-|", views: descriptionLabel)
                }
            }
            
            descriptionLabel.text = about?.description
            ApiService().get_image(url_str : "\((about?.thumbnail)!)", thumbnailView : self.thumbnailView)
//            thumbnailView.image = UIView.convertBase64ToImage(imageString: "\((about?.thumbnail)!)" )
        }
    }
    
    let nameLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.lightGray
        label.numberOfLines = 2
        label.font = UIFont.boldSystemFont(ofSize: label.font.pointSize)
        label.font = label.font.withSize(13)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let titleLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.ThemeDark(alpha: 1.0)
        label.numberOfLines = 2
        label.font = UIFont.boldSystemFont(ofSize: label.font.pointSize)
        label.font = label.font.withSize(21)
        label.translatesAutoresizingMaskIntoConstraints = false
        return label
    }()
    
    let descriptionLabel: UITextView = {
        let textView = UITextView()
        textView.textColor = UIColor.darkGray
        textView.backgroundColor = UIColor.clear
        textView.isScrollEnabled = false
        textView.isEditable = false
        textView.textAlignment = .justified
        textView.font = textView.font?.withSize(15)
        return textView
    }()
    
    let thumbnailView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.backgroundColor = .lightGray
        imageView.layer.masksToBounds = true
        imageView.image = UIImage(named: "")
        return imageView
    }()
    
    private func setupView() {
        addSubview(nameLabel)
        addSubview(titleLabel)
        addSubview(descriptionLabel)
        addSubview(thumbnailView)
    }
    
    private func setupConstraints() {
        addConstraintsFormat(format: "H:|-20-[v0]-20-|", views: nameLabel)
        addConstraintsFormat(format: "H:|-20-[v0]-20-|", views: titleLabel)
        addConstraintsFormat(format: "H:|-30-[v0]-30-|", views: thumbnailView)
        addConstraintsFormat(format: "V:|-(-20)-[v0(100)]-20-[v1(20)][v2(25)]-10-[v3]-20-|", views: thumbnailView, nameLabel, titleLabel, descriptionLabel)
    }
    
    override func setupViews() {
        super.setupViews()
        
        setupView()
        setupConstraints()
    }
}

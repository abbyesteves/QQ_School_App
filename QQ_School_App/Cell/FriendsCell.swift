//
//  FriendsCell.swift
//  QQ_School_App
//
//  Created by Abby Esteves on 15/03/2018.
//  Copyright © 2018 Abby Esteves. All rights reserved.
//

import UIKit
class FriendsCell: BaseCell {
    var friend: Friends? {
        didSet {
            //            thumbnailView.image = UIImage(named: (friend?.img)!)
            nameLabel.text = "\((friend?.name)!)"
            gradeSectionLabel.text = "Grade: \((friend?.grade)!) • Section: \((friend?.section)!)"
        }
    }
    
    let thumbnailView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.backgroundColor = .clear
        imageView.layer.masksToBounds = true
        imageView.image = UIImage(named: "base64")
        imageView.layer.cornerRadius = 25
        return imageView
    }()
    
    let statusView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.backgroundColor = .clear
        imageView.layer.masksToBounds = true
        imageView.image = UIImage(named: "ic_message")?.withRenderingMode(.alwaysTemplate)
        imageView.tintColor = UIColor.lightGray
        return imageView
    }()
    
    let nameLabel: UILabel = {
        let label = UILabel()
        label.text = "name sample"
        label.textColor = UIColor.black
        label.numberOfLines = 2
        label.font = label.font.withSize(15)
        return label
    }()
    
    let gradeSectionLabel: UILabel = {
        let label = UILabel()
        label.text = "grade sec sample"
        label.textColor = UIColor.gray
        label.numberOfLines = 2
        label.font = label.font.withSize(13)
        return label
    }()
    
    private func setupView(){
        addSubview(thumbnailView)
        addSubview(nameLabel)
        addSubview(gradeSectionLabel)
//        addSubview(statusView)
    }
    
    private func setupConstraint() {
        addConstraintsFormat(format: "H:|-20-[v0(50)]-20-[v1]-20-|", views: thumbnailView, nameLabel)
//        addConstraintsFormat(format: "H:|-\(frame.width-30-20)-[v0(30)]-20-|", views: statusView)
//        addConstraintsFormat(format: "V:|-25-[v0(30)]-25-|", views: statusView)
        addConstraintsFormat(format: "V:|-20-[v0][v1]-20-|", views: nameLabel, gradeSectionLabel)
        addConstraintsFormat(format: "H:|-90-[v0]-20-|", views: gradeSectionLabel)
        addConstraintsFormat(format: "V:|-15-[v0(50)]-15-|", views: thumbnailView)
    }
    
    override func setupViews() {
        super.setupViews()
        setupView()
        setupConstraint()
    }
}

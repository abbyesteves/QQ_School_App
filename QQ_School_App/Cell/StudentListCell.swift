//
//  StudentListCell.swift
//  QQ_School_App
//
//  Created by Abby Esteves on 21/06/2018.
//  Copyright © 2018 Abby Esteves. All rights reserved.
//

import UIKit

class StudentListCell: BaseCell {
    var student: Students? {
        didSet {
            let remarks = student?.note == "" ? "Everyone is smart in their own way" : student?.note
            nameLabel.text = "\((student?.name)!)"
            meritsDemeritsLabel.text = "Merits \((student?.merits)!) / Demerits \((student?.demerits)!)"
            thumbnailImage.image = student?.gender == "male" ? UIImage(named: "ic_male") : UIImage(named: "ic_female")
            remarksLabel.text = "— \"\((remarks)!)\""
            seatLabel.text = "seat # \((student?.seat_pos)!)".uppercased()
        }
    }

    let seatLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.lightGray
        label.numberOfLines = 2
        label.font = label.font.withSize(11)
        return label
    }()

    let meritsDemeritsLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.lightGray
        label.numberOfLines = 2
        label.font = label.font.withSize(12)
        return label
    }()
    
    let thumbnailView : UIView = {
        let view = UIView()
        view.layer.cornerRadius = 30
        view.layer.borderColor = UIColor.Alert(alpha: 1.0).cgColor
        view.layer.borderWidth = 3
        return view
    }()

    let thumbnailImage: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.clipsToBounds = true
        imageView.backgroundColor = .clear
        imageView.layer.masksToBounds = true
        return imageView
    }()

    let badgeImage: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.clipsToBounds = true
        imageView.backgroundColor = .clear
        imageView.layer.masksToBounds = true
        imageView.image = UIImage(named: "badge_level_1")
        return imageView
    }()

    let expandImage: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.clipsToBounds = true
        imageView.backgroundColor = .clear
        imageView.layer.masksToBounds = true
        imageView.image = UIImage(named: "ic_dropdown")?.withRenderingMode(.alwaysTemplate)as UIImage?
        imageView.tintColor = UIColor.lightGray
        imageView.alpha = 0.2
        return imageView
    }()

    let nameLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.gray
        label.numberOfLines = 2
        label.font = label.font.withSize(15)
        return label
    }()

    let badgeLabel: UILabel = {
        let label = UILabel()
        label.text = "Good \n Level 1"
        label.textColor = UIColor.lightGray
        label.numberOfLines = 2
        label.textAlignment = .center
        label.font = label.font.withSize(12)
        return label
    }()

    let remarksLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.lightGray
        label.numberOfLines = 2
        label.font = UIFont.italicSystemFont(ofSize: 10)
        label.font = label.font.withSize(12)
        return label
    }()
    
    let meritsAddView : UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.Accept(alpha: 1.0)
        view.layer.cornerRadius = 5
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    let addMeritLabel: UILabel = {
        let label = UILabel()
        label.text = "+"
        label.textAlignment = .center
        label.textColor = UIColor.white
        label.numberOfLines = 2
        label.font = UIFont.boldSystemFont(ofSize: label.font.pointSize)
        label.font = label.font.withSize(23)
        return label
    }()
    
    let addDemeritLabel: UILabel = {
        let label = UILabel()
        label.text = "+"
        label.textAlignment = .center
        label.textColor = UIColor.white
        label.numberOfLines = 2
        label.font = UIFont.boldSystemFont(ofSize: label.font.pointSize)
        label.font = label.font.withSize(23)
        return label
    }()
    
    let minusMeritLabel: UILabel = {
        let label = UILabel()
        label.text = "-"
        label.textAlignment = .center
        label.textColor = UIColor.Accept(alpha: 1.0)
        label.numberOfLines = 2
        label.font = UIFont.boldSystemFont(ofSize: label.font.pointSize)
        label.font = label.font.withSize(23)
        return label
    }()
    
    let minusDemeritLabel: UILabel = {
        let label = UILabel()
        label.text = "-"
        label.textAlignment = .center
        label.textColor = UIColor.AlertDark(alpha: 1.0)
        label.numberOfLines = 2
        label.font = UIFont.boldSystemFont(ofSize: label.font.pointSize)
        label.font = label.font.withSize(23)
        return label
    }()
    
    let remarksText: UITextView = {
        let textView = UITextView()
        textView.backgroundColor = UIColor.white
        textView.textColor = UIColor.darkGray
        textView.layer.borderWidth = 1.0
        textView.layer.borderColor = UIColor.lightGray.cgColor
        textView.isEditable = true
        textView.isSelectable = true
        textView.isScrollEnabled = true
        textView.layer.cornerRadius = 5
        textView.font = textView.font?.withSize(10)
        return textView
    }()
    
    let boarderHorizontalView : UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.Boarder(alpha: 1.0)
        return view
    }()
    
    let boarderVerticalView : UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.Boarder(alpha: 1.0)
        return view
    }()
    
    let meritsLabel: UILabel = {
        let label = UILabel()
        label.text = "Merits".uppercased()
        label.textAlignment = .center
        label.textColor = UIColor.lightGray
        label.numberOfLines = 2
        label.font = label.font.withSize(11)
        return label
    }()
    
    let meritsMinusView : UIView = {
        let view = UIView()
        view.layer.borderColor = UIColor.Accept(alpha: 1.0).cgColor
        view.layer.borderWidth = 3
        view.layer.cornerRadius = 5
        return view
    }()
    
    let demeritsLabel: UILabel = {
        let label = UILabel()
        label.text = "Demerits".uppercased()
        label.textAlignment = .center
        label.textColor = UIColor.lightGray
        label.numberOfLines = 2
        label.font = label.font.withSize(11)
        return label
    }()
    
    let demeritsAddView : UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.AlertDark(alpha: 1.0)
        view.layer.cornerRadius = 5
        return view
    }()
    
    let demeritsMinusView : UIView = {
        let view = UIView()
        view.layer.borderColor = UIColor.AlertDark(alpha: 1.0).cgColor
        view.layer.cornerRadius = 5
        view.layer.borderWidth = 3
        return view
    }()
    
    let remarkLabel: UILabel = {
        let label = UILabel()
        label.text = "remarks".uppercased()
        label.textColor = UIColor.lightGray
        label.numberOfLines = 2
        label.font = label.font.withSize(11)
        return label
    }()
    
    private func setupView() {
        addSubview(expandImage)
        addSubview(nameLabel)
        addSubview(remarksLabel)
        addSubview(meritsDemeritsLabel)
        addSubview(seatLabel)
        addSubview(thumbnailView)
        thumbnailView.addSubview(thumbnailImage)
        addSubview(badgeImage)
        addSubview(badgeLabel)
        
        addSubview(boarderHorizontalView)
        addSubview(boarderVerticalView)
        addSubview(meritsLabel)
        addSubview(meritsAddView)
        meritsAddView.addSubview(addMeritLabel)
        addSubview(meritsMinusView)
        meritsMinusView.addSubview(minusMeritLabel)
        addSubview(demeritsLabel)
        addSubview(demeritsAddView)
        demeritsAddView.addSubview(addDemeritLabel)
        addSubview(demeritsMinusView)
        demeritsMinusView.addSubview(minusDemeritLabel)
        addSubview(remarksText)
        addSubview(remarkLabel)
        
        meritsAddView.alpha = 0
    }
    
    private func setupConstraints() {
        seatLabel.frame = CGRect(x: 10, y: 10, width: frame.width/4, height: 20)
        thumbnailView.frame = CGRect(x: ((frame.width/4)/2)-30, y: seatLabel.frame.maxY+10, width: 60, height: 60)
        thumbnailImage.frame = CGRect(x: 5, y: 5, width: 50, height: 50)
        badgeImage.frame = CGRect(x: frame.width-60, y: 10, width: 60, height: 40)
        badgeLabel.frame = CGRect(x: badgeImage.frame.minX, y: badgeImage.frame.maxY, width: badgeImage.frame.width, height: 130-badgeImage.frame.height-40)
        expandImage.frame = CGRect(x: frame.width-30, y: badgeLabel.frame.maxY, width: 20, height: 20)

        addConstraintsFormat(format: "H:|-(\(thumbnailView.frame.maxX+(((frame.width/4)/2)-30)))-[v0]-(70)-|", views: nameLabel)
        addConstraintsFormat(format: "H:|-(\(thumbnailView.frame.maxX+(((frame.width/4)/2)-30)))-[v0]-(70)-|", views: remarksLabel)
        addConstraintsFormat(format: "H:|-(\(thumbnailView.frame.maxX+(((frame.width/4)/2)-30)))-[v0]-(70)-|", views: meritsDemeritsLabel)
        addConstraintsFormat(format: "V:|-10-[v0]-5-[v1]-5-[v2]", views: nameLabel, remarksLabel, meritsDemeritsLabel)
        
//        meritsAddView.frame = CGRect(x: 0, y: expandImage.frame.maxY + 20, width: frame.width, height: 2)
        
        boarderHorizontalView.frame = CGRect(x: 10, y: 130, width: frame.width-20, height: 1)
        meritsLabel.frame = CGRect(x: 0, y: boarderHorizontalView.frame.maxY+10, width: frame.width/4, height: 30)
        meritsAddView.frame = CGRect(x: frame.width/4, y: boarderHorizontalView.frame.maxY+10, width: 30, height: 30)
        meritsMinusView.frame = CGRect(x: meritsAddView.frame.maxX+5, y: boarderHorizontalView.frame.maxY+10, width: 30, height: 30)
        boarderVerticalView.frame = CGRect(x: frame.width/2, y: boarderHorizontalView.frame.maxY+10, width: 1, height: 50)
        demeritsLabel.frame = CGRect(x: boarderVerticalView.frame.maxX, y: boarderHorizontalView.frame.maxY+10, width: frame.width/4, height: 30)
        demeritsAddView.frame = CGRect(x: boarderVerticalView.frame.maxX+(frame.width/4), y: boarderHorizontalView.frame.maxY+10, width: 30, height: 30)
        demeritsMinusView.frame = CGRect(x: demeritsAddView.frame.maxX+5, y: boarderHorizontalView.frame.maxY+10, width: 30, height: 30)
        remarkLabel.frame = CGRect(x: 10, y: boarderVerticalView.frame.maxY+10, width: frame.width-20, height: 20)
        remarksText.frame = CGRect(x: 10, y: remarkLabel.frame.maxY+5, width: frame.width-20, height: 30)
        
        addMeritLabel.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        addDemeritLabel.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        minusMeritLabel.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
        minusDemeritLabel.frame = CGRect(x: 0, y: 0, width: 30, height: 30)
    }
    
    override func setupViews() {
        super.setupViews()
        
        setupView()
        setupConstraints()
    }
}

//
//  SideMenuCell.swift
//  QQ_School_App
//
//  Created by Abby Esteves on 01/03/2018.
//  Copyright © 2018 Abby Esteves. All rights reserved.
//

import UIKit

class SideMenuCell: BaseCell {
    var settingHold = ""
    var menu: Menu? {
        didSet {
            settingHold = (menu?.label)!
            if (settingHold == "header") {
                iconImage.removeFromSuperview()
                menuLabel.removeFromSuperview()
                headerImage.backgroundColor = UIColor.Background(alpha: 1.0)
                headerImage.image = UIImage(named: (menu?.icon)!)
                //                iconImage.image = UIImage(named: (menuLabels?.icon)!)
            } else {
                if (settingHold == "Options") {
                    iconImage.removeFromSuperview()
                    menuLabel.textColor = UIColor.gray
                    menuLabel.font = UIFont.boldSystemFont(ofSize: 14.0)
                    addConstraintsFormat(format: "H:|-15-[v0]|", views: menuLabel)
                    addConstraintsFormat(format: "V:|[v0]|", views: menuLabel)
                }
//                if (settingHold == "News") {
//                    menuLabel.textColor = UIColor.ThemeDark(alpha: 1.0)
//                    iconImage.tintColor = UIColor.ThemeDark(alpha: 1.0)
//                }
                menuLabel.text = menu?.label
                iconImage.image = UIImage(named: (menu?.icon)!)?.withRenderingMode(.alwaysTemplate)
            }
            
        }
    }
    
    
    let menuLabel: UILabel = {
        let label = UILabel()
        label.font = UIFont.boldSystemFont(ofSize: 16.0)
        label.font = label.font.withSize(12)
        return label
    }()
    
    let iconImage: UIImageView = {
        let imageView = UIImageView()
        imageView.tintColor = UIColor.black
        imageView.image = UIImage(named: "ic_action_news")
        return imageView;
    }()
    
    let headerImage: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        return imageView;
    }()
    
    override var isSelected: Bool {
        didSet {
            if (settingHold == "Options") {
                menuLabel.textColor = UIColor.gray
            } else {
//                if (settingHold == "Badges") || (settingHold == "Survey") || (settingHold == "Printable Forms") || (settingHold == "About Bocaue") {
//                    iconImage.tintColor = UIColor.black
//                    menuLabel.textColor = UIColor.black
//                } else {
                    iconImage.tintColor = isSelected ? UIColor.ThemeDark(alpha: 1.0) : UIColor.black
                    menuLabel.textColor = isSelected ? UIColor.ThemeDark(alpha: 1.0) : UIColor.black
//                }
            }
        }
    }
    
    override func setupViews() {
        super.setupViews()
        
        addSubview(iconImage)
        addSubview(menuLabel)
        addSubview(headerImage)
        
        addConstraintsFormat(format: "H:|-15-[v0(23)]-30-[v1]|", views: iconImage, menuLabel)
        addConstraintsFormat(format: "V:|[v0]|", views: menuLabel)
        addConstraintsFormat(format: "V:|-13-[v0(23)]|", views: iconImage)
        
        addConstraintsFormat(format: "H:|[v0]|", views: headerImage)
        addConstraintsFormat(format: "V:|-20-[v0]|", views: headerImage)
        
    }
}

//
//  FileService.swift
//  QQ_School_App
//
//  Created by Abby Esteves on 21/03/2018.
//  Copyright © 2018 Abby Esteves. All rights reserved.
//

import UIKit
class FileService: UIViewController {
    
    func setupMenu(icons : [String], optionView : UIView) {
        let width = (view.frame.width/CGFloat(icons.count))
        var x = 0
        
        for icon in icons{
            let iconImage: UIImageView = {
                let image = UIImageView()
                image.safeAreaInsetsDidChange()
                image.image = UIImage(named: "\(icon)")?.withRenderingMode(.alwaysTemplate)as UIImage?
                image.tintColor = UIColor.gray
                return image
            }()
            
            let titlelabel : UILabel = {
                let label = UILabel()
                label.text = "\(icon)"
                return label
            }()
            
            let menuView: UIView = {
                let view = UIView()
                return view
            }()
            
            optionView.addSubview(menuView)
            menuView.addSubview(titlelabel)
            menuView.addSubview(iconImage)
            
            view.addConstraintsFormat(format: "H:|-(\(width/2-15))-[v0(25)]|", views: iconImage)
            view.addConstraintsFormat(format: "V:|-10-[v0(25)]-10-|", views: iconImage)
            
            menuView.frame = CGRect(x: x, y: 0, width: Int(width), height: 60)
//            menuView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.selectorTapped(sender:))))
            x = x+Int(width)
        }
    }
    
    func countdown(classEnd: Int) -> [String: Any] {
        let hours = (classEnd - Int(Date().timeIntervalSince1970)) / 3600
        let minutes = ((classEnd - Int(Date().timeIntervalSince1970)) % 3600) / 60
        let seconds = ((classEnd - Int(Date().timeIntervalSince1970)) % 3600) % 60
        var count = [
            "hours":"",
            "minutes":"",
            "seconds":""
        ] as [String:Any]
        
        if (classEnd - Int(Date().timeIntervalSince1970)) > 0 {
            if "\(minutes)".count == 1{
                
                if "\(seconds)".count == 1 {
                    count["hours"] = "\(hours)"
                    count["minutes"] = "0\(minutes)"
                    count["seconds"] = "0\(seconds)"
                } else {
                    count["hours"] = "\(hours)"
                    count["minutes"] = "0\(minutes)"
                    count["seconds"] = "\(seconds)"
                }
            } else {
                
                if "\(seconds)".count == 1 {
                    count["hours"] = "\(hours)"
                    count["minutes"] = "\(minutes)"
                    count["seconds"] = "0\(seconds)"
                } else {
                    count["hours"] = "\(hours)"
                    count["minutes"] = "\(minutes)"
                    count["seconds"] = "\(seconds)"
                }
            }
            
        } else {
//            count = "0:00:00"
            count["hours"] = "0"
            count["minutes"] = "00"
            count["seconds"] = "00"
        }
        return count
    }
    
    func getDateTime(format : String) -> String {
        let date : Date = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "\(format)"
        let todaysDate = dateFormatter.string(from: date)
        return todaysDate
    }
    
    func epochConvertString(date : String, format : String) -> String {
        let date = Date(timeIntervalSince1970: Double(date)!)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "\(format)"
        return dateFormatter.string(from: date)
    }
    
    func alias(name: String) -> String {
        // create alias
        let comma : Character = ","
        let idx = name.characters.index(of: comma)
        let pos = name.characters.distance(from: name.startIndex, to: idx!)
        let count = name.count - Int(pos)
        let lastname = name.dropLast(count)
        let firstname = name.dropFirst(lastname.count+1)
        return "\(firstname.dropLast(firstname.count-1)). \(lastname)"
    }
    
    func reset() {
        let loginController = LoginController()
        self.present(loginController, animated: true, completion: nil)
//        self.navigationController?.pushViewController(loginController, animated: true)
        
        ApiService().deleteDb()
    }
    
    func watch() {
        let manager = CBLManager.sharedInstance()
        let database = try! manager.databaseNamed("mydata")
        NotificationCenter.default.addObserver(forName: NSNotification.Name.cblDatabaseChange, object: database, queue: nil) {
            (notification) -> Void in
            if let changes = notification.userInfo!["changes"] as? [CBLDatabaseChange] {
                self.reset()
            }
        }
    }
    func convertDate(date : String, format : String) -> String {
        let date = Date(timeIntervalSince1970: Double(date)!)
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "\(format)"
        return dateFormatter.string(from: date)
    }
    
    func getRecentDate(date: String) -> String {
        let datePrev = Date(timeIntervalSince1970: Double(date)!)
        let dateSent = Date(timeIntervalSince1970: Double(Date().timeIntervalSince1970))
        let elapsed = dateSent.timeIntervalSince(datePrev)
        let minute = Int(elapsed/60)
        let hour = Int(minute/60)
        let day = Int(hour/24)
        let week = Int(day/7)
        var recent = String()
        //        print(" getRecentDate " , hour)
        if minute <= 5 {
            recent = "Just now"
        } else if minute >= 6 && minute <= 59 {
            recent = "\(minute) minutes ago"
        } else if minute >= 60 && minute <= 1439 { //&& minute <= 119 {
            recent = "\(hour) hours ago"
        } else if minute >= 1440 && minute <= 8639 { // && minute <= 2879 {
            recent = "\(day) days ago"
        } else if day >= 6 && day <= 28 { //&& minute <= 15919
            recent = "\(week) weeks ago"
        } else if day >= 29 {
            recent = convertDate(date: "\(date)", format: "MM/dd/yyyy")
        }
        
        return recent
    }
    
}

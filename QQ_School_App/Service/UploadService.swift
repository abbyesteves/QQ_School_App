//
//  UploadService.swift
//  QQ_School_App
//
//  Created by Abby Esteves on 12/04/2018.
//  Copyright © 2018 Abby Esteves. All rights reserved.
//

import UIKit
import SystemConfiguration

class UploadService: UIViewController, URLSessionDelegate, URLSessionTaskDelegate, URLSessionDataDelegate {
    
    let session = URLSession.self
    
    func postModule(imageName : String, image : UIImage, fileExtension : String, subj_code: String){
        print(" postModule : ", imageName, image, fileExtension, subj_code)
        var toUpload = UIImagePNGRepresentation(image)
        if fileExtension == "jpg" || fileExtension == "peg"{
            toUpload = UIImageJPEGRepresentation(image, 1)
        }
        
        let boundary = "boundary-\(NSUUID().uuidString)"
        var request = URLRequest(url: ApiService().modulesURL!)
        request.httpMethod = "POST"
        request.setValue("Keep-Alive", forHTTPHeaderField: "Connection")
        request.setValue("multipart/form-data", forHTTPHeaderField: "ENCTYPE")
        request.setValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        request.setValue("\(toUpload)", forHTTPHeaderField: "uploaded_file")
        request.httpBody = createBodyWithParameters(filePathKey: "file", imageDataKey: toUpload!, boundary: boundary, imageName : imageName, fileExtension: fileExtension) as Data
        
        session.shared.dataTask(with: request) { (data, response, error) in
            if let response = response {
                print(response)
            }
            
            if let data = data {
                let response = String(data: data, encoding: .utf8)!
                print(" data reponse : ",response)
                if response == "fail"{
                    //                    showToast(message : String
                    let alertController = UIAlertController(title: "Upload", message: "Check your connectivity and try again.", preferredStyle: UIAlertControllerStyle.alert)
                    
                    alertController.addAction(UIAlertAction(title: NSLocalizedString("Ok", comment: "Default action"), style: UIAlertActionStyle.destructive, handler: { _ in
                        
                    }))
                    
                    let alertWindow = UIWindow(frame: UIScreen.main.bounds)
                    alertWindow.rootViewController = UIViewController()
                    alertWindow.windowLevel = UIWindowLevelAlert + 1;
                    alertWindow.makeKeyAndVisible()
                    alertWindow.rootViewController?.present(alertController, animated: true, completion: nil)
                } else {
                    DispatchQueue.main.async(execute: {
                        ApiService().createModule(file_name: imageName, subj_code: subj_code, fileExtension : fileExtension)
                    })
                }
            }
        }.resume()
        
        //        fileInputStream = new FileInputStream(selectedFile);
        //        URL url = new URL(SERVER_URL);
        //        connection = (HttpURLConnection) url.openConnection();
        //        connection.setDoInput(true);//Allow Inputs
        //        connection.setDoOutput(true);//Allow Outputs
        //        connection.setUseCaches(false);//Don't use a cached Copy
        //        connection.setRequestMethod("POST");
        //        connection.setRequestProperty("Connection", "Keep-Alive");
        //        connection.setRequestProperty("ENCTYPE", "multipart/form-data");
        //        connection.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);
        //        connection.setRequestProperty("uploaded_file", selectedFilePath);
        //
        //        //creating new dataoutputstream
        //        dataOutputStream = new DataOutputStream(connection.getOutputStream());
        //
        //        //writing bytes to data outputstream
        //        dataOutputStream.writeBytes(twoHyphens + boundary + lineEnd);
        //        dataOutputStream.writeBytes("Content-Disposition: form-data; name=\"uploaded_file\";filename=\""
        //            + selectedFilePath + "\"" + lineEnd);
        //
        //        dataOutputStream.writeBytes(lineEnd);
    }
    
    func createBodyWithParameters(filePathKey: String, imageDataKey : Data, boundary: String, imageName: String, fileExtension: String) -> NSData {
        let body = NSMutableData();
        
        let filename = "\(imageName).\(fileExtension)"
        let mimetype = "image/\(fileExtension)"
        
        body.appendString(string: "--\(boundary)\r\n")
        body.appendString(string: "Content-Disposition: form-data; name=\"uploaded_file\";filename=\"\(filename)\"\r\n")
        body.appendString(string: "Content-Type:\(mimetype)\r\n\r\n")
        body.append(imageDataKey)
        body.appendString(string: "\r\n")
        body.appendString(string: "--\(boundary)\r\n")
        
        return body
    }
    
    func urlSession(_ session: URLSession, didBecomeInvalidWithError error: Error?) {
        view.alertView(title: "Opps", message: (error?.localizedDescription)!)
    }
    
    func urlSession(_ session: URLSession, task: URLSessionTask, didSendBodyData bytesSent: Int64, totalBytesSent: Int64, totalBytesExpectedToSend: Int64) {
        var uploadProgress: Float = Float(bytesSent) / Float(totalBytesExpectedToSend)
        print(" uploadProgress ", uploadProgress)
    }
    
    func urlSession(_ session: URLSession, didReceive challenge: URLAuthenticationChallenge, completionHandler: @escaping (URLSession.AuthChallengeDisposition, URLCredential?) -> Void) {

    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
}

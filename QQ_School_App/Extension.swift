//
//  Extension.swift
//  QQ_School_App
//
//  Created by Abby Esteves on 01/03/2018.
//  Copyright © 2018 Abby Esteves. All rights reserved.
//

import UIKit

extension UIColor {
    static func Theme(alpha: CGFloat) -> UIColor {
        return UIColor(red: 0/255, green: 191/255, blue: 198/255, alpha: alpha)
    }
    
    static func ThemeDark(alpha: CGFloat) -> UIColor {
        return UIColor(red: 64/255, green: 164/255, blue: 170/255, alpha: alpha)
    }
    
    static func ThemeDarkGreen(alpha: CGFloat) -> UIColor {
        return UIColor(red: 17/255, green: 97/255, blue: 44/255, alpha: alpha)
    }
    
    static func ThemeDarkBlue(alpha: CGFloat) -> UIColor {
        return UIColor(red: 0/255, green: 59/255, blue: 93/255, alpha: alpha)
    }
    
    static func ThemeOrange(alpha: CGFloat) -> UIColor {
        return UIColor(red: 237/255, green: 171/255, blue: 92/255, alpha: alpha)
    }
    
    static func Boarder(alpha: CGFloat) -> UIColor {
        return UIColor(red: 230/255, green: 236/255, blue: 240/255, alpha: alpha)
    }
    
    static func Background(alpha: CGFloat) -> UIColor {
        return UIColor(red: 244/255, green: 244/255, blue: 244/255, alpha: alpha)
    }
    
    static func Highlight(alpha: CGFloat) -> UIColor {
        return UIColor(red: 170/255, green: 236/255, blue: 239/255, alpha: alpha)
    }
    
    static func Warning(alpha: CGFloat) -> UIColor {
        return UIColor(red: 223/255, green: 212/255, blue: 85/255, alpha: alpha)
    }
    
    static func Accept(alpha: CGFloat) -> UIColor {
        return UIColor(red: 65/255, green: 180/255, blue: 90/255, alpha: alpha)
    }
    
    static func Alert(alpha: CGFloat) -> UIColor {
        return UIColor(red: 223/255, green: 90/255, blue: 85/255, alpha: alpha)
    }
    
    static func AlertDark(alpha: CGFloat) -> UIColor {
        return UIColor(red: 153/255, green: 26/255, blue: 26/255, alpha: alpha)
    }
    
    static func rgba(red: CGFloat, green: CGFloat, blue: CGFloat, alpha: CGFloat) -> UIColor {
        return UIColor(red: red/255, green: green/255, blue: blue/255, alpha: alpha)
    }
}

extension UIView {
    func addConstraintsFormat(format: String, views: UIView...) {
        var viewsDictionary = [String:UIView]();
        for(index, view) in views.enumerated(){
            let setKey = "v\(index)"
            view.translatesAutoresizingMaskIntoConstraints = false
            viewsDictionary[setKey] = view
        }
        
        addConstraints(NSLayoutConstraint.constraints(withVisualFormat: format, options: NSLayoutFormatOptions(), metrics: nil, views: viewsDictionary))
    }
    
    func showToast(message : String) {
        if let window = UIApplication.shared.keyWindow {
//            let size = CGSize(width: window.frame.size.width/2 - 115, height: window.frame.size.height)
//            let options = NSStringDrawingOptions.usesFontLeading.union(.usesLineFragmentOrigin)
//            let estimatedFrame = NSString(string: message).boundingRect(with: size, options: options, attributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 13.83)], context: nil)
            
            let toastLabel = UILabel(frame: CGRect(x: window.frame.size.width/2 - 115, y: window.frame.size.height-100, width: 230, height: 35))
            toastLabel.backgroundColor = UIColor.black.withAlphaComponent(0.6)
            toastLabel.textColor = UIColor.white
            toastLabel.textAlignment = .center;
            toastLabel.font = toastLabel.font.withSize(13)
            toastLabel.textAlignment = .center
            toastLabel.numberOfLines = 5
            toastLabel.text = message
            toastLabel.alpha = 1.0
            toastLabel.layer.cornerRadius = 17.5;
            toastLabel.clipsToBounds  =  true
            window.addSubview(toastLabel)
            UIView.animate(withDuration: 7.0, delay: 0.1, options: .curveEaseOut, animations: {
                toastLabel.alpha = 0
            }, completion: {(isCompleted) in
                toastLabel.removeFromSuperview()
                // delete if status change
                if message == "Session Expired.\nLogging out..." {
                    ApiService().deleteDb()
                }
            })
        }
    }
    
    func imbedIcon(string : String, iconImage : String, iconSize : CGFloat, textSize : CGFloat) -> NSMutableAttributedString {
        let attachment = NSTextAttachment()
        attachment.image = UIImage(named: iconImage)
        attachment.bounds = CGRect(x: 0, y: -5, width: iconSize, height: iconSize)
        
        let attachmentStr = NSAttributedString(attachment: attachment)
        let myAttribute = [ NSAttributedStringKey.font: UIFont.systemFont(ofSize: textSize) ]
        
        let myString = NSMutableAttributedString(string: "")
        myString.append(attachmentStr)
//        myString.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.lightGray, range: NSRange(location: 0, length: 1))
        let myString1 = NSMutableAttributedString(string: string, attributes: myAttribute)
//        myString1.addAttribute(NSAttributedStringKey.foregroundColor, value: UIColor.lightGray, range: NSRange(location: 0, length: string.count))
        myString.append(myString1)
        return myString
    }
    
    func grayScale(image : UIImageView) {
        let context = CIContext(options: nil)
        let filter = CIFilter(name: "CIPhotoEffectNoir")
        filter!.setValue(CIImage(image: image.image!), forKey: kCIInputImageKey)
        let output = filter!.outputImage
        let cgimg = context.createCGImage(output!,from: output!.extent)
        let processedImage = UIImage(cgImage: cgimg!)
        image.image = processedImage
    }
    
    func alertView(title : String, message : String) {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.alert)
        
        alertController.addAction(UIAlertAction(title: NSLocalizedString("Ok", comment: "Default action"), style: UIAlertActionStyle.default, handler: { _ in
            
        }))
        
        let alertWindow = UIWindow(frame: UIScreen.main.bounds)
        alertWindow.rootViewController = UIViewController()
        alertWindow.windowLevel = UIWindowLevelAlert + 1;
        alertWindow.makeKeyAndVisible()
        alertWindow.rootViewController?.present(alertController, animated: true, completion: nil)
    }
    
    func widthEstimation(text : String, size : CGFloat) -> CGFloat {
        var whitespace = CGFloat()
        for char in text {
            if char == " " {
                whitespace = whitespace + 1
            }
        }
        
        let spaces = whitespace*3.5
        let count = CGFloat(text.count)
        let font = CGFloat(size-spaces)
        
        return count*font
    }
    
    func heightEstimation(text : String, width: CGFloat, size: CGFloat, defaultHeight: CGFloat) -> CGFloat{
        let count = CGFloat(text.count)
        let numberPerLine = CGFloat(width/size)
        let lines = count/numberPerLine
        let height = round(lines)*20
        if height <= 0 {
            return defaultHeight
        }
        return height
    }
    
    class func convertBase64ToImage(imageString: String) -> UIImage {
        let imageData = Data(base64Encoded: imageString, options: Data.Base64DecodingOptions.ignoreUnknownCharacters)!
        return UIImage(data: imageData)!
    }
    
    func dateToday() -> String {
        let date : Date = Date()
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "MMMM dd,yyyy"
        let todaysDate = dateFormatter.string(from: date)
        return todaysDate
    }
    
    func resizeImage(image: UIImage, newWidth: CGFloat) -> UIImage {
        
        let scale = newWidth / image.size.width
        let newHeight = image.size.height * scale
        UIGraphicsBeginImageContext(CGSize(width: newWidth, height: newHeight))
        image.draw(in: CGRect(x: 0, y: 0, width: newWidth, height: newHeight))
        let newImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        return newImage!
    }

}

extension UITextField {
    
    func setLeftPadding(space: CGFloat) {
        let paddingView = UIView(frame: CGRect(x: 0, y: 0, width: space, height: self.frame.size.height))
        self.leftView = paddingView
        self.leftViewMode = .always
    }
    
}

extension NSMutableData {
    
    func appendString(string: String){
        let data = string.data(using: String.Encoding.utf8, allowLossyConversion: true)
        append(data!)
    }
    
}

extension OutputStream {
    func write(data: Data) -> Int {
        return data.withUnsafeBytes { write($0, maxLength: data.count) }
    }
}

extension InputStream {
    func read(data: inout Data) -> Int {
        return data.withUnsafeMutableBytes { read($0, maxLength: data.count) }
    }
}

extension String {
    func fromBase64() -> String? {
        guard let data = Data(base64Encoded: self, options: Data.Base64DecodingOptions(rawValue: 0)) else {
            return nil
        }
        
        return String(data: data as Data, encoding: String.Encoding.utf8)
    }
}

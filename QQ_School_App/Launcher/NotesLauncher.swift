//
//  NotesLauncher.swift
//  QQ_School_App
//
//  Created by Abby Esteves on 20/03/2018.
//  Copyright © 2018 Abby Esteves. All rights reserved.
//

import UIKit
class NotesLauncher: UIViewController, UITextViewDelegate {
    
    let manager = CBLManager.sharedInstance()
    var keyboardHeight = CGFloat()
    var documentID = String()
    let contentText: UITextView = {
        let textView = UITextView()
        textView.backgroundColor = UIColor.clear
        textView.textColor = UIColor.darkGray
        textView.isEditable = true
        textView.isSelectable = true
        textView.isScrollEnabled = true
        textView.font = textView.font?.withSize(15)
        return textView
    }()
    
    let doneButton: UIButton = {
        let button = UIButton()
        button.backgroundColor = UIColor.clear
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 13)
        button.setTitleColor(UIColor.ThemeDark(alpha: 1.0), for: .normal)
        button.setTitle("Done", for: .normal)
        return button
    }()
    
    @objc func done(){
        contentText.resignFirstResponder()
        let context = (contentText.text)!
        let space : Character = "\n"
        if let idx = context.characters.index(of: space) {
            let pos = context.characters.distance(from: context.startIndex, to: idx)
            let count = context.count - Int(pos)
            let title = context.dropLast(count)
            if "\((contentText.text)!)" != "" {
                if documentID == "" {
                    ApiService().createNotes(title: "\(title)", note: context)
                } else {
                    ApiService().updateNotes(documentID: documentID, title: "\(title)", note: context)
                }
            }
        }
    }
    
    @objc func selectContext() {
        contentText.becomeFirstResponder()
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            keyboardHeight = keyboardSize.height
            self.contentText.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height-keyboardHeight)
        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
        self.contentText.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height)
    }
    
    private func loadNotes() {
        let database = try! manager.databaseNamed("notes")
        let doc = database.document(withID: documentID)
        let properties = doc?.properties
        
        if properties != nil {
//            print(" documentID ", properties!["note"])
            contentText.text = "\((properties!["note"])!)"
        } else {
//            print(" no documentID ")
            Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.selectContext), userInfo: nil, repeats: false)
        }
    }
    
    private func setupView() {
        contentText.delegate = self
        contentText.contentInset = UIEdgeInsetsMake(10, 10, 10, 10)
        view.addSubview(contentText)
        view.addSubview(doneButton)
        
        contentText.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height)
        doneButton.frame = CGRect(x: view.frame.width-60, y: 10, width: 50, height: 20)
        
        doneButton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(done)))
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        done()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.Background(alpha: 1.0)
        
        documentID = self.navigationItem.title!
        print(" documentID ", documentID)
        
        NotificationCenter.default.addObserver(self, selector: #selector(NotesLauncher.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(NotesLauncher.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
        setupView()
        loadNotes()
    }
    
}

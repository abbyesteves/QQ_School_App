//
//  ScanQRLauncher.swift
//  QQ_School_App
//
//  Created by Abby Esteves on 06/03/2018.
//  Copyright © 2018 Abby Esteves. All rights reserved.
//

import UIKit
import AVFoundation

class ScanQRLauncher: UIViewController, AVCaptureMetadataOutputObjectsDelegate, UITextFieldDelegate {
    
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var captureSession: AVCaptureSession!
    var previewLayer: AVCaptureVideoPreviewLayer!
    var subject_code = String()
    var documentID = "-"
    var gradeSection = String()
    var seatnumber = String()
    var seatplan = false
    var section = ""
    var currentClass = ApiService().currentClass()
    
    var myID = [
        "d": "",
        "i": "",
        "p": "",
        "t": "",
        "s": "",
        "n": "",
        "g": "",
        "gen": "",
        "sct": ""
    ] as [String : Any]
    
    let scanLabel: UILabel = {
        let label = UILabel()
        label.text = "Scan QR Code"
        label.textColor = UIColor.white
        label.textAlignment = .center
        label.font = label.font.withSize(13)
        return label
    }()
    
    let gridImageView: UIImageView = {
        let image = UIImageView()
        image.image = UIImage(named: "grid")?.withRenderingMode(.alwaysTemplate)
        image.tintColor = UIColor.white
        image.contentMode = .scaleAspectFill
        return image
    }()
    
    let contentText: UITextField = {
        let text = UITextField()
        text.borderStyle = .none
        text.backgroundColor = .white
        text.textColor = UIColor.gray
        text.font = .systemFont(ofSize: 13)
        var placeholder = NSMutableAttributedString()
        placeholder = NSMutableAttributedString(attributedString: NSAttributedString(string: "For the reason of...", attributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 13.0), .foregroundColor: UIColor.gray]))
        text.setLeftPadding(space: 8)
        text.attributedPlaceholder = placeholder
        return text
    }()
    
    let backView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.backgroundColor = .clear
        imageView.layer.masksToBounds = true
        imageView.image = UIImage(named: "ic_back")?.withRenderingMode(.alwaysTemplate)as UIImage?
        imageView.tintColor = UIColor.white
        return imageView
    }()
    
    let mainView : UIView = {
        let view = UIView()
        view.backgroundColor = .clear
        return view
    }()
    
    let closeView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.backgroundColor = .clear
        imageView.layer.masksToBounds = true
        imageView.image = UIImage(named: "ic_reject")?.withRenderingMode(.alwaysTemplate)
        imageView.tintColor = UIColor.lightGray
        return imageView
    }()
    
    let statusLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.white
        label.text = "Opps.. An error occured. Check your internet connectivity."
        label.backgroundColor = UIColor.Alert(alpha: 1.0)
        label.font = label.font.withSize(12)
        label.textAlignment = .center
        return label
    }()
    
    let thumbnailView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.backgroundColor = .clear
        imageView.layer.masksToBounds = true
        imageView.layer.cornerRadius = 25
        imageView.image = UIImage(named: "base64")
        return imageView
    }()
    
    @objc func reset(){
        UIView.animate(withDuration: 0.5, delay: 5, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.statusLabel.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: 0)
        }, completion: { (Bool) in
            self.setup()
        })
    }
    
    
    func setup(){
        captureSession = AVCaptureSession()
        
        guard let videoCaptureDevice = AVCaptureDevice.default(for: .video) else { return }
        let videoInput: AVCaptureDeviceInput
        
        do {
            videoInput = try AVCaptureDeviceInput(device: videoCaptureDevice)
        } catch {
            return
        }
        
        if (captureSession.canAddInput(videoInput)) {
            captureSession.addInput(videoInput)
        } else {
            failed()
            return
        }
        
        let metadataOutput = AVCaptureMetadataOutput()
        
        if (captureSession.canAddOutput(metadataOutput)) {
            captureSession.addOutput(metadataOutput)
            
            metadataOutput.setMetadataObjectsDelegate(self, queue: DispatchQueue.main)
            metadataOutput.metadataObjectTypes = [.qr]
        } else {
            failed()
            return
        }
        
        previewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        previewLayer.frame = view.layer.bounds
        previewLayer.videoGravity = .resizeAspectFill
        view.layer.addSublayer(previewLayer)
        view.addSubview(mainView)
        view.addSubview(scanLabel)
        view.addSubview(gridImageView)
        view.addSubview(statusLabel)
        
        mainView.gestureRecognizers?.removeAll()
        
        let half = view.frame.width/2
        statusLabel.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: 0)
        mainView.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height)
        

        scanLabel.frame = CGRect(x: 0, y: view.frame.height/3-50, width: view.frame.width, height: 20)
        gridImageView.frame = CGRect(x: view.frame.width/2-(half/2), y: scanLabel.frame.maxY+5, width: half, height: half)

        self.captureSession.startRunning()
        
    }
    
    func setupForOrientation() {
        previewLayer.frame = CGRect(x: 0, y: 0, width: view.frame.height, height: view.frame.width)
        scanLabel.frame = CGRect(x: 0, y: previewLayer.frame.height/3-50, width: previewLayer.frame.width, height: 20)
        gridImageView.frame = CGRect(x: previewLayer.frame.width/2-((previewLayer.frame.width/2)/2), y: scanLabel.frame.maxY+5, width: previewLayer.frame.width/2, height:  previewLayer.frame.width/2)
    }
    
    @objc func close(){
//        print(" close pressed")
        captureSession.stopRunning()
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.backView.alpha = 0
        }, completion: { (Bool) in
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                self.previewLayer.frame = CGRect(x: 0, y: self.view.frame.height, width: self.view.frame.width, height: self.view.frame.height)
                self.mainView.frame = CGRect(x: 0, y: self.view.frame.height, width: self.view.frame.width, height: self.view.frame.height)
            }, completion: { (Bool) in
                self.previewLayer.removeFromSuperlayer()
            })
        })
    }
    
    func failed() {
        let ac = UIAlertController(title: "Scanning not supported", message: "Your device does not support scanning a code from an item. Please use a device with a camera.", preferredStyle: .alert)
        ac.addAction(UIAlertAction(title: "OK", style: .default))
        present(ac, animated: true)
        captureSession = nil
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        appDelegate.myOrientation = .portrait
        UIDevice.current.setValue(UIInterfaceOrientation.portrait.rawValue, forKey: "orientation")
        if (captureSession?.isRunning == false) {
            captureSession.startRunning()
        }
    }

    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)

        if (captureSession?.isRunning == true) {
            captureSession.stopRunning()
//            self.setup()
        }
    }
    
    func metadataOutput(_ output: AVCaptureMetadataOutput, didOutput metadataObjects: [AVMetadataObject], from connection: AVCaptureConnection) {
        captureSession.stopRunning()
        
        if let metadataObject = metadataObjects.first {
            guard let readableObject = metadataObject as? AVMetadataMachineReadableCodeObject else { return }
            guard let stringValue = readableObject.stringValue else { return }
            AudioServicesPlaySystemSound(SystemSoundID(kSystemSoundID_Vibrate))
            found(code: stringValue)
        }
        
        dismiss(animated: true)
    }
    
    func deleteCurrent() {
        let manager = CBLManager.sharedInstance()
        let database = try! manager.databaseNamed("currentstudent")
        do {
            //purge last db
            try database.delete()
//            if self.subject_code == "NO_SUBJECT" {
            if "\((self.myID["t"])!)" == "1" && "\((ApiService().myQR()["t"])!)" == "2" {
                self.subject_code = "NO_SUBJECT"
                self.showQRInfo()
//                self.startReplicationLogin()
            } else if "\((self.myID["t"])!)" == "2" && "\((ApiService().myQR()["t"])!)" == "1" {
                self.subject_code = self.navigationItem.title!
                self.showQRInfo()
//                self.startReplicationLogin()
            } else if "\((self.myID["t"])!)" == "\((ApiService().myQR()["t"])!)" {
                if "\((self.myID["t"])!)" == "1" && "\((ApiService().myQR()["t"])!)" == "1" {
                    self.errorMessage(message: "Teachers cannot be scanned for attendances.", reset: false)
                }
                self.subject_code = "NO_SUBJECT"
                self.showQRInfo()
//                self.startReplicationLogin()
            } else {
                self.setup()
            }
        } catch {
            print("Can't save update document in database")
            return
        }
    }
    
    func startReplicationLogin() {
        let channel = "USER_\((myID["i"])!)"
        let manager = CBLManager.sharedInstance()
        
        let database = try! manager.databaseNamed("currentstudent")
        let pull = database.createPullReplication(ApiService().myURL())
        pull.channels = ["\(channel)"]
        pull.start()
        
        NotificationCenter.default.addObserver(forName: NSNotification.Name.cblReplicationChange, object: pull, queue: nil) {
            (notification) -> Void in
            //determins if replication is over
            let active = pull.status != CBLReplicationStatus.active
            print(" replication ind ", active)
            if active {
                self.getStudentInfo()
            }
        }
    }
    
    func getStudentInfo() {
        let manager = CBLManager.sharedInstance()
        let database = try! manager.databaseNamed("currentstudent")
        let doc = database.document(withID: "\((myID["i"])!)")
        let properties = doc?.properties
        
        if properties != nil {
            self.showStudentInfo()
        } else {
            if self.statusLabel.frame.height == 0 {
                self.statusLabel.text = "Opps.. An error occured. Check your internet connectivity."
                UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                    self.statusLabel.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: 30)
                }, completion: { (Bool) in
                    UIView.animate(withDuration: 0.5, delay: 3, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                        self.statusLabel.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: 0)
                    }, completion: { (Bool) in})
                })
            }
        }
    }
    
    let typeLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.darkGray
        label.numberOfLines = 2
        label.textAlignment = .center
        label.font = label.font.withSize(30)
        label.adjustsFontSizeToFitWidth = true
        return label
    }()
    
    func showQRInfo() {
        let type = "\((myID["t"])!)" == "1" ? "Teacher Account" : "Student Account"
        let name = "\((myID["n"])!)".capitalized
        let exist = (checkSeatPlan()["exist"])! as! Bool
        let condition = gradeSection == "GR:\((self.myID["g"])!)_SEC:\((self.myID["sct"])!)"
        
        thumbnailView.image = UIImage(named: "ic_\((myID["gen"])!)")
        typeLabel.text = type
        
        
        let alertController = UIAlertController(title: "\n \n \n \n \n \n \(name)", message: "\(section)Grade: \((myID["g"])!)-Section: \((myID["sct"])!)", preferredStyle: UIAlertControllerStyle.actionSheet)
        
        alertController.view.addSubview(self.typeLabel)
        alertController.view.addSubview(self.thumbnailView)
        
        typeLabel.frame = CGRect(x: 0, y: 10, width: alertController.view.frame.width, height: 30)
        self.thumbnailView.frame = CGRect(x: (self.view.frame.width/2)-35, y: typeLabel.frame.maxY+20, width: 50, height: 50)
        
        if self.subject_code != "NO_SUBJECT" && self.seatplan == false {
            
            if (currentClass.grade_lvl)! == "\((self.myID["g"])!)" && (currentClass.section)! == "\((self.myID["sct"])!)" {
                // on time
//                    let onTime = UIAlertAction(title: "on time".uppercased(), style: UIAlertActionStyle.default, handler:{ _ in
//                        self.setup()
//                        ApiService().getAttendance(studentId: "\((self.myID["i"])!)", subject_code: self.subject_code, is_on_time: "1", note: "")
//                    })
//                    onTime.setValue(UIColor.Accept(alpha: 1.0), forKey: "titleTextColor")
//                    alertController.addAction(onTime)
                // late
                let late = UIAlertAction(title: "late".capitalized, style: UIAlertActionStyle.default, handler:{ _ in
                    self.setup()
                    ApiService().getAttendance(studentId: "\((self.myID["i"])!)", subject_code: self.subject_code, is_on_time: "2", note: "", grade: "\((self.myID["g"])!)", section: "\((self.myID["sct"])!)")
                })
                late.setValue(UIColor.Warning(alpha: 1.0), forKey: "titleTextColor")
                alertController.addAction(late)
                //
                
                //absent
                let absent = UIAlertAction(title: "absent".capitalized, style: UIAlertActionStyle.default, handler:{ _ in
                    self.setup()
                    ApiService().getAttendance(studentId: "\((self.myID["i"])!)", subject_code: self.subject_code, is_on_time: "3", note: "", grade: "\((self.myID["g"])!)", section: "\((self.myID["sct"])!)")
                })
                absent.setValue(UIColor.Alert(alpha: 1.0), forKey: "titleTextColor")
                alertController.addAction(absent)
                //
                
                // excused
                let excused = UIAlertAction(title: "excused".capitalized, style: UIAlertActionStyle.default, handler:{ _ in
                    
                    let alertExcused = UIAlertController(title: "", message: "Excuse \((self.myID["n"])!)\n \n \n \n ", preferredStyle: UIAlertControllerStyle.alert)
                    
                    alertExcused.view.addSubview(self.contentText)
                    self.contentText.frame = CGRect(x: 10, y: 50, width: 250, height: 50)
                    
                    let confirm = UIAlertAction(title: "confirm".capitalized, style: UIAlertActionStyle.default, handler:{ _ in
                        self.setup()
                        print(" confirm ", (self.contentText.text)!)
                        ApiService().getAttendance(studentId: "\((self.myID["i"])!)", subject_code: self.subject_code, is_on_time: "4", note: "\((self.contentText.text)!)", grade: "\((self.myID["g"])!)", section: "\((self.myID["sct"])!)")
                    })
                    
                    alertExcused.addAction(UIAlertAction(title: NSLocalizedString("Re-scan", comment: "Default action"), style: UIAlertActionStyle.cancel, handler: { _ in
                        self.setup()
                    }))
                    confirm.setValue(UIColor.ThemeOrange(alpha: 1.0), forKey: "titleTextColor")
                    alertExcused.addAction(confirm)
                    
                    let alertWindow = UIWindow(frame: UIScreen.main.bounds)
                    alertWindow.rootViewController = UIViewController()
                    alertWindow.windowLevel = UIWindowLevelAlert + 1;
                    alertWindow.makeKeyAndVisible()
                    alertWindow.rootViewController?.present(alertExcused, animated: true, completion: nil)
                    
                })
                excused.setValue(UIColor.ThemeOrange(alpha: 1.0), forKey: "titleTextColor")
                alertController.addAction(excused)
                //
                
            } else {
                self.errorMessage(message: "\((self.myID["n"])!) is not in your class.", reset: false)
            }
            
        } else if self.subject_code != "NO_SUBJECT" && self.seatplan == true {
            //seatplan
            if condition {
                if exist {
                    let noun = "\((self.myID["gen"])!)" == "female" ? "her" : "his"
                    let changeController = UIAlertController(title: "\((self.myID["n"])!) is already in seat #\((checkSeatPlan()["current_seat"])!)", message: "Do you want to change \(noun) seat to #\((checkSeatPlan()["change_seat"])!)", preferredStyle: UIAlertControllerStyle.alert)
                    
                    
                    let start = UIAlertAction(title: "Change Seat".capitalized, style: UIAlertActionStyle.default, handler:{ _ in
                        self.setup()
                        self.updateSeatPlan(current_seat: "\((self.checkSeatPlan()["current_seat"])!)", change_seat: "\((self.checkSeatPlan()["change_seat"])!)")
                    })
                    start.setValue(UIColor.ThemeDarkGreen(alpha: 1.0), forKey: "titleTextColor")
                    changeController.addAction(start)
                    
                    changeController.addAction(UIAlertAction(title: NSLocalizedString("Re-scan", comment: "Default action"), style: UIAlertActionStyle.cancel, handler: { _ in
                        self.setup()
                    }))
                    
                    let alertWindow = UIWindow(frame: UIScreen.main.bounds)
                    alertWindow.rootViewController = UIViewController()
                    alertWindow.windowLevel = UIWindowLevelAlert + 1;
                    alertWindow.makeKeyAndVisible()
                    alertWindow.rootViewController?.present(changeController, animated: true, completion: nil)
                    
                    
                } else {
                    let seatplan = UIAlertAction(title: "add student".capitalized, style: UIAlertActionStyle.default, handler:{ _ in
                        self.setup()
                        self.addSeatPlan()
                    })
                    seatplan.setValue(UIColor.Accept(alpha: 1.0), forKey: "titleTextColor")
                    alertController.addAction(seatplan)
                    
                }
            } else if !condition && type == "Student" {
                self.errorMessage(message: "\((self.myID["n"])!) is not in your class.", reset: false)
                
//                self.statusLabel.text = "\((self.myID["n"])!) is not in your class."
//                UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
//                    self.statusLabel.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: 30)
//                }, completion: { (Bool) in
//                    UIView.animate(withDuration: 0.5, delay: 5, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
//                        self.statusLabel.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: 0)
//                    }, completion: { (Bool) in
//                        self.statusLabel.text = "Opps.. An error occured. Check your internet connectivity."
//                    })
//                })

            }
        }
        
        if !exist {
            alertController.addAction(UIAlertAction(title: NSLocalizedString("Re-scan", comment: "Default action"), style: UIAlertActionStyle.cancel, handler: { _ in
                self.setup()
            }))
            
            let alertWindow = UIWindow(frame: UIScreen.main.bounds)
            alertWindow.rootViewController = UIViewController()
            alertWindow.windowLevel = UIWindowLevelAlert + 1;
            alertWindow.makeKeyAndVisible()
            alertWindow.rootViewController?.present(alertController, animated: true, completion: nil)
        }
        //check connection for start replication
        self.checkConnection()
    }
    
    
    func showStudentInfo() {
        let manager = CBLManager.sharedInstance()
        let database = try! manager.databaseNamed("currentstudent")
        let doc = database.document(withID: "\((myID["i"])!)")
        let properties = doc?.properties
        
        DispatchQueue.main.async(execute: {
//            print(" showStudentInfo ", (properties?["b64"] != nil || "\((properties?["b64"])!)" != "") )
            if "\((properties?["b64"])!)" != "" {
                self.thumbnailView.image = UIView.convertBase64ToImage(imageString: "\((properties!["b64"])!)" )
            }
            
            if "\((self.myID["t"])!)" == "2" {
                self.section = "SN: \((properties!["student_id"])!) \n"
            } else {
                self.section = "SN: \((properties!["school_id"])!) \n"
            }
        })
    }
    
    func found(code: String) {
        print("Information found : \n \(code)")
        
//        print("md5Hex: \(md5Hex)")
//        let md5Base64 = md5Data.base64EncodedString()
//        print("md5Base64: \(md5Base64)")
        
        mainView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(reset)))
        
        //generate
        let data = code.data(using: .utf8)!
        
        do {
            let jsonArray = try JSONSerialization.jsonObject(with: data, options : .allowFragments) as? [String:Any]
            let md5Date = MD5(string:"\(FileService().getDateTime(format: "MM/dd/yyyy"))")
            let md5Hex =  md5Date.map { String(format: "%02hhx", $0) }.joined()
            
            print(" md5Hex ", md5Hex)
            
            if jsonArray?["d"] != nil {
                
                if md5Hex == "\((jsonArray!["d"])!)" && "\((ApiService().myQR()["t"])!)" == "2" {
                    //send attendance
                    ApiService().getAttendance(studentId: "\((ApiService().myID()["qq_id"])!)", subject_code: "parent", is_on_time: "1", note: "", grade: "\((ApiService().myID()["grade"])!)", section: "\((ApiService().myID()["section"])!)")
                    
                    let alertController = UIAlertController(title: "My Attendance", message: "Present for \(FileService().getDateTime(format: "MMMM dd,yyyy"))", preferredStyle: UIAlertControllerStyle.actionSheet)
                    alertController.addAction(UIAlertAction(title: NSLocalizedString("Re-scan", comment: "Default action"), style: UIAlertActionStyle.cancel, handler: { _ in
                        self.setup()
                    }))
                    
                    let alertWindow = UIWindow(frame: UIScreen.main.bounds)
                    alertWindow.rootViewController = UIViewController()
                    alertWindow.windowLevel = UIWindowLevelAlert + 1;
                    alertWindow.makeKeyAndVisible()
                    alertWindow.rootViewController?.present(alertController, animated: true, completion: nil)
                    
                } else if jsonArray?["i"] != nil && jsonArray?["p"] != nil && jsonArray?["d"] != nil && jsonArray?["t"] != nil && jsonArray?["s"] != nil && jsonArray?["g"] != nil && jsonArray?["gen"] != nil && jsonArray?["sct"] != nil {
                    myID["p"] = (jsonArray!["p"])!
                    myID["d"] = (jsonArray!["d"])!
                    myID["i"] = (jsonArray!["i"])!
                    myID["t"] = (jsonArray!["t"])!
                    myID["s"] = (jsonArray!["s"])!
                    myID["gen"] = String(("\((jsonArray!["gen"])!)".fromBase64())!)
                    myID["n"] = String(("\((jsonArray!["n"])!)".fromBase64())!)
                    myID["g"] = "\((jsonArray!["g"])!)" != "0" ? String(("\((jsonArray!["g"])!)".fromBase64())!) : "0"
                    myID["sct"] = "\((jsonArray!["sct"])!)" != "0" ? String(("\((jsonArray!["sct"])!)".fromBase64())!) : "0"
                    //                print("Information found : \n \(myID)")
                    self.deleteCurrent()
                } else  {
                    self.errorMessage(message: "Opps.. Invalid QR scanned.", reset: true)
                }
            } else {
                self.setup()
            }
            
        } catch {
            print("Information found : ", error)
        }
        
    }
    
    private func errorMessage(message: String, reset: Bool) {
        self.statusLabel.text = message
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.statusLabel.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: 0)
        }, completion: { (Bool) in
            UIView.animate(withDuration: 0.5, delay: 1, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                self.statusLabel.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: 30)
            }, completion: { (Bool) in
                UIView.animate(withDuration: 0.5, delay: 5, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                    self.statusLabel.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: 0)
                }, completion: { (Bool) in
                    self.statusLabel.text = "Opps.. An error occured. Check your internet connectivity."
                    if reset { self.setup() }
                })
            })
        })
    }
    
    private func checkSeatPlan() -> [String:Any]{
        let manager = CBLManager.sharedInstance()
        let database = try! manager.databaseNamed("seatplan")
        let document = database.document(withID: documentID)!//CLASS_SEAT_ENG101_GR:5_SEC:A
        var properties = document.properties
//        var exist = false
        
        var exist = [
            "exist" : false,
            "current_seat" : "none",
            "change_seat" : "\(seatnumber)",
        ] as [String:Any]
        
        if properties != nil {
            let total = Int("\((properties!["seat_column"])!)")! * Int("\((properties!["seat_row"])!)")!
            
            for count in 0...(total-1) {
                //check
                
                if (properties!["\(count)"]) != nil {
                    let seat = ApiService().stringToJsonObject(string: "\((properties!["\(count)"])!)")
                    if "\((seat["qq_id"])!)" == "\((myID["i"])!)" {
                        exist = [
                            "exist" : true,
                            "current_seat" : "\(count+1)",
                            "change_seat" : "\(seatnumber)",
                        ]
//                        exist = true
                    }
                }
            }
            
        }
//        print(" seat already assigned ", object)
        return exist
    }
    
    private func addSeatPlan() {
        ApiService().addSeatPlan(subject_code: subject_code, seatnumber: seatnumber, studentID: myID, alias: FileService().alias(name: "\((myID["n"])!)"), documentID: documentID)
    }
    
    private func updateSeatPlan(current_seat: String, change_seat: String) {
        ApiService().updateSeatPlan(studentID : myID, current_seat: current_seat, change_seat: change_seat, documentID : documentID, alias : FileService().alias(name: "\((myID["n"])!)"))
//        let manager = CBLManager.sharedInstance()
//        let database = try! manager.databaseNamed("seatplan")
//        let document = database.document(withID: documentID)!//CLASS_SEAT_ENG101_GR:5_SEC:A
//        var properties = document.properties
//
//        if properties != nil {
//            properties!["\(current_seat)"] = "null"
//            properties!["\(change_seat)"] = "null"
//            let total = Int("\((properties!["seat_column"])!)")! * Int("\((properties!["seat_row"])!)")!
//            var exist = false
//
//            for count in 0...(total-1) {
//                //check
//
//                if "\((properties!["\(count)"])!)" != "null" {
//                    let seat = ApiService().stringToJsonObject(string: "\((properties!["\(count)"])!)")
//                    if "\((seat["qq_id"])!)" == "\((myID["i"])!)" {
//                        print(" seat already assigned ", count+1, seatnumber)
//                        exist = true
//                    }
//                }
//            }
    
//        }
//    else {
//            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
//                self.statusLabel.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: 30)
//            }, completion: { (Bool) in
//                UIView.animate(withDuration: 0.5, delay: 3, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
//                    self.statusLabel.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: 0)
//                }, completion: { (Bool) in
//                    self.setup()
//                })
//            })
//        }
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        contentText.resignFirstResponder()
        return false
    }

    func MD5(string: String) -> Data {
        let messageData = string.data(using:.utf8)!
        var digestData = Data(count: Int(CC_MD5_DIGEST_LENGTH))
        
        _ = digestData.withUnsafeMutableBytes {digestBytes in
            messageData.withUnsafeBytes {messageBytes in
                CC_MD5(messageBytes, CC_LONG(messageData.count), digestBytes)
            }
        }
        
        return digestData
    }
    
    func checkConnection(){
        //check
        var request = URLRequest(url: ApiService().myURL())
        request.timeoutInterval = 5
        let session = URLSession.shared
        session.dataTask(with: request) { (data, response, error) in
            if let response = response {
                print(" replicate repsone ",response)
                DispatchQueue.main.async(execute: {
                    self.startReplicationLogin()
                })
            }
            if let error = error {
                print(" error catch replicate ", error)
                DispatchQueue.main.async(execute: {
                    if self.statusLabel.frame.height == 0 {
                        self.errorMessage(message: "Opps.. An error occured. Check your internet connectivity.", reset: false)
                    }
                })
            }
        }.resume()
    }
    
    
    override var prefersStatusBarHidden: Bool {
        return true
    }
    
    override var supportedInterfaceOrientations: UIInterfaceOrientationMask {
        return .portrait
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.clear
        
        subject_code = self.navigationItem.title!
        
        contentText.delegate = self
        
        let seatnumberLabel =  self.navigationItem.titleView as! UILabel
        seatnumber = String((seatnumberLabel.text)!.dropFirst(14))
        
        self.setup()
        
        print(" seatnumberLabel ", subject_code, ApiService().currentClass())
        // scan seating plan
        if (seatnumberLabel.text)!.dropLast(3+seatnumber.count) == "add student".uppercased() {
            let space : Character = " "
            let idx = self.navigationItem.title!.characters.index(of: space)
            let pos = self.navigationItem.title!.characters.distance(from: self.navigationItem.title!.startIndex, to: idx!)
            let count = self.navigationItem.title!.count - Int(pos)
            subject_code = "\(self.navigationItem.title!.dropLast(count))"
            documentID = "\(self.navigationItem.title!.dropFirst(subject_code.count+1))"
            gradeSection = "\(documentID.dropFirst(12+subject_code.count))"
//            print(" select seat ", gradeSection)
            
            seatplan = true
            self.setupForOrientation()
        }
        
    }
}

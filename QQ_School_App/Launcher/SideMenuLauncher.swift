//
//  SideMenuLauncher.swift
//  QQ_School_App
//
//  Created by Abby Esteves on 01/03/2018.
//  Copyright © 2018 Abby Esteves. All rights reserved.
//

import UIKit

class SideMenuLauncher: NSObject, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    
    let blackView = UIView()
    //    let blurView = UIBlurEffect(style: UIBlurEffectStyle.dark)
    let blurEffectView = UIVisualEffectView(effect: (UIBlurEffect(style: UIBlurEffectStyle.dark)))
    
    lazy var menuView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.backgroundColor = UIColor.white
        cv.dataSource = self
        cv.delegate = self
        return cv
    }()
    
    let menuCellId = "menuCellId"
    lazy var menuLabels: [Menu] = {
        var menu1 = Menu()
        menu1.icon = "sagrada"
        menu1.label = "header"
        
        var menu2 = Menu()
        menu2.icon = "ic_cap"
        menu2.label = "My School"
        
//        var menu3 = Menu()
//        menu3.icon = "ic_announcement"
//        menu3.label = "Announcements"
        
        var menu4 = Menu()
        menu4.icon = "ic_profile"
        menu4.label = "My ID"
        
        var menu5 = Menu()
        menu5.icon = "ic_class"
        menu5.label = "My Subjects"
        
//        var menu6 = Menu()
//        menu6.icon = "ic_calendar"
//        menu6.label = "School Calendar"
        
//        var menu11 = Menu()
//        menu11.icon = "ic_friends"
//        menu11.label = "My Friends"
        
        var menu7 = Menu()
        menu7.icon = "ic_school"
        menu7.label = "About Us"
        
        var menu8 = Menu()
        menu8.icon = "ic_notes"
        menu8.label = "My Notes"
//
//        var menu9 = Menu()
//        menu9.icon = "ic_task"
//        menu9.label = "My Tasks"
        
//        var menu10 = Menu()
//        menu10.icon = "ic_badge"
//        menu10.label = "My Badges"
        
        var menu12 = Menu()
        menu12.icon = ""
        menu12.label = ""

//        var menu13 = Menu()
//        menu13.icon = ""
//        menu13.label = "Options"
        
        var menu14 = Menu()
        menu14.icon = "ic_setting"
        menu14.label = "Logout"
        print(" GET USER TYPE ", "\((ApiService().myID()["qq_id"])!)")
        if "\((ApiService().myID()["user_type"])!)" == "student" {
            return [menu1, menu2, menu4, menu5, menu7, menu8, menu12, menu14]
        }
        return [menu1, menu2, menu4, menu5, menu7, menu12, menu14]
    }()

    var navigation: HomeTabBarController = {
        let launcher = HomeTabBarController()
        return launcher
    }()
    
    
    @objc func showMenu(){
        //show menu
        //show dim bg when options are clicked
        if let window = UIApplication.shared.keyWindow {
            blackView.backgroundColor = UIColor(white: 0, alpha: 0.6)
            blackView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(bgDismiss)))
            window.addSubview(blackView)
            //add view for the options
            window.addSubview(menuView)
            let width: CGFloat = 260
            let x = -width
            menuView.frame = CGRect(x: x, y: 0, width: width, height: window.frame.height)
            
            blackView.frame = window.frame
            blackView.alpha = 0
            
            //animation
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                
                self.blackView.alpha = 1
                self.menuView.frame = CGRect(x: 0, y: 0, width: self.menuView.frame.width, height: self.menuView.frame.height)
                
            }, completion:  nil)
        }
    }
    // function to dismiss menu and option view
    // also check if which controller to load
    @objc func bgDismiss(){
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            
            self.blackView.alpha = 0
            if let window = UIApplication.shared.keyWindow {
                self.menuView.frame = CGRect(x: -window.frame.width, y: 0, width: self.menuView.frame.width, height: self.menuView.frame.height)
            }
            
        }, completion: { (Bool) in
            
            
        })
    }
    
    @objc func menuDismiss(menu: Menu){
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            
            self.blackView.alpha = 0
            if let window = UIApplication.shared.keyWindow {
                self.menuView.frame = CGRect(x: -window.frame.width, y: 0, width: self.menuView.frame.width, height: self.menuView.frame.height)
            }
            
        }, completion: { (Bool) in

//            if "\((ApiService().myID()["user_type"])!)" == "student" {
//                self.homeController?.showController(menu: menu)
//            } else {
//               self.teacherHomeController?.showController(menu: menu)
//            }
            
            //            self.newsController?.showNews()
            
            self.navigation.showController(menu: menu)

        })
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return menuLabels.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: menuCellId, for: indexPath) as! SideMenuCell
        let menuLabel = menuLabels[indexPath.item]
        cell.menu = menuLabel
        cell.backgroundColor = UIColor.white
        if(indexPath.item == 0){
            cell.backgroundColor = UIColor.lightGray
        } else {
            if "\((ApiService().myID()["user_type"])!)" == "student" {
                if(indexPath.item == 6){
                    cell.backgroundColor = UIColor.rgba(red: 232, green: 232, blue: 232, alpha: 1.0)
                }
            } else if "\((ApiService().myID()["user_type"])!)" == "teacher" {
                if(indexPath.item == 5 ){
                    cell.backgroundColor = UIColor.rgba(red: 232, green: 232, blue: 232, alpha: 1.0)
                }
            }
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if(indexPath.item == 0){
            return CGSize(width: menuView.frame.width, height: 130)
        } else {
            if "\((ApiService().myID()["user_type"])!)" == "student" {
                if(indexPath.item == 6){
                    return CGSize(width: menuView.frame.width, height: 1.5)
                }
            } else if "\((ApiService().myID()["user_type"])!)" == "teacher" {
                if(indexPath.item == 5 ){
                    return CGSize(width: menuView.frame.width, height: 1.5)
                }
            }
        }
//        else if(indexPath.item == 7){
//            return CGSize(width: menuView.frame.width, height: 1.5)
//        }
        
        return CGSize(width: menuView.frame.width, height: 50)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let menu = menuLabels[indexPath.item]
        if((menu.label == "Options") || (menu.label == "")){
            //            bgDismiss()
        } else {
            menuDismiss(menu: menu)
        }
        
    }
    
    
    override init() {
        super.init()
        
        menuView.register(SideMenuCell.self, forCellWithReuseIdentifier: menuCellId)
    }
        
}

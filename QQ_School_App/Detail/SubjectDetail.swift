//
//  SubjectDetail.swift
//  QQ_School_App
//
//  Created by Abby Esteves on 23/03/2018.
//  Copyright © 2018 Abby Esteves. All rights reserved.
//

import UIKit
class SubjectDetail: UIViewController,  UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UIImagePickerControllerDelegate, UINavigationControllerDelegate, UIDocumentPickerDelegate, UITextFieldDelegate {
    
    let manager = CBLManager.sharedInstance()
    let libraryController = UIImagePickerController()
    let cameraPickerController = UIImagePickerController()
    let documentController = UIDocumentPickerViewController(documentTypes: [], in: .import)
    var moduleId = "moduleId"
    var examId = "examId"
    var options = ["modules", "exams"]
    var currentTabSelected = "MODULES"
    var modules = [Modules]()
    var exams = [Exams]()
    var subject_code = String()
    var documentID = String()
    let thumbnailView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFit
        imageView.clipsToBounds = true
        imageView.backgroundColor = .clear
        imageView.layer.masksToBounds = true
        imageView.image = UIImage(named: "base64")
        return imageView
    }()
    let titleText: UITextField = {
        let text = UITextField()
        text.layer.borderWidth = 1.0
        text.layer.borderColor = UIColor.lightGray.cgColor
        text.backgroundColor = .clear//UIColor.rgba(red: 255, green: 255, blue: 255, alpha: 0.5)
        text.textColor = UIColor.darkGray
        var placeholder = NSMutableAttributedString()
        placeholder = NSMutableAttributedString(attributedString: NSAttributedString(string: "Title", attributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 11.0), .foregroundColor: UIColor.lightGray]))
        text.setLeftPadding(space: 8)
        text.font = text.font?.withSize(11)
        text.attributedPlaceholder = placeholder
        text.layer.cornerRadius = 5
        return text
    }()
    
    let progressBar: UIProgressView = {
        let view = UIProgressView()
        view.setProgress(0, animated: true)
        view.tintColor = UIColor.ThemeDarkGreen(alpha: 1.0)
        view.trackTintColor = UIColor.lightGray
        return view
    }()
    
    let blackView = UIView()
    
    let fileView : UIView = {
        let view = UIView()
        view.backgroundColor = .white
        view.layer.cornerRadius = 5
        view.layer.shadowColor = UIColor.gray.cgColor
        view.layer.shadowOpacity = 0.5
        view.layer.shadowOffset = CGSize(width: 0, height: 1)
        view.layer.shadowRadius = 1
        return view
    }()
    
    let floatMenu: UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.ThemeDarkGreen(alpha: 1.0)
        view.layer.shadowColor = UIColor.black.cgColor
        view.layer.shadowOpacity = 0.5
        view.layer.shadowOffset = CGSize(width: 3, height: 3)
        view.layer.shadowRadius = 5
        view.layer.cornerRadius = 30
        return view
    }()
    
    let floatImage: UIImageView = {
        let image = UIImageView()
        image.safeAreaInsetsDidChange()
        image.image = UIImage(named: "ic_seatplan")?.withRenderingMode(.alwaysTemplate)as UIImage?
        image.tintColor = UIColor.white
        image.layer.cornerRadius = 20
        return image
    }()
    
//    var modules: [Modules] = {
//        var data1 = Modules()
//        data1.name = "Math.png"
//        data1.date = "1 hour ago"
//        data1.type = "jpeg"
//
//        var data2 = Modules()
//        data2.name = "English"
//        data2.date = "3 hour ago"
//        data2.type = "png"
//
//        var data3 = Modules()
//        data3.name = "History"
//        data3.date = "2 mins ago"
//        data3.type = "ppt"
//
//        var data4 = Modules()
//        data4.name = "Science"
//        data4.date = "1 day ago"
//        data4.type = "doc"
//
//        return [data1, data2, data3, data4]
//    }()
    
    let optionView : UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.Theme(alpha: 1.0)
        return view
    }()
    
    let ModuleView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.backgroundColor = UIColor.white
        return cv
    }()
    
    let ExamView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.backgroundColor = UIColor.white
        return cv
    }()
    
    @objc func goToSeatingPlan() {
        let manager = CBLManager.sharedInstance()
        let database = try! manager.databaseNamed("seatplan")
        let document = database.document(withID: documentID)!
        let properties = document.properties
        
//        print(" goToSeatingPlan ", documentID)
        
        if properties != nil {
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            }, completion: { (Bool) in
//                print(" goToSeatingPlan ", properties!)
                DispatchQueue.main.async(execute: {
                    let titleLabel : UILabel = {
                        let label = UILabel()
                        label.textColor = UIColor.white
                        label.font = UIFont(name: "GillSans-Bold", size: UIFont.systemFontSize)!
                        label.font = label.font.withSize(15)
                        label.text = "\(self.subject_code) classroom view".uppercased()
                        return label
                    }()
                    
                    let seatPlanDetail = SeatPlanDetail()
                    self.navigationController?.pushViewController(seatPlanDetail, animated: true)
                    seatPlanDetail.navigationItem.title = "\(self.subject_code) \(document.documentID)"
                    seatPlanDetail.navigationItem.titleView = titleLabel
                })
            })
        } else {
            view.showToast(message: "Class seating for \(self.subject_code) must be configured first!")
        }
    }
    
    @objc func bgDismiss() {
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.blackView.alpha = 1
            self.fileView.frame = CGRect(x: (self.view.frame.width/2), y: (self.view.frame.height/2), width: 0, height: 0)
            self.titleText.frame = CGRect(x: (self.view.frame.width/2), y: 0, width: 0, height: 0)
            self.thumbnailView.frame = CGRect(x: (self.view.frame.width/2), y: 0, width: 0, height: 0)
        }, completion: { (Bool) in })
    }
    
    @objc func goToExamDetail(documentID: String) {
        let titleLabel : UILabel = {
            let label = UILabel()
            label.textColor = UIColor.white
            label.font = UIFont(name: "GillSans-Bold", size: UIFont.systemFontSize)!
            label.font = label.font.withSize(15)
            label.text = "exam for \(subject_code)".uppercased()
            return label
        }()
        
        let examDetail = ExamDetail()
        navigationController?.pushViewController(examDetail, animated: true)
        
        examDetail.navigationItem.title = "\(subject_code) \(documentID)"
        examDetail.navigationItem.titleView = titleLabel
    }
    
    @objc func goToEditExamDetail(documentID: String) {
        let titleLabel : UILabel = {
            let label = UILabel()
            label.textColor = UIColor.white
            label.font = UIFont(name: "GillSans-Bold", size: UIFont.systemFontSize)!
            label.font = label.font.withSize(15)
            label.text = "exam".uppercased()
            return label
        }()
        
        let examDetail = ExamDetail()
        navigationController?.pushViewController(examDetail, animated: true)
        
        examDetail.navigationItem.title = "\(documentID)"
        examDetail.navigationItem.titleView = titleLabel
    }
    
    @objc func uploadExam() {
        let titleLabel : UILabel = {
            let label = UILabel()
            label.textColor = UIColor.white
            label.font = UIFont(name: "GillSans-Bold", size: UIFont.systemFontSize)!
            label.font = label.font.withSize(15)
            label.text = "create exam".uppercased()
            return label
        }()
        
        let createExamDetail = CreateExamDetail()
        navigationController?.pushViewController(createExamDetail, animated: true)
        
        createExamDetail.navigationItem.title = "\(self.navigationItem.title!)".uppercased()
        createExamDetail.navigationItem.titleView = titleLabel
    }
    
    @objc func upload(){
        let alertController = UIAlertController(title: "Add a File", message: "Choose from where to add...", preferredStyle: UIAlertControllerStyle.actionSheet)
        alertController.view.frame = CGRect(x: (view.frame.width-(view.frame.width-20))/2, y: (view.frame.height-270)/2, width: view.frame.width-20, height: 270)

        alertController.addAction(UIAlertAction(title: NSLocalizedString("Take a Photo", comment: ""), style: UIAlertActionStyle.default, handler: { _ in

            self.cameraPickerController.delegate = self
            self.cameraPickerController.sourceType = .camera

            let alertWindow = UIWindow(frame: UIScreen.main.bounds)
            alertWindow.rootViewController = UIViewController()
            alertWindow.windowLevel = UIWindowLevelAlert + 1;
            alertWindow.makeKeyAndVisible()
            alertWindow.rootViewController?.present(self.cameraPickerController, animated: true, completion: nil)
        }))

        alertController.addAction(UIAlertAction(title: NSLocalizedString("Insert from library", comment: ""), style: UIAlertActionStyle.default, handler: { _ in
            self.libraryController.delegate = self
            self.libraryController.sourceType = .photoLibrary
            self.libraryController.mediaTypes = UIImagePickerController.availableMediaTypes(for: .photoLibrary)!
            self.present(self.libraryController, animated: true, completion: nil)
        }))

        alertController.addAction(UIAlertAction(title: NSLocalizedString("Add Attachment", comment: ""), style: UIAlertActionStyle.default, handler: { _ in
            self.documentController.delegate = self
            self.present(self.documentController, animated: true, completion: nil)
        }))

        alertController.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: UIAlertActionStyle.cancel, handler: { _ in

        }))

        self.present(alertController, animated: true, completion: nil)
    }
    
    @objc func selectorTapped(sender: UITapGestureRecognizer){
        resetColor()
        let label = sender.view?.subviews[0] as! UILabel
        sender.view?.layer.cornerRadius = 5
        sender.view?.backgroundColor = UIColor.Background(alpha: 1.0)
        label.textColor = UIColor.ThemeDarkBlue(alpha: 1.0)
        currentTabSelected = (label.text)!
        
        if currentTabSelected == "EXAMS" {
            getExams()
            
            if "\((ApiService().myID()["user_type"])!)" == "teacher" {
                let uploadImage = UIImage(named: "ic_save_exam")?.withRenderingMode(.alwaysOriginal)
                let uploadItem = UIBarButtonItem(image: uploadImage, style: .plain, target: self, action: #selector(uploadExam))
                navigationItem.rightBarButtonItems = [uploadItem]
            }
            
        } else {
            getModules()
            
            if "\((ApiService().myID()["user_type"])!)" == "teacher" {
                let uploadImage = UIImage(named: "ic_upload")?.withRenderingMode(.alwaysOriginal)
                let uploadItem = UIBarButtonItem(image: uploadImage, style: .plain, target: self, action: #selector(upload))
                navigationItem.rightBarButtonItems = [uploadItem]
            }
        }
    }
    
    func documentMenu(_ documentMenu: UIDocumentPickerViewController, didPickDocumentPicker documentPicker: UIDocumentPickerViewController) {
        print(" didPickDocumentPicker ", documentPicker)
    }
    
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String : Any]) {
        print(" didFinishPickingMediaWithInfo ", info[UIImagePickerControllerReferenceURL], info)
        var fileExtension = "jpg"
        let image = info[UIImagePickerControllerOriginalImage] as? UIImage
        self.titleText.text = ""
        self.thumbnailView.image = image
        
        
        
        // make sure to update file extension
        if info[UIImagePickerControllerReferenceURL] != nil {
            let kindOfFile = "\((info[UIImagePickerControllerReferenceURL])!)"
            fileExtension = kindOfFile.suffix(3).lowercased()
        } else {
//            self.thumbnailView.image = view.resizeImage(image: image!, newWidth: 600)
        }
        
        UIView.animate(withDuration: 0.1, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.libraryController.dismiss(animated: true, completion: nil)
            self.cameraPickerController.dismiss(animated: true, completion: nil)
            self.documentController.dismiss(animated: true, completion: nil)
        }, completion: { (Bool) in
//            self.open()
        })
        
        let alertController = UIAlertController(title: "Picture Selected", message: "\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n\n", preferredStyle: UIAlertControllerStyle.alert)
        alertController.view.frame = CGRect(x: (view.frame.width-(view.frame.width-20))/2, y: 50, width: view.frame.width-20, height: 270)
        alertController.view.addSubview(thumbnailView)
        alertController.view.addSubview(titleText)


        self.titleText.frame =  CGRect(x: 15, y: 60, width: 240, height: 40)
        self.thumbnailView.frame = CGRect(x: 15, y: titleText.frame.maxY+10, width: 240, height: 240)

        alertController.addAction(UIAlertAction(title: NSLocalizedString("Upload", comment: "Default action"), style: UIAlertActionStyle.default, handler: { _ in
            let title = self.titleText.text?.capitalized
            if self.titleText.text == "" {
                self.titleText.becomeFirstResponder()
            } else {
                UploadService().postModule(imageName: title!.replacingOccurrences(of: " ", with: "_"), image : image!, fileExtension: fileExtension, subj_code: self.subject_code)
            }
        }))

        alertController.addAction(UIAlertAction(title: NSLocalizedString("Cancel", comment: ""), style: UIAlertActionStyle.cancel, handler: { _ in

        }))
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    private func open() {
        if let window = UIApplication.shared.keyWindow {
            titleText.text = ""
            blackView.backgroundColor = UIColor(white: 0, alpha: 0.6)
            blackView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(bgDismiss)))
            
            window.addSubview(blackView)
            window.addSubview(fileView)
            fileView.addSubview(titleText)
            fileView.addSubview(thumbnailView)
            
            self.blackView.alpha = 0
            
            blackView.frame = CGRect(x: 0, y: 0, width: window.frame.width, height: window.frame.height)
            fileView.frame = CGRect(x: (window.frame.width/2), y: (window.frame.height/2), width: 0, height: 0)
            titleText.frame = CGRect(x: (window.frame.width/2), y: 0, width: 0, height: 0)
            thumbnailView.frame = CGRect(x: (window.frame.width/2), y: 0, width: 0, height: 0)
            
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                self.blackView.alpha = 1
                self.fileView.frame = CGRect(x: (self.view.frame.width/2)-((self.view.frame.width-50)/2), y: (self.view.frame.height/2)-((self.view.frame.height/3)/2), width: self.view.frame.width-50, height: self.view.frame.height/3+30)
                self.titleText.frame = CGRect(x: 10, y: 10, width: self.fileView.frame.width-20, height: 40)
                self.thumbnailView.frame = CGRect(x: 10, y: self.titleText.frame.maxY+10, width: self.fileView.frame.width-20, height: (self.view.frame.height/3)-20-10)
            }, completion: { (Bool) in
                
            })
            
        }
        
    }
    
    private func resetColor() {
        for view in optionView.subviews{
//            print(" selector : ",view)
            let label = view.subviews[0] as! UILabel
            view.layer.cornerRadius = 0
            view.backgroundColor = UIColor.Theme(alpha: 1.0)
            label.textColor = .white
        }
    }
    
    private func selectedIndex() {
        let selected = 0
        let view = optionView.subviews[selected]
        let label = view.subviews[0] as! UILabel
        view.backgroundColor = UIColor.Background(alpha: 1.0)
        view.layer.cornerRadius = 5
        label.textColor = UIColor.ThemeDarkBlue(alpha: 1.0)
    }
    
    
    private func setupView() {
        ModuleView.delegate = self
        ModuleView.dataSource = self
        ModuleView.scrollsToTop = true
        ModuleView.contentInset = UIEdgeInsetsMake(10, 0, 80, 0)
        ModuleView.backgroundColor = UIColor.Background(alpha: 1.0)
        ModuleView.register(ModulesCell.self, forCellWithReuseIdentifier: moduleId)
        
        ExamView.delegate = self
        ExamView.dataSource = self
        ExamView.scrollsToTop = true
        ExamView.contentInset = UIEdgeInsetsMake(10, 0, 80, 0)
        ExamView.backgroundColor = UIColor.Background(alpha: 1.0)
        ExamView.register(CreateExamCell.self, forCellWithReuseIdentifier: examId)
        
        self.titleText.delegate = self
        
        view.addSubview(optionView)
        view.addSubview(ModuleView)
        
//        if "\((ApiService().myID()["user_type"])!)" == "teacher" {
//            ModuleView.addSubview(floatMenu)
//            floatMenu.addSubview(floatImage)
//
//            floatMenu.frame = CGRect(x: view.frame.width-80, y: 0, width: 60, height: 60)
//            floatImage.frame = CGRect(x: 15, y: 15, width: 30, height: 30)
//        }
        
        
        optionView.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: 40)
        ModuleView.frame = CGRect(x: 0, y: optionView.frame.maxY+5, width: view.frame.width, height: view.frame.height-optionView.frame.height)
        
        floatMenu.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(goToSeatingPlan)))
        
    }
    
    private func setupMenu() {
        let width = view.frame.width/2
        var x = CGFloat(0)
        for option in options {
            
            let buttonView : UIView = {
                let view = UIView()
                view.backgroundColor = UIColor.Theme(alpha: 1.0)
                return view
            }()
            
            let optionLabel : UILabel = {
                let label = UILabel()
                label.text = "\(option)".uppercased()
                label.textAlignment = .center
                label.textColor = .white
                label.font = UIFont.boldSystemFont(ofSize: label.font.pointSize)
                label.font = label.font.withSize(12)
                return label
            }()
            
            optionView.addSubview(buttonView)
            buttonView.addSubview(optionLabel)
            
            buttonView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.selectorTapped(sender:))))
            
            buttonView.frame = CGRect(x: x, y: 5, width: width, height: optionView.frame.height)
            optionLabel.frame = CGRect(x: 0, y: 0, width: buttonView.frame.width, height: buttonView.frame.height)
            x = CGFloat(x + width)
        }
    }
    
    private func watchModules(){
        //modules
        let modules = try! manager.databaseNamed("modules")
        self.getModules()
        NotificationCenter.default.addObserver(forName: NSNotification.Name.cblDatabaseChange, object: modules, queue: nil) {
            (notification) -> Void in
            if let changes = notification.userInfo!["changes"] as? [CBLDatabaseChange] {
                for change in changes {
                    print("module, revision ID '%@'", change.description)
                    self.getModules()
                }
            }
        }
        
        // exam
        let exam = try! manager.databaseNamed("exams")
        NotificationCenter.default.addObserver(forName: NSNotification.Name.cblDatabaseChange, object: exam, queue: nil) {
            (notification) -> Void in
            if let changes = notification.userInfo!["changes"] as? [CBLDatabaseChange] {
                for change in changes {
                    print("exam, revision ID '%@'", change.description)
                    self.getExams()
                }
            }
        }
    }
    
    private func getExams() {
        modules.removeAll();
        ModuleView.removeFromSuperview()

        view.addSubview(ExamView)
        
        if "\((ApiService().myID()["user_type"])!)" == "teacher" {
            ExamView.addSubview(floatMenu)
            floatMenu.addSubview(floatImage)
            
            floatMenu.frame = CGRect(x: view.frame.width-80, y: view.frame.height-optionView.frame.height-100, width: 60, height: 60)
            floatImage.frame = CGRect(x: 15, y: 15, width: 30, height: 30)
        }
        ExamView.frame = CGRect(x: 0, y: optionView.frame.maxY+5, width: view.frame.width, height: view.frame.height-optionView.frame.height)
        
        if "\((ApiService().myID()["user_type"])!)" == "teacher" {
            exams = ApiService().examParentList().filter({$0.subj_code == subject_code})
        } else {
            exams = ApiService().examParentList().filter({$0.subj_code == subject_code && $0.exam_name != "draft"})
        }
        DispatchQueue.main.async(execute: {
            self.ExamView.reloadData()
        })
    }

    private func getModules(){
        exams.removeAll()
        ExamView.removeFromSuperview()

        view.addSubview(ModuleView)
        if "\((ApiService().myID()["user_type"])!)" == "teacher" {
            ModuleView.addSubview(floatMenu)
            floatMenu.addSubview(floatImage)
            
            floatMenu.frame = CGRect(x: view.frame.width-80, y: view.frame.height-optionView.frame.height-100, width: 60, height: 60)
            floatImage.frame = CGRect(x: 15, y: 15, width: 30, height: 30)
        }
        
        ModuleView.frame = CGRect(x: 0, y: optionView.frame.maxY+5, width: view.frame.width, height: view.frame.height-optionView.frame.height)

        modules = ApiService().moduleList().filter({$0.subj_code == subject_code})
        DispatchQueue.main.async(execute: {
            self.ModuleView.reloadData()
        })
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if collectionView == ModuleView {
            return modules.count
        }
        return exams.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        
        if collectionView == ModuleView {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: moduleId, for: indexPath) as! ModulesCell
            cell.backgroundColor = UIColor.white
            cell.layer.shadowColor = UIColor.gray.cgColor
            cell.layer.shadowOpacity = 0.5
            cell.layer.shadowOffset = CGSize(width: 0, height: 1)
            cell.layer.shadowRadius = 1
            cell.layer.cornerRadius = 10
            cell.module = modules[indexPath.item]
            return cell
        }
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: examId, for: indexPath) as! CreateExamCell
        cell.backgroundColor = UIColor.white
        cell.layer.shadowColor = UIColor.gray.cgColor
        cell.layer.shadowOpacity = 0.5
        cell.layer.shadowOffset = CGSize(width: 0, height: 1)
        cell.layer.shadowRadius = 1
        cell.layer.cornerRadius = 10
        cell.exam = exams[indexPath.item]
//        print(" exam list ", cell.nameLabel.text!)
        if cell.nameLabel.text! == "Draft" {
            cell.nameLabel.textColor = UIColor.AlertDark(alpha: 1.0)
        } else {
            cell.nameLabel.textColor = UIColor.darkGray
        }
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        if collectionView == ModuleView {
            return CGSize(width: view.frame.width-20, height: 70)
        }
//        else {
//            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: examId, for: indexPath) as! CreateExamCell
//            if cell.nameLabel.text! == "Draft" {
//                return CGSize(width: view.frame.width-20, height:70)
//            }
//        }
        
        return CGSize(width: view.frame.width-20, height:90)
    }
    
//    trailingSwipeActionsConfigurationForRowAt
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        titleText.resignFirstResponder()
        return false
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        if collectionView == ModuleView {
            print(" ModuleView ", modules[indexPath.item])
        }
        
        if "\((ApiService().myID()["user_type"])!)" == "student" {
            let alertController = UIAlertController(title: "\((exams[indexPath.item].exam_name)!)", message: "\((exams[indexPath.item].exam_desc)!) \n \n Start exam for: \n \((exams[indexPath.item].allotted_time)!)", preferredStyle: UIAlertControllerStyle.alert)
            
            
            let start = UIAlertAction(title: "Start Exam".uppercased(), style: UIAlertActionStyle.default, handler:{ _ in
                self.goToExamDetail(documentID : (self.exams[indexPath.item].documentID)!)
            })
            start.setValue(UIColor.ThemeDarkGreen(alpha: 1.0), forKey: "titleTextColor")
            alertController.addAction(start)
            
            alertController.addAction(UIAlertAction(title: NSLocalizedString("Later", comment: "Default action"), style: UIAlertActionStyle.cancel, handler: { _ in
                
            }))
            
            let alertWindow = UIWindow(frame: UIScreen.main.bounds)
            alertWindow.rootViewController = UIViewController()
            alertWindow.windowLevel = UIWindowLevelAlert + 1;
            alertWindow.makeKeyAndVisible()
            alertWindow.rootViewController?.present(alertController, animated: true, completion: nil)
        } else {
            self.goToExamDetail(documentID : (self.exams[indexPath.item].documentID)!)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.Background(alpha: 1.0)
        
        let space : Character = " "
        let idx = self.navigationItem.title!.characters.index(of: space)
        let pos = self.navigationItem.title!.characters.distance(from: self.navigationItem.title!.startIndex, to: idx!)
        let count = self.navigationItem.title!.count - Int(pos)
        subject_code = "\(self.navigationItem.title!.dropLast(count))"
        documentID = "\(self.navigationItem.title!.dropFirst(subject_code.count+1))"
        
//        subject_code = self.navigationItem.title!
        
        setupView()
        setupMenu()
        selectedIndex()
        watchModules()
        
        if "\((ApiService().myID()["user_type"])!)" == "teacher" {
            let uploadImage = UIImage(named: "ic_upload")?.withRenderingMode(.alwaysOriginal)
            let uploadItem = UIBarButtonItem(image: uploadImage, style: .plain, target: self, action: #selector(upload))
            navigationItem.rightBarButtonItems = [uploadItem]
        }
        
    }
    
}

//
//  SeatPlanDetail.swift
//  QQ_School_App
//
//  Created by Abby Esteves on 04/05/2018.
//  Copyright © 2018 Abby Esteves. All rights reserved.
//

import UIKit
class SeatPlanDetail: UIViewController {
    
    let manager = CBLManager.sharedInstance()
    let appDelegate = UIApplication.shared.delegate as! AppDelegate
    var subject_code = String()
    var documentID = String()
    var gradeSection = String()
    var rowSelect = String()
    var columnSelect = String()
    var closed = false
    var timer : Timer?
    var classes = ApiService().classes(day: FileService().getDateTime(format: "EEEE"))
    
    let seatingPlanView : UIView = {
        let view = UIView()
        return view
    }()
    
    let descPlanView : UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.ThemeOrange(alpha: 1.0)
        return view
    }()
    
    let boarder1 : UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.white
        return view
    }()
    
    let boarder2 : UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.white
        return view
    }()
    
    let timeRemainingLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.white
        label.numberOfLines = 2
        label.textAlignment = .center
        label.text = "Remaining Time"
        label.font = label.font.withSize(12)
        return label
    }()
    
    let countdownLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.white
        label.numberOfLines = 2
        label.textAlignment = .center
        label.font = UIFont.boldSystemFont(ofSize: label.font.pointSize)
        label.font = label.font.withSize(23)
        label.text = "0:00:00"
        return label
    }()
    
    let timeLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.white
        label.numberOfLines = 2
        label.textAlignment = .center
        label.text = "\(FileService().getDateTime(format : "h:mm a").uppercased())".uppercased()
        label.font = label.font.withSize(12)
        return label
    }()
    
    let monthYearLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.white
        label.numberOfLines = 2
        label.textAlignment = .center
        label.text = "\(FileService().getDateTime(format : "MM/dd/yyyy").uppercased())".uppercased()
        label.font = label.font.withSize(15)
        return label
    }()
    
    let dateLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.white
        label.numberOfLines = 2
        label.textAlignment = .center
        label.font = UIFont.boldSystemFont(ofSize: label.font.pointSize)
        label.font = label.font.withSize(23)
        label.text = "\(FileService().getDateTime(format : "EEEE"))"
        return label
    }()
    
    @objc func closeDesc() {
        if !closed {
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                
                self.descPlanView.frame = CGRect(x: self.seatingPlanView.frame.maxX+120, y: 0, width: 150, height: self.view.frame.width-44)
                self.seatingPlanView.frame = CGRect(x: 0, y: 0, width: self.seatingPlanView.frame.width+120, height: self.seatingPlanView.frame.height)
                var x = 120/Int(self.rowSelect)!
                for seatView in self.seatingPlanView.subviews {
                    let width = self.seatingPlanView.frame.width/CGFloat(Int(self.rowSelect)!)
                    UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                        if seatView.frame.minX > 0 {
                            seatView.frame = CGRect(x: seatView.frame.minX+CGFloat(x), y: seatView.frame.minY, width: width, height: seatView.frame.height)
                            x = x + 120/Int(self.rowSelect)!
                        } else {
                            seatView.frame = CGRect(x: seatView.frame.minX, y: seatView.frame.minY, width: width, height: seatView.frame.height)
                        }
                    }, completion: { (Bool) in
//                        x = x + 120/Int(self.rowSelect)!
                    })
                }
//                self.getSeatPlan()
            }, completion: { (Bool) in
                self.closed = true
            })
        } else {
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                self.descPlanView.frame = CGRect(x: self.seatingPlanView.frame.maxX-120, y: 0, width: 150, height: self.view.frame.width-44)
                self.seatingPlanView.frame = CGRect(x: 0, y: 0, width: self.seatingPlanView.frame.width-120, height: self.seatingPlanView.frame.height)
            }, completion: { (Bool) in
                self.closed = false
            })
        }
    }
    
    @objc func selectSeat(sender: UITapGestureRecognizer){
        let studentID = sender.view!.subviews[0] as? UILabel
        let studentPosition = sender.view!.subviews[2] as? UILabel
//        print(" selectSeat ", (studentID?.text)!)
        if studentID?.text == nil || (studentID?.text)! == "null" {
            
//            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
//                self.appDelegate.myOrientation = .portrait
//                UIDevice.current.setValue(UIInterfaceOrientation.portrait.rawValue, forKey: "orientation")
//            }, completion: { (Bool) in
                DispatchQueue.main.async(execute: {
                    let titleLabel : UILabel = {
                        let label = UILabel()
                        label.textColor = UIColor.white
                        label.text = "Add Student # \((studentPosition?.text)!)".uppercased()
                        label.font = UIFont(name: "GillSans-Bold", size: UIFont.systemFontSize)!
                        label.font = label.font.withSize(15)
                        return label
                    }()
                    
                    let scanQRLauncher = ScanQRLauncher()
                    self.navigationController?.pushViewController(scanQRLauncher, animated: true)
                    scanQRLauncher.navigationItem.title = "\(self.subject_code) \(self.documentID)".uppercased()
                    scanQRLauncher.navigationItem.titleView = titleLabel
                })
//            })
            
        } else {
//            let student = "\((studentID?.text)!)" as [String:Any]
//            print(" selectSeat ", "\((studentID?.text)!) \(self.subject_code)")
            
            DispatchQueue.main.async(execute: {
                let titleLabel : UILabel = {
                    let label = UILabel()
                    label.textColor = UIColor.white
                    label.text = "Student List".uppercased()
                    label.font = UIFont(name: "GillSans-Bold", size: UIFont.systemFontSize)!
                    label.font = label.font.withSize(15)
                    return label
                }()
                
                let layout = UICollectionViewFlowLayout()
                let studentListController = StudentListController(collectionViewLayout: layout)
                self.navigationController?.pushViewController(studentListController, animated: true)
                studentListController.navigationItem.title = "\((studentID?.text)!) \(self.subject_code)".uppercased()
                studentListController.navigationItem.titleView = titleLabel
            })
//            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
//                self.blackView.alpha = 1
//                self.studentDetailView.frame = CGRect(x: self.blackView.frame.width/2-((self.blackView.frame.width/2-50)/2), y: self.blackView.frame.height/2-((self.blackView.frame.width/2-50)/2), width: self.blackView.frame.width/2+50, height: self.blackView.frame.width/2-50)
//            }, completion: { (Bool) in })
            
        }
    }
    
    private func getSeatPlan() {
        let manager = CBLManager.sharedInstance()
        let database = try! manager.databaseNamed("seatplan")
        let document = database.document(withID: documentID)
        let properties = document?.properties

        if properties != nil {
            rowSelect = "\((properties!["seat_row"])!)"
            columnSelect = "\((properties!["seat_column"])!)"
            let width = seatingPlanView.frame.width/CGFloat(Int(columnSelect)!)
            let height = seatingPlanView.frame.height/CGFloat(Int(rowSelect)!)

//            print(" getSeatPlan ", rowSelect, columnSelect, ((Int(rowSelect)!*Int(columnSelect)!)-Int(rowSelect)!)+1)

            var Minus = 0
            var y = 0
            var studentNum = 1
            for _ in 1...Int(rowSelect)! {
//                var studentNum = (((Int(rowSelect)!*Int(columnSelect)!)-Int(rowSelect)!)+1) + Minus
                var x = 0

                for _ in 1...Int(columnSelect)! {

                    let table : UIView = {
                        let view = UIView()
                        view.layer.borderColor = UIColor.lightGray.cgColor
                        view.layer.borderWidth = 0.2
                        return view
                    }()

                    let noStudentsLabel: UILabel = {
                        let label = UILabel()
                        label.text = "\(studentNum)"
                        label.textColor = UIColor.gray
                        label.numberOfLines = 10
                        label.textAlignment = .center
                        label.adjustsFontSizeToFitWidth = true
                        label.font = label.font.withSize(30)
                        return label
                    }()

                    let studentAliasLabel: UILabel = {
                        let label = UILabel()
                        label.textColor = UIColor.darkGray
                        label.numberOfLines = 2
                        label.textAlignment = .center
                        label.font = label.font.withSize(12)
                        return label
                    }()

                    let studentNumber: UILabel = {
                        let label = UILabel()
                        label.textColor = UIColor.white
                        label.numberOfLines = 10
                        label.textAlignment = .center
                        label.font = label.font.withSize(12)
                        return label
                    }()

                    let tableImage: UIImageView = {
                        let imageView = UIImageView()
                        imageView.contentMode = .scaleAspectFit
                        imageView.image = UIImage(named: "desk_gray")
                        imageView.alpha = 0.3
                        imageView.clipsToBounds = true
                        imageView.backgroundColor = .clear
                        imageView.layer.masksToBounds = true
                        return imageView
                    }()

                    seatingPlanView.addSubview(table)
                    table.addSubview(studentNumber)
                    table.addSubview(tableImage)
                    table.addSubview(noStudentsLabel)
                    table.addSubview(studentAliasLabel)

                    table.frame = CGRect(x: x, y: y, width: Int(width), height: Int(height))

                    if (properties!["\(studentNum)"]) == nil || "\((properties!["\(studentNum)"])!)" == "null" { //"\((properties!["\(studentNum-1)"])!)"
//                        studentNumber.text = "\((properties!["\(studentNum-1)"])!)"

                        tableImage.frame = CGRect(x: table.frame.width/2-((table.frame.width/2+20)/2), y: table.frame.height/2-((table.frame.height/2+20)/2), width: table.frame.width/2+20, height: table.frame.height/2+20)
                    } else {

                        let seat = ApiService().stringToJsonObject(string : "\((properties!["\(studentNum)"])!)")//"\((properties!["\(studentNum-1)"])!)"
//                        print(" SeatPlanDetail ", seat)
                        // create merits
                        let property = [
                            "qq_id": "\((seat["qq_id"])!)",
                            "name": "\((seat["name"])!)",
                            "grade": "\((properties!["grade"])!)",
                            "section": "\((properties!["section"])!)",
                            "gender": "\((seat["gender"])!)",
                            "seat_pos": "\(studentNum)",
                            "alias": FileService().alias(name: "\((seat["name"])!)"),
                        ] as [String:Any]

                        self.getMerits(property: property)

                        studentNumber.text = "\((seat["qq_id"])!)"
                        tableImage.alpha = 0
                        studentAliasLabel.text = FileService().alias(name: "\((seat["name"])!)") // "\((seat["alias"])!)"

                        tableImage.frame = CGRect(x: table.frame.width/2-((table.frame.width/2+10)/2), y: table.frame.height/2-((table.frame.height/2+10)/2), width: table.frame.width/2+10, height: table.frame.height/2+10)
                    }
                    table.alpha = 0

                    noStudentsLabel.frame = CGRect(x: 0, y: table.frame.height/2-5, width:  table.frame.width, height: 20)
                    studentAliasLabel.frame = CGRect(x: 0, y: tableImage.frame.maxY-5, width: table.frame.width, height: 20)

                    UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                        table.alpha = 1
                    }, completion: { (Bool) in })

                    table.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(self.selectSeat(sender:))))

                    x = x + Int(width)
                    studentNum = studentNum + 1
//                    studentNum = studentNum - Int(rowSelect)!
                }
                y = y + Int(height)
                Minus = Minus + 1
                
            }
        }
        // update attendance
        self.update()
    }
    
    private func setupMerits() {
        
    }
    
    private func update() {
        for view in self.seatingPlanView.subviews {
            let student_number = view.subviews[0] as! UILabel
            let chair = view.subviews[1] as! UIImageView
            let chairNumber = view.subviews[2] as! UILabel
            
            if  student_number.text != nil {
//                print(" getAttendance ", )
                if ApiService().getGateAttendance(studentID: (student_number.text)!) {
                    chair.image = UIImage(named: "desk")
                    chairNumber.textColor = UIColor.white
                    chair.alpha = 1
                }
            }
        }
    }
    
    private func getMerits(property : [String:Any]) {
        let manager = CBLManager.sharedInstance()
        let database = try! manager.databaseNamed("merits")
        let document = database.document(withID: "MERITS_\((property["qq_id"])!)_\(subject_code)_\(gradeSection)")!
        let properties = document.properties
        
        if properties == nil {
        //create merits
//            "qq_id": "\((seat["qq_id"])!)",
//            "name": "\((seat["name"])!)",
//            "grade": "\((properties!["grade"])!)",
//            "section": "\((properties!["section"])!)",
//            "seat_pos": "\(studentNum)",
//            "alias": FileService().alias(name: "\((seat["name"])!)"),
            let properties = [
                "i": "\((property["qq_id"])!)",
                "n": "\((property["name"])!)",
                "gen": "\((property["gender"])!)",
                "g": "\((property["grade"])!)",
                "sct": "\((property["section"])!)",
            ]
//             print(" getMerits ",subject_code, studentID)
            ApiService().createMerit(subject_code: subject_code, studentID : properties, seatnumber : "\((property["seat_pos"])!)", alias : "\((property["alias"])!)")
        }
        
    }
    
    private func refresh() {
        for view in self.seatingPlanView.subviews {
            view.removeFromSuperview()
        }
        
        self.getSeatPlan()
    }
    
    private func watch() {
        let seatplan = try! manager.databaseNamed("seatplan")
        //        self.getSubjects()
        NotificationCenter.default.addObserver(forName: NSNotification.Name.cblDatabaseChange, object: seatplan, queue: nil) {
            (notification) -> Void in
            if let changes = notification.userInfo!["changes"] as? [CBLDatabaseChange] {
                for change in changes {
                    print("seatplan , revision ID '%@'", change.description)
                    self.refresh()
                }
            }
        }
        
        let attendance = try! manager.databaseNamed("attendance")
        NotificationCenter.default.addObserver(forName: NSNotification.Name.cblDatabaseChange, object: attendance, queue: nil) {
            (notification) -> Void in
            if let changes = notification.userInfo!["changes"] as? [CBLDatabaseChange] {
                for change in changes {
                    print("attendance , revision ID '%@'", change.description)
                    self.update()
                }
            }
        }
        
        let merits = try! manager.databaseNamed("merits")
        NotificationCenter.default.addObserver(forName: NSNotification.Name.cblDatabaseChange, object: merits, queue: nil) {
            (notification) -> Void in
            if let changes = notification.userInfo!["changes"] as? [CBLDatabaseChange] {
                for change in changes {
                    print("merits , revision ID '%@'", change.description)
                    self.setupMerits()
                }
            }
        }
    }
    
    @objc func getDescPlanView() {
        timeLabel.text = "\(FileService().getDateTime(format : "h:mm a").uppercased())".uppercased()
//        print(" timerCountDown ", classes)
        for subject in classes {
            timerCountDown(classEnd: Int(subject.time_end_long)!)
        }
    }
    
    private func timerCountDown(classEnd: Int) {
//        print(" timerCountDown ", classEnd)
        let coutdown = FileService().countdown(classEnd: classEnd) as [String:Any]
        //        print(" timerCountLabel.text ", coutdown)
        let minute = Int("\((coutdown["minutes"])!)")
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.countdownLabel.textColor = UIColor.white
        }, completion: { (Bool) in })
        if minute! < 10 && minute! > 15 {
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                self.countdownLabel.textColor = UIColor.Warning(alpha: 1.0)
            }, completion: { (Bool) in })
        } else if minute! < 15 {
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                self.countdownLabel.textColor = UIColor.Alert(alpha: 1.0)
            }, completion: { (Bool) in })
        }
        countdownLabel.text = "\((coutdown["hours"])!):\((coutdown["minutes"])!):\((coutdown["seconds"])!)"
    }
    
    private func setupView() {
        view.addSubview(seatingPlanView)
        view.addSubview(descPlanView)
        descPlanView.addSubview(monthYearLabel)
        descPlanView.addSubview(dateLabel)
        descPlanView.addSubview(timeLabel)
        descPlanView.addSubview(boarder1)
        descPlanView.addSubview(timeRemainingLabel)
        descPlanView.addSubview(countdownLabel)
        descPlanView.addSubview(boarder2)
    }
    
    private func setupConstraints(){
        seatingPlanView.frame = CGRect(x: 0, y: 0, width: view.frame.height-150, height: view.frame.width-24)
        descPlanView.frame = CGRect(x: seatingPlanView.frame.maxX, y: 0, width: 150, height: view.frame.width-24)
        timeLabel.frame = CGRect(x: 10, y: 20, width: descPlanView.frame.width-20, height: 20)
        dateLabel.frame = CGRect(x: 10, y: timeLabel.frame.maxY+10, width: descPlanView.frame.width-20, height: 23)
        monthYearLabel.frame = CGRect(x: 10, y: dateLabel.frame.maxY+10, width: descPlanView.frame.width-20, height: 20)
        boarder1.frame = CGRect(x: 0, y: monthYearLabel.frame.maxY+20, width: descPlanView.frame.width, height: 1)
        timeRemainingLabel.frame = CGRect(x: 10, y: boarder1.frame.maxY+10, width: descPlanView.frame.width-20, height: 20)
        countdownLabel.frame = CGRect(x: 10, y: timeRemainingLabel.frame.maxY+10, width: descPlanView.frame.width-20, height: 23)
        boarder2.frame = CGRect(x: 0, y: countdownLabel.frame.maxY+20, width: descPlanView.frame.width, height: 1)
        
        getSeatPlan()
        self.timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(self.getDescPlanView), userInfo: nil, repeats: true)
    }
    
    private func setupGestures() {
//        descPlanView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(closeDesc)))
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        appDelegate.myOrientation = .landscape
        UIDevice.current.setValue(UIInterfaceOrientation.landscapeRight.rawValue, forKey: "orientation")
//        UIApplication.shared.isStatusBarHidden = true
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        appDelegate.myOrientation = .portrait
        UIDevice.current.setValue(UIInterfaceOrientation.portrait.rawValue, forKey: "orientation")
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.Background(alpha: 1.0)
        
        let space : Character = " "
        let idx = self.navigationItem.title!.characters.index(of: space)
        let pos = self.navigationItem.title!.characters.distance(from: self.navigationItem.title!.startIndex, to: idx!)
        let count = self.navigationItem.title!.count - Int(pos)
        subject_code = "\(self.navigationItem.title!.dropLast(count))"
        documentID = "\(self.navigationItem.title!.dropFirst(subject_code.count+1))"
        gradeSection = "\(documentID.dropFirst(12+subject_code.count))"
//        print(" SeatPlanDetail ", gradeSection, documentID)
        
        self.classes = self.classes.filter { $0.subject_code == subject_code}
        
        setupView()
        setupConstraints()
        setupGestures()
        watch()
        
        
    }
    
}

//
//  NewsDetail.swift
//  QQ_School_App
//
//  Created by Abby Esteves on 22/03/2018.
//  Copyright © 2018 Abby Esteves. All rights reserved.
//

import UIKit
class NewsDetail: UIViewController {
    
    let mainView : UIView = {
        let view = UIView()
        view.backgroundColor = .white
        return view
    }()
    
    let backImage: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.backgroundColor = .clear
        imageView.layer.masksToBounds = true
        imageView.image = UIImage(named: "ic_back")?.withRenderingMode(.alwaysTemplate)as UIImage?
        imageView.tintColor = UIColor.white
        return imageView
    }()
    
    let backView : UIView = {
        let view = UIView()
        return view
    }()
    
    let titleLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.gray
        label.numberOfLines = 2
        label.font = UIFont.boldSystemFont(ofSize: label.font.pointSize)
        label.font = label.font.withSize(15)
        return label
    }()
    
    let detailLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.lightGray
        label.numberOfLines = 2
        label.font = label.font.withSize(12)
        return label
    }()
    
    let descLabel: UITextView = {
        let textView = UITextView()
        textView.textColor = UIColor.gray
        textView.backgroundColor = UIColor.white
        textView.textAlignment = .left
        textView.isEditable = false
        textView.isScrollEnabled = false
        textView.font = textView.font?.withSize(13)
        return textView
    }()
    
    let thumbnailView: UIImageView = {
        let imageView = UIImageView()
        imageView.contentMode = .scaleAspectFill
        imageView.clipsToBounds = true
        imageView.backgroundColor = UIColor.lightGray
        imageView.layer.masksToBounds = true
        imageView.image = UIImage(named: "newspaper")
        return imageView
    }()
    
    func open(news: News){
//        print(" news :", news)
        titleLabel.text = news.title
        descLabel.text = news.desc
        detailLabel.text = "\((news.author)!) • \(ApiService().epochConvertString(date: (news.date)!, format: "MMMM dd,yyyy"))"
        if let window = UIApplication.shared.keyWindow {
            window.addSubview(mainView)
            mainView.addSubview(thumbnailView)
            window.addSubview(backView)
            backView.addSubview(backImage)
            mainView.addSubview(titleLabel)
            mainView.addSubview(descLabel)
            mainView.addSubview(detailLabel)
            
            backView.alpha = 0
            
            mainView.frame = CGRect(x: 0, y: view.frame.height, width: view.frame.width, height: view.frame.height)
            thumbnailView.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height/3)
            backView.frame = CGRect(x: 10, y: 25, width: 25, height: 25)
            backImage.frame = CGRect(x: 10, y: 25, width: 25, height: 25)
            
            let heightTitle = view.heightEstimation(text: news.title, width: view.frame.width-20, size: 13, defaultHeight: 20)
            titleLabel.frame = CGRect(x: 15, y: thumbnailView.frame.maxY+20, width: view.frame.width-30, height: heightTitle)
            let heightDetail = view.heightEstimation(text: "\((news.author)!) • \((news.date)!)", width: view.frame.width-20, size: 13, defaultHeight: 20)
            detailLabel.frame = CGRect(x: 15, y: titleLabel.frame.maxY+5, width: view.frame.width-30, height: heightDetail)
            let height = thumbnailView.frame.height + titleLabel.frame.height+40+10
            descLabel.frame = CGRect(x: 10, y: detailLabel.frame.maxY+10, width: view.frame.width-20, height: view.frame.height-height)
            
            mainView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(close)))
            
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                self.mainView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
            }, completion: { (Bool) in
                self.backView.alpha = 1
            })
        }
    }
    
    @objc func close(){
        print("close")
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.backView.alpha = 0
        }, completion: { (Bool) in
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                self.mainView.frame = CGRect(x: 0, y: self.view.frame.height, width: self.view.frame.width, height: self.view.frame.height)
            }, completion: { (Bool) in
                self.mainView.removeFromSuperview()
            })
        })
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
}

//
//  CreateExamDetail.swift
//  QQ_School_App
//
//  Created by Abby Esteves on 18/04/2018.
//  Copyright © 2018 Abby Esteves. All rights reserved.
//

import UIKit
import AnyFormatKit

class CreateExamDetail: UIViewController, UIScrollViewDelegate, UITextFieldDelegate, UITextViewDelegate, UIPickerViewDataSource, UIPickerViewDelegate, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    

    //UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout
    let cellId = "cellId"
    let manager = CBLManager.sharedInstance()
    var examId = "examId"
    var hours = [String]()
    var minutes = [String]()
    var hourStr = "0"
    var minutesStr = "00"
    let pickerHour = UIPickerView()
    let pickerMinute = UIPickerView()
    var subject_code = String()
    var MainScrollView = UIScrollView()
    var y = 0
    
    var questionsList : [Questions] = {
        var question1 = Questions()
        question1.number = "1"
        question1.question = ""
        question1.choice1 = ""
        question1.choice2 = ""
        question1.choice3 = ""
        question1.choice4 = ""
        question1.answer = "-"
        question1.answered = ""
        
        return [question1]
    }()
    
//    var questions = [Questions]()
    
    let ExamView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.backgroundColor = .clear
        return cv
    }()
    
    let questionView : UIView = {
        let view = UIView()
        return view
    }()
    
    let blackView : UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(white: 0, alpha: 0.6)
        return view
    }()
    
    let examParentView : UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.white
        view.layer.cornerRadius = 10
        return view
    }()
    
    let questionText: UITextView = {
        let textView = UITextView()
        textView.backgroundColor = UIColor.white
        textView.textColor = UIColor.darkGray
        textView.layer.borderWidth = 1.0
        textView.layer.borderColor = UIColor.lightGray.cgColor
        textView.isEditable = true
        textView.isSelectable = true
        textView.isScrollEnabled = true
        textView.layer.cornerRadius = 5
        textView.font = textView.font?.withSize(15)
        textView.text = ""
        return textView
    }()
    
    let choice1: UITextField = {
        let text = UITextField()
        text.text = ""
        text.borderStyle = .none
        text.backgroundColor = .white
        text.textColor = UIColor.darkGray
        text.font = .systemFont(ofSize: 13)
        text.layer.borderWidth = 1.0
        text.layer.borderColor = UIColor.lightGray.cgColor
        text.layer.cornerRadius = 5
        text.autocorrectionType = .no
        var placeholder = NSMutableAttributedString()
        placeholder = NSMutableAttributedString(attributedString: NSAttributedString(string: "a", attributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 13.0), .foregroundColor: UIColor.gray]))
        text.setLeftPadding(space: 8)
        text.attributedPlaceholder = placeholder
        return text
    }()
    
    let choice2: UITextField = {
        let text = UITextField()
        text.text = ""
        text.borderStyle = .none
        text.backgroundColor = .white
        text.textColor = UIColor.darkGray
        text.font = .systemFont(ofSize: 13)
        text.layer.borderWidth = 1.0
        text.layer.borderColor = UIColor.lightGray.cgColor
        text.layer.cornerRadius = 5
        text.autocorrectionType = .no
        var placeholder = NSMutableAttributedString()
        placeholder = NSMutableAttributedString(attributedString: NSAttributedString(string: "b", attributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 13.0), .foregroundColor: UIColor.gray]))
        text.setLeftPadding(space: 8)
        text.attributedPlaceholder = placeholder
        return text
    }()
    
    let choice3: UITextField = {
        let text = UITextField()
        text.text = ""
        text.borderStyle = .none
        text.backgroundColor = .white
        text.textColor = UIColor.darkGray
        text.font = .systemFont(ofSize: 13)
        text.layer.borderWidth = 1.0
        text.layer.borderColor = UIColor.lightGray.cgColor
        text.layer.cornerRadius = 5
        text.autocorrectionType = .no
        var placeholder = NSMutableAttributedString()
        placeholder = NSMutableAttributedString(attributedString: NSAttributedString(string: "c", attributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 13.0), .foregroundColor: UIColor.gray]))
        text.setLeftPadding(space: 8)
        text.attributedPlaceholder = placeholder
        return text
    }()
    
    let choice4: UITextField = {
        let text = UITextField()
        text.text = ""
        text.borderStyle = .none
        text.backgroundColor = .white
        text.textColor = UIColor.darkGray
        text.font = .systemFont(ofSize: 13)
        text.layer.borderWidth = 1.0
        text.layer.borderColor = UIColor.lightGray.cgColor
        text.layer.cornerRadius = 5
        text.autocorrectionType = .no
        var placeholder = NSMutableAttributedString()
        placeholder = NSMutableAttributedString(attributedString: NSAttributedString(string: "d", attributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 13.0), .foregroundColor: UIColor.gray]))
        text.setLeftPadding(space: 8)
        text.attributedPlaceholder = placeholder
        return text
    }()
    
    let addQuestionButton: UIButton = {
        let button = UIButton()
        button.backgroundColor = UIColor.ThemeDarkGreen(alpha: 1.0)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 13)
        button.setTitleColor(UIColor.white, for: .normal)
        button.setTitle("Add question".uppercased(), for: .normal)
        return button
    }()
    
    //exam
    let examNameLabel: UILabel = {
        let label = UILabel()
        label.text = "Exam Name :"
        label.textColor = UIColor.gray
        label.numberOfLines = 2
        label.font = label.font.withSize(15)
        return label
    }()
    
    let examNameText: UITextField = {
        let text = UITextField()
        text.text = ""
        text.borderStyle = .none
        text.backgroundColor = .white
        text.textColor = UIColor.darkGray
        text.font = .systemFont(ofSize: 13)
        text.layer.borderWidth = 1.0
        text.layer.borderColor = UIColor.lightGray.cgColor
        text.layer.cornerRadius = 5
        text.autocorrectionType = .no
        text.setLeftPadding(space: 8)
        return text
    }()
    
    let examDescriptionLabel: UILabel = {
        let label = UILabel()
        label.text = "Exam Instructions :"
        label.textColor = UIColor.gray
        label.numberOfLines = 2
        label.font = label.font.withSize(15)
        return label
    }()
    
    let examDescriptionText: UITextView = {
        let textView = UITextView()
        textView.backgroundColor = UIColor.white
        textView.textColor = UIColor.darkGray
        textView.layer.borderWidth = 1.0
        textView.layer.borderColor = UIColor.lightGray.cgColor
        textView.isEditable = true
        textView.isSelectable = true
        textView.isScrollEnabled = true
        textView.layer.cornerRadius = 5
        textView.font = textView.font?.withSize(15)
        textView.text = ""
        return textView
    }()
    
    let examDueLabel: UILabel = {
        let label = UILabel()
        label.text = "Exam Due :"
        label.textColor = UIColor.gray
        label.numberOfLines = 2
        label.font = label.font.withSize(15)
        return label
    }()
    
    let dueText: UILabel = {
        let label = UILabel()
        label.text = "MM/DD/YYYY"
        label.textColor = UIColor.lightGray
        label.numberOfLines = 1
        label.font = label.font.withSize(15)
        return label
    }()
    
    let examDueText: UITextView = {
        let textView = UITextView()
        textView.backgroundColor = UIColor.white
        textView.textColor = UIColor.clear
        textView.layer.borderWidth = 1.0
        textView.layer.borderColor = UIColor.lightGray.cgColor
        textView.isEditable = true
        textView.isSelectable = true
        textView.isScrollEnabled = false
        textView.layer.cornerRadius = 5
        textView.keyboardType = .numberPad
        textView.font = textView.font?.withSize(20)
        textView.text = ""
        return textView
    }()
    
//    let examDueText: UITextField = {
//        let text = UITextField()
//        text.text = ""
//        text.keyboardType = .numberPad
//        text.borderStyle = .none
//        text.backgroundColor = .white
//        text.textColor = UIColor.darkGray
//        text.font = .systemFont(ofSize: 13)
//        text.layer.borderWidth = 1.0
//        text.layer.borderColor = UIColor.lightGray.cgColor
//        text.layer.cornerRadius = 5
//        text.autocorrectionType = .no
//        var placeholder = NSMutableAttributedString()
//        placeholder = NSMutableAttributedString(attributedString: NSAttributedString(string: "MM/DD/YYYY", attributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 13.0), .foregroundColor: UIColor.lightGray]))
//        text.setLeftPadding(space: 8)
//        text.attributedPlaceholder = placeholder
//        return text
//    }()
    
    let examTimeLabel: UILabel = {
        let label = UILabel()
        label.text = "Allotted Time :"
        label.textColor = UIColor.gray
        label.numberOfLines = 2
        label.font = label.font.withSize(15)
        return label
    }()
    
    let saveExamButton: UIButton = {
        let button = UIButton()
        button.backgroundColor = UIColor.ThemeDarkGreen(alpha: 1.0)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 13)
        button.setTitleColor(UIColor.white, for: .normal)
        button.setTitle("Save Create Exam".uppercased(), for: .normal)
        return button
    }()
    
    let HLabel: UILabel = {
        let label = UILabel()
        label.text = "H"
        label.textColor = UIColor.ThemeDark(alpha: 1.0)
        label.numberOfLines = 2
        label.font = UIFont.boldSystemFont(ofSize: label.font.pointSize)
        label.font = label.font.withSize(25)
        return label
    }()
    
    let MLabel: UILabel = {
        let label = UILabel()
        label.text = "M"
        label.textColor = UIColor.ThemeDark(alpha: 1.0)
        label.numberOfLines = 2
        label.font = UIFont.boldSystemFont(ofSize: label.font.pointSize)
        label.font = label.font.withSize(25)
        return label
    }()
    
    //
    
    private func createDraft() {
        let parent = [
            "allotted_time": "00:00",
            "exam_desc": "draft",
            "exam_due": "due date".capitalized,
            "exam_name": "draft",
            "subj_code": "\(subject_code)",
            "teacher_id": "\((ApiService().myID()["qq_id"])!)",
            "type" : "exam"
        ] as [ String : Any]
        
        questionsList = questionsList.filter { $0.question != "" }
        
//        print(" createDraft ", questionsList.count,  questionsList )
        
        ApiService().createExamParent(exam: parent, questions: questionsList)
    }
    
    @objc func createParent() {
        if examDescriptionText.text != "" && examDueText.text != "" && examNameText.text != "" {
            let parent = [
                "allotted_time": "\(hourStr):\(minutesStr)",
                "exam_desc": "\((examDescriptionText.text)!)",
                "exam_due": "\((dueText.text)!)",
                "exam_name": "\((examNameText.text)!)",
                "subj_code": "\(subject_code)",
                "teacher_id": "\((ApiService().myID()["qq_id"])!)",
                "type": "exam"
            ] as [ String : Any]
            
            questionsList = questionsList.filter { $0.question != "" }

            ApiService().createExamParent(exam: parent, questions: questionsList)
            self.closeDetail()
//            self.removeFromSuperView(from: false)
        } else {
            if examNameText.text == ""{
                view.showToast(message: "Add the exam name")
                examNameText.becomeFirstResponder()
            } else if examDescriptionText.text == "" {
                view.showToast(message: "Add the exam decsription")
                examDescriptionText.becomeFirstResponder()
            } else if examDueText.text == "" {
                view.showToast(message: "Add the exam due date")
                examDueText.becomeFirstResponder()
            }

        }
    }

    @objc func createExam() {
        self.exitKeyboard()
        
        print(" addQuestion ", questionsList.count ,  questionsList)
        
        if questionsList[0].question != "" && questionsList[0].choice1 != "" && questionsList[0].choice2 != "" && questionsList[0].choice3 != "" && questionsList[0].choice4 != "" && questionsList[0].answer != "" {

            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                self.blackView.alpha = 1
                self.examParentView.frame = CGRect(x: 10, y: 50, width: self.blackView.frame.width-20, height:250+50+120)
                self.examNameLabel.frame = CGRect(x: 10, y: 10, width: self.examParentView.frame.width-20, height: 20)
                self.examNameText.frame = CGRect(x: 10, y: self.examNameLabel.frame.maxY+10, width: self.examParentView.frame.width-20, height: 30)
                self.examDescriptionLabel.frame = CGRect(x: 10, y: self.examNameText.frame.maxY+10, width: self.examParentView.frame.width-20, height: 20)
                self.examDescriptionText.frame = CGRect(x: 10, y: self.examDescriptionLabel.frame.maxY+10, width: self.examParentView.frame.width-20, height: 30)
                self.examDueLabel.frame = CGRect(x: 10, y: self.examDescriptionText.frame.maxY+10, width: self.examParentView.frame.width-20, height: 20)
                self.examDueText.frame = CGRect(x: 10, y: self.examDueLabel.frame.maxY+10, width: self.examParentView.frame.width-20, height: 30)
                self.dueText.frame = CGRect(x: 20, y: 0, width: self.examDueText.frame.width-40, height: self.examDueText.frame.height)
                self.examTimeLabel.frame = CGRect(x: 10, y: self.examDueText.frame.maxY+10, width: self.examParentView.frame.width-20, height: 20)
                self.pickerHour.frame = CGRect(x: 10, y: self.examTimeLabel.frame.maxY, width: self.examParentView.frame.width/2-20, height: 120)
                self.pickerMinute.frame = CGRect(x: self.pickerHour.frame.maxX, y: self.examTimeLabel.frame.maxY, width: self.examParentView.frame.width/2-20, height: 120)
                self.HLabel.frame = CGRect(x: self.pickerHour.frame.maxX-40, y: (self.examTimeLabel.frame.maxY-10)+(self.pickerHour.frame.height/2), width: 20, height: 20)
                self.MLabel.frame = CGRect(x: self.pickerMinute.frame.maxX-40, y: (self.examTimeLabel.frame.maxY-10)+(self.pickerHour.frame.height/2), width: 20, height: 20)
                self.saveExamButton.frame = CGRect(x: 10, y: self.examParentView.frame.height-60, width: self.examParentView.frame.width-20, height: 50)
            }, completion: { (Bool) in })
        }
    }

    var index = 0
    
    func collectionView(_ collectionView: UICollectionView, didEndDisplaying cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
//        index = indexPath.item-1
//        print(" didEndDisplaying ", indexPath.item-1 )
    }
    
    func addQuestionArray() {
//        print(" index ! ", index)
        let indexPath = IndexPath(item: questionsList.count-1, section: 0)
        let cell = ExamView.cellForItem(at: indexPath) as! ExamEditCell
        questionsList[questionsList.count-1].question =  cell.questionText.text
        questionsList[questionsList.count-1].choice1 = (cell.choice1.text)!
        questionsList[questionsList.count-1].choice2 = (cell.choice2.text)!
        questionsList[questionsList.count-1].choice3 = (cell.choice3.text)!
        questionsList[questionsList.count-1].choice4 = (cell.choice4.text)!
//        print(" exam create UICollectionView ", questionsList)
    }
    
    @objc func addQuestion() {
        let indexPath = IndexPath(item: questionsList.count-1, section: 0)
        print("addQuestion", questionsList.count-1)
        
        let cell = ExamView.cellForItem(at: indexPath) as! ExamEditCell
        ExamView.scrollToItem(at: indexPath, at: .centeredHorizontally, animated: true)
        
        if cell.questionText.text != "" && (cell.choice1.text)! != "" && (cell.choice2.text)! != "" && (cell.choice3.text)! != "" && (cell.choice4.text)! != "" {
            addQuestionArray()
            
            //
            var question = Questions()
            question.number = "\(questionsList.count+1)"
            question.question = ""
            question.choice1 = ""
            question.choice2 = ""
            question.choice3 = ""
            question.choice4 = ""
            question.answer = "-"
            question.answered = ""
            questionsList.append(question)
            ExamView.reloadData()
            
            index = questionsList.count-1
            
            let indexNext = IndexPath(item: questionsList.count-1, section: 0)
            ExamView.scrollToItem(at: indexNext, at: .centeredHorizontally, animated: true)
            
        } else {
            if cell.questionText.text == "" {
                view.showToast(message: "Question \(questionsList.count) is incomplete")
                questionText.becomeFirstResponder()
            } else if (cell.choice1.text)! == "" {
                view.showToast(message: "Choice 1 is incomplete")
                choice1.becomeFirstResponder()
            } else if (cell.choice2.text)! == "" {
                view.showToast(message: "Choice 2 is incomplete")
                choice2.becomeFirstResponder()
            } else if (cell.choice3.text)! == "" {
                view.showToast(message: "Choice 3 is incomplete")
                choice3.becomeFirstResponder()
            } else if (cell.choice4.text)! == ""{
                view.showToast(message: "Choice 4 is incomplete")
                choice4.becomeFirstResponder()
            }
            //        else if  questions[questions.count-1].answer == "" {
            //            view.showToast(message: "Question \(questions.count) has no answer")
            //        }
        }
    }
    
    @objc func closeDetail() {
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.blackView.alpha = 0
            self.examParentView.frame = CGRect(x: self.blackView.frame.width/2, y: self.blackView.frame.height/2, width: 0, height: 0)
            self.examNameLabel.frame = CGRect(x: self.examParentView.frame.width/2, y: self.examParentView.frame.height/2, width: 0, height: 0)
            self.examNameText.frame = CGRect(x: self.examParentView.frame.width/2, y: self.examParentView.frame.height/2, width: 0, height: 0)
            self.examDescriptionLabel.frame = CGRect(x: self.examParentView.frame.width/2, y: self.examParentView.frame.height/2, width: 0, height: 0)
            self.examDescriptionText.frame = CGRect(x: self.examParentView.frame.width/2, y: self.examParentView.frame.height/2, width: 0, height: 0)
            self.examDueLabel.frame = CGRect(x: self.examParentView.frame.width/2, y: self.examParentView.frame.height/2, width: 0, height: 0)
            self.examDueText.frame = CGRect(x: self.examParentView.frame.width/2, y: self.examParentView.frame.height/2, width: 0, height: 0)
            self.dueText.frame = CGRect(x: 20, y: 0, width: self.examDueText.frame.width-40, height: self.examDueText.frame.height)
            self.examTimeLabel.frame = CGRect(x: self.examParentView.frame.width/2, y: self.examParentView.frame.height/2, width: 0, height: 0)
            self.saveExamButton.frame = CGRect(x: self.examParentView.frame.width/2, y: self.examParentView.frame.height/2, width: 0, height: 0)
            self.pickerHour.frame = CGRect(x: self.examParentView.frame.width/2, y: self.examParentView.frame.height/2, width: 0, height: 0)
            self.pickerMinute.frame = CGRect(x: self.examParentView.frame.width/2, y: self.examParentView.frame.height/2, width: 0, height: 0)
            self.HLabel.frame = CGRect(x: self.examParentView.frame.width/2, y: self.examParentView.frame.height/2, width: 0, height: 0)
            self.MLabel.frame = CGRect(x: self.examParentView.frame.width/2, y: self.examParentView.frame.height/2, width: 0, height: 0)
        }, completion: { (Bool) in
            self.exitKeyboard()
        })
    }
    
//    @objc func select(sender: UITapGestureRecognizer) {
//        questions[questions.count-1].choice4 = choice4.text
//        let choices = [questionView.subviews[0] as UIView, questionView.subviews[1] as UIView, questionView.subviews[2] as UIView, questionView.subviews[3] as UIView]
//        let texts = [choice1.text, choice2.text, choice3.text, choice4.text]
//        // highlight answer
//        for (index, choices) in choices.enumerated() {
//            if  sender.view == choices {
//                choices.backgroundColor = UIColor.ThemeDarkGreen(alpha: 1.0)
//                //assign answer
//                for (indexText, choice) in texts.enumerated() {
//                    if index+1 == indexText+1 {
//                        questions[questions.count-1].answer = choice
//                    }
//                }
//            } else {
//                choices.backgroundColor = .clear
//            }
//        }
//    }
    
//    @objc func updateAnswer(sender: UITapGestureRecognizer) {
//
//        for views in MainScrollView.subviews {
//            let choiceView = [views.subviews[0] as UIView, views.subviews[1] as UIView, views.subviews[2] as UIView, views.subviews[3] as UIView]
//            let choiceText = [views.subviews[4] as! UITextField, views.subviews[5] as! UITextField, views.subviews[6] as! UITextField, views.subviews[7] as! UITextField]
//            let indexLabel = views.subviews[8] as! UILabel
//            let question = views.subviews[11] as! UITextView
//
//            for (index, choices) in choiceView.enumerated() {
//                if  sender.view == choices {
//                    choices.backgroundColor = UIColor.ThemeDarkGreen(alpha: 1.0)
//                    //assign answer
//                    for (indexText, choice) in choiceText.enumerated() {
//                        if index+1 == indexText+1 {
//                            questions[Int("\((indexLabel.text)!)")!-1].question = question.text
//                            questions[Int("\((indexLabel.text)!)")!-1].answer = choice.text
//                            questions[Int("\((indexLabel.text)!)")!-1].choice1 = choice.text
//                        }
//                    }
//                } else {
//                    choices.backgroundColor = .clear
//                }
//            }
//        }
//    }
    
//    func removeFromSuperView(from: Bool) {
//        for views in questionView.subviews {
//            views.removeFromSuperview()
//        }
//        for views in MainScrollView.subviews {
//            views.removeFromSuperview()
//        }
//        if from {
//            setupQuestions()
//        } else {
//            y = 0
//            questions = [Questions]()
//            MainScrollView.contentSize = CGSize(width: view.frame.width, height: view.frame.height-110)
//            setupAddQuestion()
//        }
//    }
    
    func textViewDidChange(_ textView: UITextView) {
        if textView == examDueText {
            
            let phoneFormatter = TextFormatter(textPattern: "##/##/####")
            dueText.text = (phoneFormatter.formattedText(from: "\((examDueText.text)!)"))!
            dueText.textColor = UIColor.gray
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
//        if textView == examDueText {
////             examDueText.text = dueDate
//            let phoneFormatter = TextFormatter(textPattern: "##/##/####")
//            let formatted = (phoneFormatter.formattedText(from: "\((textView.text)!)"))!
//            examDueText.text = formatted
//            print(" phoneFormatter ", formatted)
//        }
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
//        if textField == examDueText {
//            let phoneFormatter = TextFormatter(textPattern: "##/##/####")
//            let formatted = (phoneFormatter.formattedText(from: "\((examDueText.text)!)"))!
//            examDueText.text = formatted
//            print(" dueDate ", examDueText.text )
//        }
    }
    
    func textFieldDidEndEditing(_ textField: UITextField) {
//        if textField == choice1 {
//            questions[questions.count-1].choice1 = choice1.text
//            //            print(" textFieldDidBeginEditing ", choice1.text, questions[questions.count-1])
//        } else if textField == choice2 {
//            questions[questions.count-1].choice2 = choice2.text
//            //            print(" textFieldDidBeginEditing ", choice2.text, questions[questions.count-1])
//        } else if textField == choice3 {
//            questions[questions.count-1].choice3 = choice3.text
//            //            print(" textFieldDidBeginEditing ", choice3.text, questions[questions.count-1])
//        } else {
//            questions[questions.count-1].choice4 = choice4.text
//            //            print(" textFieldDidBeginEditing ", choice4.text, questions[questions.count-1])
//        }
    }
    
//    func setupAddQuestion() {
//
//    }
    
//    func setupAddQuestion() {
//        questionText.text = ""
//        choice1.text = ""
//        choice2.text = ""
//        choice3.text = ""
//        choice4.text = ""
//
//        var question = Questions()
//        question.question = ""
//        question.choice1 = ""
//        question.choice2 = ""
//        question.choice3 = ""
//        question.choice4 = ""
//        question.answer = ""
//        questions.append(question)
//
//        let boarderView : UIView = {
//            let view = UIView()
//            view.backgroundColor = UIColor.lightGray
//            return view
//        }()
//
//        let questionNoLabel: UILabel = {
//            let label = UILabel()
//            label.text = "\(questions.count)"
//            label.textColor = UIColor.darkGray
//            label.numberOfLines = 2
//            label.font = UIFont.boldSystemFont(ofSize: label.font.pointSize)
//            label.font = label.font.withSize(15)
//            return label
//        }()
//
//        let questionLabel: UILabel = {
//            let label = UILabel()
//            label.text = "Question :"
//            label.textColor = UIColor.gray
//            label.numberOfLines = 2
//            label.font = label.font.withSize(15)
//            return label
//        }()
//
//        let choicesLabel: UILabel = {
//            let label = UILabel()
//            label.text = "Choices :"
//            label.textColor = UIColor.gray
//            label.numberOfLines = 2
//            label.font = label.font.withSize(15)
//            return label
//        }()
//
//        let answer1 : UIView = {
//            let view = UIView()
//            view.layer.borderWidth = 2
//            view.layer.borderColor = UIColor.lightGray.cgColor
//            view.layer.cornerRadius = 10
//            return view
//        }()
//
//        let answer2 : UIView = {
//            let view = UIView()
//            view.layer.borderWidth = 2
//            view.layer.borderColor = UIColor.lightGray.cgColor
//            view.layer.cornerRadius = 10
//            return view
//        }()
//
//        let answer3 : UIView = {
//            let view = UIView()
//            view.layer.borderWidth = 2
//            view.layer.borderColor = UIColor.lightGray.cgColor
//            view.layer.cornerRadius = 10
//            return view
//        }()
//
//        let answer4 : UIView = {
//            let view = UIView()
//            view.layer.borderWidth = 2
//            view.layer.borderColor = UIColor.lightGray.cgColor
//            view.layer.cornerRadius = 10
//            return view
//        }()
//
//        MainScrollView.addSubview(questionView)
//        questionView.addSubview(answer1)
//        questionView.addSubview(answer2)
//        questionView.addSubview(answer3)
//        questionView.addSubview(answer4)
//        questionView.addSubview(choice1)
//        questionView.addSubview(choice2)
//        questionView.addSubview(choice3)
//        questionView.addSubview(choice4)
//        questionView.addSubview(questionNoLabel)
//        questionView.addSubview(boarderView)
//        questionView.addSubview(questionLabel)
//        questionView.addSubview(questionText)
//        questionView.addSubview(choicesLabel)
//
//
//        questionView.frame = CGRect(x: 0, y: y, width: Int(view.frame.width), height: 350)
//        questionNoLabel.frame = CGRect(x: 10, y: 10, width: questionView.frame.width-20, height: 50)
//        boarderView.frame = CGRect(x: 10, y: questionNoLabel.frame.maxY, width: questionView.frame.width-20, height: 1)
//        questionLabel.frame = CGRect(x: 10, y: boarderView.frame.maxY+20, width: questionView.frame.width-20, height: 20)
//        questionText.frame = CGRect(x: 10, y: questionLabel.frame.maxY, width: questionView.frame.width-20, height: 50)
//        choicesLabel.frame = CGRect(x: 10, y: questionText.frame.maxY+10, width: questionView.frame.width-20, height: 20)
//        answer1.frame = CGRect(x: 10, y: choicesLabel.frame.maxY+15, width: 20, height: 20)
//        answer2.frame = CGRect(x: 10, y: answer1.frame.maxY+20, width: 20, height: 20)
//        answer3.frame = CGRect(x: 10, y: answer2.frame.maxY+20, width: 20, height: 20)
//        answer4.frame = CGRect(x: 10, y: answer3.frame.maxY+20, width: 20, height: 20)
//        choice1.frame = CGRect(x: answer1.frame.maxX+10, y: choicesLabel.frame.maxY+10, width: view.frame.width-answer1.frame.maxX-20, height: 30)
//        choice2.frame = CGRect(x: answer2.frame.maxX+10, y: choice1.frame.maxY+10, width: view.frame.width-answer2.frame.maxX-20, height: 30)
//        choice3.frame = CGRect(x: answer3.frame.maxX+10, y: choice2.frame.maxY+10, width: view.frame.width-answer3.frame.maxX-20, height: 30)
//        choice4.frame = CGRect(x: answer4.frame.maxX+10, y: choice3.frame.maxY+10, width: view.frame.width-answer4.frame.maxX-20, height: 30)
//
//        answer1.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(select(sender:))))
//        answer2.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(select(sender:))))
//        answer3.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(select(sender:))))
//        answer4.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(select(sender:))))
//
//        y = y + Int(questionView.frame.height)
//        if CGFloat(y) > CGFloat(MainScrollView.frame.height) {
//            //adjust scroll
//            let adjust = MainScrollView.frame.height-350-210
//            MainScrollView.contentSize = CGSize(width: MainScrollView.frame.width, height: CGFloat(y)+adjust)
//        }
//
//        //scroll to bottom
//        let bottomOffset = CGPoint(x : 0, y : MainScrollView.contentSize.height-MainScrollView.bounds.size.height+MainScrollView.contentInset.bottom)
//        MainScrollView.setContentOffset(bottomOffset, animated: false)
//    }
    
//    func setupQuestions() {
//        y = 0
//
//        for (index, question) in questions.enumerated() {
//            let questionView : UIView = {
//                let view = UIView()
//                return view
//            }()
//
//            let boarderView : UIView = {
//                let view = UIView()
//                view.backgroundColor = UIColor.lightGray
//                return view
//            }()
//
//            let questionNoLabel: UILabel = {
//                let label = UILabel()
//                label.text = "\(index+1)"
//                label.textColor = UIColor.darkGray
//                label.numberOfLines = 2
//                label.font = UIFont.boldSystemFont(ofSize: label.font.pointSize)
//                label.font = label.font.withSize(15)
//                return label
//            }()
//
//            let questionLabel: UILabel = {
//                let label = UILabel()
//                label.text = "Question :"
//                label.textColor = UIColor.gray
//                label.numberOfLines = 2
//                label.font = label.font.withSize(15)
//                return label
//            }()
//
//            let choicesLabel: UILabel = {
//                let label = UILabel()
//                label.text = "Choices :"
//                label.textColor = UIColor.gray
//                label.numberOfLines = 2
//                label.font = label.font.withSize(15)
//                return label
//            }()
//
//            let answer1 : UIView = {
//                let view = UIView()
//                view.layer.borderWidth = 2
//                view.layer.borderColor = UIColor.lightGray.cgColor
//                view.layer.cornerRadius = 10
//                return view
//            }()
//
//            let answer2 : UIView = {
//                let view = UIView()
//                view.layer.borderWidth = 2
//                view.layer.borderColor = UIColor.lightGray.cgColor
//                view.layer.cornerRadius = 10
//                return view
//            }()
//
//            let answer3 : UIView = {
//                let view = UIView()
//                view.layer.borderWidth = 2
//                view.layer.borderColor = UIColor.lightGray.cgColor
//                view.layer.cornerRadius = 10
//                return view
//            }()
//
//            let answer4 : UIView = {
//                let view = UIView()
//                view.layer.borderWidth = 2
//                view.layer.borderColor = UIColor.lightGray.cgColor
//                view.layer.cornerRadius = 10
//                return view
//            }()
//
//            let questionText: UITextView = {
//                let textView = UITextView()
//                textView.text = "\((question.question)!)"
//                textView.backgroundColor = UIColor.white
//                textView.textColor = UIColor.darkGray
//                textView.layer.borderWidth = 1.0
//                textView.layer.borderColor = UIColor.lightGray.cgColor
//                textView.isEditable = true
//                textView.isSelectable = true
//                textView.isScrollEnabled = true
//                textView.layer.cornerRadius = 5
//                textView.font = textView.font?.withSize(15)
//                return textView
//            }()
//
//            let choice1: UITextField = {
//                let text = UITextField()
//                text.borderStyle = .none
//                text.backgroundColor = .white
//                text.textColor = UIColor.darkGray
//                text.font = .systemFont(ofSize: 13)
//                text.layer.borderWidth = 1.0
//                text.layer.borderColor = UIColor.lightGray.cgColor
//                text.layer.cornerRadius = 5
//                text.text = "\((question.choice1)!)"
////                var placeholder = NSMutableAttributedString()
////                placeholder = NSMutableAttributedString(attributedString: NSAttributedString(string: "a", attributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 13.0), .foregroundColor: UIColor.gray]))
//                text.setLeftPadding(space: 8)
////                text.attributedPlaceholder = placeholder
//                return text
//            }()
//
//            let choice2: UITextField = {
//                let text = UITextField()
//                text.borderStyle = .none
//                text.backgroundColor = .white
//                text.textColor = UIColor.darkGray
//                text.font = .systemFont(ofSize: 13)
//                text.layer.borderWidth = 1.0
//                text.layer.borderColor = UIColor.lightGray.cgColor
//                text.layer.cornerRadius = 5
//                text.text = "\((question.choice2)!)"
////                var placeholder = NSMutableAttributedString()
////                placeholder = NSMutableAttributedString(attributedString: NSAttributedString(string: "b", attributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 13.0), .foregroundColor: UIColor.gray]))
//                text.setLeftPadding(space: 8)
////                text.attributedPlaceholder = placeholder
//                return text
//            }()
//
//            let choice3: UITextField = {
//                let text = UITextField()
//                text.borderStyle = .none
//                text.backgroundColor = .white
//                text.textColor = UIColor.darkGray
//                text.font = .systemFont(ofSize: 13)
//                text.layer.borderWidth = 1.0
//                text.layer.borderColor = UIColor.lightGray.cgColor
//                text.layer.cornerRadius = 5
//                text.text = "\((question.choice3)!)"
////                var placeholder = NSMutableAttributedString()
////                placeholder = NSMutableAttributedString(attributedString: NSAttributedString(string: "c", attributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 13.0), .foregroundColor: UIColor.gray]))
//                text.setLeftPadding(space: 8)
////                text.attributedPlaceholder = placeholder
//                return text
//            }()
//
//            let choice4: UITextField = {
//                let text = UITextField()
//                text.borderStyle = .none
//                text.backgroundColor = .white
//                text.textColor = UIColor.darkGray
//                text.font = .systemFont(ofSize: 13)
//                text.layer.borderWidth = 1.0
//                text.layer.borderColor = UIColor.lightGray.cgColor
//                text.layer.cornerRadius = 5
//                text.text = "\((question.choice4)!)"
////                var placeholder = NSMutableAttributedString()
////                placeholder = NSMutableAttributedString(attributedString: NSAttributedString(string: "d", attributes: [NSAttributedStringKey.font: UIFont.systemFont(ofSize: 13.0), .foregroundColor: UIColor.gray]))
//                text.setLeftPadding(space: 8)
////                text.attributedPlaceholder = placeholder
//                return text
//            }()
//
//            if (question.answer)! == (question.choice1)! {
//                answer1.backgroundColor = UIColor.ThemeDarkGreen(alpha: 1.0)
//            } else if (question.answer)! == (question.choice2)! {
//                answer2.backgroundColor = UIColor.ThemeDarkGreen(alpha: 1.0)
//            } else if (question.answer)! == (question.choice3)! {
//                answer3.backgroundColor = UIColor.ThemeDarkGreen(alpha: 1.0)
//            } else if (question.answer)! == (question.choice4)!{
//                answer4.backgroundColor = UIColor.ThemeDarkGreen(alpha: 1.0)
//            }
//
//            answer1.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(updateAnswer(sender:))))
//            answer2.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(updateAnswer(sender:))))
//            answer3.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(updateAnswer(sender:))))
//            answer4.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(updateAnswer(sender:))))
//
//            MainScrollView.addSubview(questionView)
//            questionView.addSubview(answer1)
//            questionView.addSubview(answer2)
//            questionView.addSubview(answer3)
//            questionView.addSubview(answer4)
//            questionView.addSubview(choice1)
//            questionView.addSubview(choice2)
//            questionView.addSubview(choice3)
//            questionView.addSubview(choice4)
//            questionView.addSubview(questionNoLabel)
//            questionView.addSubview(boarderView)
//            questionView.addSubview(questionLabel)
//            questionView.addSubview(questionText)
//            questionView.addSubview(choicesLabel)
//
//
//            questionView.frame = CGRect(x: 0, y: y, width: Int(view.frame.width), height: 350)
//            questionNoLabel.frame = CGRect(x: 10, y: 10, width: questionView.frame.width-20, height: 50)
//            boarderView.frame = CGRect(x: 10, y: questionNoLabel.frame.maxY, width: questionView.frame.width-20, height: 1)
//            questionLabel.frame = CGRect(x: 10, y: boarderView.frame.maxY+20, width: questionView.frame.width-20, height: 20)
//            questionText.frame = CGRect(x: 10, y: questionLabel.frame.maxY, width: questionView.frame.width-20, height: 50)
//            choicesLabel.frame = CGRect(x: 10, y: questionText.frame.maxY+10, width: questionView.frame.width-20, height: 20)
//            answer1.frame = CGRect(x: 10, y: choicesLabel.frame.maxY+15, width: 20, height: 20)
//            answer2.frame = CGRect(x: 10, y: answer1.frame.maxY+20, width: 20, height: 20)
//            answer3.frame = CGRect(x: 10, y: answer2.frame.maxY+20, width: 20, height: 20)
//            answer4.frame = CGRect(x: 10, y: answer3.frame.maxY+20, width: 20, height: 20)
//            choice1.frame = CGRect(x: answer1.frame.maxX+10, y: choicesLabel.frame.maxY+10, width: view.frame.width-answer1.frame.maxX-20, height: 30)
//            choice2.frame = CGRect(x: answer2.frame.maxX+10, y: choice1.frame.maxY+10, width: view.frame.width-answer2.frame.maxX-20, height: 30)
//            choice3.frame = CGRect(x: answer3.frame.maxX+10, y: choice2.frame.maxY+10, width: view.frame.width-answer3.frame.maxX-20, height: 30)
//            choice4.frame = CGRect(x: answer4.frame.maxX+10, y: choice3.frame.maxY+10, width: view.frame.width-answer4.frame.maxX-20, height: 30)
//            y = y + Int(questionView.frame.height)
//        }
//        setupAddQuestion()
//    }
    
    
    private func setupView(){
        pickerHour.delegate = self
        pickerMinute.delegate = self
        examDueText.delegate = self
        
        ExamView.backgroundColor = UIColor.Background(alpha: 1.0)
        ExamView.delegate = self
        ExamView.dataSource = self
        ExamView.isPagingEnabled = true
        ExamView.isScrollEnabled = true
        ExamView.showsHorizontalScrollIndicator = false
        ExamView.register(ExamEditCell.self, forCellWithReuseIdentifier: cellId)
        
        let uploadImage = UIImage(named: "ic_exam")?.withRenderingMode(.alwaysOriginal)
        let uploadItem = UIBarButtonItem(image: uploadImage, style: .plain, target: self, action: #selector(createExam))
        navigationItem.rightBarButtonItems = [uploadItem]
        
        MainScrollView = UIScrollView(frame: view.bounds)
        MainScrollView.delegate = self
        MainScrollView.showsVerticalScrollIndicator = true
        MainScrollView.backgroundColor = UIColor.Background(alpha: 1.0)
        
        questionText.delegate = self
        choice1.delegate = self
        choice2.delegate = self
        choice3.delegate = self
        choice4.delegate = self
        
        view.addSubview(MainScrollView)
        view.addSubview(addQuestionButton)
        view.addSubview(ExamView)
        view.addSubview(blackView)
        view.addSubview(examParentView)
        examParentView.addSubview(examNameLabel)
        examParentView.addSubview(examNameText)
        examParentView.addSubview(examDescriptionLabel)
        examParentView.addSubview(examDescriptionText)
        examParentView.addSubview(examDueLabel)
        examParentView.addSubview(examDueText)
        examDueText.addSubview(dueText)
        examParentView.addSubview(examTimeLabel)
        examParentView.addSubview(pickerHour)
        examParentView.addSubview(pickerMinute)
        examParentView.addSubview(saveExamButton)
        examParentView.addSubview(HLabel)
        examParentView.addSubview(MLabel)
        
        blackView.alpha = 0
    }
    
    private func setupConstraints(){
        ExamView.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height-110)
        MainScrollView.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height-110)
        MainScrollView.contentSize = CGSize(width: view.frame.width, height: view.frame.height-110)
        
        blackView.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height)
        examParentView.frame = CGRect(x: view.frame.width/2, y: view.frame.height/2, width: 0, height: 0)
        examNameLabel.frame = CGRect(x: examParentView.frame.width/2, y: examParentView.frame.height/2, width: 0, height: 0)
        examNameText.frame = CGRect(x: examParentView.frame.width/2, y: examParentView.frame.height/2, width: 0, height: 0)
        examDescriptionLabel.frame = CGRect(x: examParentView.frame.width/2, y: examParentView.frame.height/2, width: 0, height: 0)
        examDescriptionText.frame = CGRect(x: examParentView.frame.width/2, y: examParentView.frame.height/2, width: 0, height: 0)
        examDueLabel.frame = CGRect(x: examParentView.frame.width/2, y: examParentView.frame.height/2, width: 0, height: 0)
        examDueText.frame = CGRect(x: examParentView.frame.width/2, y: examParentView.frame.height/2, width: 0, height: 0)
        dueText.frame = CGRect(x: 10, y: 0, width: examDueText.frame.width-20, height: examDueText.frame.height)
        examTimeLabel.frame = CGRect(x: examParentView.frame.width/2, y: examParentView.frame.height/2, width: 0, height: 0)
        pickerHour.frame = CGRect(x: examParentView.frame.width/2, y: examParentView.frame.height/2, width: 0, height: 0)
        pickerMinute.frame = CGRect(x: examParentView.frame.width/2, y: examParentView.frame.height/2, width: 0, height: 0)
        HLabel.frame = CGRect(x: examParentView.frame.width/2, y: examParentView.frame.height/2, width: 0, height: 0)
        MLabel.frame = CGRect(x: examParentView.frame.width/2, y: examParentView.frame.height/2, width: 0, height: 0)
        saveExamButton.frame = CGRect(x: examParentView.frame.width/2, y: examParentView.frame.height/2, width: 0, height: 0)
        
        addQuestionButton.frame = CGRect(x: 0, y: view.frame.height-110, width: view.frame.width, height: 50)
    }
    
    private func setupGestures(){
//        addQuestionButton.isEnabled = false
        addQuestionButton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(addQuestion)))
        blackView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(closeDetail)))
        saveExamButton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(createParent)))
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
        let height = self.navigationController?.navigationBar.frame.height
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            if CGFloat(y) > CGFloat(MainScrollView.frame.height) {
                self.MainScrollView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height-keyboardSize.height)
                let bottomOffset = CGPoint(x : 0, y : MainScrollView.contentSize.height-MainScrollView.bounds.size.height+MainScrollView.contentInset.bottom)
                MainScrollView.setContentOffset(bottomOffset, animated: false)
            }

        }
    }
    
    @objc func keyboardWillHide(notification: NSNotification) {
//        let height = self.navigationController?.navigationBar.frame.height
//        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
//            if CGFloat(y) > CGFloat(MainScrollView.frame.height) {
                self.MainScrollView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.view.frame.height)
//            }
//        }
    }
    
    @objc func exitKeyboard() {
        view.endEditing(true)
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView == pickerHour {
            return hours.count
        }
        return minutes.count
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        var label: UILabel
        if let view = view as? UILabel { label = view }
        else { label = UILabel() }
        label.textColor = UIColor.Theme(alpha: 1.0)
        label.backgroundColor = UIColor.clear
        label.textAlignment = .center
        label.font = label.font.withSize(25)
        label.adjustsFontSizeToFitWidth = true
        label.minimumScaleFactor = 40
        label.contentScaleFactor = 30
        if pickerView == pickerHour {
            label.text = hours[row]
        } else {
            label.text = minutes[row]
        }
        return label
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if pickerView == pickerHour {
            hourStr = (hours[row])
        } else {
            minutesStr = minutes[row]
            if "\(minutes[row])".count == 1 {
                minutesStr = "0\(minutesStr)"
            }
        }
        self.exitKeyboard()
    }
    
    private func setupHourMinute() {
        for hour in 0...3 {
            hours.append("\(hour)")
        }
        
        for minute in 0...59 {
            minutes.append("\(minute)")
        }
    }
    
    @objc func answer(sender: UITapGestureRecognizer){
        let indication = sender.view?.subviews[0] as! UILabel
        let answer = String((indication.text)!.dropFirst(1))
        let index = String((indication.text)!.dropLast(1))
        self.exitKeyboard()
        self.addQuestionArray()
        
        if answer == "1" {
            questionsList[Int("\(index)")!-1].answer = questionsList[Int("\(index)")!-1].choice1
        } else if answer == "2" {
            questionsList[Int("\(index)")!-1].answer = questionsList[Int("\(index)")!-1].choice2
        } else if answer == "3" {
            questionsList[Int("\(index)")!-1].answer = questionsList[Int("\(index)")!-1].choice3
        } else if answer == "4" {
            questionsList[Int("\(index)")!-1].answer = questionsList[Int("\(index)")!-1].choice4
        }
        
        ExamView.reloadData()
        
        print("buttonPressed !", questionsList)
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        if questionsList[0].question != "" || questionsList[0].choice1 != "" || questionsList[0].choice2 != "" || questionsList[0].choice3 != "" || questionsList[0].choice4 != "" || questionsList.count > 1 {
            self.createDraft()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return questionsList.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! ExamEditCell
        cell.answer1.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(answer(sender:))))
        cell.answer2.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(answer(sender:))))
        cell.answer3.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(answer(sender:))))
        cell.answer4.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(answer(sender:))))
        cell.backgroundColor = UIColor.white
        cell.layer.shadowColor = UIColor.gray.cgColor
        cell.layer.shadowOpacity = 0.5
        cell.layer.shadowOffset = CGSize(width: 0, height: 1)
        cell.layer.shadowRadius = 1
        cell.layer.cornerRadius = 10
        cell.question = questionsList[indexPath.item]
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width, height: view.frame.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.Background(alpha: 1.0)
        
//        subject_code = self.navigationItem.title!
        
        let space : Character = " "
        let idx = self.navigationItem.title!.characters.index(of: space)
        let pos = self.navigationItem.title!.characters.distance(from: self.navigationItem.title!.startIndex, to: idx!)
        let count = self.navigationItem.title!.count - Int(pos)
        subject_code = "\(self.navigationItem.title!.dropLast(count))"
        
        setupHourMinute()
        setupView()
        setupConstraints()
        setupGestures()
        
        view.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(exitKeyboard)))
        
        NotificationCenter.default.addObserver(self, selector: #selector(CreateExamDetail.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(CreateExamDetail.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
        
    }
}

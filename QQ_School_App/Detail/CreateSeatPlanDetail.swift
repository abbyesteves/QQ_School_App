//
//  SeatPlanDetail.swift
//  QQ_School_App
//
//  Created by Abby Esteves on 27/04/2018.
//  Copyright © 2018 Abby Esteves. All rights reserved.
//

import UIKit
class CreateSeatPlanDetail : UIViewController, UIPickerViewDataSource, UIPickerViewDelegate {
    
    var range = [String]()
    var subject_code = String()
    let pickerRow = UIPickerView()
    let pickerColumn = UIPickerView()
    var rowSelect = "1"
    var columnSelect = "1"
    
    let seatingPlanView : UIView = {
        let view = UIView()
//        view.backgroundColor = UIColor.lightGray
        return view
    }()
    
    let createSeatingButton: UIButton = {
        let button = UIButton()
        button.backgroundColor = UIColor.ThemeOrange(alpha: 1.0)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 13)
        button.setTitleColor(UIColor.white, for: .normal)
        button.setTitle("create seating arrangement".uppercased(), for: .normal)
        return button
    }()
    
    let rowLabel: UILabel = {
        let label = UILabel()
        label.text = "rows".uppercased()
        label.textColor = UIColor.ThemeDark(alpha: 1.0)
        label.numberOfLines = 10
        label.textAlignment = .left
        label.font = label.font.withSize(13)
        label.font = UIFont.boldSystemFont(ofSize: label.font.pointSize)
        return label
    }()
    
    let columnLabel: UILabel = {
        let label = UILabel()
        label.text = "columns".uppercased()
        label.textColor = UIColor.ThemeDark(alpha: 1.0)
        label.numberOfLines = 10
        label.textAlignment = .left
        label.font = label.font.withSize(13)
        label.font = UIFont.boldSystemFont(ofSize: label.font.pointSize)
        return label
    }()
    
    let frontLabel: UILabel = {
        let label = UILabel()
        label.text = "your desk".uppercased()
        label.textColor = UIColor.white
        label.backgroundColor = UIColor.ThemeDarkGreen(alpha: 1.0)
        label.textAlignment = .center
        label.font = label.font.withSize(10)
        return label
    }()
    
    let noStudentsLabel: UILabel = {
        let label = UILabel()
        label.text = "# of student: 1"
        label.textColor = UIColor.lightGray
        label.numberOfLines = 10
        label.textAlignment = .center
        label.font = label.font.withSize(12)
        return label
    }()
    
    var seatingPlan = [
        "channels": "",
        "grade": "",
        "seat_column": "",
        "seat_row": "",
        "section": "",
        "subj_code": "",
        "teacher_code": ""
        ] as [String : Any]
    
    @objc func createSeating() {
        for count in 0...((Int(columnSelect)!*Int(rowSelect)!)-1){
            range.append("\(count)")
            seatingPlan["\(count)"] = "null"
        }
        saveSeating()
    }
    
    func saveSeating() {

        seatingPlan["channels"] = ["CLASS_SEAT_\((ApiService().myID()["qq_id"])!)"]
        seatingPlan["grade"] = "\((ApiService().myID()["grade"])!)"
        seatingPlan["seat_column"] = "\(columnSelect)"
        seatingPlan["seat_row"] = "\(rowSelect)"
        seatingPlan["section"] = "\((ApiService().myID()["section"])!)"
        seatingPlan["subj_code"] = subject_code
        seatingPlan["teacher_code"] = "\((ApiService().myID()["qq_id"])!)"
        
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            ApiService().createSeating(seatplan: self.seatingPlan, subject_code: self.subject_code)
        }, completion: { (Bool) in
            self.goToSeatpLan()
        })
    }
    
    func goToSeatpLan() {
        let titleLabel : UILabel = {
            let label = UILabel()
            label.textColor = UIColor.white
            label.font = UIFont(name: "GillSans-Bold", size: UIFont.systemFontSize)!
            label.font = label.font.withSize(15)
            label.text = "classroom view".uppercased()
            return label
        }()
        
        let seatPlanDetail = SeatPlanDetail()
        self.navigationController?.pushViewController(seatPlanDetail, animated: true)
        
        seatPlanDetail.navigationItem.title = "\(subject_code)".uppercased()
        seatPlanDetail.navigationItem.titleView = titleLabel
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return range.count
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        var label: UILabel
        if let view = view as? UILabel { label = view }
        else { label = UILabel() }
        
        label.textColor = UIColor.ThemeDark(alpha: 1.0)
        label.backgroundColor = UIColor.Background(alpha: 0.5)
        label.textAlignment = .center
        label.font = label.font.withSize(20)
        label.adjustsFontSizeToFitWidth = true
        label.minimumScaleFactor = 4
        label.text = range[row]
        
        return label
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if pickerView == pickerColumn {
            rowSelect = range[row]
        } else {
            columnSelect = range[row]
        }
        resetSeatingPlan(columns: Int(columnSelect)!, rows: Int(rowSelect)!)
    }
    
    private func resetSeatingPlan(columns: Int, rows : Int) {
        for view in seatingPlanView.subviews {
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                view.alpha = 0
            }, completion: { (Bool) in
                view.removeFromSuperview()
            })
        }
        setupSeating(columns: columns, rows: rows)
    }
    
    private func setupSeating(columns: Int, rows : Int) {
//        print(" student count : ", seatingPlanView.frame.width, columns, seatingPlanView.frame.height, rows)
        let studentCount = (columns * rows)
        let width = seatingPlanView.frame.width/CGFloat(rows)
        let height = seatingPlanView.frame.height/CGFloat(columns)
        noStudentsLabel.text = "# of student: \(studentCount)"
        
        var x = 0
        for row in 1...rows {
            var y = 0
            for column in 1...columns {
                let table : UIView = {
                    let view = UIView()
                    view.layer.borderColor = UIColor.lightGray.cgColor
                    view.layer.borderWidth = 0.2
                    return view
                }()
                
                let tableImage: UIImageView = {
                    let imageView = UIImageView()
                    imageView.contentMode = .scaleAspectFit
                    imageView.image = UIImage(named: "desk")
                    imageView.clipsToBounds = true
                    imageView.backgroundColor = .clear
                    imageView.layer.masksToBounds = true
                    return imageView
                }()
                
                seatingPlanView.addSubview(table)
                table.addSubview(tableImage)
                
                table.alpha = 0
                
                table.frame = CGRect(x: CGFloat(x), y: CGFloat(y), width: width, height: height)
                tableImage.frame = CGRect(x: table.frame.width/2-((table.frame.width/2+20)/2), y: table.frame.height/2-((table.frame.height/2+20)/2), width: table.frame.width/2+20, height: table.frame.height/2+20)
                
                UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
                    table.alpha = 1
                }, completion: { (Bool) in })
                y = y + Int(height)
            }
            x = x + Int(width)
        }
    }
    
    private func setupRange() {
        for count in 1...10{
            range.append("\(count)")
        }
    }
    
    private func setupView() {
        pickerColumn.delegate = self
        pickerRow.delegate = self
        
        view.addSubview(seatingPlanView)
        view.addSubview(pickerColumn)
        view.addSubview(pickerRow)
        pickerRow.addSubview(rowLabel)
        view.addSubview(columnLabel)
        view.addSubview(noStudentsLabel)
        view.addSubview(frontLabel)
        
        view.addSubview(createSeatingButton)
        
        var height = view.frame.height-110-20-20
        
        createSeatingButton.frame = CGRect(x: 0, y: view.frame.height-110, width: view.frame.width, height: 50)
        
        seatingPlanView.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height/2+80)
        frontLabel.frame = CGRect(x: 0, y: seatingPlanView.frame.maxY, width: view.frame.width, height: 20)
        height = height-seatingPlanView.frame.height
        pickerRow.frame = CGRect(x: 0, y: frontLabel.frame.maxY, width: view.frame.width/2, height: height)
        pickerColumn.frame = CGRect(x: view.frame.width/2, y: frontLabel.frame.maxY, width: view.frame.width/2, height: height)
        rowLabel.frame = CGRect(x: 15, y: 0, width: pickerRow.frame.width, height: pickerRow.frame.height)
        noStudentsLabel.frame = CGRect(x: 0, y: pickerRow.frame.maxY, width: view.frame.width, height: 20)
        columnLabel.frame = CGRect(x: view.frame.width/2-15, y: seatingPlanView.frame.maxY+20, width: pickerColumn.frame.width, height: pickerColumn.frame.height)
        
        createSeatingButton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(createSeating)))
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.Background(alpha: 1.0)
        
        subject_code = self.navigationItem.title!
        
        setupRange()
        setupView()
        setupSeating(columns: 1, rows : 1)
    }
}

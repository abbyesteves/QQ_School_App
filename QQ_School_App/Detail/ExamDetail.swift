//
//  ExamDetail.swift
//  QQ_School_App
//
//  Created by Abby Esteves on 23/05/2018.
//  Copyright © 2018 Abby Esteves. All rights reserved.
//

import UIKit
import AnyFormatKit

class ExamDetail: UIViewController, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UIPickerViewDataSource, UIPickerViewDelegate, UITextFieldDelegate, UITextViewDelegate {
    
    let manager = CBLManager.sharedInstance()
    var documentID = String()
    var subject_code = String()
    var cellId = "cellId"
    var questions = [Questions]()
    var indexPath = 0
    var MainScrollView = UIScrollView()
    var hourStr = "0"
    var minutesStr = "00"
    let pickerHour = UIPickerView()
    let pickerMinute = UIPickerView()
    var hours = [String]()
    var minutes = [String]()
    
    var examParent = [
        "name":"",
        "desc":"",
        "due":"",
        "allotted":"",
        "hour":"",
        "minute":""
    ] as [String:Any]

    
    let ExamView: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.backgroundColor = .clear
        return cv
    }()
    
    let nextImage: UIImageView = {
        let imageView = UIImageView()
        imageView.tintColor = UIColor.black
        imageView.image = UIImage(named: "ic_next")?.withRenderingMode(.alwaysTemplate)
        imageView.tintColor = UIColor.white
        return imageView;
    }()
    
    let prevImage: UIImageView = {
        let imageView = UIImageView()
        imageView.tintColor = UIColor.black
        imageView.image = UIImage(named: "ic_previous")?.withRenderingMode(.alwaysTemplate)
        imageView.tintColor = UIColor.white
        return imageView;
    }()
    
    let previousButton: UIButton = {
        let button = UIButton()
        button.backgroundColor = UIColor.ThemeOrange(alpha: 1.0)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 13)
        button.setTitleColor(UIColor.white, for: .normal)
        button.setTitle("Prev Question", for: .normal)
        return button
    }()
    
    let nextButton: UIButton = {
        let button = UIButton()
        button.backgroundColor = UIColor.ThemeOrange(alpha: 1.0)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 13)
        button.setTitleColor(UIColor.white, for: .normal)
        button.setTitle("Next Question", for: .normal)
        return button
    }()
    
    let timerLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.lightGray
        label.numberOfLines = 2
        label.alpha = 0.8
        label.textAlignment = .right
        label.font = label.font.withSize(11)
        label.text = "Remaining Time"
        return label
    }()
    
    let timerCountLabel: UILabel = {
        let label = UILabel()
        label.textColor = UIColor.ThemeDarkGreen(alpha: 1.0)
        label.backgroundColor = .white
        label.numberOfLines = 2
        label.textAlignment = .right
        label.font = label.font.withSize(40)
        label.text = "00:00"
        return label
    }()
    
    let examInstructionsLabel: UILabel = {
        let label = UILabel()
        label.text = ""
        label.textColor = UIColor.gray
        label.numberOfLines = 2
        label.font = label.font.withSize(13)
        return label
    }()
    
    let blackView : UIView = {
        let view = UIView()
        view.backgroundColor = UIColor(white: 0, alpha: 0.6)
        return view
    }()
    
    let examParentView : UIView = {
        let view = UIView()
        view.backgroundColor = UIColor.white
        view.layer.cornerRadius = 10
        return view
    }()
    
    let examNameLabel: UILabel = {
        let label = UILabel()
        label.text = "Exam Name :"
        label.textColor = UIColor.gray
        label.numberOfLines = 2
        label.font = label.font.withSize(15)
        return label
    }()
    
    let examNameText: UITextField = {
        let text = UITextField()
        text.text = ""
        text.borderStyle = .none
        text.backgroundColor = .white
        text.textColor = UIColor.darkGray
        text.font = .systemFont(ofSize: 13)
        text.layer.borderWidth = 1.0
        text.layer.borderColor = UIColor.lightGray.cgColor
        text.layer.cornerRadius = 5
        text.autocorrectionType = .no
        text.setLeftPadding(space: 8)
        return text
    }()
    
    let examDescriptionLabel: UILabel = {
        let label = UILabel()
        label.text = "Exam Instructions :"
        label.textColor = UIColor.gray
        label.numberOfLines = 2
        label.font = label.font.withSize(15)
        return label
    }()
    
    let examDescriptionText: UITextView = {
        let textView = UITextView()
        textView.backgroundColor = UIColor.white
        textView.textColor = UIColor.darkGray
        textView.layer.borderWidth = 1.0
        textView.layer.borderColor = UIColor.lightGray.cgColor
        textView.isEditable = true
        textView.isSelectable = true
        textView.isScrollEnabled = true
        textView.layer.cornerRadius = 5
        textView.font = textView.font?.withSize(15)
        textView.text = ""
        return textView
    }()
    
    let examDueLabel: UILabel = {
        let label = UILabel()
        label.text = "Exam Due :"
        label.textColor = UIColor.gray
        label.numberOfLines = 2
        label.font = label.font.withSize(15)
        return label
    }()
    
    let dueText: UILabel = {
        let label = UILabel()
        label.text = "MM/DD/YYYY"
        label.textColor = UIColor.lightGray
        label.numberOfLines = 1
        label.font = label.font.withSize(15)
        return label
    }()
    
    let examDueText: UITextView = {
        let textView = UITextView()
        textView.backgroundColor = UIColor.white
        textView.textColor = UIColor.clear
        textView.layer.borderWidth = 1.0
        textView.layer.borderColor = UIColor.lightGray.cgColor
        textView.isEditable = true
        textView.isSelectable = true
        textView.isScrollEnabled = false
        textView.layer.cornerRadius = 5
        textView.keyboardType = .numberPad
        textView.font = textView.font?.withSize(20)
        textView.text = ""
        return textView
    }()
    
    let examTimeLabel: UILabel = {
        let label = UILabel()
        label.text = "Allotted Time :"
        label.textColor = UIColor.gray
        label.numberOfLines = 2
        label.font = label.font.withSize(15)
        return label
    }()
    
    let saveExamButton: UIButton = {
        let button = UIButton()
        button.backgroundColor = UIColor.ThemeDarkGreen(alpha: 1.0)
        button.titleLabel?.font = UIFont.boldSystemFont(ofSize: 13)
        button.setTitleColor(UIColor.white, for: .normal)
        button.setTitle("Update Exam".uppercased(), for: .normal)
        return button
    }()
    
    let HLabel: UILabel = {
        let label = UILabel()
        label.text = "H"
        label.textColor = UIColor.ThemeDark(alpha: 1.0)
        label.numberOfLines = 2
        label.font = UIFont.boldSystemFont(ofSize: label.font.pointSize)
        label.font = label.font.withSize(25)
        return label
    }()
    
    let MLabel: UILabel = {
        let label = UILabel()
        label.text = "M"
        label.textColor = UIColor.ThemeDark(alpha: 1.0)
        label.numberOfLines = 2
        label.font = UIFont.boldSystemFont(ofSize: label.font.pointSize)
        label.font = label.font.withSize(25)
        return label
    }()
    
    //
    
    @objc func exitKeyboard() {
        view.endEditing(true)
    }
    
    @objc func nextPage() {
        if indexPath < questions.count && questions.count > 1{
//            previousButton.alpha = 1
            previousButton.isEnabled = true
            previousButton.backgroundColor = UIColor.ThemeOrange(alpha: 1.0)
            indexPath = indexPath + 1
            if indexPath+2 > questions.count {
//                nextButton.alpha = 0
                nextButton.isEnabled = false
                nextButton.backgroundColor = UIColor.ThemeOrange(alpha: 0.3)
            }
            
//            previousButton.setTitle("PREV QUESTION \(indexPath)", for: .normal)
//            nextButton.setTitle("NEXT QUESTION \(indexPath+2)", for: .normal)
            
            self.getAnswer()
            self.update(index : "\(indexPath)", answer: "1")
            let index = NSIndexPath(item: indexPath, section: 0)
            ExamView.scrollToItem(at: index as IndexPath, at: .centeredHorizontally, animated: true)
        }
    }
    
    private func getAnswer() -> String {
        let choices = [questions[Int("\(indexPath)")!].choice1, questions[Int("\(indexPath)")!].choice2, questions[Int("\(indexPath)")!].choice3, questions[Int("\(indexPath)")!].choice4]
        let answer = questions[Int("\(indexPath)")!].answer
        var answerNumber = "\(Int("\(choices.index(of: answer)!)")!+1)"
        
        print(" getAnswer() ", answer!, answerNumber)
        
        return answerNumber
    }
    
    @objc func prevPage() {
        indexPath = indexPath - 1
        if indexPath >= 0 {
//            nextButton.alpha = 1
            nextButton.isEnabled = true
            nextButton.backgroundColor = UIColor.ThemeOrange(alpha: 1.0)
            if indexPath == 0 {
//                previousButton.alpha = 0
                previousButton.isEnabled = false
                previousButton.backgroundColor = UIColor.ThemeOrange(alpha: 0.3)
            }
//            previousButton.setTitle("PREV QUESTION  \(indexPath)", for: .normal)
//            nextButton.setTitle("NEXT QUESTION \(indexPath+2)", for: .normal)
            
//            self.update(index : "\(indexPath)", answer: "1")
            self.getAnswer()
            let index = NSIndexPath(item: indexPath, section: 0)
            ExamView.scrollToItem(at: index as IndexPath, at: .centeredHorizontally, animated: true)
        }
    }
    
    @objc func updateAnswer(sender: UITapGestureRecognizer){
        let indication = sender.view?.subviews[0] as! UILabel
        let answer = String((indication.text)!.dropFirst(1))
        let index = String((indication.text)!.dropLast(1))
        
        self.update(index : "\(index)", answer: answer)
        self.exitKeyboard()
        
        ExamView.reloadData()
    }
    
    private func update(index :String, answer : String) {
        let indexPath = IndexPath(item: Int("\(index)")!-1, section: 0)
        print(" update ", indexPath)
        let cell = ExamView.cellForItem(at: indexPath) as! ExamEditCell
        
        questions[Int("\(index)")!-1].question = cell.questionText.text!
        questions[Int("\(index)")!-1].choice1 = cell.choice1.text!
        questions[Int("\(index)")!-1].choice2 = cell.choice2.text!
        questions[Int("\(index)")!-1].choice3 = cell.choice3.text!
        questions[Int("\(index)")!-1].choice4 = cell.choice4.text!
        
        if answer == "1" {
            questions[Int("\(index)")!-1].answer = questions[Int("\(index)")!-1].choice1!
        } else if answer == "2" {
            questions[Int("\(index)")!-1].answer = questions[Int("\(index)")!-1].choice2!
        } else if answer == "3" {
            questions[Int("\(index)")!-1].answer = questions[Int("\(index)")!-1].choice3!
        } else if answer == "4" {
            questions[Int("\(index)")!-1].answer = questions[Int("\(index)")!-1].choice4!
        }
        
    }
    
    @objc func answer(sender: UITapGestureRecognizer){
        let indication = sender.view?.subviews[0] as! UILabel
        let answer = String((indication.text)!.dropFirst(1))
        let index = String((indication.text)!.dropLast(1))
        self.exitKeyboard()
        
        if answer == "1" {
            questions[Int("\(index)")!-1].answered = questions[Int("\(index)")!-1].choice1!
        } else if answer == "2" {
            questions[Int("\(index)")!-1].answered = questions[Int("\(index)")!-1].choice2!
        } else if answer == "3" {
            questions[Int("\(index)")!-1].answered = questions[Int("\(index)")!-1].choice3!
        } else if answer == "4" {
            questions[Int("\(index)")!-1].answered = questions[Int("\(index)")!-1].choice4!
        }
        
        ExamView.reloadData()
    }
    
    @objc func information() {
        if "\((ApiService().myID()["user_type"])!)" == "student" {
            let alertController = UIAlertController(title: "\((examNameLabel.text)!)", message: "\((examInstructionsLabel.text)!)", preferredStyle: UIAlertControllerStyle.alert)
            
            alertController.addAction(UIAlertAction(title: NSLocalizedString("Got it.", comment: "Default action"), style: UIAlertActionStyle.cancel, handler: { _ in
                
            }))
            
            let alertWindow = UIWindow(frame: UIScreen.main.bounds)
            alertWindow.rootViewController = UIViewController()
            alertWindow.windowLevel = UIWindowLevelAlert + 1;
            alertWindow.makeKeyAndVisible()
            alertWindow.rootViewController?.present(alertController, animated: true, completion: nil)
        } else {
            openCreateExam()
        }
    }
    
    @objc func closeDetail() {
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.blackView.alpha = 0
            self.examParentView.frame = CGRect(x: self.blackView.frame.width/2, y: self.blackView.frame.height/2, width: 0, height: 0)
            self.examNameLabel.frame = CGRect(x: self.examParentView.frame.width/2, y: self.examParentView.frame.height/2, width: 0, height: 0)
            self.examNameText.frame = CGRect(x: self.examParentView.frame.width/2, y: self.examParentView.frame.height/2, width: 0, height: 0)
            self.examDescriptionLabel.frame = CGRect(x: self.examParentView.frame.width/2, y: self.examParentView.frame.height/2, width: 0, height: 0)
            self.examDescriptionText.frame = CGRect(x: self.examParentView.frame.width/2, y: self.examParentView.frame.height/2, width: 0, height: 0)
            self.examDueLabel.frame = CGRect(x: self.examParentView.frame.width/2, y: self.examParentView.frame.height/2, width: 0, height: 0)
            self.examDueText.frame = CGRect(x: self.examParentView.frame.width/2, y: self.examParentView.frame.height/2, width: 0, height: 0)
            self.dueText.frame = CGRect(x: 5, y: 0, width: self.examDueText.frame.width-10, height: self.examDueText.frame.height)
            self.examTimeLabel.frame = CGRect(x: self.examParentView.frame.width/2, y: self.examParentView.frame.height/2, width: 0, height: 0)
            self.saveExamButton.frame = CGRect(x: self.examParentView.frame.width/2, y: self.examParentView.frame.height/2, width: 0, height: 0)
            self.pickerHour.frame = CGRect(x: self.examParentView.frame.width/2, y: self.examParentView.frame.height/2, width: 0, height: 0)
            self.pickerMinute.frame = CGRect(x: self.examParentView.frame.width/2, y: self.examParentView.frame.height/2, width: 0, height: 0)
            self.HLabel.frame = CGRect(x: self.examParentView.frame.width/2, y: self.examParentView.frame.height/2, width: 0, height: 0)
            self.MLabel.frame = CGRect(x: self.examParentView.frame.width/2, y: self.examParentView.frame.height/2, width: 0, height: 0)
        }, completion: { (Bool) in
            self.exitKeyboard()
        })
    }
    
    @objc func updateParentExam() {
        self.update(index : "\(indexPath+1)", answer: "1")
        let parent = [
            "allotted_time": "\(hourStr):\(minutesStr)",
            "exam_desc": "\((examDescriptionText.text)!)",
            "exam_due": "\((dueText.text)!)",
            "exam_name": "\((examNameText.text)!)",
            "subj_code": "\(subject_code)",
            "teacher_id": "\((ApiService().myID()["qq_id"])!)",
            "type": "exam"
            ] as [ String : Any]
        
//        print(" updateParentExam ", parent)
        
        questions = questions.filter { $0.question != "" }
        
        ApiService().updateExamParent(exam: parent, questions: questions, documentID: documentID)
        self.closeDetail()
    }
    
    
    @objc func keyboardWillHide(notification: NSNotification) {
        //        let height = self.navigationController?.navigationBar.frame.height
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            //            if CGFloat(y) > CGFloat(MainScrollView.frame.height) {
            self.MainScrollView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.MainScrollView.frame.height+(keyboardSize.height))
//            previousButton.frame = CGRect(x: 0, y: self.previousButton.frame.minY+(keyboardSize.height), width: view.frame.width/2, height: 50)
//            nextButton.frame = CGRect(x: view.frame.width-(view.frame.width/2), y: self.nextButton.frame.minY+(keyboardSize.height), width: view.frame.width/2, height: 50)
            //            }
        }
    }
    
    @objc func keyboardWillShow(notification: NSNotification) {
//        let height = self.navigationController?.navigationBar.frame.height
        if let keyboardSize = (notification.userInfo?[UIKeyboardFrameBeginUserInfoKey] as? NSValue)?.cgRectValue {
            //            if CGFloat(y) > CGFloat(MainScrollView.frame.height) {
            self.MainScrollView.frame = CGRect(x: 0, y: 0, width: self.view.frame.width, height: self.MainScrollView.frame.height-(keyboardSize.height))
//            self.previousButton.frame = CGRect(x: 0, y: self.previousButton.frame.minY-(keyboardSize.height), width: self.view.frame.width/2, height: 50)
//            self.nextButton.frame = CGRect(x: self.view.frame.width-(self.view.frame.width/2), y: self.nextButton.frame.minY-(keyboardSize.height), width: self.view.frame.width/2, height: 50)
            
            //                let bottomOffset = CGPoint(x : 0, y : MainScrollView.contentSize.height-MainScrollView.bounds.size.height+MainScrollView.contentInset.bottom)
            //                MainScrollView.setContentOffset(bottomOffset, animated: false)
            //            }
            
        }
    }
    
    private func openCreateExam() {
        assignParent()
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 1, initialSpringVelocity: 1, options: .curveEaseOut, animations: {
            self.blackView.alpha = 1
            self.examParentView.frame = CGRect(x: 10, y: 50, width: self.blackView.frame.width-20, height:250+50+120)
            self.examNameLabel.frame = CGRect(x: 10, y: 10, width: self.examParentView.frame.width-20, height: 20)
            self.examNameText.frame = CGRect(x: 10, y: self.examNameLabel.frame.maxY+10, width: self.examParentView.frame.width-20, height: 30)
            self.examDescriptionLabel.frame = CGRect(x: 10, y: self.examNameText.frame.maxY+10, width: self.examParentView.frame.width-20, height: 20)
            self.examDescriptionText.frame = CGRect(x: 10, y: self.examDescriptionLabel.frame.maxY+10, width: self.examParentView.frame.width-20, height: 30)
            self.examDueLabel.frame = CGRect(x: 10, y: self.examDescriptionText.frame.maxY+10, width: self.examParentView.frame.width-20, height: 20)
            self.examDueText.frame = CGRect(x: 10, y: self.examDueLabel.frame.maxY+10, width: self.examParentView.frame.width-20, height: 30)
            self.dueText.frame = CGRect(x: 5, y: 0, width: self.examDueText.frame.width-10, height: self.examDueText.frame.height)
            self.examTimeLabel.frame = CGRect(x: 10, y: self.examDueText.frame.maxY+10, width: self.examParentView.frame.width-20, height: 20)
            self.pickerHour.frame = CGRect(x: 10, y: self.examTimeLabel.frame.maxY, width: self.examParentView.frame.width/2-20, height: 120)
            self.pickerMinute.frame = CGRect(x: self.pickerHour.frame.maxX, y: self.examTimeLabel.frame.maxY, width: self.examParentView.frame.width/2-20, height: 120)
            self.HLabel.frame = CGRect(x: self.pickerHour.frame.maxX-40, y: (self.examTimeLabel.frame.maxY-10)+(self.pickerHour.frame.height/2), width: 20, height: 20)
            self.MLabel.frame = CGRect(x: self.pickerMinute.frame.maxX-40, y: (self.examTimeLabel.frame.maxY-10)+(self.pickerHour.frame.height/2), width: 20, height: 20)
            self.saveExamButton.frame = CGRect(x: 10, y: self.examParentView.frame.height-60, width: self.examParentView.frame.width-20, height: 50)
        }, completion: { (Bool) in })
    }
    
    private func assignParent() {
        examNameText.text = "\((examParent["name"])!)"
        examDescriptionText.text = "\((examParent["desc"])!)"
        dueText.text = "\((examParent["due"])!)"
        timerCountLabel.text = "\((examParent["allotted"])!)"
        pickerHour.selectRow(Int("\((examParent["hour"])!)")!, inComponent: 0, animated: true)
        pickerMinute.selectRow(Int("\((examParent["minute"])!)")!, inComponent: 0, animated: true)
        hourStr = "\((examParent["hour"])!)".count == 1 ? "0\((examParent["hour"])!)" : "\((examParent["hour"])!)"
        minutesStr = "\((examParent["minute"])!)".count == 1 ? "0\((examParent["minute"])!)" : "\((examParent["minute"])!)"
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return questions.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if "\((ApiService().myID()["user_type"])!)" == "teacher" {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! ExamEditCell
            cell.answer1.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(updateAnswer(sender:))))
            cell.answer2.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(updateAnswer(sender:))))
            cell.answer3.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(updateAnswer(sender:))))
            cell.answer4.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(updateAnswer(sender:))))
            cell.backgroundColor = UIColor.white
            cell.layer.shadowColor = UIColor.gray.cgColor
            cell.layer.shadowOpacity = 0.5
            cell.layer.shadowOffset = CGSize(width: 0, height: 3)
            cell.layer.shadowRadius = 2
            cell.layer.cornerRadius = 2
            cell.question = questions[indexPath.item]
            return cell
        }
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellId, for: indexPath) as! ExamCell
        cell.answer1.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(answer(sender:))))
        cell.answer2.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(answer(sender:))))
        cell.answer3.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(answer(sender:))))
        cell.answer4.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(answer(sender:))))
        cell.backgroundColor = UIColor.white
        cell.layer.shadowColor = UIColor.gray.cgColor
        cell.layer.shadowOpacity = 0.5
        cell.layer.shadowOffset = CGSize(width: 0, height: 3)
        cell.layer.shadowRadius = 2
        cell.layer.cornerRadius = 2
        cell.question = questions[indexPath.item]

        return cell
        
    }
    var indexDidend = Int()
    func scrollViewWillEndDragging(_ scrollView: UIScrollView, withVelocity velocity: CGPoint, targetContentOffset: UnsafeMutablePointer<CGPoint>) {
        let index = targetContentOffset.pointee.x / view.frame.width
        indexDidend = Int(index)
    }
    
//    func scrollViewDidEndDragging(_ scrollView: UIScrollView, willDecelerate decelerate: Bool) {
//        if decelerate {
//            print(decelerate, indexDidend)
//            let indexPath = NSIndexPath(item: indexDidend, section: 0)
//            ExamView.scrollToItem(at: indexPath as IndexPath, at: .centeredHorizontally, animated: true)
//        }
//
//    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
//        print(" did select : ", questions[indexPath.item])
        self.exitKeyboard()
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: view.frame.width, height: ExamView.frame.height)
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    private func setupView() {
        pickerHour.delegate = self
        pickerMinute.delegate = self
        examDueText.delegate = self
        
        ExamView.backgroundColor = UIColor.Background(alpha: 1.0)
        ExamView.delegate = self
        ExamView.dataSource = self
        ExamView.isPagingEnabled = true
        ExamView.isScrollEnabled = false
//        ExamView.contentInset = UIEdgeInsetsMake(0, 20, 50, 20)
//        ExamView.scrollIndicatorInsets = UIEdgeInsetsMake(0, 20, 50, 20)
        ExamView.showsHorizontalScrollIndicator = false
        ExamView.showsVerticalScrollIndicator = true
        ExamView.register(ExamCell.self, forCellWithReuseIdentifier: cellId)
        
        MainScrollView = UIScrollView(frame: view.bounds)
        MainScrollView.delegate = self
        MainScrollView.showsVerticalScrollIndicator = true
        MainScrollView.backgroundColor = UIColor.Background(alpha: 1.0)
        
        var infoImage = UIImage(named: "ic_info")?.withRenderingMode(.alwaysOriginal)
        
        if "\((ApiService().myID()["user_type"])!)" == "teacher" {
            infoImage = UIImage(named: "ic_save_exam")?.withRenderingMode(.alwaysOriginal)
            ExamView.register(ExamEditCell.self, forCellWithReuseIdentifier: cellId)
        }
        
        
        let information = UIBarButtonItem(image: infoImage, style: .plain, target: self, action: #selector(self.information))
        navigationItem.rightBarButtonItems = [information]
        
        view.addSubview(MainScrollView)
        MainScrollView.addSubview(ExamView)
//        view.addSubview(examNameLabel)
//        view.addSubview(examInstructionsLabel)
        MainScrollView.addSubview(timerLabel)
        MainScrollView.addSubview(timerCountLabel)
        
        MainScrollView.addSubview(nextButton)
        nextButton.addSubview(nextImage)
        MainScrollView.addSubview(previousButton)
        previousButton.addSubview(prevImage)
        
//        previousButton.alpha = 0
//        nextButton.alpha = 0
        previousButton.isEnabled = false
        nextButton.isEnabled = false
        previousButton.backgroundColor = UIColor.ThemeOrange(alpha: 0.3)
        nextButton.backgroundColor = UIColor.ThemeOrange(alpha: 0.3)
        
        blackView.alpha = 0
        
        view.addSubview(blackView)
        view.addSubview(examParentView)
        examParentView.addSubview(examNameLabel)
        examParentView.addSubview(examNameText)
        examParentView.addSubview(examDescriptionLabel)
        examParentView.addSubview(examDescriptionText)
        examParentView.addSubview(examDueLabel)
        examParentView.addSubview(examDueText)
        examDueText.addSubview(dueText)
        examParentView.addSubview(examTimeLabel)
        examParentView.addSubview(pickerHour)
        examParentView.addSubview(pickerMinute)
        examParentView.addSubview(saveExamButton)
        examParentView.addSubview(HLabel)
        examParentView.addSubview(MLabel)
    }
    
    private func setupConstraint() {
//        examNameLabel.frame = CGRect(x: 10, y: 10, width: view.frame.width-20, height: 30)
//        examInstructionsLabel.frame = CGRect(x: 10, y: examNameLabel.frame.maxY+5, width: view.frame.width-20, height: 20)
        MainScrollView.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height)//-115
        MainScrollView.contentSize = CGSize(width: view.frame.width, height: view.frame.height)
        
        timerLabel.frame = CGRect(x: 12.5, y: 5, width: view.frame.width-25, height: 20)
        timerCountLabel.frame = CGRect(x: view.frame.width-130, y: timerLabel.frame.maxY, width: 120, height: 40)
        
        previousButton.frame = CGRect(x: 0, y: MainScrollView.frame.height-115, width: view.frame.width/2-(0.5), height: 50)
        nextButton.frame = CGRect(x: view.frame.width-(view.frame.width/2)+(0.5), y: MainScrollView.frame.height-115, width: view.frame.width/2, height: 50)
        nextImage.frame = CGRect(x: (view.frame.width/2)-30, y: 10, width: 30, height: 30)
        prevImage.frame = CGRect(x: 0, y: 10, width: 30, height: 30)
        ExamView.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: (view.frame.height)-(115))
//        ExamView.contentSize = CGSize(width: view.frame.width, height: (view.frame.height)-(115+timerLabel.frame.height+timerCountLabel.frame.height))
        
        blackView.frame = CGRect(x: 0, y: 0, width: view.frame.width, height: view.frame.height)
        examParentView.frame = CGRect(x: view.frame.width/2, y: view.frame.height/2, width: 0, height: 0)
        examNameLabel.frame = CGRect(x: examParentView.frame.width/2, y: examParentView.frame.height/2, width: 0, height: 0)
        examNameText.frame = CGRect(x: examParentView.frame.width/2, y: examParentView.frame.height/2, width: 0, height: 0)
        examDescriptionLabel.frame = CGRect(x: examParentView.frame.width/2, y: examParentView.frame.height/2, width: 0, height: 0)
        examDescriptionText.frame = CGRect(x: examParentView.frame.width/2, y: examParentView.frame.height/2, width: 0, height: 0)
        examDueLabel.frame = CGRect(x: examParentView.frame.width/2, y: examParentView.frame.height/2, width: 0, height: 0)
        examDueText.frame = CGRect(x: examParentView.frame.width/2, y: examParentView.frame.height/2, width: 0, height: 0)
        dueText.frame = CGRect(x: 0, y: 0, width: examDueText.frame.width, height: examDueText.frame.height)
        examTimeLabel.frame = CGRect(x: examParentView.frame.width/2, y: examParentView.frame.height/2, width: 0, height: 0)
        pickerHour.frame = CGRect(x: examParentView.frame.width/2, y: examParentView.frame.height/2, width: 0, height: 0)
        pickerMinute.frame = CGRect(x: examParentView.frame.width/2, y: examParentView.frame.height/2, width: 0, height: 0)
        HLabel.frame = CGRect(x: examParentView.frame.width/2, y: examParentView.frame.height/2, width: 0, height: 0)
        MLabel.frame = CGRect(x: examParentView.frame.width/2, y: examParentView.frame.height/2, width: 0, height: 0)
        saveExamButton.frame = CGRect(x: examParentView.frame.width/2, y: examParentView.frame.height/2, width: 0, height: 0)
        
    }
    
    private func setupGesture() {
        nextButton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(nextPage)))
        previousButton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(prevPage)))
        blackView.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(closeDetail)))
        saveExamButton.addGestureRecognizer(UITapGestureRecognizer(target: self, action: #selector(updateParentExam)))
    }
    
    private func watchExam() {
        let exam = try! manager.databaseNamed("exams")
        self.getExams()
        NotificationCenter.default.addObserver(forName: NSNotification.Name.cblDatabaseChange, object: exam, queue: nil) {
            (notification) -> Void in
            if let changes = notification.userInfo!["changes"] as? [CBLDatabaseChange] {
                for change in changes {
                    print("exam, revision ID '%@'", change.description)
                    self.getExams()
                }
            }
        }
    }
    
    private func getExams() {
        let database = try! manager.databaseNamed("exams")
        let query = database.createAllDocumentsQuery()
        let documentParent = database.document(withID: documentID)!
        let parent = documentParent.properties
        // exam information
//        print(" parent document : ", parent)
        if parent != nil {
            if "\((ApiService().myID()["user_type"])!)" == "student" {
                examNameLabel.text = "\((parent!["exam_name"])!)"
                examInstructionsLabel.text = "\((parent!["exam_desc"])!)"
            }
            do {
                let result = try query.run()
                if result != nil {
                    while let row = result.nextRow(){
                        let document = row.document?.properties
                        // exam questions
                        if "\((document!["type"])!)" == "exam" && documentID == "\((document!["_id"])!)"{
                            
                            examParent["name"] = document!["exam_name"] == nil ? "" : "\((document!["exam_name"])!)"
                            examParent["desc"] = document!["exam_desc"] == nil ? "" : "\((document!["exam_desc"])!)"
                            examParent["due"] = document!["exam_due"] == nil ? "" : "\((document!["exam_due"])!)"
                            examParent["allotted"] = document!["allotted_time"] == nil ? "00:00" : "\((document!["allotted_time"])!)"
                            
                            //get allotted hour & minute
                            let colon : Character = ":"
                            let idx = "\((examParent["allotted"])!)".characters.index(of: colon)
                            let pos = "\((examParent["allotted"])!)".characters.distance(from: "\((examParent["allotted"])!)".startIndex, to: idx!)
                            let count = "\((examParent["allotted"])!)".count - pos
                            let hour = "\(("\((examParent["allotted"])!)".dropLast(count)))"
                            let minute = "\(("\((examParent["allotted"])!)".dropFirst(hour.count+1)))"
                            let hourIndex = hours.index(of: "\((Int(hour))!)")
                            let minuteIndex = minutes.index(of: "\((Int(minute))!)")
                            
                            examParent["hour"] = "\(hourIndex!)"
                            examParent["minute"] = "\(minuteIndex!)"
                            
//                            print(" getExams ", examParent)
                            
                        } else if "\((document!["type"])!)" == "question" && documentID == "\((document!["parent"])!)" {
                            let choicesObject = ApiService().stringToJsonObject(string : "\((document!["choices"])!)")
                            let choices = (choicesObject["choices"])! as! [String:Any]
                            
                            var question = Questions()
                            question.answer = "\((document!["answer"])!)"
                            question.question = "\((document!["question"])!)"
                            question.choice1 = "\((choices["choice 1"])!)"
                            question.choice2 = "\((choices["choice 2"]!))"
                            question.choice3 = "\((choices["choice 3"]!))"
                            question.choice4 = "\((choices["choice 4"]!))"
                            question.number = "\(questions.count+1)"
                            questions.append(question)
                        }
                    }
                    
                }
                //            print(questions)
                if questions.count > 1 {
//                    nextButton.alpha = 1
                    nextButton.isEnabled = true
                    nextButton.backgroundColor = UIColor.ThemeOrange(alpha: 1.0)
                }
                DispatchQueue.main.async(execute: {
                    self.assignParent()
                    self.ExamView.reloadData()
                })
            } catch {
                print(" error getting questions for exam :", error)
            }
        }
    }
    
    
    func textViewDidChange(_ textView: UITextView) {
        if textView == examDueText {
            
            let phoneFormatter = TextFormatter(textPattern: "##/##/####")
            dueText.text = (phoneFormatter.formattedText(from: "\((examDueText.text)!)"))!
            dueText.textColor = UIColor.gray
        }
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if pickerView == pickerHour {
            return hours.count
        }
        return minutes.count
    }
    
    func pickerView(_ pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusing view: UIView?) -> UIView {
        var label: UILabel
        if let view = view as? UILabel { label = view }
        else { label = UILabel() }
        label.textColor = UIColor.Theme(alpha: 1.0)
        label.backgroundColor = UIColor.clear
        label.textAlignment = .center
        label.font = label.font.withSize(25)
        label.adjustsFontSizeToFitWidth = true
        label.minimumScaleFactor = 40
        label.contentScaleFactor = 30
        if pickerView == pickerHour {
            label.text = hours[row]
        } else {
            label.text = minutes[row]
        }
        return label
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        if pickerView == pickerHour {
            hourStr = (hours[row])
        } else {
            minutesStr = minutes[row]
            if "\(minutes[row])".count == 1 {
                minutesStr = "0\(minutesStr)"
            }
        }
        self.exitKeyboard()
    }
    
    private func setupHourMinute() {
        for hour in 0...3 {
            hours.append("\(hour)")
        }
        
        for minute in 0...59 {
            minutes.append("\(minute)")
        }
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
        self.updateParentExam()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.backgroundColor = UIColor.Background(alpha: 1.0)
        
//        documentID = self.navigationItem.title!
        let space : Character = " "
        let idx = self.navigationItem.title!.characters.index(of: space)
        let pos = self.navigationItem.title!.characters.distance(from: self.navigationItem.title!.startIndex, to: idx!)
        let count = self.navigationItem.title!.count - Int(pos)
        subject_code = "\(self.navigationItem.title!.dropLast(count))"
        documentID = "\(self.navigationItem.title!.dropFirst(subject_code.count+1))"
        
//        print(" exam detail ",documentID)
        
        setupHourMinute()
        setupView()
        setupConstraint()
        watchExam()
        setupGesture()
        
        NotificationCenter.default.addObserver(self, selector: #selector(ExamDetail.keyboardWillShow), name: NSNotification.Name.UIKeyboardWillShow, object: nil)
        NotificationCenter.default.addObserver(self, selector: #selector(ExamDetail.keyboardWillHide), name: NSNotification.Name.UIKeyboardWillHide, object: nil)
    }
}

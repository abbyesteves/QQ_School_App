//
//  Models.swift
//  QQ_School_App
//
//  Created by Abby Esteves on 01/03/2018.
//  Copyright © 2018 Abby Esteves. All rights reserved.
//

import UIKit

class Menu: NSObject {
    var icon = String()
    var label = String()
}

struct Headlines: Decodable {
    var status: String?
    var totalResults: Int?
    var articles : [Articles]
}

struct Articles: Decodable {
    var source : Source
    var title: String!
    var description: String!
    var url: String!
    var urlToImage : String!
    var publishedAt: String!
    var author: String!
}

struct Source : Decodable {
    var id : String!
    var name : String!
}

struct Announcements : Decodable {
    var author : String!
    var date : String!
    var thumbnail : String!
    var title : String!
    var description : String!
}

struct Notes : Decodable {
    var title : String!
    var content : String!
    var date : String!
    var documentID : String!
}

struct Tasks : Decodable {
    var grade: String!
    var section: String!
    var subject_code: String!
    var title : String!
    var description : String!
    var due : String!
}

struct MySchool : Decodable {
    var thumbnail: String!
    var name: String!
    var title : String!
    var description : String!
}

struct Friends : Decodable {
    var name: String!
    var qq_id : String!
    var img : String!
    var grade : String!
    var section : String!
    var b_64 : String!
    var status : String!
    var req_name: String!
    var req_id : String!
    var req_grade : String!
    var req_section : String!
    var req_img : String!
    var req_status : String!
    var type : String!
}

struct Users :  Decodable {
    var firstname: String!
    var middlename: String!
    var lastname: String!
    var qq_id : String!
    var grade : String!
    var section : String!
    var b_64 : String!
}

struct FriendList : Decodable {
    var users : [Users]
}

struct Messages : Decodable {
    var sender_qq_id: String!
    var sender_name: String!
    var message : String!
    var datetime : String!
}

struct News : Decodable {
    var title: String!
    var b64: String!
    var desc : String!
    var date : String!
    var author : String!
}

struct Modules : Decodable {
    var name: String!
    var date: String!
    var type : String!
    var subj_code : String!
}

struct Exams : Decodable {
    var exam_name: String!
    var exam_desc: String!
    var exam_due : String!
    var allotted_time : String!
    var upload_date : String!
    var documentID : String!
    var subj_code : String!
}

struct Questions : Decodable {
    var number: String!
    var question: String!
    var answer: String!
    var answered: String!
    var choice1: String!
    var choice2: String!
    var choice3: String!
    var choice4: String!
}

struct Events : Decodable {
    var author: String!
    var b64 : String!
    var date: String!
    var date_from : String!
    var date_to : String!
    var desc : String!
    var img : String!
    var title : String!
    var type : String!
    var location : String!
}

struct Attendances : Decodable {
    var is_on_time: String!
    var scan_date: String!
    var scan_datetime : String!
    var scan_id : String!
    var scan_subj : String!
    var note : String!
}

struct Subjects : Decodable {
    var time_start: String!
    var time_end: String!
    var subject_code : String!
    var subject : String!
    var grade_lvl : String!
    var section : String!
    var teacher_id : String!
    var teacher : String!
    var time_start_long : String!
    var time_end_long : String!
}

struct QR : Decodable {
    var p: String!
    var d: String!
    var i: String!
    var t: String!
    var s: String!
}

struct Students : Decodable {
    var qq_id: String!
    var name: String!
    var merits: String!
    var gender: String!
    var demerits: String!
    var subj_code: String!
    var grade: String!
    var section: String!
    var seat_pos: String!
    var note: String!
    var documentID: String!
    var isExpanded: Bool!
}
